package tests;

import com.jme3.app.SimpleApplication;
import com.jme3.collision.CollisionResult;
import com.jme3.collision.CollisionResults;
import com.jme3.font.BitmapText;
import com.jme3.input.KeyInput;
import com.jme3.input.MouseInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.input.controls.MouseButtonTrigger;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Ray;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.shape.Box;
import com.jme3.scene.shape.Sphere;

/**
 * Sample 8 - how to let the user pick (select) objects in the scene using the
 * mouse or key presses. Can be used for shooting, opening doors, etc.
 */
public class HelloPicking extends SimpleApplication {

	public static void main(final String[] args) {
		final HelloPicking app = new HelloPicking();
		app.start();
	}

	Node shootables;
	/** маркировка столкновения */
	Geometry mark;

	/**
	 * Наш слушателть нажатий.
	 */
	private final ActionListener actionListener = new ActionListener() {

		@Override
		public void onAction(final String name, final boolean keyPressed, final float tpf) {
			// Если это событие "выстрел"
			if(name.equals("Shoot") && !keyPressed) {
				// создаем результаты столкновений
				final CollisionResults results = new CollisionResults();
				// создаем луч от начала экрана до точки, куда смотрим
				final Ray ray = new Ray(cam.getLocation(), cam.getDirection());
				// рассчитываем столкновение
				shootables.collideWith(ray, results);

				// выводим кол-во столкновений
				System.out.println("----- Collisions? " + results.size() + "-----");

				// перебираем столкновения
				for(int i = 0; i < results.size(); i++) {
					// текущий результат столкновения
					final CollisionResult currect = results.getCollision(i);

					// дистанция до точки столкновения
					final float dist = currect.getDistance();
					// точка соприконсновения
					final Vector3f pt = currect.getContactPoint();
					// название объекта, с которым столкнулись
					final String hit = currect.getGeometry().getName();
					// выводим дебаг
					System.out.println("* Collision #" + i);
					System.out.println("  You shot " + hit + " at " + pt + ", " + dist + " wu away.");
				}
				// если кесть столкновения
				if(results.size() > 0) {
					// я как понял, получаем ближайшее
					final CollisionResult closest = results.getClosestCollision();
					// маркер смещаем в точку контакта
					mark.setLocalTranslation(closest.getContactPoint());
					// и добавляем маркер в мир
					rootNode.attachChild(mark);
				}
				// иначе
				else
					// удаляем маркер
					rootNode.detachChild(mark);
			}
		}
	};

	/**
	 * Создание прицела
	 */
	protected void initCrossHairs() {
		// удаляем всю хуйню с гуи
		guiNode.detachAllChildren();
		// загружаем шрифт
		guiFont = assetManager.loadFont("Interface/Fonts/Default.fnt");
		// создаем контейнер текста
		final BitmapText ch = new BitmapText(guiFont, false);
		// определяем размеры контейнера
		ch.setSize(guiFont.getCharSet().getRenderedSize() * 2);
		// вставляем текст
		ch.setText("+"); // crosshairs
		// устанавливаем ровно в центре экрана
		ch.setLocalTranslation(
		// center
				settings.getWidth() / 2 - guiFont.getCharSet().getRenderedSize() / 3 * 2, settings.getHeight() / 2 + ch.getLineHeight() / 2, 0);
		// добавляем в ГУИ мир
		guiNode.attachChild(ch);
	}

	/**
	 * Инициализируем прослушку клавишь
	 */
	private void initKeys() {
		// ставим активацию выстрела на пробел и клик мыши
		inputManager.addMapping("Shoot", new KeyTrigger(KeyInput.KEY_SPACE), new MouseButtonTrigger(MouseInput.BUTTON_LEFT)); // trigger
																																// 2:
																																// left-button
																																// click
		// добавляем слушателя нажатий
		inputManager.addListener(actionListener, "Shoot");
	}

	/**
	 * Подготовка шарика-маркера
	 */
	protected void initMark() {
		// создаем сферу
		final Sphere sphere = new Sphere(30, 30, 0.2f);
		// создаем геометрию сферы
		mark = new Geometry("BOOM!", sphere);
		// создаем материал для шара
		final Material mark_mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
		// делаем его красным
		mark_mat.setColor("Color", ColorRGBA.Red);
		// применяем материал
		mark.setMaterial(mark_mat);
	}

	/**
	 * Метод для создание куба
	 */
	protected Geometry makeCube(final String name, final float x, final float y, final float z) {
		// создаем коробку
		final Box box = new Box(new Vector3f(x, y, z), 1, 1, 1);
		// создаем геометрию
		final Geometry cube = new Geometry(name, box);
		// создаем материал
		final Material mat1 = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
		// задаем рандомный цвет
		mat1.setColor("Color", ColorRGBA.randomColor());
		// применяем материал на геометрию
		cube.setMaterial(mat1);
		// возвращаем геометрию
		return cube;
	}

	/**
	 * Создаем пол
	 */
	protected Geometry makeFloor() {
		// создаем коробку 15х0.2х15
		final Box box = new Box(new Vector3f(0, -4, -5), 15, .2f, 15);
		// создаем геометрию пола
		final Geometry floor = new Geometry("the Floor", box);
		// создаем материал
		final Material mat1 = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
		// делаем его серым
		mat1.setColor("Color", ColorRGBA.Gray);
		// применяем материал
		floor.setMaterial(mat1);

		System.out.println(floor.getWorldBound().getClass());

		// возвращаем пол
		return floor;
	}

	@Override
	public void simpleInitApp() {
		// создаем прицел
		initCrossHairs();
		// инициализируем прослушку клавишь
		initKeys();
		// подготавливаем марку для мест попаданий
		initMark();

		// создаем таблицу целей
		shootables = new Node("Shootables");
		// добавляем ее в миру
		rootNode.attachChild(shootables);

		// добавляем в таблицу кубы
		shootables.attachChild(makeCube("a Dragon", -2f, 0f, 1f));
		shootables.attachChild(makeCube("a tin can", 1f, -2f, 0f));
		shootables.attachChild(makeCube("the Sheriff", 0f, 1f, -2f));
		shootables.attachChild(makeCube("the Deputy", 1f, 0f, -4f));
		// добавляем в таблицу пол
		shootables.attachChild(makeFloor());
	}
}