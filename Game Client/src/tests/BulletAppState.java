package tests;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.jme3.app.Application;
import com.jme3.app.state.AppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.bullet.PhysicsSpace;
import com.jme3.bullet.PhysicsSpace.BroadphaseType;
import com.jme3.bullet.PhysicsTickListener;
import com.jme3.math.Vector3f;
import com.jme3.renderer.RenderManager;

public class BulletAppState implements AppState, PhysicsTickListener {

	public enum ThreadingType {

		/**
		 * Default mode; user update, physics update and rendering happen
		 * sequentially (single threaded)
		 */
		SEQUENTIAL,
		/**
		 * Parallel threaded mode; physics update and rendering are executed in
		 * parallel, update order is kept.<br/>
		 * Multiple BulletAppStates will execute in parallel in this mode.
		 */
		PARALLEL,
	}

	protected boolean initialized = false;
	protected Application app;
	protected AppStateManager stateManager;
	protected ScheduledThreadPoolExecutor executor;
	protected PhysicsSpace pSpace;
	protected ThreadingType threadingType = ThreadingType.SEQUENTIAL;
	protected BroadphaseType broadphaseType = BroadphaseType.DBVT;
	protected Vector3f worldMin = new Vector3f(-10000f, -10000f, -10000f);
	protected Vector3f worldMax = new Vector3f(10000f, 10000f, 10000f);
	private float speed = 1;
	protected boolean active = true;
	protected float tpf;

	protected Future physicsFuture;

	private final Callable<Boolean> parallelPhysicsUpdate = new Callable<Boolean>() {

		@Override
		public Boolean call() throws Exception {
			pSpace.update(tpf * getSpeed());
			return true;
		}
	};

	long detachedPhysicsLastUpdate = 0;

	private final Callable<Boolean> detachedPhysicsUpdate = new Callable<Boolean>() {

		@Override
		public Boolean call() throws Exception {
			pSpace.update(getPhysicsSpace().getAccuracy() * getSpeed());
			pSpace.distributeEvents();
			final long update = System.currentTimeMillis() - detachedPhysicsLastUpdate;
			detachedPhysicsLastUpdate = System.currentTimeMillis();
			executor.schedule(detachedPhysicsUpdate, Math.round(getPhysicsSpace().getAccuracy() * 1000000.0f) - update * 1000, TimeUnit.MICROSECONDS);
			return true;
		}
	};

	/**
	 * Creates a new BulletAppState running a PhysicsSpace for physics
	 * simulation, use getStateManager().addState(bulletAppState) to enable
	 * physics for an Application.
	 */
	public BulletAppState() {
	}

	/**
	 * Creates a new BulletAppState running a PhysicsSpace for physics
	 * simulation, use getStateManager().addState(bulletAppState) to enable
	 * physics for an Application.
	 * 
	 * @param broadphaseType The type of broadphase collision detection,
	 * BroadphaseType.DVBT is the default
	 */
	public BulletAppState(final BroadphaseType broadphaseType) {
		this(new Vector3f(-10000f, -10000f, -10000f), new Vector3f(10000f, 10000f, 10000f), broadphaseType);
	}

	/**
	 * Creates a new BulletAppState running a PhysicsSpace for physics
	 * simulation, use getStateManager().addState(bulletAppState) to enable
	 * physics for an Application. An AxisSweep broadphase is used.
	 * 
	 * @param worldMin The minimum world extent
	 * @param worldMax The maximum world extent
	 */
	public BulletAppState(final Vector3f worldMin, final Vector3f worldMax) {
		this(worldMin, worldMax, BroadphaseType.AXIS_SWEEP_3);
	}

	public BulletAppState(final Vector3f worldMin, final Vector3f worldMax, final BroadphaseType broadphaseType) {
		this.worldMin.set(worldMin);
		this.worldMax.set(worldMax);
		this.broadphaseType = broadphaseType;
	}

	@Override
	public void cleanup() {
		if(executor != null) {
			executor.shutdown();
			executor = null;
		}
		pSpace.removeTickListener(this);
		pSpace.destroy();
	}

	public PhysicsSpace getPhysicsSpace() {
		return pSpace;
	}

	public float getSpeed() {
		return speed;
	}

	/**
	 * @return the threadingType
	 */
	public ThreadingType getThreadingType() {
		return threadingType;
	}

	@Override
	public void initialize(final AppStateManager stateManager, final Application app) {
		if(!initialized)
			startPhysics();
		initialized = true;
	}

	@Override
	public boolean isEnabled() {
		return active;
	}

	@Override
	public boolean isInitialized() {
		return initialized;
	}

	@Override
	public void physicsTick(final PhysicsSpace space, final float f) {
	}

	@Override
	public void postRender() {
		if(physicsFuture != null)
			try {
				physicsFuture.get();
				physicsFuture = null;
			} catch(final InterruptedException ex) {
				Logger.getLogger(BulletAppState.class.getName()).log(Level.SEVERE, null, ex);
			} catch(final ExecutionException ex) {
				Logger.getLogger(BulletAppState.class.getName()).log(Level.SEVERE, null, ex);
			}
	}

	@Override
	public void prePhysicsTick(final PhysicsSpace space, final float f) {
	}

	@Override
	public void render(final RenderManager rm) {
		if(!active)
			return;
		if(threadingType == ThreadingType.PARALLEL)
			physicsFuture = executor.submit(parallelPhysicsUpdate);
		else if(threadingType == ThreadingType.SEQUENTIAL)
			pSpace.update(active ? tpf * speed : 0);
		else {
		}
	}

	/**
	 * Use before attaching state
	 */
	public void setBroadphaseType(final BroadphaseType broadphaseType) {
		this.broadphaseType = broadphaseType;
	}

	@Override
	public void setEnabled(final boolean enabled) {
		this.active = enabled;
	}

	public void setSpeed(final float speed) {
		this.speed = speed;
	}

	/**
	 * Use before attaching state
	 * 
	 * @param threadingType the threadingType to set
	 */
	public void setThreadingType(final ThreadingType threadingType) {
		this.threadingType = threadingType;
	}

	/**
	 * Use before attaching state
	 */
	public void setWorldMax(final Vector3f worldMax) {
		this.worldMax = worldMax;
	}

	/**
	 * Use before attaching state
	 */
	public void setWorldMin(final Vector3f worldMin) {
		this.worldMin = worldMin;
	}

	/**
	 * The physics system is started automatically on attaching, if you want to
	 * start it before for some reason, you can use this method.
	 */
	public void startPhysics() {
		// start physics thread(pool)
		if(threadingType == ThreadingType.PARALLEL)
			startPhysicsOnExecutor();
		// } else if (threadingType == ThreadingType.DETACHED) {
		// startPhysicsOnExecutor();
		// executor.submit(detachedPhysicsUpdate);
		else
			pSpace = new PhysicsSpace(worldMin, worldMax, broadphaseType);
		pSpace.addTickListener(this);
		initialized = true;
	}

	private boolean startPhysicsOnExecutor() {
		if(executor != null)
			executor.shutdown();
		executor = new ScheduledThreadPoolExecutor(1);
		final BulletAppState app = this;
		final Callable<Boolean> call = new Callable<Boolean>() {

			@Override
			public Boolean call() throws Exception {
				detachedPhysicsLastUpdate = System.currentTimeMillis();
				pSpace = new PhysicsSpace(worldMin, worldMax, broadphaseType);
				pSpace.addTickListener(app);
				return true;
			}
		};
		try {
			return executor.submit(call).get();
		} catch(final InterruptedException ex) {
			Logger.getLogger(BulletAppState.class.getName()).log(Level.SEVERE, null, ex);
			return false;
		} catch(final ExecutionException ex) {
			Logger.getLogger(BulletAppState.class.getName()).log(Level.SEVERE, null, ex);
			return false;
		}
	}

	@Override
	public void stateAttached(final AppStateManager stateManager) {
		if(!initialized)
			startPhysics();
		if(threadingType == ThreadingType.PARALLEL)
			PhysicsSpace.setLocalThreadPhysicsSpace(pSpace);
	}

	@Override
	public void stateDetached(final AppStateManager stateManager) {
	}

	@Override
	public void update(final float tpf) {
		if(!active)
			return;
		// if (threadingType != ThreadingType.DETACHED) {
		pSpace.distributeEvents();
		// }
		this.tpf = tpf;
	}
}
