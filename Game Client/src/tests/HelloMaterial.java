package tests;

import com.jme3.app.SimpleApplication;
import com.jme3.input.KeyInput;
import com.jme3.input.MouseInput;
import com.jme3.input.controls.AnalogListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.input.controls.MouseButtonTrigger;
import com.jme3.light.AmbientLight;
import com.jme3.light.DirectionalLight;
import com.jme3.material.Material;
import com.jme3.material.RenderState.BlendMode;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.renderer.queue.RenderQueue.Bucket;
import com.jme3.scene.Geometry;
import com.jme3.scene.shape.Box;
import com.jme3.scene.shape.Sphere;
import com.jme3.texture.Texture;
import com.jme3.util.TangentBinormalGenerator;

/**
 * Пример работы с материалами.
 */
public class HelloMaterial extends SimpleApplication {

	public static void main(final String[] args) {
		final HelloMaterial app = new HelloMaterial();
		app.start();
	}

	// ссылка на геометрию сферы
	private Geometry shiny_rock;

	/**
	 * Анонимный экземпляр прослушки нажатий клавишь
	 */
	private final AnalogListener analogListener = new AnalogListener() {

		@Override
		public void onAnalog(final String name, final float value, final float tpf) {
			if(name.equals("Rotate"))
				shiny_rock.rotate(value * speed, value * speed, value * speed);
		}
	};

	/**
	 * Инициализация прослушки кнопок.
	 */
	private void initKeys() {
		// вносим типы прослушаемых действий
		inputManager.addMapping("Rotate", new KeyTrigger(KeyInput.KEY_SPACE), new MouseButtonTrigger(MouseInput.BUTTON_LEFT));
		// добавляем слушателей действий
		inputManager.addListener(analogListener, "Rotate");
	}

	@Override
	public void simpleInitApp() {
		// создаем верхний куб в указанной позиции и размерами 1х1х1
		final Box boxshape1 = new Box(new Vector3f(-3f, 1.1f, 0f), 1f, 1f, 1f);
		// создаем геометрию для куба
		final Geometry cube = new Geometry("My Textured Box", boxshape1);
		// создаем материал для куба
		final Material mat_stl = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
		// загружаем текстуру обезьяны для куба
		final Texture tex_ml = assetManager.loadTexture("Interface/Logo/Monkey.jpg");
		// применяем текстуру на материал
		mat_stl.setTexture("ColorMap", tex_ml);
		// применяем материал на куб
		cube.setMaterial(mat_stl);
		// добавляем куб в мир
		rootNode.attachChild(cube);

		// создаем куб по середине в нулевых координатах
		final Box boxshape3 = new Box(new Vector3f(0f, 0f, 0f), 1f, 1f, 0.01f);
		// создаем геометрию для этого куба
		final Geometry window_frame = new Geometry("window frame", boxshape3);
		// создаем материал для этой геометрии
		final Material mat_tt = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
		// применяем загружаемую текстуру
		mat_tt.setTexture("ColorMap", assetManager.loadTexture("Textures/ColoredTex/Monkey.png"));
		// активируем обработку альфа канала для реализации
		// прозрачность/полупрозрачность/непрозрачность
		mat_tt.getAdditionalRenderState().setBlendMode(BlendMode.Alpha);
		// применяем материал к геометрии куба
		window_frame.setMaterial(mat_tt);
		// добавлю доп. опцию для геометрии куба, для кореектной обработки
		// прозрачности
		window_frame.setQueueBucket(Bucket.Transparent);
		// добавляем куб в мир
		rootNode.attachChild(window_frame);

		// создаем нижний куб
		final Box boxshape4 = new Box(new Vector3f(3f, -1f, 0f), 1f, 1f, 1f);
		// создаем геометрию для куба
		final Geometry cube_leak = new Geometry("Leak-through color cube", boxshape4);
		// создаем материал для геометрии куба
		final Material mat_tl = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
		// загружаем и применяем в материал текстуру для куба
		mat_tl.setTexture("ColorMap", assetManager.loadTexture("Textures/ColoredTex/Monkey.png"));
		// красив в свой указанный увет
		mat_tl.setColor("Color", new ColorRGBA(1f, 0f, 1f, 1f)); // purple
		// применяем материал в геометрию
		cube_leak.setMaterial(mat_tl);
		// добавляем в мир
		rootNode.attachChild(cube_leak);

		// создаем сферу размерами 32х32х2
		final Sphere rock = new Sphere(32, 32, 2f);
		// создаем геометрию для сферы
		shiny_rock = new Geometry("Shiny rock", rock);
		// активируем режим текстуры адаптации под сферу
		rock.setTextureMode(Sphere.TextureMode.Projected);
		// генерируем нейкую светку для геометрии, которая обеспечивает более
		// качественное освещение текстуры
		TangentBinormalGenerator.generate(rock);
		// создаем освещаемый материал
		final Material mat_lit = new Material(assetManager, "Common/MatDefs/Light/Lighting.j3md");
		// загружаем несколько текстур для разных состояний?
		mat_lit.setTexture("DiffuseMap", assetManager.loadTexture("Textures/Terrain/Pond/Pond.jpg"));
		mat_lit.setTexture("NormalMap", assetManager.loadTexture("Textures/Terrain/Pond/Pond_normal.png"));
		// ставим флаг на использование цвета материала
		mat_lit.setBoolean("UseMaterialColors", false);
		// тон самых ярких мест
		mat_lit.setColor("Specular", ColorRGBA.White);
		// основной тон заданный освещением
		mat_lit.setColor("Diffuse", ColorRGBA.White);
		// устанавливаем силу блеска
		mat_lit.setFloat("Shininess", 5f); // [1,128]
		// применяем материал на геометрию
		shiny_rock.setMaterial(mat_lit);
		// устанавливаем положение
		shiny_rock.setLocalTranslation(0, 2, -2);
		// разворпачиваем
		shiny_rock.rotate(1.6f, 0, 0);
		// добавляем в мир
		rootNode.attachChild(shiny_rock);

		// создаем источник света
		final DirectionalLight sun = new DirectionalLight();
		// устанавливаем место источника
		sun.setDirection(new Vector3f(1, 0, -2).normalizeLocal());
		// ставим цвет света
		sun.setColor(ColorRGBA.White);
		// одбавляем в мир
		rootNode.addLight(sun);

		final AmbientLight light = new AmbientLight();
		light.setColor(ColorRGBA.White);

		rootNode.addLight(light);

		// инициализируем прослушки нажатий кнопок
		initKeys();
		flyCam.setMoveSpeed(flyCam.getMoveSpeed() * 2);
	}
}