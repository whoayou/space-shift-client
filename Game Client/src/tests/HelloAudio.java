package tests;

import com.jme3.app.SimpleApplication;
import com.jme3.audio.AudioNode;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.MouseButtonTrigger;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.shape.Box;

/**
 * Пример использования озвучки в игре.
 */
public class HelloAudio extends SimpleApplication {

	public static void main(final String[] args) {
		final HelloAudio app = new HelloAudio();
		app.start();
	}

	/** какой-то аудио кусок */
	private AudioNode audio_gun;
	/** какой-то аудио кусок */
	private AudioNode audio_nature;

	/** камера игрока */
	private Geometry box;

	/**
	 * Создаем анонимную прослушку
	 */
	private final ActionListener actionListener = new ActionListener() {

		@Override
		public void onAction(final String name, final boolean keyPressed, final float tpf) {
			// если сработал акшен выстрела
			if(name.equals("Shoot") && !keyPressed)
				// запускаем звук выстрела
				audio_gun.playInstance(); // play each instance once!
		}
	};

	/**
	 * Настройка озвучки
	 */
	private void initAudio() {
		// загружаем звуковую дорожку, флаг фолс для того, что бы загрузить его
		// в буфер
		audio_gun = new AudioNode(assetManager, "Sound/Effects/Gun.wav", false);
		// ставим без повторений
		audio_gun.setLooping(false);
		// модифицируем громкость в 2 раза сильнее
		audio_gun.setVolume(2);
		// добавляем в мир
		rootNode.attachChild(audio_gun);

		// создаем звук окружающей обстановки
		audio_nature = new AudioNode(assetManager, "Sound/Environment/Nature.ogg", false);
		// ставим на авто повтор
		audio_nature.setLooping(true);
		// устанавливаем учет позиции источника звука
		audio_nature.setPositional(true);
		// ставим позицию источника
		audio_nature.setLocalTranslation(Vector3f.ZERO.clone());
		// усиливаем в 3 раза
		audio_nature.setVolume(3);
		// добавляем в мир
		rootNode.attachChild(audio_nature);
		// сразу запускаем проигрывание
		audio_nature.play(); // play continuously!
	}

	/**
	 * Инициализация клавишь
	 */
	private void initKeys() {
		// ставим на клилк мышкоый выстрел
		inputManager.addMapping("Shoot", new MouseButtonTrigger(0));
		// добавляем прослушку на этот акшен
		inputManager.addListener(actionListener, "Shoot");
	}

	@Override
	public void simpleInitApp() {
		// задаем скорость камеры
		flyCam.setMoveSpeed(20);

		// создаем коробку
		final Box box1 = new Box(Vector3f.ZERO, 1, 1, 1);
		// создаем геометрию коробки
		box = new Geometry("Player", box1);
		// создаем материал
		final Material mat1 = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
		// задаем цвет материала
		mat1.setColor("Color", ColorRGBA.Blue);
		// применяем материал к коробке
		box.setMaterial(mat1);
		// добавляем коробку в мир
		rootNode.attachChild(box);

		// инициализируем прослушку кнопок
		initKeys();
		// настраиваем озвучку
		initAudio();
	}

	@Override
	public void simpleUpdate(final float tpf) {
		// обновление положение ушей игрока
		listener.setLocation(cam.getLocation());
		listener.setRotation(cam.getRotation());
	}
}