package tests;

import java.util.logging.Level;

import com.jme3.app.SimpleApplication;
import com.jme3.font.BitmapText;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.plugins.blender.BlenderLoader;

/**
 * Пример звгрузки моделй, текстур и шрифто в 3д сцену
 */
public class HelloAssets extends SimpleApplication {

	public static void main(final String[] args) {
		java.util.logging.Logger.getLogger("").setLevel(Level.SEVERE);
		final HelloAssets app = new HelloAssets();
		app.start();
	}

	BitmapText ch;

	Spatial engine;

	@Override
	public void simpleInitApp() {
		assetManager.registerLoader(BlenderLoader.class, "blend"); // BlenderModelLoader

		// ставим цвет фона голубой
		// viewPort.setBackgroundColor(new ColorRGBA(0.7f, 0.8f, 1f, 1f));
		// ставим скорость камеры
		flyCam.setMoveSpeed(50);

		// загружаем сцену ангара
		final Spatial scene = assetManager.loadModel("Models/hangar.j3o");
		System.out.println(scene.getClass());
		// добавляем сцену в сцену стадии
		rootNode.attachChild(scene);

		final Spatial ship = assetManager.loadModel("Models/StarShip/ship_1.j3o");
		((Node) scene).attachChild(ship);

		engine = assetManager.loadModel("Models/Modules/Engines/exp_engine_1000.j3o");
		((Node) ship).attachChild(engine);
	}

	@Override
	public void simpleUpdate(final float tpf) {
		super.simpleUpdate(tpf);

		engine.setLocalTranslation(0, 0, -1.45F);
	}
}