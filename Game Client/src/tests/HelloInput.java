package tests;

import com.jme3.app.SimpleApplication;
import com.jme3.input.KeyInput;
import com.jme3.input.MouseInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.AnalogListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.input.controls.MouseButtonTrigger;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.shape.Box;

/**
 * Пример обработки нажатий кнопок
 */
public class HelloInput extends SimpleApplication {

	public static void main(final String[] args) {
		final HelloInput app = new HelloInput();
		app.start();
	}

	/** это наш куб посути */
	private Geometry boxGeom;
	/** флаг паузы */
	private boolean isRunning;

	/**
	 * Анонимный экземпляр прослушки включения/Выключения паузы
	 */
	private final ActionListener actionListener = new ActionListener() {

		@Override
		public void onAction(final String name, final boolean keyPressed, final float tpf) {
			if(name.equals("Pause") && !keyPressed)
				isRunning = !isRunning;
		}
	};

	/**
	 * Анонимный экземпляр прослушки нажатий клавишь
	 */
	private final AnalogListener analogListener = new AnalogListener() {

		@Override
		public void onAnalog(final String name, final float value, final float tpf) {
			if(!isRunning)
				System.out.println("Press P to unpause.");
			else if(name.equals("Rotate"))
				boxGeom.rotate(0, value * speed, 0);
			else if(name.equals("Right")) {
				final Vector3f vector = boxGeom.getLocalTranslation();
				boxGeom.setLocalTranslation(vector.x + value * speed, vector.y, vector.z);
			} else if(name.equals("Left")) {
				final Vector3f vector = boxGeom.getLocalTranslation();
				boxGeom.setLocalTranslation(vector.x - value * speed, vector.y, vector.z);
			}
		}
	};

	/**
	 * Инициализация прослушки кнопок.
	 */
	private void initKeys() {
		// вносим типы прослушаемых действий
		inputManager.addMapping("Pause", new KeyTrigger(KeyInput.KEY_P));
		inputManager.addMapping("Left", new KeyTrigger(KeyInput.KEY_J));
		inputManager.addMapping("Right", new KeyTrigger(KeyInput.KEY_K));
		inputManager.addMapping("Rotate", new KeyTrigger(KeyInput.KEY_SPACE), new MouseButtonTrigger(MouseInput.BUTTON_LEFT));
		// добавляем слушателей действий
		inputManager.addListener(actionListener, "Pause");
		inputManager.addListener(analogListener, "Left", "Right", "Rotate");

	}

	@Override
	public void simpleInitApp() {
		// создаем пустую коробку
		final Box box = new Box(Vector3f.ZERO, 1, 1, 1);
		// созаем геометрию коробки и вставляем туда коробку
		boxGeom = new Geometry("Player", box);
		// создаем покрытие геометрии
		final Material material = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
		// задаем ей синий цвет
		material.setColor("Color", ColorRGBA.Blue);
		// применяем покрытие на геометрию
		boxGeom.setMaterial(material);
		// добавляем объект в мир
		rootNode.attachChild(boxGeom);
		// инициализируем прослушки нажатий кнопок
		initKeys();
		// ставим флаг работы
		isRunning = true;
	}
}