package tests;

import com.jme3.app.SimpleApplication;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.shape.Box;

/**
 * Пример относительного размещения элементов внутри сцены.
 */
public class HelloNode extends SimpleApplication {

	public static void main(final String[] args) {
		final HelloNode app = new HelloNode();
		app.start();
	}

	@Override
	public void simpleInitApp() {
		// создаем первую коборку куб в координатах 1, 1, 1
		final Box blueBox = new Box(Vector3f.ZERO, 1, 1, 1);
		// создаем геометрию для коробки
		final Geometry blueGeom = new Geometry("Box", blueBox);
		// создаем материал для геометрии
		final Material blueMaterial = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
		// применяем синий цвет к материалу
		blueMaterial.setColor("Color", ColorRGBA.Blue);
		// применяем материал к геометрии
		blueGeom.setMaterial(blueMaterial);
		// перемещаем куб в координаты
		blueGeom.move(1, -1, 1);

		// создаем вторую коробку в координатаъ 1, 1, 1
		final Box redBox = new Box(Vector3f.ZERO, 1, 1, 1);
		// создаем геометрию для второй коробки
		final Geometry redGeom = new Geometry("Box", redBox);
		// создаем материал для второй коробке
		final Material redMaterial = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
		// задаем красный цвет
		redMaterial.setColor("Color", ColorRGBA.Red);
		// применяем материал
		redGeom.setMaterial(redMaterial);
		// перемещаем в координаты
		redGeom.move(1, 3, 1);

		// создаем как бы невидимый соединитель 2х кубов
		final Node pivot = new Node("pivot");
		// добавляем его в мир
		rootNode.attachChild(pivot);

		// а уже к соеденителю прикрепляем первый куб
		pivot.attachChild(blueGeom);
		// и второй куб
		pivot.attachChild(redGeom);

		// теперь поворачиваем соединитель, что за собой влекет разворот кубов
		pivot.rotate(.4f, .4f, 0f);
	}
}