package tests;

import java.util.Random;

import com.jme3.app.SimpleApplication;
import com.jme3.light.PointLight;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.renderer.Camera;
import com.jme3.scene.Geometry;
import com.jme3.scene.shape.Box;
import com.jme3.system.AppSettings;

/**
 * @author Ronn
 */
public class CameraTest extends SimpleApplication {

	private static final float FLY_ACCEL = 1.5f;

	private static final float ROTATE_SPEED = 0.3f;

	public static void main(final String[] args) {
		final CameraTest app = new CameraTest();

		final AppSettings settings = new AppSettings(true);

		settings.setFrameRate(60);

		app.setSettings(settings);

		app.setLast(System.currentTimeMillis());

		app.start();
	}

	public static final void updateCameraPosition(final Camera camera, final Geometry object) {
		final Vector3f loc = object.getLocalTranslation();

		final Quaternion rotation = object.getLocalRotation();

		final Vector3f cameraLoc = rotation.getRotationColumn(2);

		final int offset = 5;

		cameraLoc.multLocal(offset * -3);

		cameraLoc.addLocal(loc);

		final Vector3f aiming = rotation.getRotationColumn(2);

		aiming.multLocal(50);

		aiming.addLocal(loc);

		final Vector3f up = rotation.getRotationColumn(1);

		cameraLoc.addLocal(up.mult(offset));

		camera.setLocation(cameraLoc);

		camera.lookAt(aiming, up);
	}

	private Geometry geom;

	private long last;

	private float lastSpeed;

	public long getLast() {
		return last;
	}

	public float getLastSpeed() {
		return lastSpeed;
	}

	private Geometry newBox() {
		final Box box = new Box(1, 1, 1);

		final Geometry geom = new Geometry("Box", box);

		final Material material = new Material(assetManager, "Common/MatDefs/Light/Lighting.j3md");

		material.setTexture("DiffuseMap", assetManager.loadTexture("Textures/Terrain/Pond/Pond.jpg"));
		material.setTexture("NormalMap", assetManager.loadTexture("Textures/Terrain/Pond/Pond_normal.png"));
		material.setBoolean("UseMaterialColors", true);
		material.setColor("Specular", ColorRGBA.White);
		material.setColor("Diffuse", ColorRGBA.White);

		geom.setMaterial(material);

		return geom;
	}

	public void setLast(final long last) {
		this.last = last;
	}

	public void setLastSpeed(final float lastSpeed) {
		this.lastSpeed = lastSpeed;
	}

	@Override
	public void simpleInitApp() {
		geom = newBox();

		final PointLight light = new PointLight();

		light.setRadius(1000);

		rootNode.addLight(light);

		rootNode.attachChild(geom);

		final Random random = new Random();

		for(int i = 0, length = 10; i < length; i++) {
			final Geometry geom = newBox();

			final int x = random.nextInt(20);
			final int y = random.nextInt(20);
			final int z = random.nextInt(20);

			geom.setLocalTranslation(x, y, z);

			rootNode.attachChild(geom);
		}
	}

	@Override
	public void simpleUpdate(final float tpf) {
		final long currentTime = System.currentTimeMillis();

		float lastSpeed = getLastSpeed();

		try {
			final long last = getLast();

			final long diff = currentTime - last;

			// рассчитываем текующую скорость
			lastSpeed = Math.max(Math.min(lastSpeed + diff / 1000F * FLY_ACCEL, 2), 0);

			final float dist = lastSpeed * diff / 1000F;

			final float rotate = ROTATE_SPEED * diff / 1000F;

			// geom.rotate(rotate, rotate, rotate);

			final Quaternion rotation = geom.getLocalRotation();

			final Vector3f direction = rotation.getRotationColumn(2);

			direction.multLocal(dist);

			direction.addLocal(geom.getLocalTranslation());

			geom.setLocalTranslation(direction);

			super.simpleUpdate(tpf);

			updateCameraPosition(cam, geom);
		} finally {
			setLast(currentTime);

			setLastSpeed(lastSpeed);
		}
	}
}