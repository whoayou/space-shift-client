package com.ss.client.table;

import com.ss.client.document.DocumentBackgroundSound;
import com.ss.client.gui.model.BackgroundSound;
import com.ss.client.util.GameUtil;
import com.ss.client.util.ReflectionMethod;

import rlib.logging.Logger;
import rlib.logging.Loggers;
import rlib.manager.InitializeManager;
import rlib.util.Rnd;
import rlib.util.array.Array;
import rlib.util.array.Arrays;
import rlib.util.table.Table;
import rlib.util.table.Tables;

/**
 * Таблица фоновой озучки.
 * 
 * @author Ronn
 */
public final class BackgroundSoundTable {

	private static final Logger log = Loggers.getLogger(BackgroundSoundTable.class);

	private static BackgroundSoundTable instance;

	@ReflectionMethod
	public static BackgroundSoundTable getInstance() {

		if(instance == null) {
			instance = new BackgroundSoundTable();
		}

		return instance;
	}

	/** таблица темплейтов локаций */
	private final Table<String, BackgroundSound> table;

	/** список фоновой музыки */
	private final Array<BackgroundSound> musicList;
	/** список всех фоновых звуков */
	private final Array<BackgroundSound> soundList;

	private BackgroundSoundTable() {
		InitializeManager.valid(getClass());

		table = Tables.newObjectTable();

		musicList = Arrays.toArray(BackgroundSound.class);
		soundList = Arrays.toArray(BackgroundSound.class);

		final Array<BackgroundSound> result = new DocumentBackgroundSound(GameUtil.getInputStream("/data/background_sound.xml")).parse();

		for(final BackgroundSound sound : result) {
			if(table.containsKey(sound.getName()))
				log.warning("found duplicate for name " + sound.getName());

			table.put(sound.getName(), sound);

			if(!sound.isEffect())
				musicList.add(sound);

			soundList.add(sound);
		}

		log.info("loaded " + table.size() + " background sounds.");
	}

	/**
	 * @return слачайная фоновая музыка.
	 */
	public final BackgroundSound getNextBackground() {
		if(musicList.isEmpty())
			return null;

		return musicList.get(Rnd.nextInt(musicList.size() - 1));
	}

	/**
	 * @param name название звука.
	 * @return звук.
	 */
	public final BackgroundSound getSound(final String name) {
		return table.get(name);
	}

	/**
	 * Обновить уровень громкости.
	 */
	public final void updateVolume() {
		for(final BackgroundSound sound : soundList)
			sound.updateVolume();
	}
}
