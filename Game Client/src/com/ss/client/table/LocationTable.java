package com.ss.client.table;

import rlib.concurrent.ConcurrentUtils;
import rlib.logging.Logger;
import rlib.logging.Loggers;
import rlib.manager.InitializeManager;
import rlib.util.table.IntKey;
import rlib.util.table.Table;
import rlib.util.table.Tables;

import com.ss.client.model.location.LocationInfo;
import com.ss.client.network.Network;
import com.ss.client.network.packet.client.RequestLocationInfo;
import com.ss.client.util.ReflectionMethod;

/**
 * Таблица космических локаций.
 * 
 * @author Ronn
 */
public final class LocationTable {

	private static final Logger LOGGER = Loggers.getLogger(LocationTable.class);

	private static final int WAIT_TIME = 100000;

	private static LocationTable instance;

	@ReflectionMethod
	public static LocationTable getInstance() {

		if(instance == null) {
			instance = new LocationTable();
		}

		return instance;
	}

	/** таблица темплейтов локаций */
	private final Table<IntKey, LocationInfo> table;

	private LocationTable() {
		InitializeManager.valid(getClass());
		table = Tables.newIntegerTable();
		LOGGER.info("initialized.");
	}

	/**
	 * Добавление нового контейнера информации по локации в таблицу.
	 * 
	 * @param info новый контейнер инфы.
	 */
	public void addLocation(final LocationInfo info) {
		synchronized(table) {
			table.put(info.getId(), info);
		}
	}

	/**
	 * @return локация соотвествующего ид.
	 */
	public LocationInfo getLocation(final int locationId) {

		LocationInfo location = table.get(locationId);

		if(location == null) {
			location = loadLocation(locationId);
		}

		return location;
	}

	/**
	 * Загрузка с сервера информации по локации.
	 * 
	 * @param locationId ид локации.
	 * @return загруженный контейнер информации.
	 */
	private LocationInfo loadLocation(final int locationId) {

		final Network network = Network.getInstance();
		network.sendPacketToGameServer(RequestLocationInfo.getInstance(locationId));

		ConcurrentUtils.wait(this, WAIT_TIME);

		return table.get(locationId);
	}
}
