package com.ss.client.table;

import rlib.concurrent.ConcurrentUtils;
import rlib.logging.Logger;
import rlib.logging.Loggers;
import rlib.manager.InitializeManager;
import rlib.util.table.IntKey;
import rlib.util.table.Table;
import rlib.util.table.Tables;

import com.ss.client.network.Network;
import com.ss.client.network.packet.client.RequestGravityObjectTemplate;
import com.ss.client.template.GravityObjectTemplate;
import com.ss.client.util.ReflectionMethod;

/**
 * Таблица шаблонов гравитационных объектов кораблей.
 * 
 * @author Ronn
 */
public final class GravityObjectTable {

	private static final Logger LOGGER = Loggers.getLogger(GravityObjectTable.class);

	private static final int WAIT_TIME = 100000;

	private static GravityObjectTable instance;

	@ReflectionMethod
	public static GravityObjectTable getInstance() {

		if(instance == null) {
			instance = new GravityObjectTable();
		}

		return instance;
	}

	/** таблица темплейтов модулей кораблей */
	private final Table<IntKey, GravityObjectTemplate> table;

	private GravityObjectTable() {
		InitializeManager.valid(getClass());
		table = Tables.newIntegerTable();
		LOGGER.info("initialized.");
	}

	/**
	 * Добавление нового шаблона грави объекта в таблицу.
	 * 
	 * @param template новый шаблон грави объекта.
	 */
	public void addTemplate(final GravityObjectTemplate template) {

		LOGGER.info("loaded template " + template.getId());

		synchronized(table) {
			table.put(template.getId(), template);
		}
	}

	/**
	 * @return темплейт соотвествующего ид.
	 */
	public GravityObjectTemplate getTemplate(final int templateId) {

		GravityObjectTemplate template = table.get(templateId);

		if(template == null) {
			template = loadTemplate(templateId);
		}

		return template;
	}

	/**
	 * Загрузка с сервера шаблона гравитационного объекта.
	 * 
	 * @param templateId ид шаблона.
	 * @return загруженный шаблон.
	 */
	private GravityObjectTemplate loadTemplate(final int templateId) {

		LOGGER.info("request template " + templateId);

		final Network network = Network.getInstance();
		network.sendPacketToGameServer(RequestGravityObjectTemplate.getInstance(templateId));

		ConcurrentUtils.wait(this, WAIT_TIME);

		return table.get(templateId);
	}
}
