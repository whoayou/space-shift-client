package com.ss.client.table;

import rlib.logging.Logger;
import rlib.logging.Loggers;
import rlib.util.Strings;
import rlib.util.table.Table;

import com.ss.client.GameConfig;
import com.ss.client.document.DocumentLanguage;
import com.ss.client.document.DocumentLocalization;
import com.ss.client.gui.controller.game.window.quest.QuestWindowTextIds;
import com.ss.client.gui.controller.game.window.ship.ShipWindowTextIds;
import com.ss.client.gui.controller.game.window.skills.SkillWindowTextIds;
import com.ss.client.gui.controller.game.window.storage.StorageWindowTextIds;
import com.ss.client.gui.model.Language;
import com.ss.client.util.GameUtil;
import com.ss.client.util.ReflectionMethod;

/**
 * Таблица локализации.
 * 
 * @author Ronn
 */
public final class LangTable {

	private static final Logger LOGGER = Loggers.getLogger(LangTable.class);

	private static final String[] TEMPLATE_PATHS = {
		"/data/localization/game_interface.xml",
		"/data/localization/interface.xml",
		"/data/localization/skills.xml",
		"/data/localization/quests.xml",
		"/data/localization/stations.xml",
		"/data/localization/modules.xml",
		"/data/localization/items.xml",
		"/data/localization/nps.xml",

		/** локализация заданий */
		"/data/localization/game_interface/quest_main.xml",
		"/data/localization/game_interface/quests/100-200.xml",

		/** локализация окон */
		QuestWindowTextIds.LOCALIZATION_FILE,
		ShipWindowTextIds.LOCALIZATION_FILE,
		SkillWindowTextIds.LOCALIZATION_FILE,
		StorageWindowTextIds.LOCALIZATION_FILE,
	};

	private static LangTable instance;

	@ReflectionMethod
	public static LangTable getInstance() {

		if(instance == null) {
			instance = new LangTable();
		}

		return instance;
	}

	/** список языков клиента */
	private final Language[] langs;

	/** таблица локализации клиента */
	private Table<String, String>[] localization;

	@SuppressWarnings("unchecked")
	private LangTable() {

		langs = new DocumentLanguage(GameUtil.getInputStream("/data/language.xml")).parse();

		LOGGER.info("loaded " + langs.length + " languages.");

		localization = new Table[langs.length];

		for(final String path : TEMPLATE_PATHS) {
			localization = new DocumentLocalization(localization, GameUtil.getInputStream(path)).parse();
		}

		int count = 0;

		for(final Table<String, String> table : localization) {
			count += table.size();
		}

		LOGGER.info("loaded " + count + " texts for localization.");
	}

	/**
	 * @return список доступных языков.
	 */
	public Language[] getLangs() {
		return langs;
	}

	/**
	 * Получение надписи в соответсвии с языком.
	 * 
	 * @param id ид напдиси.
	 * @return сама надпись.
	 */
	public String getText(final String id) {

		if(id == null || id.isEmpty()) {
			return id;
		}

		final Table<String, String> table = localization[GameConfig.LANG.getOrdinal()];

		final String text = table.get(id);
		return text == null ? "<unknown = \"" + id.replaceAll("@", "") + "\">" : text.replaceAll("@", Strings.EMPTY);
	}

	/**
	 * Возвращает ссылку на язык с указанным имененм.
	 * 
	 * @param name название языка.
	 * @return ссылка на язык.
	 */
	public Language langOf(final String name) {

		final Language[] langs = getLangs();

		for(int i = 0, length = langs.length; i < length; i++) {

			final Language lang = langs[i];

			if(lang.getName().equalsIgnoreCase(name)) {
				return lang;
			}
		}

		return null;
	}
}
