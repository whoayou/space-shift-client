package com.ss.client.table;

import rlib.concurrent.ConcurrentUtils;
import rlib.logging.Logger;
import rlib.logging.Loggers;
import rlib.manager.InitializeManager;
import rlib.util.table.IntKey;
import rlib.util.table.Table;
import rlib.util.table.Tables;

import com.ss.client.network.Network;
import com.ss.client.network.packet.client.RequestStationTemplate;
import com.ss.client.template.StationTemplate;
import com.ss.client.util.ReflectionMethod;

/**
 * Таблица моделей станций.
 * 
 * @author Ronn
 */
public final class StationTable {

	private static final Logger LOGGER = Loggers.getLogger(StationTable.class);

	private static final int WAIT_TIME = 100000;

	private static StationTable instance;

	@ReflectionMethod
	public static StationTable getInstance() {

		if(instance == null) {
			instance = new StationTable();
		}

		return instance;
	}

	/** таблица темплейтов моделей станций */
	private final Table<IntKey, StationTemplate> table;

	private StationTable() {
		InitializeManager.valid(getClass());
		table = Tables.newIntegerTable();
		LOGGER.info("initialized.");
	}

	/**
	 * Добавление нового шаблона станции в таблицу.
	 * 
	 * @param template новый шаблон станции.
	 */
	public void addTemplate(final StationTemplate template) {

		LOGGER.info("loaded template " + template.getId());

		synchronized(table) {
			table.put(template.getId(), template);
		}
	}

	/**
	 * @return темплейт соотвествующего ид.
	 */
	public StationTemplate getTemplate(final int templateId) {

		StationTemplate template = table.get(templateId);

		if(template == null) {
			template = loadTemplate(templateId);
		}

		return template;
	}

	/**
	 * Загрузка с сервера шаблона корабля.
	 * 
	 * @param templateId ид шаблона.
	 * @return загруженный шаблон.
	 */
	private StationTemplate loadTemplate(final int templateId) {

		LOGGER.info("request template " + templateId);

		final Network network = Network.getInstance();
		network.sendPacketToGameServer(RequestStationTemplate.getInstance(templateId));

		ConcurrentUtils.wait(this, WAIT_TIME);

		return table.get(templateId);
	}
}
