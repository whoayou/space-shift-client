package com.ss.client.table;

import rlib.concurrent.ConcurrentUtils;
import rlib.logging.Logger;
import rlib.logging.Loggers;
import rlib.manager.InitializeManager;
import rlib.util.table.IntKey;
import rlib.util.table.Table;
import rlib.util.table.Tables;

import com.ss.client.network.Network;
import com.ss.client.network.packet.client.RequestLocationObjectTemplate;
import com.ss.client.template.LocationObjectTemplate;
import com.ss.client.util.ReflectionMethod;

/**
 * Таблица шаблонов локационных объектов кораблей.
 * 
 * @author Ronn
 */
public final class LocationObjectTable {

	private static final Logger LOGGER = Loggers.getLogger(LocationObjectTable.class);

	private static final int WAIT_TIME = 100000;

	private static LocationObjectTable instance;

	@ReflectionMethod
	public static LocationObjectTable getInstance() {

		if(instance == null) {
			instance = new LocationObjectTable();
		}

		return instance;
	}

	/** таблица шаблонов локационных объектов */
	private final Table<IntKey, LocationObjectTemplate> table;

	private LocationObjectTable() {
		InitializeManager.valid(getClass());

		this.table = Tables.newIntegerTable();

		LOGGER.info("initialized.");
	}

	/**
	 * Добавление нового шаблона грави объекта в таблицу.
	 * 
	 * @param template новый шаблон грави объекта.
	 */
	public void addTemplate(final LocationObjectTemplate template) {

		LOGGER.info("loaded template " + template.getId());

		synchronized(table) {
			table.put(template.getId(), template);
		}
	}

	/**
	 * @return темплейт соотвествующего ид.
	 */
	public LocationObjectTemplate getTemplate(final int templateId) {

		LocationObjectTemplate template = table.get(templateId);

		if(template == null) {
			template = loadTemplate(templateId);
		}

		return template;
	}

	/**
	 * Загрузка с сервера шаблона гравитационного объекта.
	 * 
	 * @param templateId ид шаблона.
	 * @return загруженный шаблон.
	 */
	private LocationObjectTemplate loadTemplate(final int templateId) {

		LOGGER.info("request template " + templateId);

		final Network network = Network.getInstance();
		network.sendPacketToGameServer(RequestLocationObjectTemplate.getInstance(templateId));

		ConcurrentUtils.wait(this, WAIT_TIME);

		return table.get(templateId);
	}
}
