package com.ss.client.manager;

import rlib.concurrent.ConcurrentUtils;
import rlib.manager.InitializeManager;
import rlib.util.array.Array;
import rlib.util.array.Arrays;

import com.ss.client.model.util.UpdateableObject;
import com.ss.client.tasks.UpdateObjectTask;
import com.ss.client.util.ReflectionMethod;

/**
 * Менедж по обновлению различных объектов.
 * 
 * @author Ronn
 */
public class UpdateObjectManager {

	public static final int MAX_CONTAINER_LOAD = 30;
	public static final int START_CONTAINERS_COUNT = 1;

	private static UpdateObjectManager instance;

	@ReflectionMethod
	public static UpdateObjectManager getInstance() {

		if(instance == null) {
			instance = new UpdateObjectManager();
		}

		return instance;
	}

	/** контейнеры обновляемых объектов */
	private volatile Array<UpdateableObject>[] containers;
	/** список задач по обновлению элементов в контейнерах */
	private volatile UpdateObjectTask[] updateTasks;

	/** индекс последнего использованного контейнера */
	private int index;

	@SuppressWarnings("unchecked")
	public UpdateObjectManager() {
		InitializeManager.valid(getClass());

		containers = new Array[START_CONTAINERS_COUNT];
		updateTasks = new UpdateObjectTask[START_CONTAINERS_COUNT];

		for(int i = 0, length = containers.length; i < length; i++) {
			containers[i] = Arrays.toArray(UpdateableObject.class);
			updateTasks[i] = new UpdateObjectTask(containers[i], i);
		}
	}

	/**
	 * Добавление на обновление нового объекта,.
	 * 
	 * @param object новый объект.
	 */
	public void addObject(UpdateableObject object) {

		Array<UpdateableObject>[] containers = getContainers();
		Array<UpdateableObject> target = null;

		int index = getIndex();

		// фаза 1, мы пробуем найти не занятый контейнер в списке после
		// последнего используемого
		if(index < containers.length - 1) {
			for(int i = index + 1, length = containers.length; i < length; i++) {

				Array<UpdateableObject> container = containers[i];

				if(container.size() < MAX_CONTAINER_LOAD) {
					target = container;
					index = i;
					break;
				}
			}

		}

		// фаза 2, если еще не нашли контейнер, пробуем еще раз поискать с
		// самого начала списка
		if(target == null) {
			for(int i = 0, length = containers.length; i < length; i++) {

				Array<UpdateableObject> container = containers[i];

				if(container.size() < MAX_CONTAINER_LOAD) {
					target = container;
					index = i;
					break;
				}
			}
		}

		// фаза 3, если досихпор не нашли контейнер, создаем новый
		if(target == null) {
			target = createContainer(containers.length);
			index = containers.length;
		}

		synchronized(target) {

			target.add(object);

			if(target.size() == 1) {
				ConcurrentUtils.notifyAllInSynchronize(target);
			}
		}

		object.bind(target);
		setIndex(index);
	}

	/**
	 * Расширение списка контейнеров.
	 * 
	 * @param currentLength текущее кол-во контейнеров.
	 * @return новый контейнер.
	 */
	private synchronized Array<UpdateableObject> createContainer(int currentLength) {

		Array<UpdateableObject>[] containers = getContainers();

		// если уже было произведено расширение в другом потоке
		if(containers.length > currentLength) {
			return containers[currentLength];
		}

		containers = Arrays.copyOf(containers, 1);
		containers[currentLength] = Arrays.toArray(UpdateableObject.class);

		UpdateObjectTask[] updateTasks = getUpdateTasks();
		updateTasks = Arrays.copyOf(updateTasks, 1);
		updateTasks[currentLength] = new UpdateObjectTask(containers[currentLength], currentLength);

		setContainers(containers);
		setUpdateTasks(updateTasks);

		return containers[currentLength];
	}

	/**
	 * @return контейнера обновляемых объектов.
	 */
	public Array<UpdateableObject>[] getContainers() {
		return containers;
	}

	/**
	 * @return индекс контейнера, в который был последний добавлен элемент.
	 */
	public int getIndex() {
		return index;
	}

	/**
	 * @return задачи по обновлению элементов в контейнерах.
	 */
	public UpdateObjectTask[] getUpdateTasks() {
		return updateTasks;
	}

	/**
	 * @param containers контейнера обновляемых объктов.
	 */
	public void setContainers(Array<UpdateableObject>[] containers) {
		this.containers = containers;
	}

	/**
	 * @param index индекс контейнера, в который был последний добавлен элемент.
	 */
	public void setIndex(int index) {
		this.index = index;
	}

	/**
	 * @param updateTasks задачи по обновлению элементов в контейнерах.
	 */
	public void setUpdateTasks(UpdateObjectTask[] updateTasks) {
		this.updateTasks = updateTasks;
	}
}
