/*
 * Copyright (c) 2009-2012 jMonkeyEngine
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 *
 * * Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in the
 *   documentation and/or other materials provided with the distribution.
 *
 * * Neither the name of 'jMonkeyEngine' nor the names of its contributors
 *   may be used to endorse or promote products derived from this software
 *   without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.ss.client.jme.post.filter;

import com.jme3.asset.AssetManager;
import com.jme3.material.Material;
import com.jme3.math.FastMath;
import com.jme3.math.Matrix4f;
import com.jme3.math.Vector3f;
import com.jme3.post.Filter;
import com.jme3.renderer.Camera;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.renderer.queue.RenderQueue;

/**
 * Форк реализации эффекта солнечных лучей в камеру.
 * 
 * @author Rémy Bouquet aka Nehon
 * @fork by Ronn
 */
public class LightScatteringFilter extends Filter {

	private static final String MATERIAL_DISPLAY = "Display";
	private static final String MATERIAL_LIGHT_DENSITY = "LightDensity";
	private static final String MATERIAL_BLUR_WIDTH = "BlurWidth";
	private static final String MATERIAL_BLUR_START = "BlurStart";
	private static final String MATERIAL_NB_SAMPLES = "NbSamples";
	private static final String MATERIAL_LIGHT_POSITION = "LightPosition";

	public static final float DEFAULT_LIGHT_DENSITY = 1.4f;
	public static final float DEFAULT_BLUR_WIDTH = 0.9f;
	public static final float DEFAULT_BLUR_START = 0.02f;

	public static final int DEFAULT_SAMPLES = 50;

	/** позиция источника света на экране */
	private final Vector3f screenLightPos;
	/** позиция источника света на вьюхе */
	private final Vector3f viewLightPos;
	/** позиция источника света на сцене */
	private final Vector3f lightPosition;

	/** вьюха сцены */
	private ViewPort viewPort;

	/** расстояние от источника, при котором начинается размытие */
	private float blurStart;
	/** ширина размытия */
	private float blurWidth;
	/** распространение эффекта на экране */
	private float lightDensity;
	/** плотность света */
	private float innerLightDensity;

	/** качество радиального размытия */
	private int nbSamples;

	/** отображать ли эффект вобоще */
	private boolean display;

	public LightScatteringFilter(Vector3f lightPosition) {
		super("Light Scattering");
		this.screenLightPos = new Vector3f();
		this.viewLightPos = new Vector3f();
		this.blurStart = DEFAULT_BLUR_START;
		this.blurWidth = DEFAULT_BLUR_WIDTH;
		this.lightDensity = DEFAULT_LIGHT_DENSITY;
		this.nbSamples = DEFAULT_SAMPLES;
		this.display = true;
		this.lightPosition = lightPosition;
	}

	private void calcClipCoordinates(Vector3f worldPosition, Vector3f store, Camera camera) {

		Matrix4f projectionMatrix = camera.getViewProjectionMatrix();

		float w = projectionMatrix.multProj(worldPosition, store);
		store.divideLocal(w);

		store.x = ((store.x + 1f) * (camera.getViewPortRight() - camera.getViewPortLeft()) / 2f + camera.getViewPortLeft());
		store.y = ((store.y + 1f) * (camera.getViewPortTop() - camera.getViewPortBottom()) / 2f + camera.getViewPortBottom());
		store.z = (store.z + 1f) / 2f;
	}

	/**
	 * @return расстояние от источника, при котором начинается размытие.
	 */
	public float getBlurStart() {
		return blurStart;
	}

	/**
	 * @return ширина размытия.
	 */
	public float getBlurWidth() {
		return blurWidth;
	}

	/**
	 * @return плотность света.
	 */
	private float getInnerLightDensity() {
		return innerLightDensity;
	}

	/**
	 * @return распространение эффекта на экране.
	 */
	public float getLightDensity() {
		return lightDensity;
	}

	/**
	 * @return позиция источника света на сцене.
	 */
	public Vector3f getLightPosition() {
		return lightPosition;
	}

	@Override
	protected Material getMaterial() {

		material.setVector3(MATERIAL_LIGHT_POSITION, getScreenLightPos());
		material.setInt(MATERIAL_NB_SAMPLES, getNbSamples());
		material.setFloat(MATERIAL_BLUR_START, getBlurStart());
		material.setFloat(MATERIAL_BLUR_WIDTH, getBlurWidth());
		material.setFloat(MATERIAL_LIGHT_DENSITY, getInnerLightDensity());
		material.setBoolean(MATERIAL_DISPLAY, isDisplay());

		return material;
	}

	/**
	 * @return качество радиального размытияю
	 */
	public int getNbSamples() {
		return nbSamples;
	}

	/**
	 * @return позиция источника света на экране.
	 */
	private Vector3f getScreenLightPos() {
		return screenLightPos;
	}

	/**
	 * @return позиция источника света на вьюхе.
	 */
	private Vector3f getViewLightPos() {
		return viewLightPos;
	}

	/**
	 * @return вьюха сцены.
	 */
	private ViewPort getViewPort() {
		return viewPort;
	}

	@Override
	protected void initFilter(AssetManager manager, RenderManager renderManager, ViewPort viewPort, int width, int height) {
		this.viewPort = viewPort;
		this.material = new Material(manager, "Common/MatDefs/Post/LightScattering.j3md");
	}

	/**
	 * @return отображать ли эффект вобоще.
	 */
	private boolean isDisplay() {
		return display;
	}

	@Override
	protected boolean isRequiresDepthTexture() {
		return true;
	}

	@Override
	protected void postQueue(RenderQueue queue) {

		ViewPort viewPort = getViewPort();

		Vector3f screenLightPos = getScreenLightPos();
		Vector3f lightPosition = getLightPosition();
		Vector3f viewLightPos = getViewLightPos();

		calcClipCoordinates(lightPosition, screenLightPos, viewPort.getCamera());

		viewPort.getCamera().getViewMatrix().mult(lightPosition, viewLightPos);

		float densityX = 1 - FastMath.clamp(FastMath.abs(screenLightPos.getX() - 0.5f), 0, 1);
		float densityY = 1 - FastMath.clamp(FastMath.abs(screenLightPos.getY() - 0.5f), 0, 1);
		float innerLightDensity = getLightDensity() * densityX * densityY;

		setInnerLightDensity(innerLightDensity);
		setDisplay(innerLightDensity != 0.0F && viewLightPos.getZ() < 0);
	}

	/**
	 * @param blurStart расстояние от источника, при котором начинается размытие
	 */
	public void setBlurStart(float blurStart) {
		this.blurStart = blurStart;
	}

	/**
	 * @param blurWidth ширина размытия.
	 */
	public void setBlurWidth(float blurWidth) {
		this.blurWidth = blurWidth;
	}

	/**
	 * @param display отображать ли эффект вобоще.
	 */
	private void setDisplay(boolean display) {
		this.display = display;
	}

	/**
	 * @param innerLightDensity плотность света.
	 */
	private void setInnerLightDensity(float innerLightDensity) {
		this.innerLightDensity = innerLightDensity;
	}

	/**
	 * @param lightDensity распространение эффекта на экране.
	 */
	public void setLightDensity(float lightDensity) {
		this.lightDensity = lightDensity;
	}

	/**
	 * @param nbSamples качество радиального размытияю
	 */
	public void setNbSamples(int nbSamples) {
		this.nbSamples = nbSamples;
	}
}
