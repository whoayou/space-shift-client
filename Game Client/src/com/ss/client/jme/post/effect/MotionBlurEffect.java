package com.ss.client.jme.post.effect;

import com.jme3.post.FilterPostProcessor;
import com.ss.client.jme.post.PostEffect;
import com.ss.client.jme.post.filter.MotionBlurFilter;
import com.ss.client.model.ship.player.PlayerShip;
import com.ss.client.util.LocalObjects;

/**
 * Реализация эффекта смазывания при движении.
 * 
 * @author Ronn
 */
public class MotionBlurEffect implements PostEffect {

	private static MotionBlurEffect instance;

	public static PostEffect bind(FilterPostProcessor processor) {
		instance = new MotionBlurEffect(processor);
		return instance;
	}

	public static MotionBlurEffect getInstance() {
		return instance;
	}

	/** фильтр смазывания при движении */
	private MotionBlurFilter filter;

	public MotionBlurEffect(FilterPostProcessor processor) {

		MotionBlurFilter filter = new MotionBlurFilter();
		filter.setBlurSamples(8);
		filter.setStrength(0.2F);
		filter.setEnabled(false);

		processor.addFilter(filter);
		setFilter(filter);
	}

	@Override
	public void activate(LocalObjects local) {
		filter.setEnabled(true);
	}

	@Override
	public void deactivate(LocalObjects local) {
		filter.setEnabled(false);
	}

	/**
	 * @param filter фильтр смазывания при движении.
	 */
	public void setFilter(MotionBlurFilter filter) {
		this.filter = filter;
	}

	@Override
	public void setPlayerShip(PlayerShip playerShip) {
	}

	@Override
	public void update(LocalObjects local) {
		// TODO Auto-generated method stub

	}
}
