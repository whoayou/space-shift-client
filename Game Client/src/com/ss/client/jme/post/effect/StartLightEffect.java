package com.ss.client.jme.post.effect;

import com.jme3.math.Vector3f;
import com.jme3.post.FilterPostProcessor;
import com.ss.client.Game;
import com.ss.client.jme.post.PostEffect;
import com.ss.client.jme.post.filter.LightScatteringFilter;
import com.ss.client.model.ship.player.PlayerShip;
import com.ss.client.util.LocalObjects;

/**
 * Класс с реализацией эффекта солнечного света.
 * 
 * @author Ronn
 */
public class StartLightEffect implements PostEffect {

	private static final float DEFAULT_LIGHT_DENSITY = 0.5F;
	private static final float DEFAULT_BLUR_WIDTH = 0.9F;
	private static final float DEFAULT_BLUR_START = 0.3F;
	private static final int DEFAULT_SAMPLES = 50;

	private static final Game GAME = Game.getInstance();

	private static StartLightEffect instance;

	public static PostEffect bind(FilterPostProcessor processor) {
		instance = new StartLightEffect(processor);
		return instance;
	}

	public static StartLightEffect getInstance() {
		return instance;
	}

	/** положение источника относительно камеры */
	private final Vector3f offset;
	/** позиция света в сцене */
	private final Vector3f lightPos;
	/** центр сцены, на которую светит источник света */
	private final Vector3f center;

	/** реализация фильтра эффекта */
	private LightScatteringFilter filter;

	public StartLightEffect(FilterPostProcessor processor) {
		this.offset = new Vector3f();
		this.lightPos = new Vector3f();
		this.center = GAME.getCamera().getLocation();

		LightScatteringFilter filter = new LightScatteringFilter(lightPos);
		filter.setBlurStart(DEFAULT_BLUR_START);
		filter.setBlurWidth(DEFAULT_BLUR_WIDTH);
		filter.setLightDensity(DEFAULT_LIGHT_DENSITY);
		filter.setNbSamples(DEFAULT_SAMPLES);
		filter.setEnabled(false);

		// processor.addFilter(filter);
		setFilter(filter);
	}

	@Override
	public void activate(LocalObjects local) {
	}

	@Override
	public void deactivate(LocalObjects local) {
		filter.setEnabled(false);
	}

	/**
	 * @param enabled включен ли фильтр.
	 */
	public void setEnabled(boolean enabled) {
		if(filter.isEnabled() != enabled) {
			filter.setEnabled(enabled);
		}
	}

	/**
	 * @param filter реализация фильтра эффекта.
	 */
	public void setFilter(LightScatteringFilter filter) {
		this.filter = filter;
	}

	@Override
	public void setPlayerShip(PlayerShip playerShip) {
	}

	@Override
	public void update(LocalObjects local) {

		if(!filter.isEnabled()) {
			return;
		}

		lightPos.set(center).addLocal(offset);
	}

	/**
	 * Обновление отступа от центра.
	 * 
	 * @param offset новый отступ источника.
	 */
	public void updateOffset(Vector3f offset) {

		if(offset == null) {
			return;
		}

		this.offset.set(offset);
	}
}
