package com.ss.client.jme.post.effect;

import com.jme3.post.FilterPostProcessor;
import com.jme3.post.filters.BloomFilter;
import com.jme3.post.filters.BloomFilter.GlowMode;
import com.ss.client.jme.post.PostEffect;
import com.ss.client.model.ship.player.PlayerShip;
import com.ss.client.util.LocalObjects;

/**
 * Реализация блум фильтра.
 * 
 * @author Ronn
 */
public class BloomEffect implements PostEffect {

	private static BloomEffect instance;

	public static PostEffect bind(FilterPostProcessor processor) {
		instance = new BloomEffect(processor);
		return instance;
	}

	public static BloomEffect getInstance() {
		return instance;
	}

	/** фильт для сцены */
	private BloomFilter sceneFilter;
	/** фильтр для объектов */
	private BloomFilter objectFilter;

	public BloomEffect(FilterPostProcessor processor) {

		BloomFilter sceneFilter = new BloomFilter(GlowMode.SceneAndObjects);
		sceneFilter.setDownSamplingFactor(5);
		sceneFilter.setBloomIntensity(1.1F);
		sceneFilter.setBlurScale(1.0F);
		sceneFilter.setExposureCutOff(0);
		sceneFilter.setExposurePower(4);
		sceneFilter.setEnabled(false);

		BloomFilter objectFilter = new BloomFilter(GlowMode.Objects);
		objectFilter.setBloomIntensity(1.0F);
		objectFilter.setBlurScale(0);
		objectFilter.setDownSamplingFactor(1);
		objectFilter.setExposureCutOff(0);
		objectFilter.setExposurePower(6);
		objectFilter.setEnabled(false);

		processor.addFilter(objectFilter);
		processor.addFilter(sceneFilter);

		setObjectFilter(objectFilter);
		setSceneFilter(sceneFilter);
	}

	@Override
	public void activate(LocalObjects local) {
		sceneFilter.setEnabled(true);
		objectFilter.setEnabled(true);
	}

	@Override
	public void deactivate(LocalObjects local) {
		sceneFilter.setEnabled(false);
		objectFilter.setEnabled(false);
	}

	/**
	 * @param objectFilter фильтр для объектов.
	 */
	private void setObjectFilter(BloomFilter objectFilter) {
		this.objectFilter = objectFilter;
	}

	@Override
	public void setPlayerShip(PlayerShip playerShip) {
	}

	/**
	 * @param sceneFilter фильт для сцены.
	 */
	private void setSceneFilter(BloomFilter sceneFilter) {
		this.sceneFilter = sceneFilter;
	}

	@Override
	public void update(LocalObjects local) {
	}
}
