package com.ss.client.jme.post;

import com.ss.client.model.ship.player.PlayerShip;
import com.ss.client.util.LocalObjects;

/**
 * Интерфейс для реализации пост эффектов в игре.
 * 
 * @author Ronn
 */
public interface PostEffect {

	/**
	 * Активация эффекта.
	 * 
	 * @param local контейнер локальных объектов.
	 */
	public void activate(LocalObjects local);

	/**
	 * Деактивация эффекта.
	 * 
	 * @param local контейнер локальных объектов.
	 */
	public void deactivate(LocalObjects local);

	/**
	 * Установка сссылки на корабль игрока.
	 * 
	 * @param playerShip корабль игрока.
	 */
	public void setPlayerShip(PlayerShip playerShip);

	/**
	 * Обновление состояния эффекта.
	 * 
	 * @param local контейнер локальных объектов.
	 */
	public void update(LocalObjects local);
}
