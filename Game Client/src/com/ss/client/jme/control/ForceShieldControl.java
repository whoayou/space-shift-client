package com.ss.client.jme.control;

import java.io.IOException;

import rlib.util.array.Array;
import rlib.util.array.Arrays;
import rlib.util.wraps.Wrap;
import rlib.util.wraps.Wraps;

import com.jme3.asset.AssetManager;
import com.jme3.export.JmeExporter;
import com.jme3.export.JmeImporter;
import com.jme3.material.Material;
import com.jme3.material.RenderState;
import com.jme3.material.RenderState.BlendMode;
import com.jme3.material.RenderState.FaceCullMode;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.renderer.queue.RenderQueue.Bucket;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.Control;
import com.jme3.shader.VarType;
import com.jme3.texture.Texture;
import com.ss.client.Game;
import com.ss.client.util.LocalObjects;

/**
 * Реализация силового порля.
 * 
 * @author Ronn
 */
public class ForceShieldControl implements Control {

	public static final String MATERIAL_COLLISION_NUM = "CollisionNum";
	public static final String MATERIAL_COLLISIONS_KEY = "Collisions";
	public static final String MATERIAL_COLLISION_ALPHAS_KEY = "CollisionAlphas";
	public static final String MATERIAL_WORK_KEY = "Work";
	public static final String MATERIAL_EFFECT_SIZE_KEY = "MaxDistance";
	public static final String MATERIAL_TEXTURE_KEY = "ColorMap";
	public static final String MATERIAL_VISIBILITY_KEY = "MinAlpha";
	public static final String MATERIAL_COLOR_KEY = "Color";

	private static final Game GAME = Game.getInstance();

	/**
	 * Max number of hits displayed I've experienced crashes with 7 or 8 hits
	 */
	private static final int MAX_HITS = 30;

	/** пул векторов для щита */
	private final Array<Vector3f> vectorPool = Arrays.toArray(Vector3f.class);
	/** пул оберток для щита */
	private final Array<Wrap> wrapPool = Arrays.toArray(Wrap.class);

	/** список точек попаданий */
	private final Array<Vector3f> collisions;
	/** время анимаций точек попаданий */
	private final Array<Wrap> collisionTimes;

	/** материал щита */
	private final Material material;

	/** модель щита */
	private Spatial model;

	/** время продолжительности анимации удара */
	private float maxTime;
	/** текущее время итоговой анимации */
	private float timer;
	/** длинна итоговой анимации */
	private float timerSize;

	/** было ли изменено кол-во отображаемых ударов */
	private boolean numChanged;
	/** включено ли поле */
	private boolean enabled;
	/** запущена ли анимация */
	private boolean work;

	public ForceShieldControl(final AssetManager assetManager) {
		this.material = new Material(assetManager, "matdefs/force_shield.j3md");

		RenderState additionalRenderState = this.material.getAdditionalRenderState();

		additionalRenderState.setBlendMode(BlendMode.Alpha);
		additionalRenderState.setFaceCullMode(FaceCullMode.Off);

		this.material.setFloat(MATERIAL_EFFECT_SIZE_KEY, 1);
		this.numChanged = false;
		this.enabled = true;
		this.work = false;
		this.timerSize = 3f;
		this.maxTime = 0.5f;
		this.collisionTimes = Arrays.toArray(Wrap.class);
		this.collisions = Arrays.toArray(Vector3f.class);
		this.timer = 0;
	}

	public ForceShieldControl(final AssetManager assetManager, final float duration) {
		this(assetManager);
		this.maxTime = duration;
		this.timerSize *= duration;
	}

	@Override
	public Control cloneForSpatial(final Spatial spatial) {
		ForceShieldControl control = new ForceShieldControl(GAME.getAssetManager());
		control.maxTime = getMaxTime();
		control.timerSize = getTimerSize();
		return control;
	}

	/**
	 * @return список точек попаданий.
	 */
	public Array<Vector3f> getCollisions() {
		return collisions;
	}

	/**
	 * @return время анимаций точек попаданий.
	 */
	public Array<Wrap> getCollisionTimes() {
		return collisionTimes;
	}

	/**
	 * @return материал щита.
	 */
	public Material getMaterial() {
		return material;
	}

	/**
	 * @return максимальное времяю
	 */
	public float getMaxTime() {
		return maxTime;
	}

	/**
	 * @return модель щитаю
	 */
	public Spatial getModel() {
		return model;
	}

	/**
	 * @return текущее время итоговой анимации.
	 */
	public float getTimer() {
		return timer;
	}

	/**
	 * @return длинна итоговой анимации.
	 */
	public float getTimerSize() {
		return timerSize;
	}

	/**
	 * @return
	 */
	public Array<Vector3f> getVectorPool() {
		return vectorPool;
	}

	/**
	 * @return
	 */
	public Array<Wrap> getWrapPool() {
		return wrapPool;
	}

	/**
	 * @return включено ли полею
	 */
	public boolean isEnabled() {
		return enabled;
	}

	/**
	 * @return было ли изменено кол-во отображаемых ударовю
	 */
	public boolean isNumChanged() {
		return numChanged;
	}

	/**
	 * @return запущена ли анимацияю
	 */
	public boolean isWork() {
		return work;
	}

	@Override
	public void read(final JmeImporter importer) throws IOException {
	}

	/**
	 * Регистрация попадания в щит.
	 * 
	 * @param position позиция попадания.
	 * @param local контейнер локальных объектов.
	 */
	public void registerHit(final Vector3f position, LocalObjects local) {

		if(!isEnabled()) {
			return;
		}

		Material material = getMaterial();
		Spatial model = getModel();

		Array<Wrap> collisionTimes = getCollisionTimes();
		Array<Vector3f> collisions = getCollisions();

		material.setBoolean(MATERIAL_WORK_KEY, true);

		synchronized(collisions) {

			Vector3f hitPosition = vectorPool.pop();

			if(hitPosition == null) {
				hitPosition = new Vector3f();
			}

			model.worldToLocal(position, hitPosition);

			Wrap wrap = wrapPool.pop();

			if(wrap == null) {
				wrap = Wraps.newFloatWrap(getMaxTime(), false);
			} else {
				wrap.setFloat(getMaxTime());
			}

			collisions.add(hitPosition);
			collisionTimes.add(wrap);

			setTimer(0);
			setWork(true);
			setNumChanged(true);
			updateCollisionPoints();
		}
	}

	@Override
	public void render(final RenderManager rm, final ViewPort vp) {
	}

	/**
	 * @param color цвет материала щита.
	 */
	public void setColor(final ColorRGBA color) {
		getMaterial().setColor(MATERIAL_COLOR_KEY, color);
	}

	/**
	 * @param size размер волны от попадания на щите.
	 */
	public void setEffectSize(final float size) {
		getMaterial().setFloat(MATERIAL_EFFECT_SIZE_KEY, size / getModel().getLocalScale().x);
	}

	/**
	 * @param enabled включено ли полею
	 */
	public void setEnabled(final boolean enabled) {
		this.enabled = enabled;
	}

	/**
	 * @param numChanged было ли изменено кол-во отображаемых ударовю
	 */
	public void setNumChanged(boolean numChanged) {
		this.numChanged = numChanged;
	}

	@Override
	public void setSpatial(final Spatial model) {
		this.model = model;
		model.setMaterial(getMaterial());
		model.setQueueBucket(Bucket.Transparent);
	}

	/**
	 * @param texture текстура материала щита.
	 */
	public void setTexture(final Texture texture) {
		getMaterial().setTexture(MATERIAL_TEXTURE_KEY, texture);
	}

	public void setTimer(float timer) {
		this.timer = timer;
	}

	/**
	 * @param percent процентр прозрачности материала щита.
	 */
	public void setVisibility(final float percent) {
		getMaterial().setFloat(MATERIAL_VISIBILITY_KEY, percent);
	}

	/**
	 * @param work запущена ли анимация.
	 */
	public void setWork(boolean work) {
		this.work = work;
	}

	@Override
	public void update(final float tpf) {

		if(isWork() && isEnabled()) {

			Array<Wrap> collisionTimes = getCollisionTimes();
			Array<Vector3f> collisions = getCollisions();

			Array<Vector3f> vectorPool = getVectorPool();
			Array<Wrap> wrapPool = getWrapPool();

			float timer = getTimer();

			if(timer > getTimerSize()) {
				collisions.clear();
				collisionTimes.clear();
				getMaterial().setBoolean(MATERIAL_WORK_KEY, false);
				setTimer(0F);
				setWork(false);
				setNumChanged(false);
				return;
			}

			timer += tpf * 3F;

			for(int i = 0, length = collisionTimes.size(); i < length; i++) {

				Wrap wrap = collisionTimes.get(i);

				float time = wrap.getFloat();
				time -= tpf;

				if(time <= 0) {
					vectorPool.add(collisions.get(i));
					wrapPool.add(wrap);
					collisionTimes.slowRemove(i);
					collisions.slowRemove(i);
					setNumChanged(true);
					length -= 1;
					i -= 1;
					continue;
				}

				wrap.setFloat(time);
			}

			if(isNumChanged()) {
				updateCollisionPoints();
				setNumChanged(false);
			}

			setTimer(timer);
			updateCollisionAlpha();
		}
	}

	/**
	 * Обновление прозрачностей эффектов попаданий.
	 */
	protected void updateCollisionAlpha() {

		final Array<Wrap> collisionTimes = getCollisionTimes();
		final Array<Vector3f> collisions = getCollisions();

		final float[] alphas = new float[Math.min(collisionTimes.size(), MAX_HITS)];
		final float maxTime = getMaxTime();

		for(int i = 0, length = Math.min(collisions.size(), MAX_HITS); i < length; i++) {
			Wrap wrap = collisionTimes.get(length - 1 - i);
			alphas[i] = wrap.getFloat() / maxTime;
		}

		getMaterial().setParam(MATERIAL_COLLISION_ALPHAS_KEY, VarType.FloatArray, alphas);
	}

	/**
	 * Обновление точек попаданий.
	 */
	protected void updateCollisionPoints() {

		final Array<Vector3f> collisions = getCollisions();
		final Vector3f[] array = new Vector3f[Math.min(collisions.size(), MAX_HITS)];

		for(int i = 0, length = collisions.size(); i < length && i < MAX_HITS; i++) {
			array[i] = collisions.get(length - 1 - i);
		}

		Material material = getMaterial();
		material.setParam(MATERIAL_COLLISIONS_KEY, VarType.Vector3Array, array);
		material.setInt(MATERIAL_COLLISION_NUM, Math.min(collisions.size(), MAX_HITS));
	}

	@Override
	public void write(final JmeExporter exporter) throws IOException {
	}
}