package com.ss.client.jme.util;

import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.ss.client.Game;

/**
 * Утильный класс с методами по работе с узлами геометрии.
 * 
 * @author Ronn
 */
public class NodeUtils {

	private static final Game GAME = Game.getInstance();

	public static void addToNode(Node node, Spatial spatial) {

		if(!GAME.isHeldByCurrentThread()) {
			new Exception(Thread.currentThread().getName()).printStackTrace();
		}

		node.attachChild(spatial);
	}

	public static void removeToNode(Node node, Spatial spatial) {

		if(!GAME.isHeldByCurrentThread()) {
			new Exception(Thread.currentThread().getName()).printStackTrace();
		}
		node.detachChild(spatial);
	}

	private NodeUtils() {
		throw new RuntimeException();
	}
}
