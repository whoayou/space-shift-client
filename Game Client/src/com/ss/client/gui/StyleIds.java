package com.ss.client.gui;

/**
 * @author Ronn
 */
public interface StyleIds {

	public static final String LOGIN_PAGE_BUTTON_AUTH = "login-button-auth";
	public static final String LOGIN_PAGE_BUTTON_SETTING = "login-button-setting";
	public static final String LOGIN_PAGE_BUTTON_ABOUT = "login-button-about";

	public static final String LOGIN_PAGE_LABEL = "login-label";
	public static final String LOGIN_PAGE_LABEL_LINK = "login-label-link";

	public static final String GENERAL_TEXT_FIELD = "general-textfield";
	public static final String GENERAL_DROP_DOWN = "general-drop-down";
	public static final String GENERAL_LIST_BOX_STYLE = "general-list-box-style";

	public static final String GENERAL_CHECK_BOX = "general-checkbox";

	public static final String GENERAL_BLACK_PANEL = "general-black-panel";

	public static final String GENERAL_LABEL_16 = "general-label-16";

	public static final String GENERAL_HORIZONTAL_SLIDER = "general-horizontal-slider";

	public static final String SPACE_LABEL = "space-label";
	public static final String SPACE_DISABLEABLE_LABEL = "space-disableable-label";
	public static final String SPACE_LOGIN_BUTTON_LABEL = "space-login-button-label";

	public static final String SPACE_TEXT_FIELD = "space-textfield";

	public static final String SPACE_LOGIN_BUTTON = "space-login-button";
	public static final String SPACE_CUSTOM_BUTTON = "space-custom-button";
}
