package com.ss.client.gui.util;

import rlib.util.array.Array;

import com.ss.client.util.LocalObjects;

/**
 * Интерфейс для описания асинхронно обновляемого УИ элемента.
 * 
 * @author Ronn
 */
public interface UpdateUIElement {

	/**
	 * Запомнить контейнер, в котором находится элемент для обновления.
	 * 
	 * @param container контейнер обновляемых элементов.
	 */
	public void bind(Array<UpdateUIElement> container);

	/**
	 * Обработка завершения выполнения задачи.
	 */
	public void finish();

	/**
	 * @return сложный ли элемент.
	 */
	public boolean isComplexElement();

	/**
	 * Обновление элемента.
	 * 
	 * @param local контейнер локальных объектов.
	 * @param currentTime текущее время.
	 * @return завершена ли задача элемента.
	 */
	public boolean update(LocalObjects local, long currentTime);
}
