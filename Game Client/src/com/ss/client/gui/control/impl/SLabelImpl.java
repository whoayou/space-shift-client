package com.ss.client.gui.control.impl;

import java.util.Properties;

import com.ss.client.gui.control.SLabel;
import com.ss.client.gui.element.impl.AbstractElementUI;
import com.ss.client.util.ReflectionClass;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.elements.render.TextRenderer;
import de.lessvoid.nifty.layout.align.HorizontalAlign;
import de.lessvoid.nifty.layout.align.VerticalAlign;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.spi.render.RenderFont;
import de.lessvoid.nifty.tools.Color;
import de.lessvoid.xml.xpp3.Attributes;

/**
 * Реализация надписи для Space Shift.
 * 
 * @author Ronn
 */
@ReflectionClass
public final class SLabelImpl extends AbstractElementUI implements SLabel {

	/** рендер текста надписи */
	private TextRenderer textRenderer;

	@Override
	public void bind(Nifty nifty, Screen screen, Element element, Properties parameter, Attributes attributes) {
		super.bind(nifty, screen, element, parameter, attributes);

		final boolean wrap = attributes.getAsBoolean(PARAMETR_WRAP, false);

		TextRenderer textRenderer = element.getRenderer(TextRenderer.class);
		textRenderer.setLineWrapping(wrap);
		setTextRenderer(textRenderer);
	}

	@Override
	public Color getColor() {
		return getTextRenderer().getColor();
	}

	@Override
	public RenderFont getFont() {
		return getTextRenderer().getFont();
	}

	@Override
	public String getOriginalText() {
		return getTextRenderer().getOriginalText();
	}

	/**
	 * @return рендер текста надписи.
	 */
	private TextRenderer getTextRenderer() {
		return textRenderer;
	}

	@Override
	public String getWrappedText() {
		return getTextRenderer().getWrappedText();
	}

	@Override
	public boolean isLineWrapping() {
		return getTextRenderer().isLineWrapping();
	}

	@Override
	public void setColor(Color color) {
		getTextRenderer().setColor(color);
	}

	@Override
	public void setFont(RenderFont fontParam) {
		getTextRenderer().setFont(fontParam);
	}

	@Override
	public void setText(String text) {
		getTextRenderer().setText(text);
	}

	@Override
	public void setTextHAlign(HorizontalAlign newTextHAlign) {
		getTextRenderer().setTextHAlign(newTextHAlign);
	}

	/**
	 * @param textRenderer рендер текста надписи.
	 */
	private void setTextRenderer(TextRenderer textRenderer) {
		this.textRenderer = textRenderer;
	}

	@Override
	public void setTextVAlign(VerticalAlign newTextVAlign) {
		getTextRenderer().setTextVAlign(newTextVAlign);
	}
}
