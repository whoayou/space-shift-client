package com.ss.client.gui.control.builder;

import de.lessvoid.nifty.builder.ControlBuilder;

/**
 * Реализация строителя кнопки.
 * 
 * @author Ronn
 */
public class SButtonBuilder extends ControlBuilder {

	public SButtonBuilder(String id) {
		super(id, "s_button");
	}

	/**
	 * @param text надпись на кнопке.
	 */
	public void setText(final String text) {
		set("text", text);
	}
}
