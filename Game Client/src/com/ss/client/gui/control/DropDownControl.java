package com.ss.client.gui.control;

import java.util.List;
import java.util.Properties;

import com.ss.client.gui.control.event.DropDownListBoxSelectionChangedEventSubscriber;
import com.ss.client.util.ReflectionClass;

import rlib.logging.Logger;
import rlib.logging.Loggers;
import de.lessvoid.nifty.EndNotify;
import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.controls.AbstractController;
import de.lessvoid.nifty.controls.DropDown;
import de.lessvoid.nifty.controls.FocusHandler;
import de.lessvoid.nifty.controls.ListBox;
import de.lessvoid.nifty.controls.ListBox.ListBoxViewConverter;
import de.lessvoid.nifty.controls.ListBoxSelectionChangedEvent;
import de.lessvoid.nifty.controls.listbox.ListBoxControl;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.input.NiftyInputEvent;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.xml.xpp3.Attributes;

/**
 * Реализация выпадающего списка.
 * 
 * @author Ronn
 */
@ReflectionClass
public class DropDownControl<T> extends AbstractController implements DropDown<T> {

	private static final Logger LOGGER = Loggers.getLogger(DropDownControl.class);

	public static final String ATTRIBUTE_DISPLAY_ITEMS = "displayItems";
	public static final String ATTRIBUTE_POPUP_CONTROL_ID = "popupControlId";
	public static final String ATTRIBUTE_VIEW_CONVERTER_CLASS = "viewConverterClass";
	public static final String ATTRIBUTE_HORIZONTAL_SCROLL = "horizontal";
	public static final String ATTRIBUTE_VERTICAL_SCROLL = "vertical";

	public static final String SCROLL_ON = "on";
	public static final String SCROLL_OFF = "off";
	public static final String SCROLL_OPTIONAL = "optional";

	private Nifty nifty;
	/** UI экран */
	private Screen screen;

	private FocusHandler focusHandler;

	/** выпадающий список */
	private Element popup;

	/** список элементов */
	private ListBox<T> listBox;

	/** можно ли открывать список */
	private boolean alreadyOpen;

	@Override
	public void addAllItems(final List<T> itemsToAdd) {
		listBox.addAllItems(itemsToAdd);
		updateEnabled();
	}

	@Override
	public void addItem(final T newItem) {
		listBox.addItem(newItem);
		updateEnabled();
	}

	@Override
	@SuppressWarnings("unchecked")
	public void bind(final Nifty niftyParam, final Screen screenParam, final Element newElement, final Properties properties, final Attributes controlDefinitionAttributesParam) {
		super.bind(newElement);
		nifty = niftyParam;
		screen = screenParam;
		focusHandler = screen.getFocusHandler();

		Element element = getElement();

		final String elementId = element.getId();

		if(elementId == null) {
			LOGGER.warning("The DropDownControl requires an id but this one is missing it.");
			return;
		}

		Attributes attributes = element.getElementType().getAttributes();
		Attributes parameters = new Attributes();
		parameters.set(ATTRIBUTE_DISPLAY_ITEMS, properties.getProperty(ATTRIBUTE_DISPLAY_ITEMS, "4"));
		parameters.set(ATTRIBUTE_VERTICAL_SCROLL, properties.getProperty(ATTRIBUTE_VERTICAL_SCROLL, "on"));
		parameters.set(ATTRIBUTE_HORIZONTAL_SCROLL, properties.getProperty(ATTRIBUTE_HORIZONTAL_SCROLL, "off"));

		String style = attributes.get("style");
		String popupControlId = properties.getProperty(ATTRIBUTE_POPUP_CONTROL_ID);

		Element popup = nifty.createPopupWithStyle(popupControlId, style, parameters);
		DropDownPopup<T> control = popup.getControl(DropDownPopup.class);

		if(control == null) {
			LOGGER.warning("not found popup control instanceof " + DropDownPopup.class.getName());
			return;
		}

		control.setDropDownElement(this, popup);

		ListBox<T> listBox = popup.findNiftyControl("#listBox", ListBox.class);

		final DropDownViewConverter<T> converter = createViewConverter(properties.getProperty(ATTRIBUTE_VIEW_CONVERTER_CLASS));

		if(converter != null) {
			setViewConverter(converter);
		}

		setPopup(popup);
		setListBox(listBox);
	}

	@Override
	public void clear() {
		listBox.clear();
		updateEnabled();
	}

	public void close() {
		closeInternal(null);
	}

	public void close(final EndNotify endNotify) {
		closeInternal(endNotify);
	}

	private void closeInternal(final EndNotify endNotify) {

		setAlreadyOpen(false);
		getNifty().closePopup(getPopup().getId(), new EndNotify() {

			@Override
			public void perform() {

				ListBox<T> listBox = getListBox();
				Nifty nifty = getNifty();
				Screen screen = getScreen();
				Element popup = getPopup();
				DropDownListBoxSelectionChangedEventSubscriber event = new DropDownListBoxSelectionChangedEventSubscriber(nifty, screen, listBox, DropDownControl.this, popup);

				nifty.subscribe(screen, listBox.getId(), ListBoxSelectionChangedEvent.class, event);

				if(endNotify != null) {
					endNotify.perform();
				}
			}
		});
	}

	@SuppressWarnings("unchecked")
	private DropDownViewConverter<T> createViewConverter(final String className) {

		if(className == null) {
			return null;
		}

		try {
			return (DropDownViewConverter<T>) Class.forName(className).newInstance();
		} catch(Exception e) {
			LOGGER.warning(e);
		}

		return null;
	}

	public void dropDownClicked() {

		Element popup = getPopup();
		Nifty nifty = getNifty();

		if(popup == null || isAlreadyOpen()) {
			return;
		}

		setAlreadyOpen(true);
		nifty.showPopup(nifty.getCurrentScreen(), popup.getId(), null);
	}

	@Override
	public List<T> getItems() {
		return listBox.getItems();
	}

	/**
	 * @return список элементов.
	 */
	public ListBox<T> getListBox() {
		return listBox;
	}

	/**
	 * @return nifty.
	 */
	public Nifty getNifty() {
		return nifty;
	}

	/**
	 * @returnвыпадающий список.
	 */
	public Element getPopup() {
		return popup;
	}

	/**
	 * @return UI экран.
	 */
	public Screen getScreen() {
		return screen;
	}

	@Override
	public int getSelectedIndex() {

		List<Integer> selection = listBox.getSelectedIndices();

		if(selection.isEmpty()) {
			return -1;
		}

		return selection.get(0);
	}

	@Override
	public T getSelection() {

		List<T> selection = listBox.getSelection();

		if(selection.isEmpty()) {
			return null;
		}

		return selection.get(0);
	}

	@Override
	public boolean inputEvent(final NiftyInputEvent inputEvent) {

		if(inputEvent == NiftyInputEvent.NextInputElement) {
			focusHandler.getNext(getElement()).setFocus();
			return true;
		} else if(inputEvent == NiftyInputEvent.PrevInputElement) {
			focusHandler.getPrev(getElement()).setFocus();
			return true;
		} else if(inputEvent == NiftyInputEvent.Activate) {
			dropDownClicked();
			return true;
		} else if(inputEvent == NiftyInputEvent.MoveCursorUp) {
			listBox.selectPrevious();
			return true;
		} else if(inputEvent == NiftyInputEvent.MoveCursorDown) {
			listBox.selectNext();
			return true;
		}

		return false;
	}

	@Override
	public void insertItem(final T item, final int index) {
		listBox.insertItem(item, index);
		updateEnabled();
	}

	// DropDown implementation that forwards to the internal ListBox

	/**
	 * @return можно ли открывать список.
	 */
	public boolean isAlreadyOpen() {
		return alreadyOpen;
	}

	@Override
	public int itemCount() {
		return listBox.itemCount();
	}

	@SuppressWarnings({
		"unchecked",
		"deprecation",
		"rawtypes"
	})
	@Override
	public void onStartScreen() {
		updateEnabled();

		ListBoxControl listBoxControl = (ListBoxControl) listBox;
		listBoxControl.getViewConverter().display(getElement().findElementByName("#text"), getSelection());

		ListBox<T> listBox = getListBox();
		Nifty nifty = getNifty();
		Screen screen = getScreen();
		Element popup = getPopup();
		DropDownListBoxSelectionChangedEventSubscriber event = new DropDownListBoxSelectionChangedEventSubscriber(nifty, screen, listBox, this, popup);

		nifty.subscribe(screen, listBox.getId(), ListBoxSelectionChangedEvent.class, event);

	}

	public void refresh() {
	}

	@Override
	public void removeAllItems(final List<T> itemsToRemove) {
		listBox.removeAllItems(itemsToRemove);
		updateEnabled();
	}

	@Override
	public void removeItem(final T item) {
		listBox.removeItem(item);
		updateEnabled();
	}

	@Override
	public void removeItemByIndex(final int itemIndex) {
		listBox.removeItemByIndex(itemIndex);
		updateEnabled();
	}

	@Override
	public void selectItem(final T item) {
		listBox.selectItem(item);
	}

	@Override
	public void selectItemByIndex(final int selectionIndex) {
		listBox.selectItemByIndex(selectionIndex);
	}

	/**
	 * @param alreadyOpen можно ли открывать список.
	 */
	public void setAlreadyOpen(boolean alreadyOpen) {
		this.alreadyOpen = alreadyOpen;
	}

	/**
	 * @param listBox список элементов.
	 */
	public void setListBox(ListBox<T> listBox) {
		this.listBox = listBox;
	}

	/**
	 * @param popup выпадающий список.
	 */
	public void setPopup(Element popup) {
		this.popup = popup;
	}

	@Override
	public void setViewConverter(final DropDownViewConverter<T> viewConverter) {

		getListBox().setListBoxViewConverter(new ListBoxViewConverter<T>() {

			@Override
			public void display(final Element listBoxItem, final T item) {
				viewConverter.display(listBoxItem, item);
			}

			@Override
			public int getWidth(final Element element, final T item) {
				return viewConverter.getWidth(element, item);
			}
		});
	}

	private void updateEnabled() {
		setEnabled(!listBox.getItems().isEmpty());
	}
}
