package com.ss.client.gui.control.event;

import com.ss.client.gui.control.SButton;

import de.lessvoid.nifty.NiftyEvent;

/**
 * Событие о нажатии на кнопку.
 * 
 * @author Ronn
 */
public class ButtonClickedEvent implements NiftyEvent<Void> {

	private static final ThreadLocal<ButtonClickedEvent> LOCAL_EVENT = new ThreadLocal<ButtonClickedEvent>() {

		@Override
		protected ButtonClickedEvent initialValue() {
			return new ButtonClickedEvent();
		}
	};

	public static ButtonClickedEvent get(SButton button) {

		ButtonClickedEvent event = LOCAL_EVENT.get();
		event.setButton(button);

		return event;
	}

	/** нажатая кнопка */
	private SButton button;

	/**
	 * @return нажатая кнопка.
	 */
	public SButton getButton() {
		return button;
	}

	private void setButton(SButton button) {
		this.button = button;
	}
}
