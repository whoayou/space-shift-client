package com.ss.client.gui.control;

import com.ss.client.gui.element.ElementUI;

import de.lessvoid.nifty.layout.align.HorizontalAlign;
import de.lessvoid.nifty.layout.align.VerticalAlign;
import de.lessvoid.nifty.spi.render.RenderFont;
import de.lessvoid.nifty.tools.Color;

/**
 * Интерфейс для реализации кнопки.
 * 
 * @author Ronn
 */
public interface SButton extends ElementUI {

	/**
	 * Активация кнопки.
	 */
	public void activate();

	/**
	 * @return текущий шрифт кнопки.
	 */
	public RenderFont getFont();

	/**
	 * @return текущий текст кнопки.
	 */
	public String getText();

	/**
	 * @return текущий цвет текста.
	 */
	public Color getTextColor();

	/**
	 * @return текущее горизонтальное выравнивание текста.
	 */
	public HorizontalAlign getTextHAlign();

	/**
	 * @return текущая высота текста.
	 */
	public int getTextHeight();

	/**
	 * @return текущее вертикальное выравнивание текста.
	 */
	public VerticalAlign getTextVAlign();

	/**
	 * @return текущая ширина текста.
	 */
	public int getTextWidth();

	/**
	 * @param font новый шрифт для текста.
	 */
	public void setFont(final RenderFont font);

	/**
	 * @param текущий текст кнопки.
	 */
	public void setText(final String text);

	/**
	 * @param newColor новый цвет текста.
	 */
	public void setTextColor(final Color newColor);

	/**
	 * @param newTextHAlign новое горизонтальное выравнивание.
	 */
	public void setTextHAlign(final HorizontalAlign newTextHAlign);

	/**
	 * @param newTextVAlign новое вертикальное выравнивание текста.
	 */
	public void setTextVAlign(final VerticalAlign newTextVAlign);
}
