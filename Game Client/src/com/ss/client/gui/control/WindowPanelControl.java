package com.ss.client.gui.control;

import java.util.Properties;

import com.ss.client.gui.events.WindowMinimizedEvent;
import com.ss.client.gui.model.ElementUtils;
import com.ss.client.util.GameUtil;
import com.ss.client.util.Initializable;
import com.ss.client.util.ReflectionClass;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.controls.window.WindowControl;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.tools.SizeValue;
import de.lessvoid.xml.xpp3.Attributes;

/**
 * Модель контрола сворачиваемого окошка.
 * 
 * @author Ronn
 */
@ReflectionClass
public abstract class WindowPanelControl extends WindowControl implements Initializable {

	private Nifty nifty;

	/** стартовая высота окна */
	private int startHeight;

	/** скрыт ли контент */
	private boolean closed;

	@Override
	public void bind(final Nifty nifty, final Screen screen, final Element element, final Properties parameter, final Attributes controlDefinitionAttributes) {
		this.nifty = nifty;
		super.bind(nifty, screen, element, parameter, controlDefinitionAttributes);
	}

	@Override
	public void closeWindow() {
		minimize();
		nifty.publishEvent(getId(), WindowMinimizedEvent.getInstance(getElement(), isClosed()));
	}

	/**
	 * @return стартовая высота окна.
	 */
	public int getStartHeight() {
		return startHeight;
	}

	public boolean isClosed() {
		return closed;
	}

	/**
	 * Обработка сворачивания/разоврачивания.
	 */
	public void minimize() {

		final Element element = getElement();
		final Element content = getContent();

		final boolean closed = isClosed();

		if(closed) {

			content.show();

			final SizeValue height = GameUtil.getPixelSize(getStartHeight());

			element.setConstraintHeight(height);
		} else {

			final SizeValue height = GameUtil.getPixelSize(getStartHeight() - content.getHeight());

			element.setConstraintHeight(height);

			content.hide();
		}

		setClosed(!closed);

		ElementUtils.updateLayout(element.getParent());
	}

	@Override
	public void onStartScreen() {

		super.onStartScreen();

		final Element element = getElement();

		startHeight = element.getHeight();
	}

	public void setClosed(final boolean closed) {
		this.closed = closed;
	}
}
