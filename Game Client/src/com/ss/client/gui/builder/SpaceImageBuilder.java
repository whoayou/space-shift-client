package com.ss.client.gui.builder;

import com.ss.client.gui.ElementId;
import com.ss.client.gui.ImageId;

import de.lessvoid.nifty.builder.ImageBuilder;

/**
 * @author Ronn
 */
public class SpaceImageBuilder extends ImageBuilder {

	public SpaceImageBuilder(final ElementId elementId) {
		super(elementId.getId());
	}

	public SpaceImageBuilder(final String id) {
		super(id);
	}

	public void filename(final ImageId imageId) {
		super.filename(imageId.getPath());
	}
}
