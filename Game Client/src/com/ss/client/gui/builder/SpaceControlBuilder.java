package com.ss.client.gui.builder;

import com.ss.client.gui.ElementId;

import de.lessvoid.nifty.builder.ControlBuilder;

/**
 * Конструктор кастомных контролов.
 * 
 * @author Ronn
 */
public class SpaceControlBuilder extends ControlBuilder {

	public SpaceControlBuilder(final ElementId elementId, final String name) {
		super(elementId.getId(), name);
	}

	public SpaceControlBuilder(final String id, final String name) {
		super(id, name);
	}
}
