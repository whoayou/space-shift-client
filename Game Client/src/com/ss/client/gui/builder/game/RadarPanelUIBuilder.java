package com.ss.client.gui.builder.game;

import com.ss.client.gui.builder.SpaceLayerBuilder;
import com.ss.client.gui.controller.game.panel.radar.RadarPanelUIController;
import com.ss.client.gui.element.builder.impl.DraggableElementUIBuilder;
import com.ss.client.gui.element.builder.impl.ImageUIBuilder;
import com.ss.client.gui.element.builder.impl.PanelUIBuilder;

import de.lessvoid.nifty.builder.ElementBuilder.Align;
import de.lessvoid.nifty.builder.ElementBuilder.ChildLayoutType;

/**
 * Конструктор главной панели.
 * 
 * @author Ronn
 */
public class RadarPanelUIBuilder {

	public static void build(final SpaceLayerBuilder layer) {

		PanelUIBuilder objectPanel = new PanelUIBuilder(RadarPanelUIController.OBJECT_PANEL_ID);
		objectPanel.setLayoutType(ChildLayoutType.AbsoluteInside);
		objectPanel.setMarginBottom("1px");
		objectPanel.setMarginRight("3px");
		objectPanel.setWidth("200px");
		objectPanel.setHeight("200px");

		ImageUIBuilder zoomIn = new ImageUIBuilder(RadarPanelUIController.ZOOM_IN_ID);
		zoomIn.setPath(RadarPanelUIController.ZOOM_IN_IMAGE);
		zoomIn.setVisibleToMouse(true);
		zoomIn.setAlign(Align.Right);
		zoomIn.setWidth("25px");
		zoomIn.setHeight("25px");

		ImageUIBuilder zoomOut = new ImageUIBuilder(RadarPanelUIController.ZOOM_OUT_ID);
		zoomOut.setPath(RadarPanelUIController.ZOOM_OUT_IMAGE);
		zoomOut.setVisibleToMouse(true);
		zoomOut.setMarginLeft("17px");
		zoomOut.setAlign(Align.Right);
		zoomOut.setWidth("20px");
		zoomOut.setHeight("20px");

		PanelUIBuilder zoomPanel = new PanelUIBuilder(RadarPanelUIController.ZOOM_PANEL_ID);
		zoomPanel.setLayoutType(ChildLayoutType.Vertical);
		zoomPanel.setWidth("89%");
		zoomPanel.add(zoomIn);
		zoomPanel.add(zoomOut);

		DraggableElementUIBuilder mainPanel = new DraggableElementUIBuilder(RadarPanelUIController.PANEL_ID);
		mainPanel.setController(RadarPanelUIController.class);
		mainPanel.setBackgroundImage(RadarPanelUIController.BACKGROUND_IMAGE);
		mainPanel.setWidth("202px");
		mainPanel.setHeight("205px");
		mainPanel.add(objectPanel);
		mainPanel.add(zoomPanel);
		mainPanel.buildTo(layer);
	}

}
