package com.ss.client.gui.builder.game.factory;

import com.ss.client.gui.controller.game.item.ItemDescriptionEffect;
import com.ss.client.gui.controller.game.skill.SkillDescriptionBuilder;
import com.ss.client.gui.controller.game.skill.SkillElementUIController;
import com.ss.client.gui.controller.game.skill.SkillReuseUIController;
import com.ss.client.gui.element.DynamicPanelUI;
import com.ss.client.gui.element.builder.impl.DraggableElementUIBuilder;
import com.ss.client.gui.element.builder.impl.DynamicPanelUIBuilder;
import com.ss.client.gui.element.builder.impl.MessageElementUIBuilder;
import com.ss.client.gui.element.builder.impl.PanelUIBuilder;
import com.ss.client.model.skills.Skill;
import com.ss.client.template.SkillTemplate;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.builder.ElementBuilder.ChildLayoutType;
import de.lessvoid.nifty.builder.ElementBuilder.VAlign;
import de.lessvoid.nifty.effects.EffectEventId;
import de.lessvoid.nifty.effects.EffectProperties;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.tools.Color;

/**
 * Фабрика элементов умений.
 * 
 * @author Ronn
 */
public class SkillElementUIFactory {

	private static final SkillDescriptionBuilder DESCRIPTION_BUILDER = new SkillDescriptionBuilder();

	/**
	 * Создание элемента скила для списка скилов.
	 * 
	 * @param parent кнопка содержащая скил.
	 * @param skill скил.
	 * @return элемент отображающий скил.
	 */
	public static Element build(final Element parent, final Skill skill, final Nifty nifty, final Screen screen) {

		final SkillTemplate template = skill.getTemplate();

		final String id = "#skill:" + skill.getId() + "_" + skill.getObjectId();

		DraggableElementUIBuilder skillElement = new DraggableElementUIBuilder(id);
		skillElement.setController(SkillElementUIController.class);
		skillElement.setBackgroundImage(template.getIcon());
		skillElement.setLayoutType(ChildLayoutType.Center);
		skillElement.setDoppable(true);
		skillElement.setWidth("32px");
		skillElement.setHeight("32px");
		skillElement.add(buildReuse());

		DESCRIPTION_BUILDER.addTo(skillElement, EffectEventId.onHover);

		Element result = skillElement.build(nifty, screen, parent);

		SkillElementUIController control = result.getControl(SkillElementUIController.class);
		control.setSkill(skill);

		return result;
	}

	/**
	 * @return панель с элементами отката.
	 */
	public static PanelUIBuilder buildReuse() {

		PanelUIBuilder reuseImage = new PanelUIBuilder(SkillReuseUIController.REUSE_IMAGE_ID);
		reuseImage.setBackgroundImage(SkillReuseUIController.REUSE_IMAGE);
		reuseImage.setWidth("34px");
		reuseImage.setHeight("34px");

		PanelUIBuilder reusePanel = new PanelUIBuilder(SkillElementUIController.REUSE_PANEL_ID);
		reusePanel.setController(SkillReuseUIController.class);
		reusePanel.setLayoutType(ChildLayoutType.Center);
		reusePanel.setVisibleToMouse(false);
		reusePanel.setMarginBottom("1px");
		reusePanel.setValign(VAlign.Top);
		reusePanel.setChildClip(true);
		reusePanel.setWidth("34px");
		reusePanel.setHeight("34px");
		reusePanel.setVisible(false);
		reusePanel.add(reuseImage);
		return reusePanel;
	}

	public static Element build(Nifty nifty, Screen screen, Element parent, Element activator, EffectProperties properties) {

		SkillElementUIController controller = activator.getControl(SkillElementUIController.class);

		Skill skill = controller.getSkill();
		SkillTemplate template = skill.getTemplate();

		MessageElementUIBuilder name = new MessageElementUIBuilder(ItemDescriptionEffect.NAME_ID);
		name.setText(template.getName());
		name.setMarginBottom("2px");
		name.setMarginTop("2px");
		name.setWidth("200px");

		MessageElementUIBuilder description = new MessageElementUIBuilder(ItemDescriptionEffect.DESCRIPTION_ID);
		description.setText(template.getDescription());
		description.setMarginBottom("2px");
		description.setMarginTop("2px");
		description.setWidth("200px");

		PanelUIBuilder builder = new DynamicPanelUIBuilder(activator.getId());
		builder.setBackgroundColor(Color.randomColor());
		builder.setLayoutType(ChildLayoutType.Vertical);
		builder.setWidth("220px");
		builder.setHeight("20px");
		builder.setVisible(false);
		// builder.add(name);
		builder.add(description);

		Element element = builder.build(nifty, screen, parent);

		DynamicPanelUI panel = element.getControl(DynamicPanelUI.class);
		panel.updateSize(true, false);

		return element;
	}
}
