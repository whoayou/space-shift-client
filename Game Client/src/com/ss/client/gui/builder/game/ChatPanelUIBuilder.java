package com.ss.client.gui.builder.game;

import com.ss.client.gui.builder.SpaceLayerBuilder;
import com.ss.client.gui.controller.game.panel.chat.ChatPanelUIController;
import com.ss.client.gui.controller.game.panel.chat.ChatTabElementUI;
import com.ss.client.gui.controller.game.panel.chat.ChatTabElementUIBuilder;
import com.ss.client.gui.controller.game.panel.chat.ChatTabType;
import com.ss.client.gui.element.ScrollPanelUI.AutoScroll;
import com.ss.client.gui.element.builder.impl.DraggableElementUIBuilder;
import com.ss.client.gui.element.builder.impl.ImageUIBuilder;
import com.ss.client.gui.element.builder.impl.PanelUIBuilder;
import com.ss.client.gui.element.builder.impl.ScrollPanelUIBuilder;
import com.ss.client.gui.element.builder.impl.TextFieldUIBuilder;

import de.lessvoid.nifty.builder.ElementBuilder.ChildLayoutType;

/**
 * Реализация конструктора чата.
 * 
 * @author Ronn
 */
public class ChatPanelUIBuilder {

	public static void build(final SpaceLayerBuilder layer) {

		DraggableElementUIBuilder chatPanel = new DraggableElementUIBuilder(ChatPanelUIController.PANEL_ID);
		chatPanel.setController(ChatPanelUIController.class);
		chatPanel.setBackgroundImage(ChatPanelUIController.BACKGROUND_IMAGE);
		chatPanel.setWidth("357px");
		chatPanel.setHeight("198px");
		chatPanel.add(buildContent());
		chatPanel.buildTo(layer);
	}

	/**
	 * @return панель с содержанием чата.
	 */
	protected static PanelUIBuilder buildContent() {

		PanelUIBuilder tabPanel = new PanelUIBuilder(ChatPanelUIController.TAB_PANEL_ID);
		tabPanel.setLayoutType(ChildLayoutType.Horizontal);
		tabPanel.setHeight("20px");
		tabPanel.setWidth("340px");

		for(int i = 0, length = 4; i < length; i++) {
			buildTab(tabPanel, i, i != 0 && i < 4);
		}

		PanelUIBuilder splitPanel = new PanelUIBuilder(ChatPanelUIController.SPLIT_PANEL_ID);
		splitPanel.setBackgroundImage(ChatPanelUIController.PANEL_LINE_IMAGE);
		splitPanel.setWidth("294px");
		splitPanel.setHeight("1px");
		splitPanel.setMargin("2,0,2,0");

		PanelUIBuilder messagePanel = new PanelUIBuilder(ChatPanelUIController.MESSAGE_PANEL_ID);
		messagePanel.setWidth("334px");
		messagePanel.setHeight("*");

		TextFieldUIBuilder textField = new TextFieldUIBuilder(ChatPanelUIController.TEXT_FIELD_ID);
		textField.setFieldImage(ChatPanelUIController.INPUT_WINDOW_IMAGE);
		textField.setMaxLength(300);
		textField.setWidth("297px");
		textField.setHeight("22px");

		ImageUIBuilder sendButton = new ImageUIBuilder(ChatPanelUIController.SEND_BUTTON_ID);
		sendButton.setPath(ChatPanelUIController.SEND_BUTTON_IMAGE);
		sendButton.setVisibleToMouse(true);
		sendButton.setWidth("27px");
		sendButton.setHeight("27px");
		sendButton.setMarginLeft("7px");

		PanelUIBuilder inputPanel = new PanelUIBuilder(ChatPanelUIController.INPUT_PANEL_ID);
		inputPanel.setLayoutType(ChildLayoutType.Horizontal);
		inputPanel.setMarginBottom("7px");
		inputPanel.setMarginLeft("7px");
		inputPanel.setHeight("34px");
		inputPanel.setWidth("340px");
		inputPanel.add(textField);
		inputPanel.add(sendButton);

		PanelUIBuilder content = new PanelUIBuilder(ChatPanelUIController.CONTENT_ID);
		content.setLayoutType(ChildLayoutType.Vertical);
		content.setHeight("90%");
		content.setWidth("360px");
		content.add(tabPanel);
		content.add(splitPanel);
		content.add(messagePanel);
		content.add(inputPanel);
		return content;
	}

	public static final void buildTab(PanelUIBuilder parent, int order, boolean needSplit) {

		ChatTabType tabType = ChatTabType.valueOf(order);

		PanelUIBuilder messagePanel = new PanelUIBuilder(ChatTabElementUI.MESSAGE_CONTAINER_ID);
		messagePanel.setLayoutType(ChildLayoutType.Vertical);
		messagePanel.setWidth("100%");
		messagePanel.setHeight("1px");

		ScrollPanelUIBuilder messageView = new ScrollPanelUIBuilder(ChatTabElementUI.MESSAGE_VIEW_ID);
		messageView.setAutoScroll(AutoScroll.BOTTOM);
		messageView.add(messagePanel);

		ChatTabElementUIBuilder tab = new ChatTabElementUIBuilder(ChatTabElementUI.TAB_ID + order);
		tab.setController(ChatTabElementUI.class);
		tab.setLayoutType(ChildLayoutType.Center);
		tab.setText(tabType.getTabName());
		tab.setColor(ChatTabElementUI.NO_ACTIVE_COLOR);
		tab.setTabType(tabType);
		tab.setWidth("80px");
		tab.add(messageView);

		if(needSplit) {
			ImageUIBuilder split = new ImageUIBuilder(ChatTabElementUI.TAB_SPLIT_ID + order);
			split.setPath(ChatTabElementUI.TAB_SPLIT_IMAGE);
			split.setWidth("5px");
			split.setHeight("9px");
			parent.add(split);
		}

		parent.add(tab);
	}
}
