package com.ss.client.gui.builder.game.factory;

import java.util.concurrent.atomic.AtomicInteger;

import rlib.util.Strings;

import com.ss.client.gui.controller.game.panel.chat.ChatMessage;
import com.ss.client.gui.element.builder.impl.MessageElementUIBuilder;
import com.ss.client.gui.model.MessageType;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.screen.Screen;

/**
 * Фабрика элементов сообщения.
 * 
 * @author Ronn
 */
public class ChatElementUIFactory {

	public static final String MESSAGE_ID = "#message_";

	/**
	 * Создание элемента сообщения в списке сообщений чата.
	 * 
	 * @param message сообщение.
	 * @param parent контейнер сообщений.
	 * @param screen экран этого чата.
	 * @param nifty контрол UI.
	 * @param messageIndex счетчек индексов элементов.
	 * @return новый UI элемент.
	 */
	public static Element build(ChatMessage message, Element parent, Screen screen, Nifty nifty, AtomicInteger messageIndex) {

		MessageType messageType = message.getMessageType();

		String name = message.getName();
		String text = message.getMessage();

		StringBuilder result = new StringBuilder();

		if(messageType.isVisibleType()) {
			result.append("[").append(messageType.getTypeName()).append("]");
		}

		if(!Strings.isEmpty(name)) {
			result.append("[").append(name).append("]");
		}

		result.append(" : ").append(text);

		MessageElementUIBuilder builder = new MessageElementUIBuilder(MESSAGE_ID + messageIndex.incrementAndGet());
		builder.setColor(messageType.getColor());
		builder.setText(result.toString());
		builder.setMarginTop("1px");
		builder.setTextPaddingHeight(1);
		builder.setTextPaddingWidth(1);

		return builder.build(nifty, screen, parent);
	}
}
