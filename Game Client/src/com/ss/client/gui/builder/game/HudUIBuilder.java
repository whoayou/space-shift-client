package com.ss.client.gui.builder.game;

import com.ss.client.gui.builder.SpaceLayerBuilder;
import com.ss.client.gui.controller.game.hud.HudController;

import de.lessvoid.nifty.builder.ElementBuilder.ChildLayoutType;
import de.lessvoid.nifty.builder.ScreenBuilder;

/**
 * Конструктор слоя HUD.
 * 
 * @author Ronn
 */
public class HudUIBuilder {

	public static void build(ScreenBuilder screen) {

		SpaceLayerBuilder layer = new SpaceLayerBuilder(HudController.HUD_LAYER_ID);
		layer.controller(HudController.class.getName());
		layer.childLayout(ChildLayoutType.Center);

		IndicatorUIBuilder.build(layer);
		SelectorUIBuilder.build(layer);
		VectorUIBuilder.build(layer);
		AIMUIBuilder.build(layer);

		screen.layer(layer);
	}
}
