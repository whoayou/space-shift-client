package com.ss.client.gui.builder.game;

import com.ss.client.gui.builder.SpaceLayerBuilder;
import com.ss.client.gui.controller.game.panel.MainPanelController;
import com.ss.client.gui.element.builder.impl.ButtonUIBuilder;
import com.ss.client.gui.element.builder.impl.DraggableElementUIBuilder;

import de.lessvoid.nifty.builder.ElementBuilder.ChildLayoutType;
import de.lessvoid.nifty.tools.Color;

/**
 * Конструктор главной панели.
 * 
 * @author Ronn
 */
public class MainPanelUIBuilder {

	private static final String[] BUTTON_IDS = {
		MainPanelController.BUTTON_OPEN_WIDWOW_SKILL_ID,
		MainPanelController.BUTTON_OPEN_WIDWOW_STORAGE_ID,
		MainPanelController.BUTTON_OPEN_WIDWOW_SHIP_ID,
		MainPanelController.BUTTON_OPEN_WIDWOW_QUEST_ID,
	};

	private static final String[] BUTTON_NAMES = {
		"SKW",
		"INW",
		"SHW",
		"QST",
	};

	public static void build(final SpaceLayerBuilder layer) {

		DraggableElementUIBuilder mainPanel = new DraggableElementUIBuilder(MainPanelController.PANEL_ID);
		mainPanel.setController(MainPanelController.class);
		mainPanel.setBackgroundColor(Color.randomColor());
		mainPanel.setLayoutType(ChildLayoutType.Vertical);
		mainPanel.setWidth("40px");
		mainPanel.setHeight("200px");

		for(int i = 0, length = BUTTON_IDS.length; i < length; i++) {

			String id = BUTTON_IDS[i];

			ButtonUIBuilder button = new ButtonUIBuilder(id);
			button.setWidth("32px");
			button.setHeight("32px");
			button.setMarginTop("2px");
			button.setBackgroundColor(Color.randomColor());
			button.setText(BUTTON_NAMES[i]);

			mainPanel.add(button);
		}

		mainPanel.buildTo(layer);
	}
}
