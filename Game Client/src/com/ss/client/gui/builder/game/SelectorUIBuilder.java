package com.ss.client.gui.builder.game;

import static de.lessvoid.nifty.builder.ElementBuilder.ChildLayoutType.Center;
import rlib.util.Strings;

import com.ss.client.gui.builder.ElementFactory;
import com.ss.client.gui.builder.SpaceLayerBuilder;
import com.ss.client.gui.controller.game.hud.selector.ItemSelector;
import com.ss.client.gui.controller.game.hud.selector.ObjectSelector;
import com.ss.client.gui.controller.game.hud.selector.SelectorsController;
import com.ss.client.gui.controller.game.hud.selector.SpaceShipSelector;
import com.ss.client.gui.element.builder.impl.ImageUIBuilder;
import com.ss.client.gui.element.builder.impl.LabelUIBuilder;
import com.ss.client.gui.element.builder.impl.PanelUIBuilder;
import com.ss.client.model.SpaceObject;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.builder.ElementBuilder.Align;
import de.lessvoid.nifty.builder.ElementBuilder.ChildLayoutType;
import de.lessvoid.nifty.builder.ElementBuilder.VAlign;
import de.lessvoid.nifty.builder.PanelBuilder;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.tools.Color;

/**
 * Реализация билдера селекторов.
 * 
 * @author Ronn
 */
public class SelectorUIBuilder {

	private static final Color STRENGTH_PANEL_BACKGROUND_COLOR = new Color("#35c804");
	private static final Color ENERGY_PANEL_BACKGROUND_COLOR = new Color("#29abe2");

	public static void build(final SpaceLayerBuilder layer) {
		PanelUIBuilder builder = new PanelUIBuilder(SelectorsController.BASE_PANEL_ID);
		builder.setController(SelectorsController.class);
		builder.setLayoutType(ChildLayoutType.AbsoluteInside);
		builder.buildTo(layer);
	}

	public static ObjectSelector buildItemSelector(SpaceObject object, Nifty nifty, Screen screen, Element basePanel) {

		LabelUIBuilder nameLabel = new LabelUIBuilder(ObjectSelector.NAME_LABEL_ID);
		nameLabel.setWidth("200px");
		nameLabel.setHeight("17px");
		nameLabel.setAlign(Align.Left);
		nameLabel.setTextHAlign(Align.Left);

		LabelUIBuilder distLabel = new LabelUIBuilder(ObjectSelector.DIST_LABEL_ID);
		distLabel.setWidth("200px");
		distLabel.setHeight("17px");
		distLabel.setAlign(Align.Left);
		distLabel.setTextHAlign(Align.Left);

		ImageUIBuilder image = new ImageUIBuilder(ObjectSelector.SELECTOR_ICON_ID);
		image.setPath(ObjectSelector.SELECTOR_ICON);

		PanelUIBuilder infoPanel = new PanelUIBuilder(ObjectSelector.INFO_PANEL_ID);
		infoPanel.setLayoutType(ChildLayoutType.Vertical);
		infoPanel.setWidth("100px");
		infoPanel.setHeight("40px");
		infoPanel.setMarginLeft("85px");
		infoPanel.setValign(VAlign.Center);
		infoPanel.add(nameLabel);
		infoPanel.add(distLabel);

		PanelBuilder panel = ElementFactory.getPanelBuilder(ItemSelector.SELECTOR_ID, Center, Strings.EMPTY, "32px", "32px");
		panel.controller(ItemSelector.class.getName());

		image.buildTo(panel);
		infoPanel.buildTo(panel);

		Element element = panel.build(nifty, screen, basePanel);
		return element.getNiftyControl(ObjectSelector.class);
	}

	public static ObjectSelector buildSpaceShipSelector(SpaceObject object, Nifty nifty, Screen screen, Element basePanel) {

		LabelUIBuilder nameLabel = new LabelUIBuilder(ObjectSelector.NAME_LABEL_ID);
		nameLabel.setWidth("200px");
		nameLabel.setHeight("17px");
		nameLabel.setAlign(Align.Left);
		nameLabel.setTextHAlign(Align.Left);

		LabelUIBuilder distLabel = new LabelUIBuilder(ObjectSelector.DIST_LABEL_ID);
		distLabel.setWidth("200px");
		distLabel.setHeight("17px");
		distLabel.setAlign(Align.Left);
		distLabel.setTextHAlign(Align.Left);

		ImageUIBuilder image = new ImageUIBuilder(ObjectSelector.SELECTOR_ICON_ID);
		image.setPath(ObjectSelector.SELECTOR_ICON);

		PanelUIBuilder energyPanel = new PanelUIBuilder(SpaceShipSelector.ENERGY_INDICATOR_ID);
		energyPanel.setAlign(Align.Left);
		energyPanel.setLayoutType(ChildLayoutType.Horizontal);
		energyPanel.setBackgroundColor(ENERGY_PANEL_BACKGROUND_COLOR);
		energyPanel.setMarginTop("2px");
		energyPanel.setWidth("100%");
		energyPanel.setHeight("6px");

		PanelUIBuilder strengthPanel = new PanelUIBuilder(SpaceShipSelector.STRENGTH_INDICATOR_ID);
		strengthPanel.setAlign(Align.Left);
		strengthPanel.setLayoutType(ChildLayoutType.Horizontal);
		strengthPanel.setBackgroundColor(STRENGTH_PANEL_BACKGROUND_COLOR);
		strengthPanel.setMarginTop("2px");
		strengthPanel.setWidth("100%");
		strengthPanel.setHeight("6px");

		PanelUIBuilder infoPanel = new PanelUIBuilder(ObjectSelector.INFO_PANEL_ID);
		infoPanel.setLayoutType(ChildLayoutType.Vertical);
		infoPanel.setWidth("100px");
		infoPanel.setHeight("54px");
		infoPanel.setMarginLeft("85px");
		infoPanel.setValign(VAlign.Center);
		infoPanel.add(nameLabel);
		infoPanel.add(energyPanel);
		infoPanel.add(strengthPanel);
		infoPanel.add(distLabel);

		PanelBuilder panel = ElementFactory.getPanelBuilder(SelectorsController.SELECTOR_ID, Center, Strings.EMPTY, "32px", "32px");
		panel.controller(SpaceShipSelector.class.getName());

		image.buildTo(panel);
		infoPanel.buildTo(panel);

		Element element = panel.build(nifty, screen, basePanel);
		return element.getNiftyControl(ObjectSelector.class);
	}
}
