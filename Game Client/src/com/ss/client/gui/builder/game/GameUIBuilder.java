package com.ss.client.gui.builder.game;

import com.ss.client.gui.ElementId;
import com.ss.client.gui.ImageId;
import com.ss.client.gui.builder.ElementFactory;
import com.ss.client.gui.builder.SpaceLayerBuilder;
import com.ss.client.gui.controller.game.GameUIController;
import com.ss.client.gui.model.KeySetting;
import com.ss.client.model.SpaceObjectView;
import com.ss.client.table.LangTable;

import de.lessvoid.nifty.builder.ImageBuilder;
import de.lessvoid.nifty.builder.LayerBuilder;
import de.lessvoid.nifty.builder.ScreenBuilder;
import de.lessvoid.nifty.controls.window.builder.WindowBuilder;

/**
 * Конструктор игрового интерфейса.
 * 
 * @author Ronn
 */
public class GameUIBuilder extends ScreenBuilder {

	private static final LangTable LANG_TABLE = LangTable.getInstance();

	private static final String INTERFACE_GAME_ANGAR_INDICATOR = LANG_TABLE.getText("@interface:gameAngarIndicator@");

	public static final String DEFAULT_PANEL_STYLE = "nifty-panel";
	public static final String SIMPLE_PANEL_STYLE = "nifty-panel-simple";

	public static final int DEFAULT_CHAT_WIDTH = 400;
	public static final int DEFAULT_CHAT_HEIGHT = 205;

	public static final int FAST_PANEL_ROWS = 2;
	public static final int FAST_PANEL_COLUMNS = 6;

	public GameUIBuilder() {
		super(ElementId.GAME_SCREEN.getId());

		final LayerBuilder layer = new LayerBuilder(SpaceObjectView.NAME_OBJECT_LAYER);
		layer.childLayoutAbsolute();
		layer(layer);

		controller(GameUIController.getInstance());

		// FIXME переделать нафиг
		buildHangarIndicator();

		// билд HUD
		HudUIBuilder.build(this);

		final SpaceLayerBuilder panelLayer = new SpaceLayerBuilder(ElementId.GAME_PANEL_LAYER);
		panelLayer.childLayoutAbsoluteInside();
		layer(panelLayer);

		ChatPanelUIBuilder.build(panelLayer);
		FastPanelUIBuilder.build(panelLayer);
		MainPanelUIBuilder.build(panelLayer);
		RadarPanelUIBuilder.build(panelLayer);
		ShipStatusUIBuilder.build(panelLayer);

		final SpaceLayerBuilder windowLayer = new SpaceLayerBuilder(ElementId.GAME_WINDOW_LAYER);
		windowLayer.childLayoutAbsoluteInside();
		layer(windowLayer);

		SkillWindowUIBuilder.build(windowLayer);
		StorageWindowUIBuilder.build(windowLayer);
		ShipWindowUIBuilder.build(windowLayer);
		QuestWindowUIBuilder.build(windowLayer);
	}

	/**
	 * Формирование индикатора отображения возможности взаимодействия с ангаром.
	 */
	private void buildHangarIndicator() {
		final LayerBuilder layer = new LayerBuilder(ElementId.GAME_HANGAR_INDICATOR_LAYER.getId());
		layer.childLayoutAbsoluteInside();
		layer(layer);

		final WindowBuilder window = ElementFactory.getWindow(ElementId.GAME_WINDOW_ANGAR_INDICATOR, "w", "50px", "50px", false);
		window.childLayoutCenter();

		layer.control(window);

		final ImageBuilder image = new ImageBuilder(ElementId.GAME_HANGAR_INDICATOR_IMAGE.getId());
		image.filename(ImageId.GAME_ANGAR_INDICATOR.getPath());
		image.interactOnClick("openHangarDialog()");
		image.focusable(false);
		image.width("48px");
		image.height("48px");

		ElementFactory.addHintTo(image, INTERFACE_GAME_ANGAR_INDICATOR.replace("%keyname%", KeySetting.OPEN_DIALOG.getKeyName()));

		ElementFactory.addShowAnimation(window);

		window.image(image);
	}
}
