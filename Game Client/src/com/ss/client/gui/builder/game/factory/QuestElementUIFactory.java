package com.ss.client.gui.builder.game.factory;

import com.ss.client.gui.controller.game.quest.QuestCounterElementController;
import com.ss.client.gui.controller.game.quest.reward.QuestRewardElementController;
import com.ss.client.gui.controller.game.quest.reward.impl.ExperienceQuestRewardElementController;
import com.ss.client.gui.controller.game.quest.reward.impl.ItemQuestRewardController;
import com.ss.client.gui.element.builder.impl.ImageUIBuilder;
import com.ss.client.gui.element.builder.impl.LabelUIBuilder;
import com.ss.client.gui.element.builder.impl.MessageElementUIBuilder;
import com.ss.client.gui.element.builder.impl.PanelUIBuilder;
import com.ss.client.gui.model.quest.QuestCounter;
import com.ss.client.gui.model.quest.reward.impl.ExperienceQuestReward;
import com.ss.client.gui.model.quest.reward.impl.ItemQuestReward;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.builder.ElementBuilder.Align;
import de.lessvoid.nifty.builder.ElementBuilder.ChildLayoutType;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.tools.Color;

/**
 * Фабрика элементов наград заданий.
 * 
 * @author Ronn
 */
public final class QuestElementUIFactory {

	public static final Element build(ExperienceQuestReward reward, Element parent, Nifty nifty, Screen screen, long exp, int order) {

		LabelUIBuilder name = new LabelUIBuilder(QuestRewardElementController.QUEST_REWARD_NAME_ID);
		name.setTextHAlign(Align.Left);
		name.setAlign(Align.Left);
		name.setMarginLeft("5px");

		ImageUIBuilder icon = new ImageUIBuilder(QuestRewardElementController.QUEST_REWARD_ICON_ID);
		icon.setWidth("32px");
		icon.setHeight("32px");
		icon.setMarginRight("5px");
		icon.setAlign(Align.Right);

		PanelUIBuilder builder = new PanelUIBuilder(QuestRewardElementController.ID + order);
		builder.setController(ExperienceQuestRewardElementController.class);
		builder.setBackgroundColor(Color.randomColor());
		builder.setHeight("36px");
		builder.setWidth("80%");
		builder.setMargin("5,0,5,0");
		builder.add(name);
		builder.add(icon);

		Element element = builder.build(nifty, screen, parent);

		QuestRewardElementController controller = element.getControl(QuestRewardElementController.class);
		controller.setQuestReward(reward);

		return element;
	}

	public static final Element build(ItemQuestReward reward, Element parent, Nifty nifty, Screen screen, int id, long count, int order) {

		LabelUIBuilder name = new LabelUIBuilder(QuestRewardElementController.QUEST_REWARD_NAME_ID);
		name.setTextHAlign(Align.Left);
		name.setAlign(Align.Left);
		name.setMarginLeft("5px");

		ImageUIBuilder icon = new ImageUIBuilder(QuestRewardElementController.QUEST_REWARD_ICON_ID);
		icon.setWidth("32px");
		icon.setHeight("32px");
		icon.setMarginRight("5px");
		icon.setAlign(Align.Right);

		PanelUIBuilder builder = new PanelUIBuilder(QuestRewardElementController.ID + order);
		builder.setController(ItemQuestRewardController.class);
		builder.setBackgroundColor(Color.randomColor());
		builder.setHeight("36px");
		builder.setWidth("80%");
		builder.setMargin("5,0,5,0");
		builder.add(name);
		builder.add(icon);

		Element element = builder.build(nifty, screen, parent);

		QuestRewardElementController controller = element.getControl(QuestRewardElementController.class);
		controller.setQuestReward(reward);

		return element;
	}

	/**
	 * Построение элемента счетчика.
	 * 
	 * @param counter счетчик задания.
	 * @param parent родительский элемент.
	 * @param nifty контроллер UI.
	 * @param screen экран UI.
	 * @param order порядок счетчика.
	 * @return элемент отображающий счетчик.
	 */
	public static final Element build(QuestCounter counter, Element parent, Nifty nifty, Screen screen, int order) {

		MessageElementUIBuilder description = new MessageElementUIBuilder(QuestCounterElementController.QUEST_COUNTER_DESCRIPTION_ID);
		description.setBackgroundColor(Color.randomColor());
		description.setMarginLeft("5px");
		description.setWidth("280px");
		description.setTextHAlign(Align.Left);

		LabelUIBuilder status = new LabelUIBuilder(QuestCounterElementController.QUEST_COUNTER_STATUS_ID);
		status.setMarginRight("5px");
		status.setWidth("80px");
		status.setTextHAlign(Align.Right);

		PanelUIBuilder builder = new PanelUIBuilder(QuestCounterElementController.ID + order);
		builder.setController(QuestCounterElementController.class);
		builder.setBackgroundColor(Color.randomColor());
		builder.setLayoutType(ChildLayoutType.Horizontal);
		builder.setHeight("20px");
		builder.setWidth("360px");
		builder.setMargin("5,0,5,0");
		builder.add(description);
		builder.add(status);

		Element element = builder.build(nifty, screen, parent);

		QuestCounterElementController controller = element.getControl(QuestCounterElementController.class);
		controller.setCounter(counter);
		controller.refresh();

		return element;
	}

	private QuestElementUIFactory() {
		throw new RuntimeException();
	}
}
