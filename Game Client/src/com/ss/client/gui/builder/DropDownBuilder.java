package com.ss.client.gui.builder;

import com.ss.client.gui.control.DropDownControl;

/**
 * Реализация своего билдера выпадающего списка.
 * 
 * @author Ronn
 */
public class DropDownBuilder extends de.lessvoid.nifty.controls.dropdown.builder.DropDownBuilder {

	public DropDownBuilder(String id) {
		super(id);
		controller(DropDownControl.class.getName());
		set(DropDownControl.ATTRIBUTE_POPUP_CONTROL_ID, "dropDownBoxSelectPopup");
	}
}
