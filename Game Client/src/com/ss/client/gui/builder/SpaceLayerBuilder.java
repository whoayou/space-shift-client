package com.ss.client.gui.builder;

import com.ss.client.gui.ElementId;

import de.lessvoid.nifty.builder.LayerBuilder;

/**
 * @author Ronn
 */
public class SpaceLayerBuilder extends LayerBuilder {

	public SpaceLayerBuilder(final ElementId elementId) {
		super(elementId.getId());
	}

	public SpaceLayerBuilder(final String id) {
		super(id);
	}
}
