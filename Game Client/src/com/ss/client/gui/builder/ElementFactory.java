package com.ss.client.gui.builder;

import java.util.List;

import com.ss.client.gui.ElementId;

import rlib.util.Strings;
import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.builder.EffectBuilder;
import de.lessvoid.nifty.builder.ElementBuilder;
import de.lessvoid.nifty.builder.ElementBuilder.Align;
import de.lessvoid.nifty.builder.ElementBuilder.ChildLayoutType;
import de.lessvoid.nifty.builder.ElementBuilder.VAlign;
import de.lessvoid.nifty.builder.HoverEffectBuilder;
import de.lessvoid.nifty.builder.PanelBuilder;
import de.lessvoid.nifty.controls.button.builder.ButtonBuilder;
import de.lessvoid.nifty.controls.dropdown.builder.DropDownBuilder;
import de.lessvoid.nifty.controls.label.builder.LabelBuilder;
import de.lessvoid.nifty.controls.listbox.builder.ListBoxBuilder;
import de.lessvoid.nifty.controls.textfield.builder.TextFieldBuilder;
import de.lessvoid.nifty.controls.window.builder.WindowBuilder;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.screen.Screen;

/**
 * Набор методов для созданий билдеров элементов интерфейса.
 * 
 * @author Ronn
 */
public class ElementFactory {

	public static final String DEFAULT_TEXT_FIELD_WIDTH = "200px";
	public static final String DEFAULT_TEXT_FIELD_HEIGHT = "30px";
	public static final String DEFAULT_TEXT_FIELD_STYLE = "nifty-textfield";
	public static final String DEFAULT_PANEL_STYLE = "nifty-panel";
	public static final String DEFAULT_LABEL_STYLE = "nifty-label";
	public static final String DEFAULT_BUTTON_STYLE = "nifty-button";
	public static final String DEFAULT_DROP_DOWN_STYLE = "nifty-drop-down";
	public static final String DEFAULT_LIST_BOX_STYLE = "nifty-listbox";
	public static final String DEFAULT_WINDOW_STYLE = "nifty-window";

	/**
	 * Добавление подсказки указанному элементу.
	 * 
	 * @param builder конструктор элемента.
	 * @param text текст подсказки.
	 */
	public static final void addHintTo(final ElementBuilder builder, final String text) {

		final HoverEffectBuilder effect = new HoverEffectBuilder("hint");
		effect.effectParameter("hintText", text);

		builder.onHoverEffect(effect);
	}

	public static final void addShowAnimation(final ElementBuilder builder) {

		EffectBuilder effect = new EffectBuilder("imageSize");
		effect.effectParameter("timeType", "exp");
		effect.effectParameter("factor", "3.5");
		effect.effectParameter("startSize", "1.5");
		effect.effectParameter("endSize", "1.0");
		effect.effectParameter("inherit", "true");
		effect.effectParameter("length", "150");
		effect.effectParameter("startDelay", "0");

		builder.onShowEffect(effect);

		effect = new EffectBuilder("imageSize");
		effect.effectParameter("timeType", "exp");
		effect.effectParameter("factor", "3.5");
		effect.effectParameter("startSize", "1.0");
		effect.effectParameter("endSize", "1.5");
		effect.effectParameter("inherit", "true");
		effect.effectParameter("length", "150");
		effect.effectParameter("startDelay", "0");

		builder.onHideEffect(effect);

		final List<ElementBuilder> builders = builder.getElementBuilders();

		for(final ElementBuilder child : builders)
			addShowAnimation(child);
	}

	public static Element createQuestBookButton(final int id, final String name, String hint, final Element container, final Nifty nifty, final Screen screen) {

		final ButtonBuilder button = getButtonBuild(String.valueOf(id), name);
		button.width("70%");
		button.alignCenter();
		button.valignCenter();
		button.interactOnClick("press(" + id + ")");
		button.focusable(false);
		button.marginBottom("4px");

		if(hint == null)
			hint = Strings.EMPTY;

		addHintTo(button, hint);

		return button.build(nifty, screen, container);
	}

	public static Element createQuestDialogButton(final int id, final String name, final Element container, final Nifty nifty, final Screen screen) {

		final ButtonBuilder button = getButtonBuild(String.valueOf(id), name);
		button.width("70%");
		button.alignCenter();
		button.valignCenter();
		button.interactOnClick("press(" + id + ")");
		button.focusable(false);

		return button.build(nifty, screen, container);
	}

	/**
	 * Создание билдера кнопки.
	 * 
	 * @param id ид кнопки.
	 * @param text текст на кнопке.
	 * @return новый билдер кнопки.
	 */
	public static ButtonBuilder getButtonBuild(final ElementId id, final String text) {
		return getButtonBuild(id.getId(), text, DEFAULT_BUTTON_STYLE, "100px", "30px");
	}

	/**
	 * Создание билдера кнопки.
	 * 
	 * @param id ид кнопки.
	 * @param text текст на кнопке.
	 * @param width ширина кнопки.
	 * @param height высота кнопки.
	 * @return новый билдер кнопки.
	 */
	public static ButtonBuilder getButtonBuild(final ElementId id, final String text, final String width, final String height) {
		return getButtonBuild(id.getId(), text, DEFAULT_BUTTON_STYLE, width, height);
	}

	/**
	 * Создание билдера кнопки.
	 * 
	 * @param id ид кнопки.
	 * @param text текст на кнопке.
	 * @return новый билдер кнопки.
	 */
	public static ButtonBuilder getButtonBuild(final String id, final String text) {
		return getButtonBuild(id, text, DEFAULT_BUTTON_STYLE, "100px", "30px");
	}

	/**
	 * Создание билдера кнопки.
	 * 
	 * @param id ид кнопки.
	 * @param text текст на кнопке.
	 * @param style стиль кнопки.
	 * @param width ширина кнопки.
	 * @param height высота кнопки.
	 * @return новый билдер кнопки.
	 */
	public static ButtonBuilder getButtonBuild(final String id, final String text, final String style, final String width, final String height) {
		final ButtonBuilder button = new ButtonBuilder(id, text);
		button.height(height);
		button.width(width);
		button.style(style);

		return button;
	}

	/**
	 * Создание билдера выпадающего списка.
	 * 
	 * @param id ид элемента.
	 * @param width ширина элемента.
	 * @param height высота элемента.
	 * @return новый билдер.
	 */
	public static DropDownBuilder getDropDownBuilder(final ElementId id, final String width, final String height) {
		return getDropDownBuilder(id.getId(), DEFAULT_DROP_DOWN_STYLE, width, height);
	}

	/**
	 * Создание билдера выпадающего списка.
	 * 
	 * @param id ид элемента.
	 * @param width ширина элемента.
	 * @param height высота элемента.
	 * @return новый билдер.
	 */
	public static DropDownBuilder getDropDownBuilder(final String id, final String width, final String height) {
		return getDropDownBuilder(id, DEFAULT_DROP_DOWN_STYLE, width, height);
	}

	/**
	 * Создание билдера выпадающего списка.
	 * 
	 * @param id ид элемента.
	 * @param style стиль элемента.
	 * @param width ширина элемента.
	 * @param height высота элемента.
	 * @return новый билдер.
	 */
	public static DropDownBuilder getDropDownBuilder(final String id, final String style, final String width, final String height) {

		final DropDownBuilder dropDown = new DropDownBuilder(id);

		if(style != null) {
			dropDown.style(style);
		}

		if(width != null) {
			dropDown.width(width);
		}

		if(height != null) {
			dropDown.height(height);
		}

		return dropDown;
	}

	/**
	 * Получение билдера пустой панели.
	 * 
	 * @param width ширина панели.
	 * @param height высота панели.
	 * @return билдер пустой панели.
	 */
	public static PanelBuilder getEmptyPanel(int width, int height) {
		return getPanelBuilder("#empty_panel", ChildLayoutType.Center, Strings.EMPTY, width + "px", height + "px");
	}

	/**
	 * Создание билдера надписи.
	 * 
	 * @param id ид надписи.
	 * @param text текст надписи.
	 * @return новый билдер.
	 */
	public static LabelBuilder getLabelBuilder(final ElementId id, final String text) {
		return getLabelBuilder(id.getId(), text, null, null, Align.Center, VAlign.Center);
	}

	/**
	 * Создание билдера надписи.
	 * 
	 * @param id ид надписи.
	 * @param text текст надписи.
	 * @param width ширина.
	 * @param height высота.
	 * @return новый билдер.
	 */
	public static LabelBuilder getLabelBuilder(final ElementId id, final String text, final String width, final String height) {
		return getLabelBuilder(id.getId(), text, width, height, Align.Center, VAlign.Center);
	}

	/**
	 * Создание билдера надписи.
	 * 
	 * @param id ид надписи.
	 * @param text текст надписи.
	 * @return новый билдер.
	 */
	public static LabelBuilder getLabelBuilder(final String id, final String text) {
		return getLabelBuilder(id, text, null, null, Align.Center, VAlign.Center);
	}

	/**
	 * Создание билдера надписи.
	 * 
	 * @param id ид надписи.
	 * @param text текст надписи.
	 * @param width ширина надписи.
	 * @param heigth высота надписи.
	 * @return новый билдер.
	 */
	public static LabelBuilder getLabelBuilder(final String id, final String text, final int width, final int heigth) {
		return getLabelBuilder(id, text, width + "px", heigth + "px", Align.Center, VAlign.Center);
	}

	public static LabelBuilder getLabelBuilder(final String id, final String text, final String width, final String height) {
		return getLabelBuilder(id, text, width, height, Align.Center, VAlign.Center);
	}

	/**
	 * Создание билдера надписи.
	 * 
	 * @param id ид надписи.
	 * @param text текст надписи.
	 * @param hAlign горизонтальная центровка текста.
	 * @param vAlign вертикальная центровка текста.
	 * @return новый билдер.
	 */
	public static LabelBuilder getLabelBuilder(final String id, final String text, final String width, final String height, final Align hAlign, final VAlign vAlign) {

		final LabelBuilder label = new LabelBuilder(id, text);
		label.textHAlign(hAlign);
		label.textVAlign(vAlign);
		label.style(DEFAULT_LABEL_STYLE);

		if(width != null)
			label.width(width);

		if(height != null)
			label.height(height);

		return label;
	}

	/**
	 * Создание билдера лист бокса.
	 * 
	 * @param id ид лист бокса.
	 * @param height высота.
	 * @param width ширина.
	 * @return новый билдер.
	 */
	public static ListBoxBuilder getListBoxBuilder(final ElementId id, final String height, final String width) {
		return getListBoxBuilder(id.getId(), DEFAULT_LIST_BOX_STYLE, height, width);
	}

	/**
	 * Создание билдера лист бокса.
	 * 
	 * @param id ид лист бокса.
	 * @param style стиль.
	 * @param height высота.
	 * @param width ширина.
	 * @return новый билдер.
	 */
	public static ListBoxBuilder getListBoxBuilder(final String id, final String style, final String height, final String width) {

		final ListBoxBuilder listBox = new ListBoxBuilder(id);
		listBox.width(width);
		listBox.height(height);
		listBox.style(style);

		return listBox;
	}

	/**
	 * Создание билдера для панели.
	 * 
	 * @param id ид панели.
	 * @param childLayout тип размещения элементовв на панели.
	 * @param width ширина в пикселях панели.
	 * @param height высота в пикселях панели.
	 * @return билдер панели.
	 */
	public static PanelBuilder getPanelBuilder(final ElementId id, final ChildLayoutType childLayout, final int width, final int height) {
		return getPanelBuilder(id.getId(), childLayout, DEFAULT_PANEL_STYLE, width + "px", height + "px");
	}

	/**
	 * Создание билдера для панели.
	 * 
	 * @param id ид панели.
	 * @param childLayout тип размещения элементовв на панели.
	 * @param width ширина панели.
	 * @param height высота.
	 * @return билдер с панели.
	 */
	public static PanelBuilder getPanelBuilder(final ElementId id, final ChildLayoutType childLayout, final String style, final String width, final String height) {
		return getPanelBuilder(id.getId(), childLayout, style, width, height);
	}

	/**
	 * Создание билдера для панели.
	 * 
	 * @param id ид панели.
	 * @param childLayout тип размещения элементовв на панели.
	 * @param width ширина панели.
	 * @param height высота.
	 * @return билдер с панели.
	 */
	public static PanelBuilder getPanelBuilder(final String id, final ChildLayoutType childLayout, final String width, final String height) {
		return getPanelBuilder(id, childLayout, DEFAULT_PANEL_STYLE, width, height);
	}

	/**
	 * Создание билдера для панели.
	 * 
	 * @param id ид панели.
	 * @param childLayout тип размещения элементовв на панели.
	 * @param style стиль панели.
	 * @param width ширина панели.
	 * @param height высота.
	 * @return билдер с панели.
	 */
	public static PanelBuilder getPanelBuilder(final String id, final ChildLayoutType childLayout, final String style, final String width, final String height) {

		final PanelBuilder panel = new PanelBuilder(id);
		panel.childLayout(childLayout);
		panel.style(style);
		panel.width(width);
		panel.height(height);

		return panel;
	}

	/**
	 * Создание билдера для панели.
	 * 
	 * @param id ид панели.
	 * @param width ширина в пикселях панели.
	 * @param height высота в пикселях панели.
	 * @return билдер панели.
	 */
	public static PanelBuilder getPanelBuilder(final String id, final int width, final int height) {
		return getPanelBuilder(id, ChildLayoutType.Center, DEFAULT_PANEL_STYLE, width + "px", height + "px");
	}

	/**
	 * Создание билдера для панели.
	 * 
	 * @param id ид панели.
	 * @param style стиль панели.
	 * @param width ширина в пикселях панели.
	 * @param height высота в пикселях панели.
	 * @return билдер панели.
	 */
	public static PanelBuilder getPanelBuilder(final String id, final String style, final int width, final int height) {
		return getPanelBuilder(id, ChildLayoutType.Center, style, width + "px", height + "px");
	}

	/**
	 * Создание билдера для поля ввода.
	 * 
	 * @param id ид поля.
	 * @return новый билдер.
	 */
	public static TextFieldBuilder getTextFieldBuilder(final ElementId id) {
		return getTextFieldBuilder(id.getId(), DEFAULT_TEXT_FIELD_WIDTH, DEFAULT_TEXT_FIELD_HEIGHT, DEFAULT_TEXT_FIELD_STYLE, Align.Center);
	}

	/**
	 * Создание билдера для поля ввода.
	 * 
	 * @param id ид поля.
	 * @return новый билдер.
	 */
	public static TextFieldBuilder getTextFieldBuilder(final String id) {
		return getTextFieldBuilder(id, DEFAULT_TEXT_FIELD_WIDTH, DEFAULT_TEXT_FIELD_HEIGHT, DEFAULT_TEXT_FIELD_STYLE, Align.Center);
	}

	/**
	 * Создание билдера поля для ввода.
	 * 
	 * @param id ид элемента.
	 * @param width ширина.
	 * @param height высота.
	 * @param style стиль элемента.
	 * @param align центровка текста.
	 * @return новый билдер.
	 */
	public static TextFieldBuilder getTextFieldBuilder(final String id, final String width, final String height, final String style, final Align align) {

		final TextFieldBuilder textField = new TextFieldBuilder(id);
		textField.width(width);
		textField.height(height);
		textField.style(style);
		textField.align(align);

		return textField;
	}

	public static WindowBuilder getWindow(final ElementId id, final String text, final String width, final String height, final boolean closeable) {
		return getWindow(id.getId(), text, DEFAULT_WINDOW_STYLE, width, height, closeable);
	}

	public static WindowBuilder getWindow(final String id, final String text, final String width, final String height, final boolean closeable) {
		return getWindow(id, text, DEFAULT_WINDOW_STYLE, width, height, closeable);
	}

	public static WindowBuilder getWindow(final String id, final String text, final String style, final String width, final String height, final boolean closeable) {

		final WindowBuilder window = new WindowBuilder(id, text);
		window.width(width);
		window.height(height);
		window.closeable(closeable);
		window.set("hideOnClose", "true");

		if(!closeable)
			window.set("name", "window-no-close");

		return window;
	}
}
