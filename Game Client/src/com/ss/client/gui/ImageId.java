package com.ss.client.gui;

import com.ss.client.GameConfig;
import com.ss.client.gui.model.ScreenSize;

/**
 * Перечисление используемых изображений в игре.
 * 
 * @author Ronn
 */
public enum ImageId {

	/** элементы логмн экрана */
	LOGIN_PAGE_BACKGROUND("login_page/login_background%size%.png"),
	LOGIN_PAGE_LOGO("login_page/login_logo.png"),
	LOGIN_PAGE_ACTIONS_PANEL("login_page/login_actions_panel.png"),
	LOGIN_PAGE_BUTTON_PANEL("login_page/button_panel.png"),
	LOGIN_PAGE_BUTTON_REGISTER("login_page/login_button_register.png"),
	LOGIN_PAGE_BUTTON_REGISTER_SPLIT("login_page/login_button_register_split.png"),
	LOGIN_PAGE_BUTTON_CONFIG("login_page/login_button_config.png"),
	LOGIN_PAGE_BUTTON_CONFIG_SPLIT("login_page/login_button_config_split.png"),
	LOGIN_PAGE_BUTTON_AUTHORS("login_page/login_button_authors.png"),

	LOGIN_PAGE_QUARDS("login_page/quards.png"),
	LOGIN_PAGE_LOGIN_QUARDS("login_page/login_quards.png"),
	LOGIN_PAGE_PASSWORD_QUARDS("login_page/password_quards.png"),
	LOGIN_PAGE_REGISTER_QUARDS("/login_page/register_quards.png"),
	LOGIN_PAGE_SETTING_QUARDS("/login_page/setting_quards.png"),
	LOGIN_PAGE_AUTHORS_QUARDS("/login_page/authors_quards.png"),

	LOGIN_CONNECTED("interface/login_connected.png"),
	LOGIN_DISCONNECTED("interface/login_disconnected.png"),

	GAME_MAP_LOCATION("interface/location_map.png"),
	GAME_MAIN_EXIT("interface/main_exit.png"),
	GAME_MAIN_MODULE_LIST("interface/main_module_list.png"),
	GAME_MAIN_SHIP_INFO("interface/main_ship_info.png"),
	GAME_SPACE_VECTOR("interface/space_vector.png"),
	GAME_SPACE_AIM("interface/space_aim.png"),
	GAME_ANGAR_INDICATOR("interface/angar_indicator.png"),

	GAME_MAP_ICON_SPACE_PLANET("interface/map_icons/space_planet.png"),
	GAME_MAP_ICON_SPACE_STAR("interface/map_icons/space_star.png"),
	GAME_MAP_ICON_SPACE_STATION("interface/map_icons/space_station.png"),
	GAME_MAP_ICON_UNKNOWN("interface/map_icons/unknown.png"),

	;

	public static String getLoginBackground() {

		Class<ImageId> cs = ImageId.class;

		ScreenSize screenSize = GameConfig.SCREEN_SIZE;

		String path = LOGIN_PAGE_BACKGROUND.getPath();
		path = path.replace("%size%", "_" + screenSize.toString());

		if(cs.getResourceAsStream("/" + path) != null) {
			return path;
		}

		path = LOGIN_PAGE_BACKGROUND.getPath();
		path = path.replace("%size%", "_default");

		if(cs.getResourceAsStream("/" + path) != null) {
			return path;
		}

		throw new RuntimeException("not found login background image.");
	}

	/** путь к изображению */
	private String path;

	/**
	 * @param path путь к изображению.
	 */
	private ImageId(final String path) {
		this.path = path;
	}

	/**
	 * @return путь к изображению.
	 */
	public String getPath() {
		return path;
	}
}
