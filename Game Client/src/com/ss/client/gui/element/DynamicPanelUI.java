package com.ss.client.gui.element;

/**
 * Интерфейс для реализации панели с динамическим размером.
 * 
 * @author Ronn
 */
public interface DynamicPanelUI extends PanelUI {

	/**
	 * Обновить размер панели под размеры дочерних элементов.
	 */
	public void updateSize(boolean updateHeight, boolean updateWidth);
}
