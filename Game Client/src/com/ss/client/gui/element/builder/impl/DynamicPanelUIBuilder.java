package com.ss.client.gui.element.builder.impl;

import com.ss.client.gui.element.impl.DynamicPanelUIImpl;

/**
 * Реализация конструкта панели с динамическим размером.
 * 
 * @author Ronn
 */
public class DynamicPanelUIBuilder extends PanelUIBuilder {

	public DynamicPanelUIBuilder(String id) {
		super(id);
		setController(DynamicPanelUIImpl.class);
	}
}
