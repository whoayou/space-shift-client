package com.ss.client.gui.element.builder;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.builder.ElementBuilder;
import de.lessvoid.nifty.effects.EffectEventId;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.screen.Screen;

/**
 * Интерфейс для реализации билдера UI элемента.
 * 
 * @author Ronn
 */
public interface ElementUIBuilder {

	/**
	 * Добавление билдера дочернего элемента.
	 * 
	 * @param builder билдер.
	 */
	public void add(ElementUIBuilder builder);

	/**
	 * Добавление эффекта к итоговому элементу.
	 * 
	 * @param eventId событие запуска эффекта.
	 * @param builder конструктор эффекта.
	 */
	public void addEffect(EffectEventId eventId, Object builder);

	/**
	 * Построить контрол на указанном элементе.
	 * 
	 * @param nifty главный контрол UI.
	 * @param screen текущий экран UI.
	 * @param parent родительский элемент контрола.
	 * @return элемент контрола.
	 */
	public Element build(final Nifty nifty, final Screen screen, final Element parent);

	/**
	 * Построить контрол на указанном элементе.
	 * 
	 * @param builder билдер родительского элемента.
	 */
	public void buildTo(ElementBuilder builder);

	/**
	 * Формирование стандартного билдера этого контрола.
	 * 
	 * @return стандартный билдер.
	 */
	public ElementBuilder prepareBuilder();
}
