package com.ss.client.gui.element.builder.impl;

import com.ss.client.gui.element.TextFieldUI;
import com.ss.client.gui.element.impl.TextFieldUIImpl;
import com.ss.client.gui.element.util.ElementEffectUtils;

import de.lessvoid.nifty.builder.ElementBuilder.Align;
import de.lessvoid.nifty.builder.ElementBuilder.ChildLayoutType;
import de.lessvoid.nifty.builder.PanelBuilder;
import de.lessvoid.nifty.controls.textfield.TextFieldInputMapping;
import de.lessvoid.nifty.tools.Color;

/**
 * Реализация конструктора поля для ввода.
 * 
 * @author Ronn
 */
public class TextFieldUIBuilder extends AbstractElementUIBuilder<PanelBuilder> {

	protected static final String CURSOR_EMPTY_IMAGE = "general/text_field/cursor-empty.png";
	protected static final String DEFAULT_CURSOR_IMAGE = "general/text_field/cursor.png";

	/** путь к картинке куврсора */
	private String cursorImage;
	/** путь к пустой картинки курсора */
	private String cursorImageEmpty;
	/** путь к картинке поля ввода */
	private String fieldImage;

	/** текст надписи */
	private String text;
	/** шрифт надписи */
	private String font;

	/** ширина курсора */
	private String cursorWidth;
	/** высота курсора */
	private String cursorHeight;

	/** цвет текста */
	private Color color;
	/** цвет выделенного текста */
	private Color selectedColor;
	/** цвет поля ввода */
	private Color fieldColor;

	/** максимальная длинна сообщения */
	private int maxLength;

	public TextFieldUIBuilder(String id) {
		super(id);

		this.cursorImage = DEFAULT_CURSOR_IMAGE;
		this.cursorImageEmpty = CURSOR_EMPTY_IMAGE;
		this.cursorHeight = "26px";
		this.cursorWidth = "5px";
		this.maxLength = TextFieldUI.UNLIMITED_LENGTH;

		setVisibleToMouse(true);
	}

	/**
	 * @return цвет текста.
	 */
	public Color getColor() {
		return color;
	}

	/**
	 * @return высота курсора.
	 */
	public String getCursorHeight() {
		return cursorHeight;
	}

	/**
	 * @return путь к картинке куврсора.
	 */
	public String getCursorImage() {
		return cursorImage;
	}

	/**
	 * @return путь к пустой картинки курсора.
	 */
	public String getCursorImageEmpty() {
		return cursorImageEmpty;
	}

	/**
	 * @return ширина курсора.
	 */
	public String getCursorWidth() {
		return cursorWidth;
	}

	/**
	 * @return цвет поля ввода.
	 */
	public Color getFieldColor() {
		return fieldColor;
	}

	/**
	 * @return путь к картинке поля ввода.
	 */
	public String getFieldImage() {
		return fieldImage;
	}

	/**
	 * @return шрифт надписи.
	 */
	public String getFont() {
		return font;
	}

	/**
	 * @return максимальная длинна сообщения.
	 */
	public int getMaxLength() {
		return maxLength;
	}

	/**
	 * @return цвет выделенного текста.
	 */
	public Color getSelectedColor() {
		return selectedColor;
	}

	/**
	 * @return текст надписи.
	 */
	public String getText() {
		return text;
	}

	@Override
	public PanelBuilder prepareBuilder() {

		LabelUIBuilder label = new LabelUIBuilder(TextFieldUI.TEXT_ELEMENT_ID);
		label.setVisibleToMouse(false);
		label.setTextHAlign(Align.Left);

		if(getColor() != null) {
			label.setColor(getColor());
		}

		if(getSelectedColor() != null) {
			label.setSelectedColor(getSelectedColor());
		}

		if(getText() != null) {
			label.setText(getText());
		}

		if(getFont() != null) {
			label.setFont(getFont());
		}

		PanelUIBuilder textFieldPanel = new PanelUIBuilder(TextFieldUI.TEXT_FIELD_ID);
		textFieldPanel.setVisibleToMouse(false);
		textFieldPanel.setChildClip(true);
		textFieldPanel.add(label);

		if(getFieldColor() != null) {
			textFieldPanel.setBackgroundColor(getFieldColor());
		}

		if(getFieldImage() != null) {
			textFieldPanel.setBackgroundImage(getFieldImage());
		}

		ImageUIBuilder cursor = new ImageUIBuilder(TextFieldUI.TEXT_CURSOR_ID);
		cursor.setPath(getCursorImageEmpty());
		cursor.setWidth(getCursorWidth());
		cursor.setHeight(getCursorHeight());

		ElementEffectUtils.addImagePulsateEffect(cursor, getCursorImage(), 250, true);

		PanelUIBuilder cursorPanel = new PanelUIBuilder(TextFieldUI.TEXT_CURSOR_PANEL_ID);
		cursorPanel.setVisibleToMouse(false);
		cursorPanel.setLayoutType(ChildLayoutType.Absolute);
		cursorPanel.add(cursor);

		PanelBuilder builder = new PanelBuilder(getId());
		builder.controller(TextFieldUIImpl.class.getName());
		builder.inputMapping(TextFieldInputMapping.class.getName());
		builder.set(TextFieldUI.PROPERTY_MAX_LENGTH, String.valueOf(getMaxLength()));
		builder.focusable(true);

		textFieldPanel.buildTo(builder);
		cursorPanel.buildTo(builder);

		return builder;
	}

	/**
	 * @param color цвет текста.
	 */
	public void setColor(Color color) {
		this.color = color;
	}

	/**
	 * @param cursorHeight высота курсора.
	 */
	public void setCursorHeight(String cursorHeight) {
		this.cursorHeight = cursorHeight;
	}

	/**
	 * @param cursorImage путь к картинке куврсора.
	 */
	public void setCursorImage(String cursorImage) {
		this.cursorImage = cursorImage;
	}

	/**
	 * @param cursorImageEmpty путь к пустой картинки курсора.
	 */
	public void setCursorImageEmpty(String cursorImageEmpty) {
		this.cursorImageEmpty = cursorImageEmpty;
	}

	/**
	 * @param cursorWidth ширина курсора.
	 */
	public void setCursorWidth(String cursorWidth) {
		this.cursorWidth = cursorWidth;
	}

	/**
	 * @param fieldColor цвет поля ввода.
	 */
	public void setFieldColor(Color fieldColor) {
		this.fieldColor = fieldColor;
	}

	/**
	 * @param fieldImage путь к картинке поля ввода.
	 */
	public void setFieldImage(String fieldImage) {
		this.fieldImage = fieldImage;
	}

	/**
	 * @param font шрифт надписи.
	 */
	public void setFont(String font) {
		this.font = font;
	}

	/**
	 * @param maxLength максимальная длинна сообщения.
	 */
	public void setMaxLength(int maxLength) {
		this.maxLength = maxLength;
	}

	/**
	 * @param selectedColor цвет выделенного текста.
	 */
	public void setSelectedColor(Color selectedColor) {
		this.selectedColor = selectedColor;
	}

	/**
	 * @param text текст надписи.
	 */
	public void setText(String text) {
		this.text = text;
	}
}
