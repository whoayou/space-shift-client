package com.ss.client.gui.element.builder.impl;

import rlib.util.Strings;

import com.ss.client.gui.FontIds;
import com.ss.client.gui.element.MessageElementUI;
import com.ss.client.gui.element.impl.MessageElementUIImpl;

import de.lessvoid.nifty.builder.ElementBuilder.Align;
import de.lessvoid.nifty.builder.ElementBuilder.VAlign;
import de.lessvoid.nifty.builder.PanelBuilder;
import de.lessvoid.nifty.tools.Color;

/**
 * Реализация контсруктора итема сообщения.
 * 
 * @author Ronn
 */
public class MessageElementUIBuilder extends PanelUIBuilder {

	/** горизонтальное выравнивание */
	private Align textHAlign;
	/** вертикальное выравнивание */
	private VAlign textVAlign;

	/** текст надписи */
	private String text;
	/** шрифт надписи */
	private String font;

	/** цвет текста */
	private Color color;

	/** отступ текста по ширине от краев */
	private int textPaddingWidth;
	/** отступ текста по высоте от краев */
	private int textPaddingHeight;

	public MessageElementUIBuilder(String id) {
		super(id);

		this.text = Strings.EMPTY;
		this.textHAlign = Align.Left;
		this.textVAlign = VAlign.Top;
		this.color = Color.WHITE;
		this.font = FontIds.MAIN_16;
		this.textPaddingHeight = 5;
		this.textPaddingWidth = 5;

		setHeight("1px");
		setChildClip(true);
	}

	/**
	 * @return цвет текста.
	 */
	public Color getColor() {
		return color;
	}

	/**
	 * @return шрифт надписи.
	 */
	public String getFont() {
		return font;
	}

	/**
	 * @return текст надписи.
	 */
	public String getText() {
		return text;
	}

	/**
	 * @return горизонтальное выравнивание.
	 */
	public Align getTextHAlign() {
		return textHAlign;
	}

	/**
	 * @return отступ текста по высоте от краев.
	 */
	public int getTextPaddingHeight() {
		return textPaddingHeight;
	}

	/**
	 * @return отступ текста по ширине от краев.
	 */
	public int getTextPaddingWidth() {
		return textPaddingWidth;
	}

	/**
	 * @return вертикальное выравнивание.
	 */
	public VAlign getTextVAlign() {
		return textVAlign;
	}

	@Override
	public PanelBuilder prepareBuilder() {

		PanelBuilder builder = new PanelBuilder(getId());
		builder.controller(MessageElementUIImpl.class.getName());
		builder.set(MessageElementUI.PROPERTY_TEXT, getText());
		builder.set(MessageElementUI.PROPERTY_WRAP, String.valueOf(true));
		builder.set(MessageElementUI.PROPERTY_TEXT_PADDING_HEIGHT, String.valueOf(getTextPaddingHeight()));
		builder.set(MessageElementUI.PROPERTY_TEXT_PADDING_WIDTH, String.valueOf(getTextPaddingWidth()));
		builder.style(Strings.EMPTY);

		if(getBackgroundColor() != null) {
			builder.backgroundColor(getBackgroundColor());
		}

		if(getBackgroundImage() != null) {
			builder.backgroundImage(getBackgroundImage());
		}

		LabelUIBuilder text = new LabelUIBuilder(MessageElementUI.TEXT_ELEMENT_ID);
		text.setTextHAlign(getTextHAlign());
		text.setTextVAlign(getTextVAlign());
		text.setColor(getColor());
		text.setFont(getFont());
		text.buildTo(builder);

		return builder;
	}

	/**
	 * @param color цвет текста.
	 */
	public void setColor(Color color) {
		this.color = color;
	}

	/**
	 * @param font шрифт надписи.
	 */
	public void setFont(String font) {
		this.font = font;
	}

	/**
	 * @param text текст надписи.
	 */
	public void setText(String text) {
		this.text = text;
	}

	/**
	 * @param textHAlign горизонтальное выравнивание.
	 */
	public void setTextHAlign(Align textHAlign) {
		this.textHAlign = textHAlign;
	}

	/**
	 * @param textPaddingHeight отступ текста по высоте от краев.
	 */
	public void setTextPaddingHeight(int textPaddingHeight) {
		this.textPaddingHeight = textPaddingHeight;
	}

	/**
	 * @param textPaddingWidth отступ текста по ширине от краев.
	 */
	public void setTextPaddingWidth(int textPaddingWidth) {
		this.textPaddingWidth = textPaddingWidth;
	}

	/**
	 * @param textVAlign вертикальное выравнивание.
	 */
	public void setTextVAlign(VAlign textVAlign) {
		this.textVAlign = textVAlign;
	}
}
