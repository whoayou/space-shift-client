package com.ss.client.gui.element.builder.impl;

import java.lang.reflect.Field;

import rlib.logging.Logger;
import rlib.logging.Loggers;
import rlib.util.array.Array;
import rlib.util.array.Arrays;
import rlib.util.table.Table;
import rlib.util.table.Tables;

import com.ss.client.gui.element.ElementUI;
import com.ss.client.gui.element.builder.ElementUIBuilder;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.builder.ControlBuilder;
import de.lessvoid.nifty.builder.EffectBuilder;
import de.lessvoid.nifty.builder.ElementBuilder;
import de.lessvoid.nifty.builder.ElementBuilder.Align;
import de.lessvoid.nifty.builder.ElementBuilder.ChildLayoutType;
import de.lessvoid.nifty.builder.ElementBuilder.VAlign;
import de.lessvoid.nifty.builder.HoverEffectBuilder;
import de.lessvoid.nifty.builder.ImageBuilder;
import de.lessvoid.nifty.builder.PanelBuilder;
import de.lessvoid.nifty.builder.TextBuilder;
import de.lessvoid.nifty.controls.dynamic.attributes.ControlInteractAttributes;
import de.lessvoid.nifty.effects.EffectEventId;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.screen.Screen;

/**
 * Базовая реализация билдера UI элемента.
 * 
 * @author Ronn
 */
public abstract class AbstractElementUIBuilder<T extends ElementBuilder> implements ElementUIBuilder {

	protected static final Logger LOGGER = Loggers.getLogger(ElementUIBuilder.class);

	private static Field interactAttributesField;

	static {

		try {
			interactAttributesField = ElementBuilder.class.getDeclaredField("interactAttributes");
			interactAttributesField.setAccessible(true);
		} catch(NoSuchFieldException | SecurityException e) {
			LOGGER.warning(e);
		}
	}

	private static final ThreadLocal<Array<EffectEventId>> LOCAL_KEY_CONTAINER = new ThreadLocal<Array<EffectEventId>>() {

		@Override
		protected Array<EffectEventId> initialValue() {
			return Arrays.toArray(EffectEventId.class);
		}
	};

	/** таблица добавляемых эффектов */
	private final Table<EffectEventId, Array<Object>> effects;

	/** список билдеров дочерних элементов */
	private final Array<ElementUIBuilder> builders;

	/** ид контрола */
	private final String id;

	/** контролер элемента */
	private Class<?> controller;

	/** ширина контрола */
	private String width;
	/** высота контрола */
	private String height;

	/** отступ вниз */
	private String marginTop;
	/** отступ вправо */
	private String marginLeft;
	/** отступ вверх */
	private String marginBottom;
	/** отступ влево */
	private String marginRight;
	/** набор оступов */
	private String margin;

	/** название метода обрабатывающего ЛКМ */
	private String methodNamePrimaryClick;
	/** название метода, орабатывающего движение мыши с зажатой ЛКМ */
	private String methodNamePrimaryClickMouseMove;
	/** название метода ??? */
	private String methodNamePrimaryClickRepeat;
	/** название метода, обрабатывающего ПКМ */
	private String methodNameSecondaryClick;
	/** название метода, обрабатывающего отпускаение зажатой кнопки мыши */
	private String methodNameMouseRelease;
	/** название метода, обрабатывающего кручение колесом */
	private String methodNameMouseWheel;
	/** название метода, обрабатывающего вхождение курсора на элемент */
	private String methodNameMouseOver;

	/** горизонтальное выравнивание */
	private Align align;
	/** вертикальное выравнивание */
	private VAlign valign;

	/** тип размещения элементов внутри */
	private ChildLayoutType layoutType;

	/** видим ля элемент для действий мыши */
	private boolean visibleToMouse;
	/** рассчитывать ли расположение элементов с учетом невидимых элементов */
	private boolean layoutInvisibleElements;
	/** можт ли дочерний элемент выходить за пределы родительского */
	private boolean childClip;
	/** видим ли элемент после создания */
	private boolean visible;

	public AbstractElementUIBuilder(String id) {
		this.id = id;
		this.width = "100%";
		this.height = "100%";
		this.align = Align.Center;
		this.valign = VAlign.Center;
		this.layoutType = ChildLayoutType.Center;
		this.builders = Arrays.toArray(ElementUIBuilder.class);
		this.effects = Tables.newObjectTable();
		this.visibleToMouse = true;
		this.methodNamePrimaryClick = ElementUI.METHOD_NAME_PRIMARY_CKILCK;
		this.methodNamePrimaryClickMouseMove = ElementUI.METHOD_NAME_PRIMARY_CKILCK_MOUSE_MOVE;
		this.methodNameSecondaryClick = ElementUI.METHOD_NAME_SECONDARY_CKILCK;
		this.methodNameMouseRelease = ElementUI.METHOD_NAME_MOUSE_RELEASE;
		this.methodNameMouseWheel = ElementUI.METHOD_NAME_MOUSE_WHEEL;
		this.methodNameMouseOver = ElementUI.METHOD_NAME_MOUSE_OVER;
		this.visibleToMouse = false;
		this.visible = true;
		this.layoutInvisibleElements = true;
	}

	@Override
	public void add(ElementUIBuilder builder) {
		builders.add(builder);
	}

	@Override
	public void addEffect(EffectEventId eventId, Object builder) {

		Table<EffectEventId, Array<Object>> effects = getEffects();
		Array<Object> container = effects.get(eventId);

		if(container == null) {
			container = Arrays.toArray(Object.class);
			effects.put(eventId, container);
		}

		container.add(builder);
	}

	/**
	 * Приминение эффекта на элемент.
	 * 
	 * @param effectBuilder конструктор эффекта.
	 * @param eventId тип события запуска эффекта.
	 * @param builder конструктор элемента.
	 */
	private void applyEffect(Object effectBuilder, EffectEventId eventId, ElementBuilder builder) {

		switch(eventId) {
			case onCustom: {
				builder.onCustomEffect((EffectBuilder) effectBuilder);
				break;
			}
			case onClick: {
				builder.onClickEffect((EffectBuilder) effectBuilder);
				break;
			}
			case onActive: {
				builder.onActiveEffect((EffectBuilder) effectBuilder);
				break;
			}
			case onDisabled: {
				break;
			}
			case onEnabled: {
				break;
			}
			case onEndHover: {
				builder.onEndHoverEffect((HoverEffectBuilder) effectBuilder);
				break;
			}
			case onEndScreen: {
				builder.onEndScreenEffect((EffectBuilder) effectBuilder);
				break;
			}
			case onFocus: {
				builder.onFocusEffect((EffectBuilder) effectBuilder);
				break;
			}
			case onGetFocus: {
				builder.onGetFocusEffect((EffectBuilder) effectBuilder);
				break;
			}
			case onHide: {
				builder.onHideEffect((EffectBuilder) effectBuilder);
				break;
			}
			case onHover: {
				builder.onHoverEffect((HoverEffectBuilder) effectBuilder);
				break;
			}
			case onLostFocus: {
				builder.onLostFocusEffect((EffectBuilder) effectBuilder);
				break;
			}
			case onShow: {
				builder.onShowEffect((EffectBuilder) effectBuilder);
				break;
			}
			case onStartHover: {
				builder.onStartHoverEffect((HoverEffectBuilder) effectBuilder);
				break;
			}
			case onStartScreen: {
				builder.onStartScreenEffect((EffectBuilder) effectBuilder);
				break;
			}
			default: {
				LOGGER.warning("incorrect effectType " + eventId);
				break;
			}
		}
	}

	/**
	 * Приминение эффектов на элемент.
	 * 
	 * @param builder конструктор элемента.
	 */
	private void applyEffects(ElementBuilder builder) {

		Table<EffectEventId, Array<Object>> effects = getEffects();

		if(effects.isEmpty()) {
			return;
		}

		Array<EffectEventId> keys = LOCAL_KEY_CONTAINER.get();
		keys.clear();

		effects.keyArray(keys);

		for(EffectEventId effectType : keys.array()) {

			if(effectType == null) {
				break;
			}

			Array<Object> container = effects.get(effectType);

			for(Object effectBuilder : container.array()) {

				if(effectBuilder == null) {
					break;
				}

				applyEffect(effectBuilder, effectType, builder);
			}
		}
	}

	/**
	 * приминение настроек к итоговому билдеру.
	 * 
	 * @param builder билдер элемента.
	 */
	protected T applySettings(T builder) {

		builder.set(Element.PROP_WIDTH, getWidth());
		builder.set(Element.PROP_HEIGHT, getHeight());
		builder.set(Element.PROP_ALIGN, getAlign().getLayout());
		builder.set(Element.PROP_VALIGN, getValign().getLayout());
		builder.set(Element.PROP_CHILD_LAYOUT, getLayoutType().getLayout());
		builder.set(Element.PROP_VISIBLE_TO_MOUSE, String.valueOf(isVisibleToMouse()));
		builder.set(Element.PROP_CHILD_CLIP, String.valueOf(isChildClip()));
		builder.set(Element.PROP_VISIBLE, String.valueOf(isVisible()));
		builder.set(Element.PROP_LAYOUT_INVISIBLE_ELEMENTS, String.valueOf(isLayoutInvisibleElements()));
		builder.set(Element.PROP_X, "0px");
		builder.set(Element.PROP_Y, "0px");

		if(getController() != null) {
			builder.controller(getController().getName());
		}

		if(getMarginTop() != null) {
			builder.marginTop(getMarginTop());
		}

		if(getMarginLeft() != null) {
			builder.marginLeft(getMarginLeft());
		}

		if(getMarginBottom() != null) {
			builder.marginBottom(getMarginBottom());
		}

		if(getMarginRight() != null) {
			builder.marginRight(getMarginRight());
		}

		if(getMargin() != null) {
			builder.margin(getMargin());
		}

		if(isVisibleToMouse()) {

			String methodName = getMethodNamePrimaryClick();

			if(methodName != null) {
				builder.interactOnClick(methodName);
			}

			methodName = getMethodNamePrimaryClickMouseMove();

			if(methodName != null) {
				builder.interactOnClickMouseMove(methodName);
			}

			methodName = getMethodNamePrimaryClickRepeat();

			if(methodName != null) {
				builder.interactOnClickRepeat(methodName);
			}

			methodName = getMethodNameMouseOver();

			if(methodName != null) {
				builder.interactOnMouseOver(methodName);
			}

			methodName = getMethodNameMouseRelease();

			if(methodName != null) {
				builder.interactOnRelease(methodName);
			}

			methodName = getMethodNameSecondaryClick();

			if(methodName != null) {

				try {
					ControlInteractAttributes attributes = (ControlInteractAttributes) interactAttributesField.get(builder);
					attributes.setAttribute("onSecondaryClick", methodName);
				} catch(IllegalArgumentException | IllegalAccessException e) {
					LOGGER.warning(this, e);
				}
			}

			methodName = getMethodNameMouseWheel();

			if(methodName != null) {

				try {
					ControlInteractAttributes attributes = (ControlInteractAttributes) interactAttributesField.get(builder);
					attributes.setAttribute("onMouseWheel", methodName);
				} catch(IllegalArgumentException | IllegalAccessException e) {
					LOGGER.warning(this, e);
				}
			}
		}

		applyEffects(builder);

		for(ElementUIBuilder child : getBuilders()) {
			child.buildTo(builder);
		}

		return builder;
	}

	@Override
	public Element build(Nifty nifty, Screen screen, Element parent) {
		T prepare = applySettings(prepareBuilder());
		return prepare.build(nifty, screen, parent);
	}

	@Override
	public void buildTo(ElementBuilder builder) {

		T prepare = applySettings(prepareBuilder());

		if(prepare instanceof PanelBuilder) {
			builder.panel((PanelBuilder) prepare);
		} else if(prepare instanceof ControlBuilder) {
			builder.control((ControlBuilder) prepare);
		} else if(prepare instanceof TextBuilder) {
			builder.text((TextBuilder) prepare);
		} else if(prepare instanceof ImageBuilder) {
			builder.image((ImageBuilder) prepare);
		} else {
			throw new RuntimeException("not supported builder " + prepare);
		}
	}

	/**
	 * @return горизонтальное выравнивание.
	 */
	public Align getAlign() {
		return align;
	}

	/**
	 * @return список билдеров.
	 */
	protected Array<ElementUIBuilder> getBuilders() {
		return builders;
	}

	/**
	 * @return контролер элемента.
	 */
	public Class<?> getController() {
		return controller;
	}

	/**
	 * @return таблица добавляемых эффектов.
	 */
	private Table<EffectEventId, Array<Object>> getEffects() {
		return effects;
	}

	/**
	 * @return высота контрола.
	 */
	public String getHeight() {
		return height;
	}

	/**
	 * @return ид контрола.
	 */
	public String getId() {
		return id;
	}

	/**
	 * @return тип размещения элементов внутри.
	 */
	public ChildLayoutType getLayoutType() {
		return layoutType;
	}

	/**
	 * @return набор оступов.
	 */
	public String getMargin() {
		return margin;
	}

	/**
	 * @return отступ вверх.
	 */
	public String getMarginBottom() {
		return marginBottom;
	}

	/**
	 * @return отступ вправо.
	 */
	public String getMarginLeft() {
		return marginLeft;
	}

	/**
	 * @return отступ вверх.
	 */
	public String getMarginRight() {
		return marginRight;
	}

	/**
	 * @return отступ вниз.
	 */
	public String getMarginTop() {
		return marginTop;
	}

	/**
	 * @return название метода, обрабатывающего вхождение курсора на элемент.
	 */
	public String getMethodNameMouseOver() {
		return methodNameMouseOver;
	}

	/**
	 * @return название метода, обрабатывающего отпускаение зажатой кнопки мыши.
	 */
	public String getMethodNameMouseRelease() {
		return methodNameMouseRelease;
	}

	/**
	 * @return название метода, обрабатывающего кручение колесом.
	 */
	public String getMethodNameMouseWheel() {
		return methodNameMouseWheel;
	}

	/**
	 * @return название метода обрабатывающего ЛКМ.
	 */
	public String getMethodNamePrimaryClick() {
		return methodNamePrimaryClick;
	}

	/**
	 * @return название метода, орабатывающего движение мыши с зажатой ЛКМ.
	 */
	public String getMethodNamePrimaryClickMouseMove() {
		return methodNamePrimaryClickMouseMove;
	}

	/**
	 * @return название метода ???.
	 */
	public String getMethodNamePrimaryClickRepeat() {
		return methodNamePrimaryClickRepeat;
	}

	/**
	 * @return название метода, обрабатывающего ПКМ.
	 */
	public String getMethodNameSecondaryClick() {
		return methodNameSecondaryClick;
	}

	/**
	 * @return вертикальное выравнивание.
	 */
	public VAlign getValign() {
		return valign;
	}

	/**
	 * @return ширина контрола.
	 */
	public String getWidth() {
		return width;
	}

	/**
	 * @return может ли дочерний элемент выходить за пределы родительского.
	 */
	public boolean isChildClip() {
		return childClip;
	}

	/**
	 * @return рассчитывать ли расположение элементов с учетом невидимых
	 * элементов.
	 */
	public boolean isLayoutInvisibleElements() {
		return layoutInvisibleElements;
	}

	/**
	 * @return видим ли элемент после создания.
	 */
	public boolean isVisible() {
		return visible;
	}

	/**
	 * @return видим ля элемент для действий мыши.
	 */
	public boolean isVisibleToMouse() {
		return visibleToMouse;
	}

	@Override
	public T prepareBuilder() {
		return null;
	}

	/**
	 * @param align горизонтальное выравнивание.
	 */
	public void setAlign(Align align) {
		this.align = align;
	}

	/**
	 * @param childClip может ли дочерний элемент выходить за пределы
	 * родительского.
	 */
	public void setChildClip(boolean childClip) {
		this.childClip = childClip;
	}

	/**
	 * @param controller контролер элемента.
	 */
	public void setController(Class<?> controller) {
		this.controller = controller;
	}

	/**
	 * @param height высота контрола.
	 */
	public void setHeight(String height) {
		this.height = height;
	}

	/**
	 * @param layoutInvisibleElements рассчитывать ли расположение элементов с
	 * учетом невидимых элементов.
	 */
	public void setLayoutInvisibleElements(boolean layoutInvisibleElements) {
		this.layoutInvisibleElements = layoutInvisibleElements;
	}

	/**
	 * @param layoutType тип размещения элементов внутри.
	 */
	public void setLayoutType(ChildLayoutType layoutType) {
		this.layoutType = layoutType;
	}

	/**
	 * @param margin набор оступов.
	 */
	public void setMargin(String margin) {
		this.margin = margin;
	}

	/**
	 * @param marginBottom отступ вверх.
	 */
	public void setMarginBottom(String marginBottom) {
		this.marginBottom = marginBottom;
	}

	/**
	 * @param marginLeft отступ вправо.
	 */
	public void setMarginLeft(String marginLeft) {
		this.marginLeft = marginLeft;
	}

	/**
	 * @param marginRight отступ вверх.
	 */
	public void setMarginRight(String marginRight) {
		this.marginRight = marginRight;
	}

	/**
	 * @param marginTop отступ вниз.
	 */
	public void setMarginTop(String marginTop) {
		this.marginTop = marginTop;
	}

	/**
	 * @param methodNameMouseOver название метода, обрабатывающего вхождение
	 * курсора на элемент.
	 */
	public void setMethodNameMouseOver(String methodNameMouseOver) {
		this.methodNameMouseOver = methodNameMouseOver;
	}

	/**
	 * @param methodNameMouseRelease название метода, обрабатывающего
	 * отпускаение зажатой кнопки мыши.
	 */
	public void setMethodNameMouseRelease(String methodNameMouseRelease) {
		this.methodNameMouseRelease = methodNameMouseRelease;
	}

	/**
	 * @param methodNameMouseWheel название метода, обрабатывающего кручение
	 * колесом.
	 */
	public void setMethodNameMouseWheel(String methodNameMouseWheel) {
		this.methodNameMouseWheel = methodNameMouseWheel;
	}

	/**
	 * @param methodNamePrimaryClick название метода обрабатывающего ЛКМ.
	 */
	public void setMethodNamePrimaryClick(String methodNamePrimaryClick) {
		this.methodNamePrimaryClick = methodNamePrimaryClick;
	}

	/**
	 * @param methodNamePrimaryClickMouseMove название метода, орабатывающего
	 * движение мыши с зажатой ЛКМ.
	 */
	public void setMethodNamePrimaryClickMouseMove(String methodNamePrimaryClickMouseMove) {
		this.methodNamePrimaryClickMouseMove = methodNamePrimaryClickMouseMove;
	}

	/**
	 * @param methodNamePrimaryClickRepeat название метода ???.
	 */
	public void setMethodNamePrimaryClickRepeat(String methodNamePrimaryClickRepeat) {
		this.methodNamePrimaryClickRepeat = methodNamePrimaryClickRepeat;
	}

	/**
	 * @param methodNameSecondaryClick название метода, обрабатывающего ПКМ.
	 */
	public void setMethodNameSecondaryClick(String methodNameSecondaryClick) {
		this.methodNameSecondaryClick = methodNameSecondaryClick;
	}

	/**
	 * @param valign вертикальное выравнивание.
	 */
	public void setValign(VAlign valign) {
		this.valign = valign;
	}

	/**
	 * @param visible видим ли элемент после создания.
	 */
	public void setVisible(boolean visible) {
		this.visible = visible;
	}

	/**
	 * @param visibleToMouse видим ля элемент для действий мыши.
	 */
	public void setVisibleToMouse(boolean visibleToMouse) {
		this.visibleToMouse = visibleToMouse;
	}

	/**
	 * @param width ширина контрола.
	 */
	public void setWidth(String width) {
		this.width = width;
	}
}
