package com.ss.client.gui.element.builder.impl;

import rlib.util.Strings;

import com.ss.client.gui.element.impl.PanelUIImpl;

import de.lessvoid.nifty.builder.PanelBuilder;
import de.lessvoid.nifty.tools.Color;

/**
 * Конструктор элемента панели.
 * 
 * @author Ronn
 */
public class PanelUIBuilder extends AbstractElementUIBuilder<PanelBuilder> {

	/** фоновое изображение панели */
	private String backgroundImage;
	/** фоновый цвет панели */
	private Color backgroundColor;

	public PanelUIBuilder(String id) {
		super(id);
	}

	/**
	 * @return фоновое изображение панели.
	 */
	public Color getBackgroundColor() {
		return backgroundColor;
	}

	/**
	 * @return фоновое изображение панели.
	 */
	public String getBackgroundImage() {
		return backgroundImage;
	}

	@Override
	public PanelBuilder prepareBuilder() {

		PanelBuilder builder = new PanelBuilder(getId());
		builder.controller(PanelUIImpl.class.getName());
		builder.style(Strings.EMPTY);

		if(getBackgroundColor() != null) {
			builder.backgroundColor(getBackgroundColor());
		}

		if(getBackgroundImage() != null) {
			builder.backgroundImage(getBackgroundImage());
		}

		return builder;
	}

	/**
	 * @param backgroundColor фоновое изображение панели.
	 */
	public void setBackgroundColor(Color backgroundColor) {
		this.backgroundColor = backgroundColor;
	}

	/**
	 * @param backgroundImage фоновое изображение панели.
	 */
	public void setBackgroundImage(String backgroundImage) {
		this.backgroundImage = backgroundImage;
	}
}
