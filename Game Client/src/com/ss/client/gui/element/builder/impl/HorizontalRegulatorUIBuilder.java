package com.ss.client.gui.element.builder.impl;

import com.ss.client.gui.element.RegulatorUI;
import com.ss.client.gui.element.impl.HorizontalRegulatorUIImpl;

import de.lessvoid.nifty.builder.ElementBuilder.ChildLayoutType;
import de.lessvoid.nifty.builder.ElementBuilder.VAlign;
import de.lessvoid.nifty.builder.PanelBuilder;
import de.lessvoid.nifty.tools.Color;

/**
 * Билдер элемента горизонтального регулятора.
 * 
 * @author Ronn
 */
public class HorizontalRegulatorUIBuilder extends AbstractElementUIBuilder<PanelBuilder> {

	/** картинка кнопки увеличения */
	private String buttonAddImage;
	/** картинка кнопки уменьшения */
	private String buttonSubImage;
	/** картинка панели значения */
	private String valuePanelImage;
	/** картинка панели фона значения */
	private String basePanelImage;
	/** размер кнопки */
	private String buttonSize;

	private String basePanelHeight;

	private String basePanelWidth;

	/** цвет кнопки увеличкения */
	private Color buttonAddColor;
	/** цвет кнопки уменьшения */
	private Color buttonSubColor;

	/** цвет панели значения */
	private Color valuePanelColor;
	/** цвет панели фона значения */
	private Color basePanelColor;

	/** минимальное значение */
	private float minimum;
	/** максимальное значение */
	private float maximum;
	/** шаг регулятора */
	private float step;

	public HorizontalRegulatorUIBuilder(String id) {
		super(id);

		this.buttonSize = "16px";

		setLayoutType(ChildLayoutType.Horizontal);
	}

	/**
	 * @return цвет панели фона значения.
	 */
	public Color getBasePanelColor() {
		return basePanelColor;
	}

	public String getBasePanelHeight() {
		return basePanelHeight;
	}

	/**
	 * @return картинка панели фона значения.
	 */
	public String getBasePanelImage() {
		return basePanelImage;
	}

	public String getBasePanelWidth() {
		return basePanelWidth;
	}

	/**
	 * @return цвет кнопки увеличкения.
	 */
	public Color getButtonAddColor() {
		return buttonAddColor;
	}

	/**
	 * @return картинка кнопки увеличения.
	 */
	public String getButtonAddImage() {
		return buttonAddImage;
	}

	/**
	 * @return размер кнопки.
	 */
	public String getButtonSize() {
		return buttonSize;
	}

	/**
	 * @return цвет кнопки уменьшения.
	 */
	public Color getButtonSubColor() {
		return buttonSubColor;
	}

	/**
	 * @return картинка кнопки уменьшения.
	 */
	public String getButtonSubImage() {
		return buttonSubImage;
	}

	/**
	 * @return максимальное значение.
	 */
	public float getMaximum() {
		return maximum;
	}

	/**
	 * @return минимальное значение.
	 */
	public float getMinimum() {
		return minimum;
	}

	/**
	 * @return шаг регулятора.
	 */
	public float getStep() {
		return step;
	}

	/**
	 * @return цвет панели значения.
	 */
	public Color getValuePanelColor() {
		return valuePanelColor;
	}

	/**
	 * @return картинка панели значения.
	 */
	public String getValuePanelImage() {
		return valuePanelImage;
	}

	@Override
	public PanelBuilder prepareBuilder() {

		String buttonSize = getButtonSize();

		ButtonUIBuilder buttonAdd = new ButtonUIBuilder(RegulatorUI.BUTTON_ADD_ID);
		buttonAdd.setBackgroundImage(getButtonAddImage());
		buttonAdd.setBackgroundColor(getButtonAddColor());
		buttonAdd.setMarginRight("2px");
		buttonAdd.setValign(VAlign.Center);
		buttonAdd.setWidth(buttonSize);
		buttonAdd.setHeight(buttonSize);

		ButtonUIBuilder buttonSub = new ButtonUIBuilder(RegulatorUI.BUTTON_SUB_ID);
		buttonSub.setBackgroundImage(getButtonSubImage());
		buttonSub.setBackgroundColor(getButtonSubColor());
		buttonSub.setMarginRight("2px");
		buttonSub.setValign(VAlign.Center);
		buttonSub.setWidth(buttonSize);
		buttonSub.setHeight(buttonSize);

		PanelUIBuilder valuePanel = new PanelUIBuilder(RegulatorUI.VALUE_PANEL_ID);
		valuePanel.setBackgroundColor(getValuePanelColor());
		valuePanel.setBackgroundImage(getValuePanelImage());
		valuePanel.setWidth("100%");
		valuePanel.setHeight("100%");

		PanelUIBuilder basePanel = new PanelUIBuilder(RegulatorUI.BASE_PANEL_ID);
		basePanel.setBackgroundImage(getBasePanelImage());
		basePanel.setBackgroundColor(getBasePanelColor());
		basePanel.setWidth(getBasePanelWidth());
		basePanel.setHeight(getBasePanelHeight());
		basePanel.setLayoutType(ChildLayoutType.Horizontal);
		basePanel.setMarginRight("2px");
		basePanel.add(valuePanel);

		PanelBuilder panel = new PanelBuilder(getId());
		panel.controller(HorizontalRegulatorUIImpl.class.getName());
		panel.set(RegulatorUI.MAXIMUM, String.valueOf(getMaximum()));
		panel.set(RegulatorUI.MINIMUM, String.valueOf(getMinimum()));
		panel.set(RegulatorUI.STEP, String.valueOf(getStep()));

		buttonSub.buildTo(panel);
		basePanel.buildTo(panel);
		buttonAdd.buildTo(panel);

		return panel;
	}

	public void setBasePanelColor(Color basePanelColor) {
		this.basePanelColor = basePanelColor;
	}

	public void setBasePanelHeight(String basePanelHeight) {
		this.basePanelHeight = basePanelHeight;
	}

	public void setBasePanelWidth(String basePanelWidth) {
		this.basePanelWidth = basePanelWidth;
	}

	public void setButtonAddImage(String buttonAddImage) {
		this.buttonAddImage = buttonAddImage;
	}

	/**
	 * @param buttonSize размер кнопки.
	 */
	public void setButtonSize(String buttonSize) {
		this.buttonSize = buttonSize;
	}

	public void setButtonSubImage(String buttonSubImage) {
		this.buttonSubImage = buttonSubImage;
	}

	/**
	 * @param maximum максимальное значение.
	 */
	public void setMaximum(float maximum) {
		this.maximum = maximum;
	}

	/**
	 * @param minimum минимальное значение.
	 */
	public void setMinimum(float minimum) {
		this.minimum = minimum;
	}

	/**
	 * @param step шаг регулятора.
	 */
	public void setStep(float step) {
		this.step = step;
	}

	public void setValuePanelColor(Color valuePanelColor) {
		this.valuePanelColor = valuePanelColor;
	}
}
