package com.ss.client.gui.element.listener;

import com.ss.client.gui.element.ElementUI;
import com.ss.client.gui.element.event.ElementUIEvent;

/**
 * Интерфейс для реализации слушателя событий элемента UI.
 * 
 * @author Ronn
 */
public interface ElementUIEventListener<C extends ElementUI> {

	/**
	 * Уведомление о событии слушателя.
	 * 
	 * @param event событие контрола.
	 */
	public void notifyEvent(ElementUIEvent<C> event);
}
