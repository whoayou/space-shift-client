package com.ss.client.gui.element;

import com.ss.client.gui.element.event.ElementEventType;
import com.ss.client.gui.element.listener.ElementUIEventListener;

import de.lessvoid.nifty.controls.Controller;
import de.lessvoid.nifty.controls.NiftyControl;

/**
 * Интерфейс для реализации элемента UI в игре Space Shift.
 * 
 * @author Ronn
 */
public interface ElementUI extends Controller, NiftyControl {

	public static final String METHOD_NAME_PRIMARY_CKILCK = "elementPrimaryClick()";
	public static final String METHOD_NAME_PRIMARY_CKILCK_MOUSE_MOVE = "elementPrimaryClickMouseMove()";
	public static final String METHOD_NAME_PRIMARY_CKILCK_REPEAT = "elementPrimaryClickRepeat()";
	public static final String METHOD_NAME_SECONDARY_CKILCK = "elementSecondaryClick()";
	public static final String METHOD_NAME_MOUSE_RELEASE = "elementMouseRelease()";
	public static final String METHOD_NAME_MOUSE_WHEEL = "elementMouseWheel()";
	public static final String METHOD_NAME_MOUSE_OVER = "elementMouseOver()";

	/**
	 * Добавление слушателя событий элемента UI.
	 * 
	 * @param eventType тип события.
	 * @param listener слушатель событий.
	 */
	public void addElementListener(ElementEventType eventType, ElementUIEventListener<?> listener);

	/**
	 * Снять фокус с элемента.
	 */
	public void disableFocus();

	/**
	 * @return видим ли элемент.
	 */
	public boolean isVisible();

	/**
	 * Удаление слушателя событий элемента UI.
	 * 
	 * @param eventType тип события.
	 * @param listener слушатель событий.
	 */
	public void removeElementListener(ElementEventType eventType, ElementUIEventListener<?> listener);

	/**
	 * Изменение видимости элемента.
	 * 
	 * @param visible видимый ли элемент.
	 */
	public void setVisible(boolean visible);
}
