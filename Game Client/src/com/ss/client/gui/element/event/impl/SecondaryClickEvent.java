package com.ss.client.gui.element.event.impl;

import com.ss.client.gui.element.ElementUI;
import com.ss.client.gui.element.event.ElementEventType;

/**
 * Реализация события о клике ПКМ по элементу UI.
 * 
 * @author Ronn
 */
public class SecondaryClickEvent<T extends ElementUI> extends AbstractElementUIEvent<T> {

	public static final ElementEventType EVENT_TYPE = new ElementEventType() {
	};

	/** координата курсора */
	private int mouseX;
	/** координата курсора */
	private int mouseY;

	@Override
	public ElementEventType getEventType() {
		return EVENT_TYPE;
	}

	/**
	 * @return координата курсора.
	 */
	public int getMouseX() {
		return mouseX;
	}

	/**
	 * @return координата курсора.
	 */
	public int getMouseY() {
		return mouseY;
	}

	/**
	 * @param mouseX координата курсора.
	 */
	public void setMouseX(int mouseX) {
		this.mouseX = mouseX;
	}

	/**
	 * @param mouseY координата курсора.
	 */
	public void setMouseY(int mouseY) {
		this.mouseY = mouseY;
	}
}
