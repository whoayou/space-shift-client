package com.ss.client.gui.element.event.impl;

import com.ss.client.gui.element.ElementUI;
import com.ss.client.gui.element.event.ElementEventType;

/**
 * Реализация события о вращении колеса мышки.
 * 
 * @author Ronn
 */
public class MouseWheelEvent<T extends ElementUI> extends AbstractElementUIEvent<T> {

	public static final ElementEventType EVENT_TYPE = new ElementEventType() {
	};

	/** значение вращение колеса */
	private float value;

	@Override
	public ElementEventType getEventType() {
		return EVENT_TYPE;
	}

	/**
	 * @return значение вращение колеса.
	 */
	public float getValue() {
		return value;
	}

	/**
	 * @param value значение вращение колеса.
	 */
	public void setValue(float value) {
		this.value = value;
	}
}
