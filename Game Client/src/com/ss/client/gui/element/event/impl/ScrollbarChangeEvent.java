package com.ss.client.gui.element.event.impl;

import com.ss.client.gui.element.ScrollbarUI;
import com.ss.client.gui.element.event.ElementEventType;

/**
 * Событие изменения позиции скроллбара.
 * 
 * @author Ronn
 */
public class ScrollbarChangeEvent extends AbstractElementUIEvent<ScrollbarUI> {

	public static final ElementEventType EVENT_TYPE = new ElementEventType() {
	};

	/** значение, на которое изменился ползунок */
	private float value;

	@Override
	public ElementEventType getEventType() {
		return EVENT_TYPE;
	}

	/**
	 * @return значение, на которое изменился ползунок.
	 */
	public float getValue() {
		return value;
	}

	/**
	 * @param value значение, на которое изменился ползунок.
	 */
	public void setValue(float value) {
		this.value = value;
	}

}
