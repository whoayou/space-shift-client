package com.ss.client.gui.element.event;

import com.ss.client.gui.element.ElementUI;

/**
 * Интерфейс для реализации события контрола.
 * 
 * @author Ronn
 */
public interface ElementUIEvent<C extends ElementUI> {

	/**
	 * @return контрол запустивший событие.
	 */
	public C getElement();

	/**
	 * @return тип события.
	 */
	public ElementEventType getEventType();

	/**
	 * @param control контрол запустивший событие.
	 */
	public void setElement(Object control);
}
