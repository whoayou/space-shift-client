package com.ss.client.gui.element.event.impl;

import com.ss.client.gui.element.ElementUI;
import com.ss.client.gui.element.event.ElementEventType;

/**
 * Реализация события об изменении элемента UI.
 * 
 * @author Ronn
 */
public class ChangeElementUIEvent<T extends ElementUI> extends AbstractElementUIEvent<T> {

	public static final ElementEventType CHANGE_ELEMENT_UI_EVENT = new ElementEventType() {
	};

	@Override
	public ElementEventType getEventType() {
		return CHANGE_ELEMENT_UI_EVENT;
	}
}
