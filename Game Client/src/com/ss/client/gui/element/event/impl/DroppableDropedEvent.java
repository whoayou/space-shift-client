package com.ss.client.gui.element.event.impl;

import com.ss.client.gui.element.DraggableElementUI;
import com.ss.client.gui.element.DroppableElementUI;
import com.ss.client.gui.element.ElementUI;
import com.ss.client.gui.element.event.ElementEventType;

/**
 * Реализация события по завершению перемещения элемента в контейнер.
 * 
 * @author Ronn
 */
public class DroppableDropedEvent<T extends ElementUI> extends AbstractElementUIEvent<T> {

	public static final ElementEventType EVENT_TYPE = new ElementEventType() {
	};

	/** перемещаемый элемнт */
	private DraggableElementUI draggable;

	/** исходный контейнер элемента */
	private DroppableElementUI source;

	/**
	 * @return перемещаемый элемнт.
	 */
	public DraggableElementUI getDraggable() {
		return draggable;
	}

	@Override
	public ElementEventType getEventType() {
		return EVENT_TYPE;
	}

	/**
	 * @return исходный контейнер элемента.
	 */
	public DroppableElementUI getSource() {
		return source;
	}

	/**
	 * @param draggable перемещаемый элемнт.
	 */
	public void setDraggable(DraggableElementUI draggable) {
		this.draggable = draggable;
	}

	/**
	 * @param source исходный контейнер элемента.
	 */
	public void setSource(DroppableElementUI source) {
		this.source = source;
	}
}
