package com.ss.client.gui.element.event.impl;

import com.ss.client.gui.element.DraggableElementUI;
import com.ss.client.gui.element.DroppableElementUI;
import com.ss.client.gui.element.event.ElementEventType;

/**
 * Реализация события о завершении перемещения элемента.
 * 
 * @author Ronn
 */
public class DraggableStopEvent extends AbstractElementUIEvent<DraggableElementUI> {

	public static final ElementEventType DRAGGABLE_STOP_EVENT = new ElementEventType() {
	};

	/** исходный контейнер */
	private DroppableElementUI source;

	/** небыл найден контейнер */
	private boolean notFound;

	@Override
	public ElementEventType getEventType() {
		return DRAGGABLE_STOP_EVENT;
	}

	/**
	 * @return исходный контейнер.
	 */
	public DroppableElementUI getSource() {
		return source;
	}

	/**
	 * @return небыл найден контейнер.
	 */
	public boolean isNotFound() {
		return notFound;
	}

	/**
	 * @param notFound небыл найден контейнер.
	 */
	public void setNotFound(boolean notFound) {
		this.notFound = notFound;
	}

	/**
	 * @param source исходный контейнер.
	 */
	public void setSource(DroppableElementUI source) {
		this.source = source;
	}
}
