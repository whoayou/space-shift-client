package com.ss.client.gui.element;

/**
 * Интерфейс для реализации элемента сообщения.
 * 
 * @author Ronn
 */
public interface MessageElementUI extends LabelUI {

	public static final String TEXT_ELEMENT_ID = "#text_element";

	public static final String PROPERTY_TEXT_PADDING_WIDTH = "textPaddingWidth";
	public static final String PROPERTY_TEXT_PADDING_HEIGHT = "textPaddingHeight";

	/**
	 * @return отступ текста по высоте от краев.
	 */
	public int getTextPaddingHeight();

	/**
	 * @return отступ текста по ширине от краев.
	 */
	public int getTextPaddingWidth();

	/**
	 * @param textPaddingHeight отступ текста по высоте от краев.
	 */
	public void setTextPaddingHeight(int textPaddingHeight);

	/**
	 * @param textPaddingWidth отступ текста по ширине от краев.
	 */
	public void setTextPaddingWidth(int textPaddingWidth);
}
