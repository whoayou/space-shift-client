package com.ss.client.gui.element.filter;

/**
 * Интерфейс для реализации конвертера реального текста в отображаемый.
 * 
 * @author Ronn
 */
public interface TextFieldDisplayFormat {

	/**
	 * Получение отображаемого текста.
	 * 
	 * @param original оригинальный фрагмент текста.
	 * @param start позиция старта фрагмента.
	 * @param end позиция конца фрагмента.
	 * @return отображаемый фрагмент.
	 */
	public CharSequence getDisplayText(CharSequence original, int start, int end);
}
