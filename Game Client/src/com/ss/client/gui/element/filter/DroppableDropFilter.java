package com.ss.client.gui.element.filter;

import com.ss.client.gui.element.DraggableElementUI;
import com.ss.client.gui.element.DroppableElementUI;

/**
 * Интерфейс для реализации фильтра принимаемых элементов контейнером
 * перемещаещихся элементов.
 * 
 * @author Ronn
 */
public interface DroppableDropFilter {

	/**
	 * Можно ли принять такой элемент.
	 * 
	 * @param source исходный контейнер элемента.
	 * @param draggable перемещаемый элемент.
	 * @param target целевой контейнер элемента.
	 * @return можно ли принять такой элемент.
	 */
	public boolean accept(DroppableElementUI source, DraggableElementUI draggable, DroppableElementUI target);
}