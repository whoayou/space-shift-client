package com.ss.client.gui.element.filter;

/**
 * Интерфейс для реализации фильтра ввода текста.
 * 
 * @author Ronn
 */
public interface TextFieldInputFilter {

	/**
	 * Проверка возможности ввода указанного символа.
	 * 
	 * @param index позиция в тексте.
	 * @param newChar вводимый символ.
	 * @return можно ли его ввести.
	 */
	public boolean acceptInput(int index, char newChar);

	/**
	 * Проверка возможности ввода указанного набора символов.
	 * 
	 * @param index позиция в текщем тексте.
	 * @param newChars набор новых символов.
	 * @return можно ли их ввести.
	 */
	public boolean acceptInput(int index, CharSequence newChars);
}
