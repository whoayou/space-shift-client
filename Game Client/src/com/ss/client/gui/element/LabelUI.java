package com.ss.client.gui.element;

import de.lessvoid.nifty.layout.align.HorizontalAlign;
import de.lessvoid.nifty.layout.align.VerticalAlign;
import de.lessvoid.nifty.spi.render.RenderFont;
import de.lessvoid.nifty.tools.Color;

/**
 * Интерфейс для реализации элемента надписи.
 * 
 * @author Ronn
 */
public interface LabelUI extends ElementUI {

	public static final String PROPERTY_WRAP = "wrap";
	public static final String PROPERTY_TEXT = "text";

	/**
	 * @return текущий цвет надписи.
	 */
	public Color getColor();

	/**
	 * @return текущий шрифт надписи.
	 */
	public RenderFont getFont();

	/**
	 * @return оригинальный текст.
	 */
	public String getOriginalText();

	/**
	 * @return разбитый текст.
	 */
	public String getWrappedText();

	/**
	 * @return активирован ли перенос по строкам.
	 */
	public boolean isLineWrapping();

	/**
	 * @param color новый цвет надписи.
	 */
	public void setColor(Color color);

	/**
	 * @param font новый шрифт надписи.
	 */
	public void setFont(RenderFont font);

	/**
	 * @param text новый текст.
	 */
	public void setText(String text);

	/**
	 * @param newTextHAlign горизонтальное выравнивание.
	 */
	public void setTextHAlign(HorizontalAlign newTextHAlign);

	/**
	 * @param newTextVAlign вертикальное выравнивание.
	 */
	public void setTextVAlign(VerticalAlign newTextVAlign);
}
