package com.ss.client.gui.element;

import rlib.util.array.Array;

/**
 * Интерфейс для реализации просмотра списка.
 * 
 * @author Ronn
 */
public interface ListUI<T> extends ElementUI {

	public static final String PROP_ITEM_FACTORY_CLASS = "itemFactoryClass";

	/**
	 * @param item новый итем в списке.
	 */
	public void addItem(T item);

	/**
	 * @param container контейнер, в котороый положить все итемы из списка.
	 */
	public void addItemsTo(Array<T> container);

	/**
	 * Очистка списка.
	 */
	public void clear();

	/**
	 * @param index индекс итема в списке.
	 * @return итем из списка.
	 */
	public T getItem(int index);

	/**
	 * @return выбранный итем в списке.
	 */
	public T getSelected();

	/**
	 * @return индекс выбранного итема в списке.
	 */
	public int getSelectedIndex();

	/**
	 * @param item старый итем в списке.
	 */
	public void removeItem(T item);

	/**
	 * @param item выбранный итем в списке.
	 */
	public void setSelected(T item);

	/**
	 * @param index индекс выбранного итема в списке.
	 */
	public void setSelectedIndex(int index);

	/**
	 * @return размер списка.
	 */
	public int size();
}
