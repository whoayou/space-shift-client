package com.ss.client.gui.element.effect.builder.impl;

import com.ss.client.Game;
import com.ss.client.gui.element.builder.ElementUIBuilder;
import com.ss.client.gui.element.effect.builder.EffectUIBuilder;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.effects.EffectEventId;

/**
 * Базовая реализация конструктора эффекта.
 * 
 * @author Ronn
 */
public abstract class AbstractEffectUIBuilder<T> implements EffectUIBuilder {

	protected static final Game GAME = Game.getInstance();
	protected static final Nifty NIFTY = GAME.getNifty();

	/** конструктор эффекта */
	private final T builder;

	public AbstractEffectUIBuilder() {
		this.builder = createBuilder();
	}

	@Override
	public void addTo(ElementUIBuilder builder, EffectEventId eventId) {
		builder.addEffect(eventId, getBuilder());
	}

	/**
	 * @return констурктор эффекта.
	 */
	protected abstract T createBuilder();

	/**
	 * @return конструктор эффекта.
	 */
	public T getBuilder() {
		return builder;
	}

	@Override
	public String getEffectName() {
		return getClass().getName();
	}
}
