package com.ss.client.gui.element.effect.builder.impl;

import com.ss.client.gui.element.builder.ElementUIBuilder;
import com.ss.client.gui.element.effect.EffectUI;
import com.ss.client.gui.element.effect.builder.EffectTimeType;

import de.lessvoid.nifty.builder.EffectBuilder;
import de.lessvoid.nifty.effects.EffectEventId;
import de.lessvoid.nifty.tools.Color;

/**
 * Базовая реализация конструктора обычного эффекта.
 * 
 * @author Ronn
 */
public abstract class AbstractSimpleEffectUIBuilder extends AbstractEffectUIBuilder<EffectBuilder> {

	@Override
	public void addTo(ElementUIBuilder builder, EffectEventId eventId) {

		if(eventId == EffectEventId.onHover || eventId == EffectEventId.onEndHover || eventId == EffectEventId.onStartHover) {
			new RuntimeException("incorrect effect event id.");
		}

		super.addTo(builder, eventId);
	}

	@Override
	protected EffectBuilder createBuilder() {
		return new EffectBuilder(getEffectName());
	}

	/**
	 * @param color цвет эффекта.
	 */
	public void setColor(Color color) {
		getBuilder().effectParameter(EffectUI.PROP_COLOR, color.getColorString());
	}

	/**
	 * @param customKey дополнительный признак эффекта.
	 */
	public void setCustomKey(String customKey) {
		getBuilder().effectParameter(EffectUI.PROP_CUSTOM_KEY, customKey);
	}

	/**
	 * @param filename путь к файлу для эффекта.
	 */
	public void setFilename(String filename) {
		getBuilder().effectParameter(EffectUI.PROP_FILENAME, filename);
	}

	/**
	 * @param height высота эффекта.
	 */
	public void setHeight(String height) {
		getBuilder().effectParameter(EffectUI.PROP_HEIGHT, height);
	}

	/**
	 * @param inset отступы эффекта.
	 */
	public void setInset(String inset) {
		getBuilder().effectParameter(EffectUI.PROP_INSET, inset);
	}

	/**
	 * @param neverStopRendering бесконечный ли рендер эффекта.
	 */
	public void setNeverStopRendering(boolean neverStopRendering) {
		getBuilder().effectParameter(EffectUI.PROP_NEVER_STOP_RENDERING, String.valueOf(neverStopRendering));
	}

	/**
	 * @param post пост ли эффект.
	 */
	public void setPost(boolean post) {
		getBuilder().effectParameter(EffectUI.PROP_POST, String.valueOf(post));
	}

	/**
	 * @param timeType тип времни эффекта.
	 */
	public void setTimeType(EffectTimeType timeType) {
		getBuilder().effectParameter(EffectUI.PROP_TIME_TYPE, String.valueOf(timeType));
	}

	/**
	 * @param width ширина цветовой полоски.
	 */
	public void setWidth(String width) {
		getBuilder().effectParameter(EffectUI.PROP_WIDTH, width);
	}
}
