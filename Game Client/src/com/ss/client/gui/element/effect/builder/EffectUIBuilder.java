package com.ss.client.gui.element.effect.builder;

import com.ss.client.gui.element.builder.ElementUIBuilder;

import de.lessvoid.nifty.effects.EffectEventId;

/**
 * Интерфейс для реализации конструктора эффекта.
 * 
 * @author Ronn
 */
public interface EffectUIBuilder {

	/**
	 * Добавление этого эффекта в конструктор элемента.
	 * 
	 * @param builder конструктор элемента.
	 */
	public void addTo(ElementUIBuilder builder, EffectEventId eventId);

	/**
	 * @return название эффекта.
	 */
	public String getEffectName();
}
