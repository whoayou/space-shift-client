package com.ss.client.gui.element.effect.impl;

import rlib.logging.Logger;
import rlib.logging.Loggers;

import com.ss.client.gui.element.effect.EffectUI;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.effects.EffectProperties;
import de.lessvoid.nifty.effects.Falloff;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.render.NiftyRenderEngine;

/**
 * Базовая реализация эффекта элемента UI.
 * 
 * @author Ronn
 */
public abstract class AbstractEffectUI implements EffectUI {

	protected static final Logger LOGGER = Loggers.getLogger(EffectUI.class);

	@Override
	public void activate(Nifty nifty, Element element, EffectProperties parameter) {
	}

	@Override
	public void deactivate() {
	}

	@Override
	public void execute(Element element, float effectTime, Falloff falloff, NiftyRenderEngine r) {
	}
}
