package com.ss.client.gui.element.effect.builder.impl;

import com.ss.client.gui.element.builder.ElementUIBuilder;
import com.ss.client.gui.element.effect.EffectUI;
import com.ss.client.gui.element.effect.builder.EffectTimeType;

import de.lessvoid.nifty.builder.HoverEffectBuilder;
import de.lessvoid.nifty.effects.EffectEventId;
import de.lessvoid.nifty.tools.Color;

/**
 * Базовая реализация конструктора для эффектов при наведении курсора.
 * 
 * @author Ronn
 */
public abstract class AbstractHoverEffectUIBuilder extends AbstractEffectUIBuilder<HoverEffectBuilder> {

	@Override
	public void addTo(ElementUIBuilder builder, EffectEventId eventId) {

		if(!(eventId == EffectEventId.onHover || eventId == EffectEventId.onEndHover || eventId == EffectEventId.onStartHover)) {
			new RuntimeException("incorrect effect event id.");
		}

		super.addTo(builder, eventId);
	}

	@Override
	protected HoverEffectBuilder createBuilder() {
		return new HoverEffectBuilder(getEffectName());
	}

	public void setColor(Color color) {
		getBuilder().effectParameter(EffectUI.PROP_COLOR, color.getColorString());
	}

	public void setInset(String inset) {
		getBuilder().effectParameter(EffectUI.PROP_INSET, inset);
	}

	public void setNeverStopRendering(boolean neverStopRendering) {
		getBuilder().effectParameter(EffectUI.PROP_NEVER_STOP_RENDERING, String.valueOf(neverStopRendering));
	}

	public void setOffsetX(String offsetX) {
		getBuilder().effectParameter(EffectUI.PROP_OFFSET_X, offsetX);
	}

	public void setOffsetY(String offsetY) {
		getBuilder().effectParameter(EffectUI.PROP_OFFSET_Y, offsetY);
	}

	public void setPost(boolean post) {
		getBuilder().effectParameter(EffectUI.PROP_POST, String.valueOf(post));
	}

	public void setTimeType(EffectTimeType timeType) {
		getBuilder().effectParameter(EffectUI.PROP_TIME_TYPE, String.valueOf(timeType));
	}
}
