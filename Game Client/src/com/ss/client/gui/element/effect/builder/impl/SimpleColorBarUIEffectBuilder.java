package com.ss.client.gui.element.effect.builder.impl;

import com.ss.client.gui.element.effect.builder.EffectUIBuilder;
import com.ss.client.gui.element.effect.impl.ColorBarEffectUI;

/**
 * Реализация конструктора эффекта ColorBar.
 * 
 * @author Ronn
 */
public class SimpleColorBarUIEffectBuilder extends AbstractSimpleEffectUIBuilder {

	static {
		EffectUIBuilder builder = new SimpleColorBarUIEffectBuilder();
		NIFTY.registerEffect(builder.getEffectName(), ColorBarEffectUI.class.getName());
	}
}
