package com.ss.client.gui.element;

/**
 * Интерфейс для реализации контрола элемента регулятора.
 * 
 * @author Ronn
 */
public interface RegulatorUI extends ElementUI {

	public static final String VALUE_PANEL_ID = "#value_panel";
	public static final String BASE_PANEL_ID = "#base_panel";
	public static final String BUTTON_ADD_ID = "#button_add";
	public static final String BUTTON_SUB_ID = "#button_sub";

	public static final String METHOD_ADD_NAME = "regulatorAdd()";
	public static final String METHOD_SUB_NAME = "regulatorSub()";

	public static final String MINIMUM = "minimum";
	public static final String MAXIMUM = "maximum";
	public static final String CURRENT = "current";
	public static final String STEP = "step";

	/**
	 * @return текущее значение регулятора.
	 */
	public float getCurrent();

	/**
	 * @return максимальное значение регулятора.
	 */
	public float getMaximum();

	/**
	 * @return минимальное значение регулятора.
	 */
	public float getMinimum();

	/**
	 * @return шаг регулятора.
	 */
	public float getStep();

	/**
	 * @param current текущее значение регулятора.
	 */
	public void setCurrent(float current);

	/**
	 * @param maximum максимальное значение регулятора.
	 */
	public void setMaximum(float maximum);

	/**
	 * @param minimum минимальное значение регулятора.
	 */
	public void setMinimum(float minimum);

	/**
	 * @param step шаг регулятора.
	 */
	public void setStep(float step);
}
