package com.ss.client.gui.element;

import com.ss.client.gui.element.filter.TextFieldDeleteFilter;
import com.ss.client.gui.element.filter.TextFieldDisplayFormat;
import com.ss.client.gui.element.filter.TextFieldInputFilter;

import de.lessvoid.nifty.controls.textfield.TextFieldView;
import de.lessvoid.nifty.tools.Color;

/**
 * Интерфейс для реализации поля для ввода.
 * 
 * @author Ronn
 */
public interface TextFieldUI extends ElementUI, TextFieldView {

	public static final String PROPERTY_TEXT = "text";
	public static final String PROPERTY_MAX_LENGTH = "maxLength";
	public static final String TEXT_CURSOR_ID = "#cursor";
	public static final String TEXT_FIELD_ID = "#field";
	public static final String TEXT_ELEMENT_ID = "#text";
	public static final String TEXT_CURSOR_PANEL_ID = "#cursor_panel";

	/** неограниченый размер текста */
	public static final int UNLIMITED_LENGTH = -1;

	/**
	 * @return текущий цвет надписи.
	 */
	public Color getColor();

	/**
	 * @return отображаемый текст на элементе.
	 */
	public String getDisplayedText();

	/**
	 * @return реальный текст в элементе.
	 */
	public String getRealText();

	/**
	 * @param color новый цвет надписи.
	 */
	public void setColor(Color color);

	/**
	 * @param position позиция курсора для ввода.
	 */
	public void setCursorPosition(int position);

	/**
	 * @param filter фильтр удаления текста.
	 */
	public void setDeleteFilter(TextFieldDeleteFilter filter);

	/**
	 * @param format конвертер реального текста в отображаемый.
	 */
	public void setFormat(TextFieldDisplayFormat format);

	/**
	 * @param filter фильтр ввода текста.
	 */
	public void setInputFilter(TextFieldInputFilter filter);

	/**
	 * @param maxLength максимальная длинна вводимого текста.
	 */
	public void setMaxLength(int maxLength);

	/**
	 * @param text новый текст элемента.
	 */
	public void setText(CharSequence text);
}
