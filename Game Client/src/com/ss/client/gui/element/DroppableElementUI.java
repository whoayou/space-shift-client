package com.ss.client.gui.element;

import com.ss.client.gui.element.filter.DroppableDropFilter;

import de.lessvoid.nifty.EndNotify;

/**
 * Интерфейс для реализации контейнера перемещаещихся элементов.
 * 
 * @author Ronn
 */
public interface DroppableElementUI extends ElementUI {

	public static final String CONTAINER_ELEMENT_ID = "#container";

	/**
	 * Проверка совместимости элемента с контейнером.
	 * 
	 * @param source исходный контейнер.
	 * @param draggable перемещаемый элемент.
	 * @return подходит ли элемент.
	 */
	public boolean accept(final DroppableElementUI source, final DraggableElementUI draggable);

	/**
	 * Добавление нового фильтра принимаемых элементов.
	 * 
	 * @param filter новый фильтр.
	 */
	public void addFilter(final DroppableDropFilter filter);

	/**
	 * Обработка принятия элемента в контейнер.
	 * 
	 * @param draggable перемещенный элемент.
	 * @param endNotify информация о завершении процесса.
	 */
	public void drop(final DraggableElementUI draggable, final EndNotify endNotify);

	/**
	 * Обработка принятия элемента в контейнер.
	 * 
	 * @param draggable перемещенный элемент.
	 * @param endNotify информация о завершении процесса.
	 * @param notify уведомлять ли событием слушателей.
	 */
	public void drop(final DraggableElementUI draggable, final EndNotify endNotify, final boolean notify);

	/**
	 * @return размещенный элемент в контейнере.
	 */
	public DraggableElementUI getDraggable();

	/**
	 * Удаление старого фильтра принимаемых элементов.
	 * 
	 * @param filter старый фильтр.
	 */
	public void removeFilter(final DroppableDropFilter filter);

	/**
	 * @param draggable размещенный элемент в контейнере.
	 */
	public void setDraggable(DraggableElementUI draggable);
}
