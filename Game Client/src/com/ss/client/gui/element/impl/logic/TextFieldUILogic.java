package com.ss.client.gui.element.impl.logic;

import com.ss.client.gui.element.TextFieldUI;
import com.ss.client.gui.element.filter.TextFieldDeleteFilter;
import com.ss.client.gui.element.filter.TextFieldDisplayFormat;
import com.ss.client.gui.element.filter.TextFieldInputFilter;

import de.lessvoid.nifty.Clipboard;
import de.lessvoid.nifty.controls.textfield.TextFieldView;
import de.lessvoid.nifty.controls.textfield.format.FormatPassword;

/**
 * Реализация логики работы текстового поля для ввода.
 * 
 * @author Ronn
 */
public class TextFieldUILogic {

	/** стандартный фильтр */
	private static final TextFieldInputFilter DEFAULT_INPUT_FILTER = new TextFieldInputFilter() {

		@Override
		public boolean acceptInput(int index, char newChar) {
			return true;
		}

		@Override
		public boolean acceptInput(int index, CharSequence newChars) {
			return true;
		}
	};

	/** стандартный фильтр */
	private static final TextFieldDeleteFilter DEFAULT_DELETE_FILTER = new TextFieldDeleteFilter() {

		@Override
		public boolean acceptDelete(char oldChar, int index) {
			return true;
		}

		@Override
		public boolean acceptDelete(CharSequence oldSequence, int deleteStart, int deleteEnd) {
			return true;
		}
	};

	private static final TextFieldDisplayFormat DEFAULT_FORMAT = new TextFieldDisplayFormat() {

		@Override
		public CharSequence getDisplayText(CharSequence original, int start, int end) {
			return original.subSequence(start, end);
		}
	};

	/** текущий текст в элементе */
	private final StringBuilder text;

	/** буфер обмена */
	private final Clipboard clipboard;
	/** отображатель текста */
	private final TextFieldView view;

	/** фильтр ввода */
	private TextFieldInputFilter inputFilter;
	/** фильтр удаления */
	private TextFieldDeleteFilter deleteFilter;
	/** конвертер */
	private TextFieldDisplayFormat format;

	/** максимальная длинна строки */
	private int maxLength;
	/** текущая позиция курсора */
	private int cursorPosition;
	/** позиция начала выделения текста */
	private int selectionStart = -1;
	/** позиция конца выделения текста */
	private int selectionEnd = -1;

	/** позиция с которой наачалось выделение текста */
	private int selectionStartIndex;
	/** есть ли сейчас выделение */
	private boolean selecting;

	public TextFieldUILogic(final CharSequence newText, final Clipboard newClipboard, final TextFieldView textFieldView) {
		this.view = textFieldView;
		this.clipboard = newClipboard;
		this.maxLength = TextFieldUI.UNLIMITED_LENGTH;
		this.inputFilter = DEFAULT_INPUT_FILTER;
		this.deleteFilter = DEFAULT_DELETE_FILTER;
		this.format = DEFAULT_FORMAT;
		this.text = new StringBuilder(100);

		setText(newText);
	}

	/**
	 * Удаление текста через бэкспейс.
	 */
	public void backspace() {
		if(hasSelection()) {
			delete();
		} else if(getCursorPosition() > 0) {
			cursorLeft();
			delete();
		}
	}

	/**
	 * Копирование выделенного текста.
	 */
	public void copy() {

		final CharSequence selectedText = getDisplayedSelectedText();

		if(selectedText != null) {
			getClipboard().put(selectedText.toString());
		}
	}

	/**
	 * Перемещения курсора на шаг назад.
	 */
	public void cursorLeft() {
		moveCursor(-1);
	}

	/**
	 * Перемещение курсора на шаг вперед.
	 */
	public void cursorRight() {
		moveCursor(1);
	}

	/**
	 * Вырезание выделенного текста.
	 */
	public void cut() {

		final CharSequence selectedText = getDisplayedSelectedText();

		if(selectedText == null) {
			return;
		}

		getClipboard().put(selectedText.toString());
		delete();
	}

	/**
	 * Удаление символов на текущей позиции кусора.
	 */
	public void delete() {

		TextFieldDeleteFilter deleteFilter = getDeleteFilter();
		StringBuilder text = getText();

		int cursorPosition = getCursorPosition();

		if(hasSelection()) {
			deleteSelectedText();
		} else if((cursorPosition < text.length()) && deleteFilter.acceptDelete(text, cursorPosition, cursorPosition + 1)) {
			text.delete(cursorPosition, cursorPosition + 1);
		} else {
			return;
		}

		getView().textChangeEvent(text.toString());
	}

	/**
	 * Удаление выделенного текста.
	 */
	private void deleteSelectedText() {

		if(!hasSelection()) {
			throw new IllegalStateException("Can't delete selected text without selection");
		}

		TextFieldDeleteFilter deleteFilter = getDeleteFilter();
		StringBuilder text = getText();

		int selectionStart = getSelectionStart();
		int selectionEnd = getSelectionEnd();

		if(deleteFilter.acceptDelete(text, selectionStart, selectionEnd)) {
			text.delete(selectionStart, selectionEnd);
			setCursorPosition(selectionStart);
			resetSelection();
		}
	}

	/**
	 * Отмена выделения текста.
	 */
	public void endSelecting() {
		setSelecting(false);
	}

	/**
	 * Вставка нового символа в текст.
	 * 
	 * @param inputChar новый символ.
	 * @return вставился ли новый символ.
	 */
	private boolean filterAndInsert(final char inputChar) {

		StringBuilder text = getText();

		int cursorPosition = getCursorPosition();
		int maxLength = getMaxLength();

		if((maxLength == TextFieldUI.UNLIMITED_LENGTH) || (text.length() < maxLength)) {
			if(getInputFilter().acceptInput(cursorPosition, inputChar)) {
				text.insert(cursorPosition, inputChar);
				setCursorPosition(cursorPosition + 1);
				return true;
			}
		}

		return false;
	}

	/**
	 * Вставка новой строки в текст.
	 * 
	 * @param chars новый текст.
	 * @return был ли вставлен текст.
	 */
	private boolean filterAndInsert(final CharSequence chars) {

		if(chars.length() == 0) {
			return false;
		}

		StringBuilder text = getText();

		int cursorPosition = getCursorPosition();
		int maxLength = getMaxLength();

		if((maxLength == TextFieldUI.UNLIMITED_LENGTH) || (text.length() < maxLength)) {

			final int insertCount;

			if(maxLength == TextFieldUI.UNLIMITED_LENGTH) {
				insertCount = chars.length();
			} else {
				insertCount = Math.min(maxLength - text.length(), chars.length());
			}

			final CharSequence insertSequence = chars.subSequence(0, insertCount);

			if(getInputFilter().acceptInput(cursorPosition, insertSequence)) {
				text.insert(cursorPosition, chars);
				setCursorPosition(cursorPosition + insertCount);
				return true;
			}
		}

		return false;
	}

	private CharSequence filterNewLines(final String input) {
		return input.replaceAll("\\r\\n|\\r|\\n", "");
	}

	/**
	 * @return буфер обмена.
	 */
	public Clipboard getClipboard() {
		return clipboard;
	}

	/**
	 * @return текущая позиция курсора.
	 */
	public int getCursorPosition() {
		return cursorPosition;
	}

	/**
	 * @return фильтр удаления.
	 */
	public TextFieldDeleteFilter getDeleteFilter() {
		return deleteFilter;
	}

	/**
	 * @return отображаемый выделенный текст.
	 */
	public CharSequence getDisplayedSelectedText() {

		if(!hasSelection()) {
			return null;
		}

		return getFormat().getDisplayText(getText(), getSelectionStart(), getSelectionEnd());
	}

	/**
	 * @return отображаемый текст.
	 */
	public CharSequence getDisplayedText() {
		StringBuilder text = getText();
		return getFormat().getDisplayText(text, 0, text.length());
	}

	/**
	 * @return конвертер.
	 */
	public TextFieldDisplayFormat getFormat() {
		return format;
	}

	/**
	 * @return фильтр ввода.
	 */
	public TextFieldInputFilter getInputFilter() {
		return inputFilter;
	}

	/**
	 * @return максимальная длинна.
	 */
	private int getMaxLength() {
		return maxLength;
	}

	/**
	 * Get the character that is used to mask the password.
	 * 
	 * @return the character masking the password or {@code null} in case no
	 * character is used and the normal text is displayed
	 */
	public Character getPasswordChar() {
		if(format instanceof FormatPassword) {
			return ((FormatPassword) format).getPasswordChar();
		}
		return null;
	}

	/**
	 * @return реальный выделенный текст.
	 */
	public CharSequence getRealSelectedText() {

		if(!hasSelection()) {
			return null;
		}

		int selectionStart = getSelectionStart();
		int selectionEnd = getSelectionEnd();

		return getText().subSequence(selectionStart, selectionEnd);
	}

	/**
	 * @return реальный текст элемента.
	 */
	public CharSequence getRealText() {
		return text;
	}

	/**
	 * @return конец выделения текста.
	 */
	public int getSelectionEnd() {
		return selectionEnd;
	}

	/**
	 * @return длинна выделенного текста.
	 */
	public int getSelectionLength() {

		if(hasSelection()) {
			return getSelectionEnd() - getSelectionStart();
		}

		return 0;
	}

	/**
	 * @return начало выделения текста.
	 */
	public int getSelectionStart() {
		return selectionStart;
	}

	/**
	 * @return позиция с которой наачалось выделение текста.
	 */
	private int getSelectionStartIndex() {
		return selectionStartIndex;
	}

	/**
	 * @return текущий текст в элементе.
	 */
	private StringBuilder getText() {
		return text;
	}

	/**
	 * @return отображатель текста.
	 */
	public TextFieldView getView() {
		return view;
	}

	/**
	 * @return есть ли выделение текста.
	 */
	public boolean hasSelection() {
		return (selectionStart != -1) && (selectionEnd != -1);
	}

	/**
	 * Вставка нового символа.
	 * 
	 * @param c новый символ.
	 */
	public void insert(final char c) {

		if(hasSelection()) {
			deleteSelectedText();
		}

		if(filterAndInsert(c)) {
			getView().textChangeEvent(getText().toString());
		}
	}

	/**
	 * Вставка новой строки в текст.
	 * 
	 * @param chars новая строка.
	 */
	public void insert(final CharSequence chars) {

		if(hasSelection()) {
			deleteSelectedText();
		}

		if(filterAndInsert(chars)) {
			getView().textChangeEvent(getText().toString());
		}
	}

	/**
	 * @return есть ли сейчас выделение.
	 */
	private boolean isSelecting() {
		return selecting;
	}

	/**
	 * Перемещение курсора на определнный шаг.
	 * 
	 * @param direction кол-во пропускаемых символов.
	 */
	private void moveCursor(final int direction) {
		updateCursorPosition(getCursorPosition() + direction);
	}

	/**
	 * Вставка из буфера строки.
	 */
	public void put() {

		final String clipboardText = getClipboard().get();

		if(clipboardText != null) {
			insert(filterNewLines(clipboardText));
		}
	}

	/**
	 * Сбор выделения текста.
	 */
	public void resetSelection() {
		setSelectionStart(-1);
		setSelectionEnd(-1);
		setSelecting(false);
	}

	/**
	 * Выделение всего текста.
	 */
	public void selectAll() {

		StringBuilder text = getText();

		setSelectionStartIndex(0);
		setSelectionStart(0);
		setSelectionEnd(text.length());
		setCursorPosition(text.length());
	}

	/**
	 * Обновление границ выделение текста.
	 */
	private void selectionFromCursorPosition() {

		int selectionStartIndex = getSelectionStartIndex();
		int cursorPosition = getCursorPosition();

		if(!isSelecting() || (cursorPosition == selectionStartIndex)) {
			resetSelection();
		} else if(cursorPosition > selectionStartIndex) {
			setSelectionStart(selectionStartIndex);
			setSelectionEnd(cursorPosition);
		} else {
			setSelectionStart(cursorPosition);
			setSelectionEnd(selectionStartIndex);
		}
	}

	/**
	 * @param cursorPosition текущая позиция курсора.
	 */
	private void setCursorPosition(int cursorPosition) {
		this.cursorPosition = cursorPosition;
	}

	/**
	 * @param filter фильтр удаления.
	 */
	public void setDeleteFilter(final TextFieldDeleteFilter filter) {
		this.deleteFilter = (filter == null) ? DEFAULT_DELETE_FILTER : filter;
	}

	/**
	 * @param newFormat конвертер.
	 */
	public void setFormat(final TextFieldDisplayFormat newFormat) {
		this.format = (newFormat == null) ? DEFAULT_FORMAT : newFormat;
	}

	/**
	 * @param filter фильтр ввода.
	 */
	public void setInputFilter(final TextFieldInputFilter filter) {
		this.inputFilter = (filter == null) ? DEFAULT_INPUT_FILTER : filter;
	}

	/**
	 * @param maxLength новая ммаксимальная длинна текста.
	 */
	public void setMaxLength(final int maxLength) {

		StringBuilder text = getText();

		this.maxLength = maxLength;

		if(maxLength != TextFieldUI.UNLIMITED_LENGTH) {
			if(text.length() > maxLength) {
				text.setLength(maxLength);
				updateCursorPosition(Math.min(getCursorPosition(), text.length()));
				getView().textChangeEvent(text.toString());
			}
		}
	}

	/**
	 * @param selecting есть ли сейчас выделение.
	 */
	private void setSelecting(boolean selecting) {
		this.selecting = selecting;
	}

	/**
	 * @param selectionEnd позиция конца выделения текста.
	 */
	private void setSelectionEnd(int selectionEnd) {
		this.selectionEnd = selectionEnd;
	}

	/**
	 * @param selectionStart позиция начала выделения текста.
	 */
	private void setSelectionStart(int selectionStart) {
		this.selectionStart = selectionStart;
	}

	/**
	 * @param selectionStartIndex позиция с которой наачалось выделение текста.
	 */
	private void setSelectionStartIndex(int selectionStartIndex) {
		this.selectionStartIndex = selectionStartIndex;
	}

	/**
	 * Вставка нового текста.
	 */
	public void setText(final CharSequence newText) {

		StringBuilder text = getText();
		text.setLength(0);

		if(newText != null) {
			text.append(newText);
		}

		setCursorPosition(0);
		resetSelection();
	}

	/**
	 * Установка текста с уведомление представления.
	 * 
	 * @param newText новый текст.
	 */
	public void setTextAndNotify(final CharSequence newText) {

		setText(newText);

		if((newText != null) && (newText.length() > 0)) {
			getView().textChangeEvent(newText.toString());
		}
	}

	/**
	 * Старт выделения текста.
	 */
	public void startSelecting() {
		setSelecting(true);
		setSelectionStartIndex(getCursorPosition());
	}

	/**
	 * Перенос курсора в начало текста.
	 */
	public void toFirstPosition() {
		updateCursorPosition(0);
	}

	/**
	 * Перенос курсора в конец текста.
	 */
	public void toLastPosition() {
		updateCursorPosition(Integer.MAX_VALUE);
	}

	/**
	 * @param newIndex новая позиция курсора.
	 */
	public void updateCursorPosition(final int newIndex) {
		final int clampedIndex = Math.max(0, Math.min(newIndex, text.length()));

		if(cursorPosition != clampedIndex) {
			cursorPosition = clampedIndex;
			selectionFromCursorPosition();
		}
	}
}
