package com.ss.client.gui.element.impl;

import java.util.List;
import java.util.ListIterator;
import java.util.Properties;

import rlib.util.array.Array;

import com.ss.client.gui.element.DraggableElementUI;
import com.ss.client.gui.element.DroppableElementUI;
import com.ss.client.gui.element.event.impl.DraggableStartEvent;
import com.ss.client.gui.element.event.impl.DraggableStopEvent;
import com.ss.client.gui.model.ElementUtils;
import com.ss.client.util.GameUtil;

import de.lessvoid.nifty.EndNotify;
import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.controls.FocusHandler;
import de.lessvoid.nifty.controls.NiftyInputControl;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.input.NiftyInputEvent;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.tools.SizeValue;
import de.lessvoid.xml.xpp3.Attributes;

/**
 * Реализация перемещаемого элемента.
 * 
 * @author Ronn
 */
public class DraggableElementUIImpl extends AbstractElementUI implements DraggableElementUI {

	public static final String METHOD_NAME_PRIMARY_CLICK = "dragStart()";
	public static final String METHOD_NAME_PRIMARY_CLICK_MOUSE_MOVE = "drag()";
	public static final String METHOD_NAME_PRIMARY_CLICK_RELEASE = "dragStop()";

	private static final String POPUP = "draggablePopup";

	private final ThreadLocal<DraggableStartEvent> localDraggableStartEvent = new ThreadLocal<DraggableStartEvent>() {

		@Override
		protected DraggableStartEvent initialValue() {
			return new DraggableStartEvent();
		}
	};

	private final ThreadLocal<DraggableStopEvent> localDraggableStopEvent = new ThreadLocal<DraggableStopEvent>() {

		@Override
		protected DraggableStopEvent initialValue() {
			return new DraggableStopEvent();
		}
	};

	private Nifty nifty;

	private Screen screen;

	/** перемещаемый элемент */
	private Element draggable;
	/** оригинальный родительский элемент */
	private Element originalParent;
	/** элемент для размещения элемента */
	private Element popup;

	/** текущий контейнер элемента */
	private DroppableElementUI droppable;

	/** изначальное положение */
	private SizeValue originalConstraintX;
	/** изначальное положение */
	private SizeValue originalConstraintY;

	/** изначальное положение Х */
	private int originalPositionX;
	/** изначальное положение У */
	private int originalPositionY;

	/** точка старта перемещения Х */
	private int dragStartX;
	/** точка старта перемещения У */
	private int dragStartY;

	/** возвращать на исходную позицию поле перемещения */
	private boolean revert;
	/** может ли этот элемент размещаться в контейнере */
	private boolean dropEnabled;
	/** идет ли сейчас процесс перемещения */
	private boolean dragged;

	@Override
	public void bind(final Nifty nifty, final Screen screen, final Element element, final Properties properties, final Attributes attributes) {
		super.bind(element);
		this.nifty = nifty;
		this.screen = screen;
		this.draggable = element;
		this.revert = attributes.getAsBoolean(PROPERTY_REVERTABLE, true);
		this.dropEnabled = attributes.getAsBoolean(PROPERTY_DROPPABLE, true);
	}

	/**
	 * Отмена перемещения.
	 */
	private void dragCancel(boolean notFound) {

		Element draggable = getDraggable();

		if(isRevert()) {
			draggable.setConstraintX(getOriginalConstraintX());
			draggable.setConstraintY(getOriginalConstraintY());
		} else {
			draggable.setConstraintX(GameUtil.getPixelSize(draggable.getX()));
			draggable.setConstraintY(GameUtil.getPixelSize(draggable.getY()));
		}

		moveToOriginalParent();
		notifyStop(notFound);
	}

	@Override
	public boolean elementMouseRelease() {

		if(!isDragged()) {
			return super.elementMouseRelease();
		}

		Element element = findDroppableAtCurrentCoordinates();

		if(element == getOriginalParent()) {
			dragCancel(false);
		} else if(element == null) {
			dragCancel(true);
		} else {

			DroppableElementUI droppable = element.getControl(DroppableElementUI.class);

			if(droppable.accept(getDroppable(), this)) {
				droppable.drop(this, getEndNotify());
			} else {
				dragCancel(false);
			}
		}

		setDragged(false);
		return super.elementMouseRelease();
	}

	@Override
	public boolean elementPrimaryClick(int mouseX, int mouseY) {

		if(isDragged()) {
			return super.elementPrimaryClick(mouseX, mouseY);
		}

		Element draggable = getDraggable();

		setOriginalParent(draggable.getParent());
		setOriginalPositionX(draggable.getX());
		setOriginalPositionY(draggable.getY());
		setOriginalConstraintX(draggable.getConstraintX());
		setOriginalConstraintY(draggable.getConstraintY());
		setDragStartX(mouseX);
		setDragStartY(mouseY);

		if(draggable.isMouseInsideElement(mouseX, mouseY)) {
			startDrag();
		} else {
			moveDraggableOnTop();
		}

		return super.elementPrimaryClick(mouseX, mouseY);
	}

	@Override
	public boolean elementPrimaryClickMouseMove(int mouseX, int mouseY) {

		if(!isDragged()) {
			return super.elementPrimaryClickMouseMove(mouseX, mouseY);
		}

		int newPositionX = getOriginalPositionX() + mouseX - getDragStartX();
		int newPositionY = getOriginalPositionY() + mouseY - getDragStartY();

		Element draggable = getDraggable();
		draggable.setConstraintX(GameUtil.getPixelSize(newPositionX));
		draggable.setConstraintY(GameUtil.getPixelSize(newPositionY));

		Element popup = getPopup();

		if(popup != null) {
			popup.layoutElements();
		}

		return super.elementPrimaryClickMouseMove(mouseX, mouseY);
	}

	/**
	 * Поиск контейнера дял перемещаемого элемента по указанным координатам.
	 */
	private Element findDroppableAtCoordinates(final Element context, final int x, final int y) {

		Array<Element> elements = context.getFastElements();
		Element[] array = elements.array();

		for(int i = elements.size() - 1; i >= 0; i--) {

			Element element = array[i];

			// we can only drop stuff on visible droppables
			boolean mouseInsideAndVisible = element.isMouseInsideElement(x, y) && element.isVisible();
			if(mouseInsideAndVisible && isDroppable(element)) {
				return element;
			}

			// nothing found for this element check it's child elements
			Element droppable = findDroppableAtCoordinates(element, x, y);
			if(droppable != null) {
				return droppable;
			}
		}

		return null;
	}

	/**
	 * Поиск подходящего контейнера для текущего элемента.
	 */
	private Element findDroppableAtCurrentCoordinates() {

		if(isDropEnabled()) {

			Element draggable = getDraggable();

			int dragAnkerX = draggable.getX() + draggable.getWidth() / 2;
			int dragAnkerY = draggable.getY() + draggable.getHeight() / 2;

			Element popup = getPopup();

			List<Element> layers = screen.getLayerElements();
			ListIterator<Element> iter = layers.listIterator(layers.size());

			while(iter.hasPrevious()) {

				Element layer = iter.previous();

				if(popup != null && layer != popup) {

					Element droppable = findDroppableAtCoordinates(layer, dragAnkerX, dragAnkerY);

					if(droppable != null) {
						return droppable;
					}
				}
			}

			return null;
		}

		return getOriginalParent();
	}

	@Override
	public Element getDraggable() {
		return draggable;
	}

	/**
	 * @return точка старта перемещения Х.
	 */
	public int getDragStartX() {
		return dragStartX;
	}

	/**
	 * @return точка старта перемещения У.
	 */
	public int getDragStartY() {
		return dragStartY;
	}

	@Override
	public DroppableElementUI getDroppable() {
		return droppable;
	}

	@Override
	public EndNotify getEndNotify() {

		if(popup == null) {
			return null;
		}

		return new EndNotify() {

			@Override
			public void perform() {

				nifty.closePopup(popup.getId(), new EndNotify() {

					@Override
					public void perform() {
						draggable.reactivate();
						popup.markForRemoval(new EndNotify() {

							@Override
							public void perform() {
								popup = null;
							}
						});
					}
				});
			}
		};
	}

	/**
	 * @return изначальное положение.
	 */
	public SizeValue getOriginalConstraintX() {
		return originalConstraintX;
	}

	/**
	 * @return изначальное положение.
	 */
	public SizeValue getOriginalConstraintY() {
		return originalConstraintY;
	}

	/**
	 * @return оригинальный родительский элемент.
	 */
	public Element getOriginalParent() {
		return originalParent;
	}

	/**
	 * @return изначальное положение Х.
	 */
	public int getOriginalPositionX() {
		return originalPositionX;
	}

	/**
	 * @return изначальное положение У.
	 */
	public int getOriginalPositionY() {
		return originalPositionY;
	}

	/**
	 * @return элемент для размещения элемента.
	 */
	public Element getPopup() {
		return popup;
	}

	@Override
	public boolean inputEvent(final NiftyInputEvent inputEvent) {
		return false;
	}

	/**
	 * @return идет ли сейчас процесс перемещения.
	 */
	public boolean isDragged() {
		return dragged;
	}

	/**
	 * @return может ли этот элемент размещаться в контейнере.
	 */
	public boolean isDropEnabled() {
		return dropEnabled;
	}

	/**
	 * @return можно ли разместить текущий элемент в указанном.
	 */
	private boolean isDroppable(final Element element) {

		NiftyInputControl control = element.getAttachedInputControl();

		if(control != null) {
			return control.getController() instanceof DroppableElementUI;
		}

		return false;
	}

	/**
	 * @return возвращать на исходную позицию поле перемещения.
	 */
	public boolean isRevert() {
		return revert;
	}

	private void moveDraggableOnTop() {

		getDraggable().markForMove(getOriginalParent(), new EndNotify() {

			@Override
			public void perform() {
				getDraggable().reactivate();
			}
		});
	}

	private void moveDraggableToPopup() {

		Element popup = nifty.createPopup(POPUP);
		setPopup(popup);

		nifty.showPopup(screen, popup.getId(), null);

		getDraggable().markForMove(popup, new EndNotify() {

			@Override
			public void perform() {

				Element draggable = getDraggable();
				draggable.setConstraintX(GameUtil.getPixelSize(getOriginalPositionX()));
				draggable.setConstraintY(GameUtil.getPixelSize(getOriginalPositionY()));

				ElementUtils.updateLayout(draggable.getParent());

				FocusHandler focusHandler = draggable.getFocusHandler();
				focusHandler.requestExclusiveMouseFocus(draggable);
			}
		});
	}

	/**
	 * Перемещение элемента к оригинальному родительскому элементу.
	 */
	private void moveToOriginalParent() {
		getDraggable().markForMove(getOriginalParent(), getEndNotify());
	}

	/**
	 * Уведомление о старте перемещения.
	 */
	private void notifyStart() {

		DraggableStartEvent event = localDraggableStartEvent.get();
		event.setElement(this);
		event.setSource(getDroppable());

		notifyEvent(event);
	}

	/**
	 * Уведомление о прекращения перемещении.
	 */
	private void notifyStop(boolean notFound) {

		DraggableStopEvent event = localDraggableStopEvent.get();
		event.setElement(this);
		event.setSource(getDroppable());
		event.setNotFound(notFound);

		notifyEvent(event);
	}

	@Override
	public void onStartScreen() {
	}

	@Override
	public void setDraggable(Element draggable) {
		this.draggable = draggable;
	}

	/**
	 * @param dragged идет ли сейчас процесс перемещения.
	 */
	public void setDragged(boolean dragged) {
		this.dragged = dragged;
	}

	/**
	 * @param dragStartX точка старта перемещения Х.
	 */
	public void setDragStartX(int dragStartX) {
		this.dragStartX = dragStartX;
	}

	/**
	 * @param dragStartY точка старта перемещения У.
	 */
	public void setDragStartY(int dragStartY) {
		this.dragStartY = dragStartY;
	}

	@Override
	public void setDroppable(DroppableElementUI droppable) {
		this.droppable = droppable;
	}

	/**
	 * @param originalConstraintX изначальное положение.
	 */
	public void setOriginalConstraintX(SizeValue originalConstraintX) {
		this.originalConstraintX = originalConstraintX;
	}

	/**
	 * @param originalConstraintY изначальное положение.
	 */
	public void setOriginalConstraintY(SizeValue originalConstraintY) {
		this.originalConstraintY = originalConstraintY;
	}

	/**
	 * @param originalParent оригинальный родительский элемент.
	 */
	public void setOriginalParent(Element originalParent) {
		this.originalParent = originalParent;
	}

	/**
	 * @param originalPositionX изначальное положение Х.
	 */
	public void setOriginalPositionX(int originalPositionX) {
		this.originalPositionX = originalPositionX;
	}

	/**
	 * @param originalPositionY изначальное положение У.
	 */
	public void setOriginalPositionY(int originalPositionY) {
		this.originalPositionY = originalPositionY;
	}

	/**
	 * @param popup элемент для размещения элемента.
	 */
	public void setPopup(Element popup) {
		this.popup = popup;
	}

	/**
	 * Запуск таскания элемента.
	 */
	private void startDrag() {
		moveDraggableToPopup();
		setDragged(true);
		notifyStart();
	}
}
