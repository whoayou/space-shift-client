package com.ss.client.gui.element.impl;

import java.util.Properties;

import rlib.util.ClassUtil;
import rlib.util.array.Array;
import rlib.util.linkedlist.LinkedList;
import rlib.util.linkedlist.LinkedLists;
import rlib.util.linkedlist.Node;
import rlib.util.table.Table;
import rlib.util.table.Tables;

import com.ss.client.gui.element.ListElementUI;
import com.ss.client.gui.element.ListUI;
import com.ss.client.gui.element.ScrollPanelUI;
import com.ss.client.gui.element.event.ElementUIEvent;
import com.ss.client.gui.element.event.impl.PrimaryClickEvent;
import com.ss.client.gui.element.event.impl.SelectedChangeEvent;
import com.ss.client.gui.element.impl.list.LabelListElementUIFactory;
import com.ss.client.gui.element.list.ListElementUIFactory;
import com.ss.client.gui.element.listener.ElementUIEventListener;
import com.ss.client.util.GameUtil;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.xml.xpp3.Attributes;

/**
 * Реализация списка элементов.
 * 
 * @author Ronn
 */
public class ListUIImpl<T> extends AbstractElementUI implements ListUI<T> {

	@SuppressWarnings("rawtypes")
	public static final LabelListElementUIFactory DEFAULT_ITEM_FACTORY = new LabelListElementUIFactory<>();

	/** слушатель событий с попыткой выделить элемент */
	private final ElementUIEventListener<ListElementUI<T>> elementSelectedListener = new ElementUIEventListener<ListElementUI<T>>() {

		@Override
		public void notifyEvent(ElementUIEvent<ListElementUI<T>> event) {
			processSelected(event.getElement());
		}
	};

	private final ThreadLocal<SelectedChangeEvent<ListUI<T>, T>> localSelectedChangeEvent = new ThreadLocal<SelectedChangeEvent<ListUI<T>, T>>() {

		@Override
		protected SelectedChangeEvent<ListUI<T>, T> initialValue() {
			return new SelectedChangeEvent<>();
		}
	};

	/** мапинг итемов с элементами */
	private final Table<T, ListElementUI<T>> mapping;
	/** список итемов */
	private final LinkedList<T> items;
	/** список элементов */
	private final LinkedList<ListElementUI<T>> elements;

	/** фабрика элементов итемов */
	private ListElementUIFactory<T> itemFactory;

	/** скролируемая панель */
	private ScrollPanelUI scrollPanel;

	/** окнтроллер UI */
	private Nifty nifty;
	/** экран UI */
	private Screen screen;
	/** панель для размещения элементов итемов */
	private Element content;

	/** выделенный итем */
	private T selected;

	public ListUIImpl() {
		this.items = LinkedLists.newLinkedList(Object.class);
		this.elements = LinkedLists.newLinkedList(ListElementUI.class);
		this.mapping = Tables.newObjectTable();
	}

	@Override
	public void addItem(T item) {

		ListElementUIFactory<T> itemFactory = getItemFactory();

		Element contentPanel = getContent();

		ListElementUI<T> control = itemFactory.build(item, contentPanel, getNifty(), getScreen(), items.size());
		control.addElementListener(PrimaryClickEvent.EVENT_TYPE, getElementSelectedListener());

		Table<T, ListElementUI<T>> mapping = getMapping();
		mapping.put(item, control);

		items.add(item);
		elements.add(control);

		relayout();
	}

	@Override
	public void addItemsTo(Array<T> container) {

		for(Node<T> node = items.getFirstNode(); node != null; node = node.getNext()) {

			T item = node.getItem();

			if(item != null) {
				container.add(item);
			}
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void bind(Nifty nifty, Screen screen, Element element, Properties parameter, Attributes attributes) {
		super.bind(nifty, screen, element, parameter, attributes);

		this.screen = screen;
		this.nifty = nifty;
		this.scrollPanel = element.findControl("#scroll_panel", ScrollPanelUI.class);
		this.content = element.findElementByName("#content_panel");

		String factoryClass = parameter.getProperty(PROP_ITEM_FACTORY_CLASS);

		if(factoryClass == null) {
			this.itemFactory = DEFAULT_ITEM_FACTORY;
		} else {
			this.itemFactory = ClassUtil.newInstance(factoryClass);
		}
	}

	@Override
	public void clear() {

		for(Node<ListElementUI<T>> node = elements.getFirstNode(); node != null; node = node.getNext()) {

			ListElementUI<T> listElement = node.getItem();
			Element element = listElement.getElement();
			element.markForRemoval();
		}

		mapping.clear();
		elements.clear();
		items.clear();

		relayout();
	}

	/**
	 * @return панель для размещения элементов итемов.
	 */
	public Element getContent() {
		return content;
	}

	/**
	 * @return слушатель событий с попыткой выделить элемент.
	 */
	public ElementUIEventListener<ListElementUI<T>> getElementSelectedListener() {
		return elementSelectedListener;
	}

	@Override
	public T getItem(int index) {
		return items.get(index);
	}

	/**
	 * @return фабрика элементов итемов.
	 */
	public ListElementUIFactory<T> getItemFactory() {
		return itemFactory;
	}

	/**
	 * @return мапинг итемов с элементами.
	 */
	public Table<T, ListElementUI<T>> getMapping() {
		return mapping;
	}

	/**
	 * @return окнтроллер UI.
	 */
	public Nifty getNifty() {
		return nifty;
	}

	/**
	 * @return экран UI.
	 */
	public Screen getScreen() {
		return screen;
	}

	/**
	 * @return скролируемая панель.
	 */
	public ScrollPanelUI getScrollPanel() {
		return scrollPanel;
	}

	@Override
	public T getSelected() {
		return selected;
	}

	@Override
	public int getSelectedIndex() {
		T selected = getSelected();
		return selected == null ? -1 : items.indexOf(selected);
	}

	/**
	 * Обработка процесса выделения элемента.
	 * 
	 * @param control выделяемый элемент.
	 */
	protected void processSelected(ListElementUI<T> control) {

		if(control == null) {

			for(Node<ListElementUI<T>> node = elements.getFirstNode(); node != null; node = node.getNext()) {

				ListElementUI<T> element = node.getItem();

				if(element.isSelected()) {
					element.setSelected(false);
					break;
				}
			}

			SelectedChangeEvent<ListUI<T>, T> event = localSelectedChangeEvent.get();
			event.setObject(null);

			this.selected = null;

			notifyEvent(event);
			return;
		}

		T selected = getSelected();

		if(selected != null && selected == control.getItem()) {
			return;
		}

		for(Node<ListElementUI<T>> node = elements.getFirstNode(); node != null; node = node.getNext()) {

			ListElementUI<T> element = node.getItem();

			if(element.isSelected()) {
				element.setSelected(false);
				break;
			}
		}

		control.setSelected(true);
		this.selected = control.getItem();

		SelectedChangeEvent<ListUI<T>, T> event = localSelectedChangeEvent.get();
		event.setObject(control.getItem());
		notifyEvent(event);
	}

	/**
	 * Обновление лаяута.
	 */
	protected void relayout() {

		int height = 0;

		for(Node<ListElementUI<T>> node = elements.getFirstNode(); node != null; node = node.getNext()) {
			ListElementUI<T> child = node.getItem();
			height += child.getHeight();
		}

		Element contentPanel = getContent();
		contentPanel.setConstraintHeight(GameUtil.getPixelSize(height));

		ScrollPanelUI scrollPanel = getScrollPanel();
		scrollPanel.layoutCallback();
	}

	@Override
	public void removeItem(T item) {

		ListElementUI<T> control = getMapping().get(item);

		if(control == null) {
			return;
		}

		items.remove(item);
		elements.remove(control);

		Element element = control.getElement();
		element.markForRemoval();

		relayout();
	}

	@Override
	public void setSelected(T item) {

		if(item == null) {
			processSelected(null);
			return;
		}

		processSelected(getMapping().get(item));
	}

	@Override
	public void setSelectedIndex(int index) {

		if(index < 0) {
			processSelected(null);
			return;
		}

		setSelected(items.get(index));
	}

	@Override
	public int size() {
		return items.size();
	}
}
