package com.ss.client.gui.model;

import com.jme3.input.KeyInput;

/**
 * Печечисление функций клавишь в управлении и соотвественно, какие кнопки
 * задействованы.
 * 
 * @author Ronn
 */
public enum KeySetting {
	/** клавиши панели быстрого доступа */
	FAST_MENU_1(KeyInput.KEY_1),
	FAST_MENU_2(KeyInput.KEY_2),
	FAST_MENU_3(KeyInput.KEY_3),
	FAST_MENU_4(KeyInput.KEY_4),
	FAST_MENU_5(KeyInput.KEY_5),
	FAST_MENU_6(KeyInput.KEY_6),
	FAST_MENU_7(KeyInput.KEY_7),
	FAST_MENU_8(KeyInput.KEY_8),
	FAST_MENU_9(KeyInput.KEY_9),
	FAST_MENU_10(0, false),
	FAST_MENU_11(0, false),

	/** переключение режима контролера */
	SWITCH_CONTROL_MODE(KeyInput.KEY_CAPITAL),
	/** смены цели селектора */
	SWITCH_SELECTOR_INDICATOR(KeyInput.KEY_Q),

	/** активация индикатора */
	ACTIVATE_INDICATOR(KeyInput.KEY_E),

	/** клавиши управления */
	FLY_LEFT(KeyInput.KEY_A),
	FLY_RIGHT(KeyInput.KEY_D),
	FLY_UP(KeyInput.KEY_W),
	FLY_DOWN(KeyInput.KEY_S),

	/** открыть окно диалога с ангаром */
	OPEN_DIALOG(KeyInput.KEY_F, "F"), ;

	/** название кнопки */
	private String keyName;

	/** код кнопки */
	private int key;

	/** является ли клавиша клавиатуры */
	private boolean keyboard;

	private KeySetting(final int key) {
		this.key = key;
		this.keyboard = true;
	}

	private KeySetting(final int key, final boolean keyboard) {
		this.key = key;
		this.keyboard = keyboard;
	}

	private KeySetting(final int key, final String keyName) {
		this.key = key;
		this.keyboard = true;
		this.keyName = keyName;
	}

	public int getKey() {
		return key;
	}

	public String getKeyName() {
		return keyName;
	}

	public boolean isKeyboard() {
		return keyboard;
	}
}
