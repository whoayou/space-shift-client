package com.ss.client.gui.model;

/**
 * Модель сообщения чата.
 * 
 * @author Ronn
 */
public class ChatMessage implements Comparable<ChatMessage> {

	/** тип сообщения */
	private MessageType type;

	/** имя отправителя */
	private String sender;
	/** содержание сообщения */
	private String message;

	/** время получения */
	private long time;

	@Override
	public int compareTo(final ChatMessage message) {
		return (int) (time - message.time);
	}

	/**
	 * @return содержание сообщения.
	 */
	public final String getMessage() {
		return message;
	}

	/**
	 * @return sender имя отправителя.
	 */
	public final String getSender() {
		return sender;
	}

	/**
	 * @return время получения.
	 */
	public final long getTime() {
		return time;
	}

	/**
	 * @return тип сообщения.
	 */
	public final MessageType getType() {
		return type;
	}

	/**
	 * @param message содержание сообщения.
	 */
	public final void setMessage(final String message) {
		this.message = message;
	}

	/**
	 * @param sender имя отправителя.
	 */
	public final void setSender(final String sender) {
		this.sender = sender;
	}

	/**
	 * @param time время получения.
	 */
	public final void setTime(final long time) {
		this.time = time;
	}

	/**
	 * @param type тип сообщения.
	 */
	public final void setType(final MessageType type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "[" + type.getTypeName() + "]" + (sender.isEmpty() ? "" : "[" + sender + "]") + ":" + message;
	}
}
