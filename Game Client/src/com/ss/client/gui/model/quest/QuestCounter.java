package com.ss.client.gui.model.quest;

import rlib.util.pools.Foldable;
import rlib.util.pools.FoldablePool;
import rlib.util.pools.Pools;

import com.ss.client.gui.builder.game.factory.QuestElementUIFactory;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.screen.Screen;

/**
 * Квестовый счетчик чего-то.
 * 
 * @author Ronn
 */
public class QuestCounter implements Foldable {

	private static final FoldablePool<QuestCounter> POOL = Pools.newConcurrentFoldablePool(QuestCounter.class);

	public static final QuestCounter newInstance(int id, int value, int limit, String description) {

		QuestCounter counter = POOL.take();

		if(counter == null) {
			counter = new QuestCounter();
		}

		counter.id = id;
		counter.value = value;
		counter.limit = limit;
		counter.description = description;
		return counter;
	}

	/** элемент, отображающий счетчик */
	private Element element;

	/** описание счетчика */
	private String description;

	/** ид счетчика */
	private int id;
	/** лимит счетчика */
	private int limit;
	/** текущее значение */
	private int value;

	/**
	 * Создание элемента, который будет отображать счетчик.
	 * 
	 * @param parent родительский элемент.
	 * @param nifty контроллер UI.
	 * @param screen текущий экран UI.
	 * @return построенный элемент.
	 */
	public Element buildElement(Element parent, Nifty nifty, Screen screen, int order) {
		return QuestElementUIFactory.build(this, parent, nifty, screen, order);
	}

	@Override
	public void finalyze() {
		description = null;
		element = null;
		id = 0;
		limit = 0;
		value = 0;
	}

	/**
	 * Складирование в пул.
	 */
	public void fold() {
		POOL.put(this);
	}

	/**
	 * @return описание счетчика.
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @return элемент, отображающий счетчик.
	 */
	public Element getElement() {
		return element;
	}

	/**
	 * @return ид счетчика.
	 */
	public int getId() {
		return id;
	}

	/**
	 * @return лимит счетчика.
	 */
	public int getLimit() {
		return limit;
	}

	/**
	 * @return текущее значение.
	 */
	public int getValue() {
		return value;
	}

	@Override
	public void reinit() {
	}

	/**
	 * @param value текущее значение.
	 */
	public void setValue(int value) {
		this.value = value;
	}
}
