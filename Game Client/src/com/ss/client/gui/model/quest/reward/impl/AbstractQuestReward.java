package com.ss.client.gui.model.quest.reward.impl;

import rlib.logging.Logger;
import rlib.logging.Loggers;
import rlib.util.pools.Foldable;

import com.ss.client.gui.model.quest.reward.QuestReward;

import de.lessvoid.nifty.elements.Element;

/**
 * Базовая реализация награды за задание.
 * 
 * @author Ronn
 */
public abstract class AbstractQuestReward implements QuestReward, Foldable {

	protected static final Logger LOGGER = Loggers.getLogger(QuestReward.class);

	/** лемент отображающий награду */
	private Element element;

	@Override
	public void finalyze() {
		element = null;
	}

	@Override
	public Element getElement() {
		return element;
	}

	@Override
	public void reinit() {
	}
}
