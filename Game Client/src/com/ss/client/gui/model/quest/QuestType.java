package com.ss.client.gui.model.quest;

import com.ss.client.table.LangTable;

/**
 * Перечисление типов заданий.
 * 
 * @author Ronn
 */
public enum QuestType {

	LOCATION("@quest:typeLocation@", "game/quest/type/location.png"),
	STORY("@quest:typeStory@", "game/quest/type/story.png"),
	FRACTION("@quest:typeFraction@", "game/quest/type/fraction.png");

	/** название типа задания */
	private String name;
	/** иконка типа задания */
	private String image;

	private QuestType(String name, String image) {

		LangTable langTable = LangTable.getInstance();

		this.name = langTable.getText(name);
		this.image = image;
	}

	/**
	 * @return иконка типа задания.
	 */
	public String getImage() {
		return image;
	}

	/**
	 * @return название типа задания.
	 */
	public String getName() {
		return name;
	}
}
