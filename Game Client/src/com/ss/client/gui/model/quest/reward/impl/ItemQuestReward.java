package com.ss.client.gui.model.quest.reward.impl;

import rlib.util.pools.FoldablePool;
import rlib.util.pools.Pools;

import com.ss.client.gui.builder.game.factory.QuestElementUIFactory;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.screen.Screen;

/**
 * Реализация награды за задание в виде получения предметов.
 * 
 * @author Ronn
 */
public class ItemQuestReward extends AbstractQuestReward {

	private static final FoldablePool<ItemQuestReward> POOL = Pools.newConcurrentFoldablePool(ItemQuestReward.class);

	public static final ItemQuestReward newInstance(int id, long count) {

		ItemQuestReward reward = POOL.take();

		if(reward == null) {
			reward = new ItemQuestReward();
		}

		reward.id = id;
		reward.count = count;
		return reward;
	}

	/** кол-во предметов */
	private long count;
	/** ид шаблона предмета */
	private int id;

	@Override
	public Element buildTo(Element parent, Nifty nifty, Screen screen, int order) {
		return QuestElementUIFactory.build(this, parent, nifty, screen, id, count, order);
	}

	@Override
	public void finalyze() {
		super.finalyze();
		id = 0;
		count = 0;
	}

	@Override
	public void fold() {
		POOL.put(this);
	}

	/**
	 * @return кол-во предметов.
	 */
	public long getCount() {
		return count;
	}

	/**
	 * @return ид шаблона предмета.
	 */
	public int getId() {
		return id;
	}
}
