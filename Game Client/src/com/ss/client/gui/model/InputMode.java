package com.ss.client.gui.model;

/**
 * Перечисление режимов обработки ввода.
 * 
 * @author Ronn
 */
public enum InputMode {
	CONTROL_MODE,
	INTERFACE_MODE,
}
