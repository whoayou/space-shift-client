package com.ss.client.gui.model;

import rlib.util.pools.Foldable;
import rlib.util.pools.FoldablePool;
import rlib.util.pools.Pools;

/**
 * Модель настроек положения предметов на панели.
 * 
 * @author Ronn
 */
public class ItemPanelInfo implements Foldable {

	private static final FoldablePool<ItemPanelInfo> pool = Pools.newConcurrentFoldablePool(ItemPanelInfo.class);

	public static ItemPanelInfo newInstance(final long objectId, final int order) {

		ItemPanelInfo info = pool.take();

		if(info == null) {
			info = new ItemPanelInfo();
		}

		info.objectId = objectId;
		info.order = order;

		return info;
	}

	/** уникальный ид предмета */
	private long objectId;

	/** позиция на панели */
	private int order;

	@Override
	public void finalyze() {
	}

	/**
	 * Складирование настроек в пул.
	 */
	public void fold() {
		pool.put(this);
	}

	/**
	 * @return objectId уникальный ид предмета.
	 */
	public final long getObjectId() {
		return objectId;
	}

	/**
	 * @return order положение на панели.
	 */
	public final int getOrder() {
		return order;
	}

	@Override
	public void reinit() {
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + " objectId = " + objectId + ",  order = " + order;
	}
}
