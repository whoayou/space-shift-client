package com.ss.client.gui.model;

import rlib.util.pools.Foldable;
import rlib.util.pools.FoldablePool;
import rlib.util.pools.Pools;

/**
 * Модель точки элемента на экране.
 * 
 * @author Ronn
 */
public class Point implements Foldable {

	private static final FoldablePool<Point> pool = Pools.newConcurrentFoldablePool(Point.class);

	/**
	 * @return новый экземпляр точки.
	 */
	public static Point newInstance(final int x, final int y) {
		Point point = pool.take();

		if(point == null)
			point = new Point();

		point.setX(x);
		point.setY(y);

		return point;
	}

	/** координата */
	private int x;
	/** координата */
	private int y;

	private Point() {
		super();
	}

	@Override
	public void finalyze() {
		x = 0;
		y = 0;
	}

	public void fold() {
		pool.put(this);
	}

	/**
	 * @return x координата.
	 */
	public final int getX() {
		return x;
	}

	/**
	 * @return y координата.
	 */
	public final int getY() {
		return y;
	}

	@Override
	public void reinit() {
	}

	/**
	 * @param x координата.
	 */
	public final void setX(final int x) {
		this.x = x;
	}

	/**
	 * @param y координата.
	 */
	public final void setY(final int y) {
		this.y = y;
	}

	@Override
	public String toString() {
		return "Point x = " + x + ",  y = " + y;
	}
}
