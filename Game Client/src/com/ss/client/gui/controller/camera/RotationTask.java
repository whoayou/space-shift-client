package com.ss.client.gui.controller.camera;

import com.jme3.math.Quaternion;
import com.jme3.renderer.Camera;
import com.ss.client.Game;
import com.ss.client.model.ship.SpaceShip;
import com.ss.client.model.ship.SpaceShipView;
import com.ss.client.util.LocalObjects;

/**
 * Реализация задачи камеры по плавному выцентровыванию камеры относительно
 * корабля игрока.
 * 
 * @author Ronn
 */
public final class RotationTask {

	private static final Game GAME = Game.getInstance();

	private final CameraController controller;
	private final Camera camera;

	/** стартовый разворот */
	private final Quaternion startRotation;
	/** конечный разворот */
	private final Quaternion endRotation;
	/** итоговый текущий разворот */
	private final Quaternion resultRotation;
	/** буферный разворот */
	private final Quaternion bufferRotation;

	/** корабль игрока */
	private SpaceShip spaceShip;

	/** процент выполненности догонки корабял */
	private double done;
	/** шаг разворота */
	private double step;

	public RotationTask(CameraController controller) {
		this.camera = GAME.getCamera();
		this.startRotation = new Quaternion();
		this.endRotation = new Quaternion();
		this.resultRotation = new Quaternion();
		this.bufferRotation = new Quaternion();
		this.controller = controller;
	}

	/**
	 * @return буферный разворот.
	 */
	public Quaternion getBufferRotation() {
		return bufferRotation;
	}

	/**
	 * @return камера игрока.
	 */
	public Camera getCamera() {
		return camera;
	}

	/**
	 * @return контроллер камеры.
	 */
	public CameraController getController() {
		return controller;
	}

	/**
	 * @return процент выполненности разворота.
	 */
	public double getDone() {
		return done;
	}

	/**
	 * @return конечный разворот.
	 */
	public Quaternion getEndRotation() {
		return endRotation;
	}

	/**
	 * @return итоговый текущий разворот.
	 */
	public Quaternion getResultRotation() {
		return resultRotation;
	}

	/**
	 * @return корабль игрока.
	 */
	public SpaceShip getSpaceShip() {
		return spaceShip;
	}

	/**
	 * @return стартовый разворот.
	 */
	public Quaternion getStartRotation() {
		return startRotation;
	}

	/**
	 * @return шаг разворота.
	 */
	public double getStep() {
		return step;
	}

	/**
	 * @param done процент выполненности разворота.
	 */
	public void setDone(double done) {
		this.done = done;
	}

	/**
	 * @param spaceShip корабль.
	 */
	public void setSpaceShip(SpaceShip spaceShip) {
		this.spaceShip = spaceShip;
	}

	/**
	 * @param step шаг разворота.
	 */
	public void setStep(double step) {
		this.step = step;
	}

	public void update(LocalObjects local, long currentTime) {

		CameraController controller = getController();

		if(controller.isRotationActive()) {
			return;
		}

		SpaceShip ship = getSpaceShip();

		if(ship == null) {
			return;
		}

		SpaceShipView view = ship.getView();

		if(view == null) {
			return;
		}

		Camera camera = getCamera();

		Quaternion shipRotation = view.getRotation();
		Quaternion cameraRotation = camera.getRotation();

		if(shipRotation.equals(cameraRotation)) {
			return;
		}

		Quaternion startRotation = getStartRotation();
		Quaternion endRotation = getEndRotation();

		if(!endRotation.equals(shipRotation)) {
			startRotation.set(cameraRotation);
			endRotation.set(shipRotation);
			setStep(1D / 30);
			setDone(0);
		}

		double done = getDone();
		double step = getStep();

		done += step;

		setDone(done);
		try {

			if(done >= 1D) {
				camera.setRotation(endRotation);
				endRotation.set(0, 0, 0, 1);
				return;
			}

			Quaternion buffer = getBufferRotation().set(endRotation);
			camera.setRotation(getResultRotation().slerp(startRotation, buffer, (float) done));

			if(shipRotation.equals(cameraRotation)) {
				camera.setRotation(endRotation);
				endRotation.set(0, 0, 0, 1);
				return;
			}

		} finally {
			controller.update();
		}
	}
}
