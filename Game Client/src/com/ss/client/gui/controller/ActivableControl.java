package com.ss.client.gui.controller;

import de.lessvoid.nifty.controls.Controller;

/**
 * Интерфейс для реализации активируемого контроля.
 * 
 * @author Ronn
 */
public interface ActivableControl extends Controller {

	/**
	 * Активировать элемент.
	 */
	public void activate();

	/**
	 * Обновить положение на сервере.
	 * 
	 * @param index индекс нового положения.
	 */
	public void updateToServer(int index);
}
