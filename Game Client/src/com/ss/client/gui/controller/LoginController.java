package com.ss.client.gui.controller;

import java.net.InetSocketAddress;
import java.util.concurrent.ScheduledFuture;

import rlib.util.SafeTask;
import rlib.util.Strings;
import rlib.util.array.Array;
import rlib.util.array.Arrays;
import rlib.util.array.FuncElement;

import com.ss.client.Config;
import com.ss.client.Game;
import com.ss.client.GameConfig;
import com.ss.client.gui.ElementId;
import com.ss.client.gui.model.ElementUtils;
import com.ss.client.gui.model.Language;
import com.ss.client.gui.model.ScreenSize;
import com.ss.client.manager.ExecutorManager;
import com.ss.client.model.Account;
import com.ss.client.model.ServerInfo;
import com.ss.client.network.Network;
import com.ss.client.network.model.NetServer;
import com.ss.client.network.packet.client.LoginRequestAuth;
import com.ss.client.network.packet.client.LoginRequestRegister;
import com.ss.client.network.packet.client.LoginRequestServerList;
import com.ss.client.table.BackgroundSoundTable;
import com.ss.client.table.LangTable;
import com.ss.client.util.ReflectionMethod;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.NiftyEventSubscriber;
import de.lessvoid.nifty.controls.Button;
import de.lessvoid.nifty.controls.DropDown;
import de.lessvoid.nifty.controls.Label;
import de.lessvoid.nifty.controls.Slider;
import de.lessvoid.nifty.controls.TextField;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.input.NiftyInputEvent;
import de.lessvoid.nifty.screen.DefaultScreenController;
import de.lessvoid.nifty.screen.Screen;

/**
 * Контролер экрана авторизации.
 * 
 * @author Ronn
 */
@SuppressWarnings("unchecked")
public final class LoginController extends DefaultScreenController {

	/**
	 * Перечисление состояний экрана.
	 * 
	 * @author Ronn
	 */
	public static enum LoginState {
		WAIT_AUTH,
		REGISTRATION,
		CONFIG,
	}
	public static final String ACTIONS_BUTTON_CONFIG_SPLIT_ID = "#actions_button_config_split";
	public static final String ACTIONS_BUTTON_REGISTER_SPLIT_ID = "#actions_button_register_split";
	public static final String BUTTON_IMAGE_ID = "#button_image";
	public static final String BUTTON_LABEL_ID = "#button_label";
	public static final String ACTIONS_PANEL_ID = "#actions_panel";
	public static final String ACTIONS_BACKGRAUND_ID = "#actions_backgraund";

	public static final String ACTIONS_PARENT_PANEL_ID = "#actions_parent_panel";
	public static final String AUTH_BUTTON_LOGIN_ID = "#auth_button_login";
	public static final String AUTH_BUTTON_SETTINGS_ID = "#actions_button_settings";

	public static final String AUTH_BUTTON_ABOUT_ID = "#actions_button_about";

	public static final String AUTH_LINK_REGISTER_ID = "#actions_link_register";

	public static final String AUTH_BUTTON_PANEL_ID = "#auth_button_panel";
	public static final String AUTH_LOGIN_FIELD_PASSWORD_ID = "#auth_login_field_password";

	public static final String AUTH_LOGIN_FIELD_NAME_ID = "#auth_login_field_name";
	public static final String AUTH_LOGIN_LABEL_PASSWORD_ID = "#auth_login_label_password";
	public static final String AUTH_LOGIN_LABEL_NAME_ID = "#auth_login_label_name";

	public static final String AUTH_LOGIN_LABEL_SERVER_LIST_ID = "#auth_login_label_server_list";

	public static final String AUTH_LOGIN_DROP_DOWN_SERVER_LIST_ID = "#auth_login_drop_down_server_list";
	public static final String AUTH_PASSWORD_PANEL_ID = "#auth_password_panel";
	public static final String AUTH_LOGIN_PANEL = "#login_panel";
	public static final String AUTH_PANEL_ID = "#auth_panel";

	public static final String AUTH_PARENT_PANEL_ID = "#auth_panel";
	public static final String BACKGROUND_IMAGE_ID = "#background_image";
	public static final String BACKGROUND_LAYER_ID = "#background_layer";
	public static final String BACKGROUND_LOGO_ID = "#background_logo_image";

	public static final String BACKGROUND_LABEL_PANEL_ID = "#background_label_panel";

	public static final String BUFFER_PANEL_ID = "#buffer_panel";

	private static final LangTable LANG_TABLE = LangTable.getInstance();
	private static final Network NETWORK = Network.getInstance();
	private static final ExecutorManager EXECUTOR_MANAGER = ExecutorManager.getInstance();
	private static final BackgroundSoundTable SOUND_TABLE = BackgroundSoundTable.getInstance();
	private static final Game GAME = Game.getInstance();
	private static final Nifty NIFTY = GAME.getNifty();

	/** сообщение при отсутствии конекта к логин серверу */
	private static final String NO_CONNECT = LANG_TABLE.getText("@interface:loginLabelNoConnect@");
	/** сообщение при изменении настроек */
	private static final String WARNING_CONFIG = LANG_TABLE.getText("@interface:loginLabeWarningConfig@");

	private static final FuncElement<ServerInfo> SERVER_INFO_FOLD_FUNC = new FuncElement<ServerInfo>() {

		@Override
		public void apply(final ServerInfo info) {
			info.fold();
		}
	};

	/** экземпляр контролера */
	private static LoginController instance = new LoginController();

	/**
	 * @return инстанс контролера логин меню.
	 */
	public static LoginController getInstance() {
		return instance;
	}

	/** список серверов */
	private final Array<ServerInfo> servers;

	/** задача проверки конекта к логину */
	private final SafeTask connectTask;
	/** задача авторазблокировки */
	private final SafeTask unblockTask;
	/** задача по обновлению списка серверов */
	private final SafeTask updateServerListTask;

	/** поле ввода логина */
	private TextField loginField;
	/** поле ввода пароля */
	private TextField passwordField;
	/** поле ввода логина нового аккаунта. */
	private TextField registerLoginField;
	/** поле ввода пароля нового аккаунта. */
	private TextField registerPasswordField;
	/** поле ввода почты нового аккаунта. */
	private TextField registerEmailField;

	/** кнопка авторизации */
	private Button auth;
	/** кнопка регистрации */
	private Button register;
	/** кнопка закрытия окна предупреждения */
	private Button warningCancel;

	/** слой авторизации */
	private Element authLayer;
	/** слой конфига */
	private Element configLayer;
	/** слой предупреждения */
	private Element warningLayer;
	/** слой регистрации */
	private Element registerLayer;

	/** регулятор громкости музыки */
	private Slider musicVolume;
	/** регулятор громкости эффектов */
	private Slider effectVolume;

	/** список разрешений экрана из конфига */
	private DropDown<ScreenSize> screenDropdown;
	/** список языков из конфига */
	private DropDown<Language> langDropdown;
	/** выпадающий список серверов */
	private DropDown<ServerInfo> serverListDropdown;

	/** сообщение окна внимания */
	private Label warning;

	/** текущее состояние логин меню */
	private LoginState state;
	/** следующее состояние логин меню */
	private LoginState nextState;

	/** таск теста коннекта к логину */
	private volatile ScheduledFuture<SafeTask> connectSchedule;
	/** таск авторазблокировки */
	private volatile ScheduledFuture<SafeTask> unblockSchedule;
	/** ссылка на задачу по обновлению списка серверов */
	private volatile ScheduledFuture<SafeTask> updateServerListSchedule;

	/** состояние индикатора коннекта к логину */
	private volatile boolean connected;
	/** инициализирован ли */
	private volatile boolean initialized;

	private LoginController() {
		this.servers = Arrays.toArray(ServerInfo.class);
		this.connectTask = new SafeTask() {

			@Override
			protected void runImpl() {
				updateConnect(NETWORK.isLoginConnected());
			}
		};
		this.unblockTask = new SafeTask() {

			@Override
			protected void runImpl() {
				unblocking();
			}
		};
		this.updateServerListTask = new SafeTask() {

			@Override
			protected void runImpl() {
				requestServerList();
			}
		};
	}

	/**
	 * Добавление сервервов в список.
	 */
	public void addServers(final Array<ServerInfo> newServers) {

		GAME.syncLock();
		try {

			final Array<ServerInfo> servers = getServers();

			if(servers.size() == newServers.size()) {

				boolean equals = true;

				for(final ServerInfo server : newServers.array()) {

					if(server == null) {
						break;
					}

					if(servers.contains(server)) {
						continue;
					}

					equals = false;
					break;
				}

				if(equals) {
					return;
				}
			}

			final DropDown<ServerInfo> serverList = getServerListDropdown();

			ServerInfo selected = serverList.getSelection();
			serverList.clear();

			for(final ServerInfo server : newServers.array()) {

				if(server == null) {
					break;
				}

				serverList.addItem(server);
			}

			if(newServers.contains(selected)) {
				serverList.selectItem(selected);
			}

			servers.apply(SERVER_INFO_FOLD_FUNC);
			servers.clear();
			servers.addAll(newServers);

		} finally {
			GAME.syncUnlock();
		}
	}

	/**
	 * Процесс авторизации игрока.
	 */
	@ReflectionMethod
	public void auth() {
		GAME.syncLock();
		try {

			final LoginState state = getState();

			if(state != LoginState.WAIT_AUTH || warningLayer.isVisible()) {
				return;
			}

			final NetServer server = NETWORK.getLoginServer();

			if(!NETWORK.isLoginConnected() || server == null) {
				warningShow(NO_CONNECT, state);
				return;
			}

			final Account account = GAME.getAccount();

			account.setName(loginField.getDisplayedText());
			account.setPassword(Strings.passwordToHash(passwordField.getDisplayedText()));

			server.sendPacket(LoginRequestAuth.getInstance(account.getName(), account.getPassword()));

			ElementUtils.disable(authLayer);

			unblockSchedule = EXECUTOR_MANAGER.scheduleGeneral(getUnblockTask(), 5000);

		} finally {
			GAME.syncUnlock();
		}
	}

	/**
	 * Скрыть меню авторизации.
	 */
	private void authDisable() {
		ElementUtils.disable(authLayer);
	}

	/**
	 * показать меню авторизации.
	 */
	private void authEnable() {
		ElementUtils.enable(authLayer);
		loginField.setFocus();
	}

	@Override
	public void bind(Nifty nifty, Screen screen) {
		super.bind(nifty, screen);

		final Screen login = NIFTY.getScreen(ElementId.LOGIN_SCREEN.getId());

		loginField = screen.findNiftyControl(AUTH_LOGIN_FIELD_NAME_ID, TextField.class);
		passwordField = screen.findNiftyControl(AUTH_LOGIN_FIELD_PASSWORD_ID, TextField.class);

		serverListDropdown = screen.findNiftyControl(AUTH_LOGIN_DROP_DOWN_SERVER_LIST_ID, DropDown.class);

		registerLoginField = login.findNiftyControl(ElementId.LOGIN_REGISTER_FIELD_LOGIN.getId(), TextField.class);
		registerPasswordField = login.findNiftyControl(ElementId.LOGIN_REGISTER_FIELD_PASSWORD.getId(), TextField.class);
		registerEmailField = login.findNiftyControl(ElementId.LOGIN_REGISTER_FIELD_EMAIL.getId(), TextField.class);

		warning = login.findNiftyControl(ElementId.LOGIN_WARNING_LABEL.getId(), Label.class);

		authLayer = login.findElementByName(ElementId.LOGIN_AUTH_LAYER.getId());
		configLayer = login.findElementByName(ElementId.LOGIN_CONFIG_LAYER.getId());
		warningLayer = login.findElementByName(ElementId.LOGIN_WARNING_LAYER.getId());
		registerLayer = login.findElementByName(ElementId.LOGIN_REGISTER_LAYER.getId());

		auth = login.findNiftyControl(AUTH_BUTTON_LOGIN_ID, Button.class);
		register = login.findNiftyControl(ElementId.LOGIN_REGISTER_BUTTON_REGISTER.getId(), Button.class);
		warningCancel = login.findNiftyControl(ElementId.LOGIN_WARNING_BUTTON.getId(), Button.class);

		langDropdown = login.findNiftyControl(ElementId.LOGIN_CONFIG_DROP_DOWN_LANG.getId(), DropDown.class);
		langDropdown.clear();

		for(final Language lang : LANG_TABLE.getLangs())
			langDropdown.addItem(lang);

		langDropdown.selectItem(GameConfig.LANG);

		screenDropdown = login.findNiftyControl(ElementId.LOGIN_CONFIG_DROP_DOWN_SCREEN.getId(), DropDown.class);
		screenDropdown.clear();

		for(final ScreenSize size : ScreenSize.values())
			screenDropdown.addItem(size);

		screenDropdown.selectItem(GameConfig.SCREEN_SIZE);

		musicVolume = login.findNiftyControl(ElementId.LOGIN_CONFIG_SLIDER_MUSIC.getId(), Slider.class);
		musicVolume.setMin(0F);
		musicVolume.setMax(2F);
		musicVolume.setStepSize(0.1F);
		musicVolume.setValue(GameConfig.MUSIC_VOLUME);

		effectVolume = login.findNiftyControl(ElementId.LOGIN_CONFIG_SLIDER_EFFECT.getId(), Slider.class);
		effectVolume.setMin(0F);
		effectVolume.setMax(2F);
		effectVolume.setStepSize(0.1F);
		effectVolume.setValue(GameConfig.EFFECT_VOLUME);

		updateConnect(connected);

		loginField.setFocus();

		configHide();
		warningHide();
		registerHide();

		setState(LoginState.WAIT_AUTH);
		setNextState(null);

		if(connectSchedule == null) {
			connectSchedule = EXECUTOR_MANAGER.scheduleGeneralAtFixedRate(connectTask, 1000, 1000);
		}

		if(updateServerListSchedule == null) {
			updateServerListSchedule = EXECUTOR_MANAGER.scheduleGeneralAtFixedRate(getUpdateServerListTask(), 1000, 5000);
		}
	}

	/**
	 * Открытие настроек клиента.
	 */
	@ReflectionMethod
	public void config() {
		GAME.syncLock();
		try {

			if(warningLayer.isVisible()) {
				return;
			}

			switch(getState()) {
				case CONFIG: {
					return;
				}
				case REGISTRATION: {
					registerCancel();
					break;
				}
				default: {
					break;
				}
			}

			if(getState() != LoginState.WAIT_AUTH) {
				return;
			}

			toState(LoginState.CONFIG);

		} finally {
			GAME.syncUnlock();
		}
	}

	/**
	 * Сохранение настроек клиента.
	 */
	@ReflectionMethod
	public void configApply() {

		GAME.syncLock();
		try {

			final float effectVolume = GameConfig.EFFECT_VOLUME;
			final float musicVolume = GameConfig.MUSIC_VOLUME;

			final ScreenSize size = GameConfig.SCREEN_SIZE;
			final Language lang = GameConfig.LANG;

			GameConfig.LANG = getLanguageSelection();
			GameConfig.SCREEN_SIZE = getScreenSizeSelection();

			GameConfig.MUSIC_VOLUME = getMusicVolume();
			GameConfig.EFFECT_VOLUME = getEffectVolume();

			GameConfig.save();

			if(effectVolume != GameConfig.EFFECT_VOLUME || musicVolume != GameConfig.MUSIC_VOLUME) {
				SOUND_TABLE.updateVolume();
			}

			if(!(lang != GameConfig.LANG || size != GameConfig.SCREEN_SIZE)) {
				toState(LoginState.WAIT_AUTH);
				return;
			}

			warningShow(WARNING_CONFIG, LoginState.WAIT_AUTH);

		} finally {
			GAME.syncUnlock();
		}
	}

	/**
	 * Сохранение настроек клиента.
	 */
	@ReflectionMethod
	public void configCancel() {
		GAME.syncLock();
		try {

			musicVolume.setValue(GameConfig.MUSIC_VOLUME);
			effectVolume.setValue(GameConfig.EFFECT_VOLUME);
			langDropdown.selectItem(GameConfig.LANG);
			screenDropdown.selectItem(GameConfig.SCREEN_SIZE);

			toState(LoginState.WAIT_AUTH);

		} finally {
			GAME.syncUnlock();
		}
	}

	/**
	 * Спрятать слой настроек.
	 */
	private void configHide() {
		configLayer.hide();
	}

	/**
	 * Отобразить слой настроек.
	 */
	private void configShow() {
		configLayer.show();
		langDropdown.setFocus();
	}

	@ReflectionMethod
	public void enterToServer() {

		GAME.syncLock();
		try {

			if(getState() != LoginState.WAIT_AUTH) {
				return;
			}

			DropDown<ServerInfo> serverList = getServerListDropdown();
			final ServerInfo selection = serverList.getSelection();

			if(selection == null) {
				return;
			}

			NETWORK.connectToGame(Config.LOCAL_MODE ? new InetSocketAddress("localhost", 21100) : selection.getAdress());

		} finally {
			GAME.syncUnlock();
		}
	}

	/**
	 * Обработка выхода из клиента.
	 */
	public void exit() {
		System.exit(0);
	}

	/**
	 * @return задача проверки конекта к логину.
	 */
	public SafeTask getConnectTask() {
		return connectTask;
	}

	/**
	 * @return выбранная в настройках громкость эффектов.
	 */
	private float getEffectVolume() {
		return effectVolume.getValue();
	}

	/**
	 * @return выбранный в настройках язык.
	 */
	private Language getLanguageSelection() {
		return langDropdown.getSelection();
	}

	/**
	 * @return выбранная в настройках громкость мызыки.
	 */
	private float getMusicVolume() {
		return musicVolume.getValue();
	}

	/**
	 * @return стадия после предупреждения.
	 */
	public LoginState getNextState() {
		return nextState;
	}

	/**
	 * @return выбранное в настройках резрешение экрана.
	 */
	private ScreenSize getScreenSizeSelection() {
		return screenDropdown.getSelection();
	}

	/**
	 * @return выбранный сервер.
	 */
	public ServerInfo getSelectionServer() {
		return getServerListDropdown().getSelection();
	}

	/**
	 * @return выпадающий список серверов.
	 */
	public DropDown<ServerInfo> getServerListDropdown() {
		return serverListDropdown;
	}

	/**
	 * @return список инфы по доступным серверам.
	 */
	public Array<ServerInfo> getServers() {
		return servers;
	}

	/**
	 * @return стадия меню.
	 */
	public final LoginState getState() {
		return state;
	}

	/**
	 * @return задача авторазблокировки.
	 */
	public SafeTask getUnblockTask() {
		return unblockTask;
	}

	/**
	 * @return задача по обновлению списка серверов.
	 */
	public SafeTask getUpdateServerListTask() {
		return updateServerListTask;
	}

	@NiftyEventSubscriber(pattern = ".*")
	public void inputKey(final String id, final NiftyInputEvent event) {

		if(event == NiftyInputEvent.SubmitText) {

			if(id == AUTH_LOGIN_FIELD_NAME_ID) {
				passwordField.setFocus();
			} else if(id == AUTH_LOGIN_FIELD_PASSWORD_ID) {
				auth.setFocus();
			}
		}

		// case LOGIN_REGISTER_FIELD_LOGIN:
		// registerPasswordField.setFocus();
		// break;
		// case LOGIN_REGISTER_FIELD_PASSWORD:
		// registerEmailField.setFocus();
		// break;
		// case LOGIN_REGISTER_FIELD_EMAIL:
		// register.setFocus();
	}

	/**
	 * @return есть ли подключение к логин серверу.
	 */
	public boolean isConnected() {
		return connected;
	}

	/**
	 * @return инициализированна ли стадия.
	 */
	public boolean isInitialized() {
		return initialized;
	}

	@Override
	public void onEndScreen() {
		super.onEndScreen();

		if(connectSchedule != null) {
			connectSchedule.cancel(false);
			connectSchedule = null;
		}

		if(unblockSchedule != null) {
			unblockSchedule.cancel(false);
			unblockSchedule = null;
		}

		if(updateServerListSchedule != null) {
			updateServerListSchedule.cancel(false);
			updateServerListSchedule = null;
		}
		System.gc();
	}

	/**
	 * Открытие регистрации нового аккаунта.
	 */
	@ReflectionMethod
	public void register() {
		GAME.syncLock();
		try {

			if(warningLayer.isVisible()) {
				return;
			}

			switch(getState()) {
				case REGISTRATION: {
					return;
				}
				case CONFIG: {
					configCancel();
					break;
				}
				default: {
					break;
				}
			}

			if(getState() != LoginState.WAIT_AUTH) {
				return;
			}

			if(!connected) {
				warningShow(NO_CONNECT, null);
				return;
			}

			toState(LoginState.REGISTRATION);

		} finally {
			GAME.syncUnlock();
		}
	}

	/**
	 * Попытаться зарегистрироваться.
	 */
	@ReflectionMethod
	public void registerApply() {
		GAME.syncLock();
		try {

			if(getState() != LoginState.REGISTRATION) {
				return;
			}

			final String login = registerLoginField.getDisplayedText();
			final String password = registerPasswordField.getDisplayedText();
			final String email = registerEmailField.getDisplayedText();

			final NetServer server = NETWORK.getLoginServer();

			if(server == null) {
				warningShow(NO_CONNECT, LoginState.WAIT_AUTH);
				return;
			}

			server.sendPacket(LoginRequestRegister.getInstance(login, Strings.passwordToHash(password), email));

			ElementUtils.disable(registerLayer);

			unblockSchedule = EXECUTOR_MANAGER.scheduleGeneral(getUnblockTask(), 5000);

		} finally {
			GAME.syncUnlock();
		}
	}

	/**
	 * Отмена регистрации.
	 */
	@ReflectionMethod
	public void registerCancel() {
		GAME.syncLock();
		try {

			if(getState() != LoginState.REGISTRATION) {
				return;
			}

			registerClear();
			toState(LoginState.WAIT_AUTH);

		} finally {
			GAME.syncUnlock();
		}
	}

	/**
	 * Очистка полей регистрации.
	 */
	public void registerClear() {
		registerLoginField.setText(Strings.EMPTY);
		registerPasswordField.setText(Strings.EMPTY);
		registerEmailField.setText(Strings.EMPTY);
	}

	/**
	 * Спрятать слой регистрации.
	 */
	private void registerHide() {
		registerLayer.hide();
	}

	/**
	 * Отобразить слой регистрации.
	 */
	private void registerShow() {
		registerLayer.show();
		registerLoginField.setFocus();
	}

	/**
	 * Запрос на получение списка серверов.
	 */
	private void requestServerList() {

		final NetServer server = NETWORK.getLoginServer();

		if(!NETWORK.isLoginConnected() || server == null) {
			return;
		}

		server.sendPacket(LoginRequestServerList.getInstance());
	}

	/**
	 * @param connected есть ли подключение к логин серверу.
	 */
	public void setConnected(final boolean connected) {
		this.connected = connected;
	}

	/**
	 * @param initialized инициализированна ли стадия.
	 */
	public void setInitialized(final boolean initialized) {
		this.initialized = initialized;
	}

	/**
	 * @param nextState стадия после предупреждения.
	 */
	public void setNextState(final LoginState nextState) {
		this.nextState = nextState;
	}

	/**
	 * @param state стадия меню.
	 */
	public final void setState(final LoginState state) {
		this.state = state;
	}

	/**
	 * Перейти к стадии.
	 */
	public void toState(final LoginState state) {

		GAME.syncLock();
		try {

			setState(state);

			switch(state) {
				case CONFIG: {
					authDisable();
					registerHide();
					warningHide();
					configShow();
					break;
				}
				case WAIT_AUTH: {
					configHide();
					registerHide();
					warningHide();
					authEnable();
					break;
				}
				case REGISTRATION: {
					authDisable();
					configHide();
					warningHide();
					registerShow();
					break;
				}
			}

		} finally {
			GAME.syncUnlock();
		}
	}

	/**
	 * Разблокировка меню.
	 */
	public void unblocking() {
		GAME.syncLock();
		try {

			if(unblockSchedule != null) {
				unblockSchedule.cancel(false);
				unblockSchedule = null;
			}

			switch(state) {
				case REGISTRATION: {
					ElementUtils.enable(registerLayer);
					break;
				}
				case WAIT_AUTH: {
					ElementUtils.enable(authLayer);
					break;
				}
				case CONFIG: {
					ElementUtils.enable(configLayer);
					break;
				}
			}

		} finally {
			GAME.syncUnlock();
		}
	}

	/**
	 * Обновление индикатора коннекта к логин серверу.
	 */
	private void updateConnect(final boolean state) {
		setConnected(state);
	}

	/**
	 * Нажать на ОК в предупреждении.
	 */
	@ReflectionMethod
	public void warningCancel() {
		GAME.syncLock();
		try {

			switch(state) {
				case CONFIG: {
					ElementUtils.enable(configLayer);
					break;
				}
				case REGISTRATION: {
					ElementUtils.enable(registerLayer);
					break;
				}
				case WAIT_AUTH: {
					ElementUtils.enable(authLayer);
					break;
				}
			}

			warningHide();

			final LoginState nextState = getNextState();

			if(nextState != null) {
				toState(nextState);
				setNextState(nextState);
			}

		} finally {
			GAME.syncUnlock();
		}
	}

	/**
	 * Спрятать предупреждение.
	 */
	public void warningHide() {
		warningLayer.hide();
	}

	/**
	 * Отобразить предупреждение.
	 */
	public void warningShow(final String text, final LoginState next) {
		GAME.syncLock();
		try {

			switch(state) {
				case CONFIG: {
					ElementUtils.disable(configLayer);
					break;
				}
				case REGISTRATION: {
					ElementUtils.disable(registerLayer);
					break;
				}
				case WAIT_AUTH: {
					ElementUtils.disable(authLayer);
					break;
				}
			}

			setNextState(next);

			warning.setText(text);
			warningLayer.show();
			warningCancel.setFocus();

		} finally {
			GAME.syncUnlock();
		}
	}
}
