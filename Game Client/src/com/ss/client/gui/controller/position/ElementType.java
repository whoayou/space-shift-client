package com.ss.client.gui.controller.position;

import com.ss.client.gui.ElementId;
import com.ss.client.gui.controller.game.panel.MainPanelController;
import com.ss.client.gui.controller.game.panel.chat.ChatPanelUIController;
import com.ss.client.gui.controller.game.panel.fast.FastPanelUIController;
import com.ss.client.gui.controller.game.panel.radar.RadarPanelUIController;
import com.ss.client.gui.controller.game.window.skills.SkillWindowUIController;
import com.ss.client.gui.controller.game.window.storage.StorageWindowUIController;
import com.ss.client.gui.model.Point;

/**
 * Перечисление типов элементов, чьи позиции запоминаются сервером.
 * 
 * @author Ronn
 */
public enum ElementType {
	PANEL_CHAT(ChatPanelUIController.PANEL_ID, Point.newInstance(0, 380)),
	PANEL_FAST(FastPanelUIController.PANEL_ID, Point.newInstance(659, 540)),
	PANEL_MAIN(MainPanelController.PANEL_ID, Point.newInstance(944, 6)),
	PANEL_RADAR(RadarPanelUIController.PANEL_ID, Point.newInstance(9, 9)),
	HANGAR_INDICATOR(ElementId.GAME_WINDOW_ANGAR_INDICATOR.getId(), Point.newInstance(80, 80)),

	SKILL_LIST_WINDOW(SkillWindowUIController.WINDOW_ID, Point.newInstance(309, 67)),
	QUEST_BOOK_WINDOW(ElementId.GAME_WINDOW_QUEST_BOOK.getId(), Point.newInstance(227, 78)),
	QUEST_DIALOG_WINDOW(ElementId.GAME_WINDOW_QUEST_DIALOG.getId(), Point.newInstance(227, 78)),
	SHIP_PANEL_WINDOW(ElementId.GAME_WINDOW_SHIP_PANEL.getId(), Point.newInstance(280, 115)),
	WINDOW_STORAGE(StorageWindowUIController.WINDOW_ID, Point.newInstance(256, 37)), ;

	private static final ElementType[] values = values();

	public static final int length = values.length;

	public static ElementType valueOf(final int index) {
		return values[index];
	}

	/** ид элемента */
	private String elementId;
	/** положение по умолчанию */
	private Point def;

	private ElementType(final String elementId, final Point def) {
		this.elementId = elementId;
		this.def = def;
	}

	/**
	 * @return позиция по умолчанию.
	 */
	public Point getDef() {
		return def;
	}

	/**
	 * @return ид элемента.
	 */
	public String getElementId() {
		return elementId;
	}
}
