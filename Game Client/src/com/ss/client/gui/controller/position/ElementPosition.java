package com.ss.client.gui.controller.position;

import rlib.logging.Logger;
import rlib.logging.Loggers;
import rlib.util.array.Array;

import com.ss.client.gui.control.WindowPanelControl;
import com.ss.client.gui.controller.game.GameUIController;
import com.ss.client.gui.model.ElementUtils;
import com.ss.client.gui.model.InterfaceInfo;
import com.ss.client.gui.model.Point;
import com.ss.client.network.ClientPacket;
import com.ss.client.network.Network;
import com.ss.client.network.packet.client.RequestInterfaceUpdate;
import com.ss.client.util.GameUtil;

import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.screen.Screen;

/**
 * Контролер позиции интерфейса.
 * 
 * @author Ronn
 */
public class ElementPosition {

	private static final Logger LOGGER = Loggers.getLogger(ElementPosition.class);

	private static final Network NETWORK = Network.getInstance();

	/** список контролируемых элементов */
	private final Element[] elements;

	/** таблица свернутых элементов */
	private final boolean minimizeds[];

	public ElementPosition(GameUIController controller) {
		this.elements = new Element[ElementType.length];
		this.minimizeds = new boolean[ElementType.length];
	}

	/**
	 * Применитть настройки с сервера.
	 * 
	 * @param interfaceInfo список настроек.
	 */
	public void apply(final Array<InterfaceInfo> interfaceInfo) {

		final InterfaceInfo[] array = interfaceInfo.array();

		for(int i = 0, length = interfaceInfo.size(); i < length; i++) {

			final InterfaceInfo info = array[i];

			if(info.isMinimized()) {
				minimize(info.getType());
			}

			moveElement(info.getType(), info.getX(), info.getY());
		}
	}

	/**
	 * Приминение настроек по умолчанию.
	 */
	public void applyDefault(final Screen screen) {

		final ElementType[] types = ElementType.values();

		for(final ElementType type : types) {

			final String elementId = type.getElementId();

			final Element element = screen.findElementByName(elementId);

			if(element == null) {
				LOGGER.warning("not found element for " + elementId);
				continue;
			}

			final Point point = type.getDef();

			setElement(type, element);
			moveElement(type, point.getX(), point.getY());
		}
	}

	/**
	 * Изменение статуса свернутости элемента.
	 * 
	 * @param element изменившийся элемент.
	 * @param minimized свернут ли элмент.
	 */
	public void changeMinimized(final Element element, final boolean minimized) {

		final Element[] elements = getElements();

		final boolean[] minimizeds = getMinimizeds();

		for(int i = 0, length = elements.length; i < length; i++) {

			if(elements[i] != element) {
				continue;
			}

			minimizeds[i] = minimized;

			final ClientPacket packet = RequestInterfaceUpdate.getInstance(ElementType.valueOf(i), element.getX(), element.getY(), minimized);

			NETWORK.sendPacketToGameServer(packet);
			return;
		}
	}

	/**
	 * Увндомление об изменении позиции указанного эелмента.
	 * 
	 * @param id ид элемента.
	 */
	public void changePosition(final Element element) {

		final Element[] elements = getElements();

		for(int i = 0, length = elements.length; i < length; i++) {

			if(elements[i] != element) {
				continue;
			}

			final ClientPacket packet = RequestInterfaceUpdate.getInstance(ElementType.valueOf(i), element.getX(), element.getY(), minimizeds[i]);

			NETWORK.sendPacketToGameServer(packet);
			return;
		}
	}

	/**
	 * Увндомление об изменении позиции указанного эелмента.
	 * 
	 * @param id ид элемента.
	 */
	public void changePosition(final String id) {

		final Element[] elements = getElements();

		for(int i = 0, length = elements.length; i < length; i++) {

			final Element element = elements[i];

			if(element == null || !id.equals(element.getId())) {
				continue;
			}

			final ClientPacket packet = RequestInterfaceUpdate.getInstance(ElementType.valueOf(i), element.getX(), element.getY(), minimizeds[i]);

			NETWORK.sendPacketToGameServer(packet);
			return;
		}
	}

	/**
	 * @return список контролируемых элементов.
	 */
	public Element[] getElements() {
		return elements;
	}

	/**
	 * @return таблица свернутых элементов.
	 */
	public boolean[] getMinimizeds() {
		return minimizeds;
	}

	/**
	 * Свернуть элемент.
	 * 
	 * @param type тип сворачиваемого элемента.
	 */
	public void minimize(final ElementType type) {

		final Element element = elements[type.ordinal()];

		if(element == null) {
			return;
		}

		final WindowPanelControl control = element.getControl(WindowPanelControl.class);

		if(control != null) {
			control.minimize();
		}
	}

	/**
	 * Перемещение элемента в указанную точку.
	 * 
	 * @param type тип элемента.
	 * @param x новая координата.
	 * @param y новая координата.
	 */
	public void moveElement(final ElementType type, final int x, final int y) {

		final Element element = elements[type.ordinal()];

		if(element == null) {
			return;
		}

		element.setConstraintX(GameUtil.getPixelSize(x));
		element.setConstraintY(GameUtil.getPixelSize(y));

		ElementUtils.updateLayout(element.getParent());
	}

	/**
	 * Установка элемента интерфейса.
	 * 
	 * @param type тип элемента.
	 * @param element ссылка на элемент.
	 */
	public void setElement(final ElementType type, final Element element) {
		elements[type.ordinal()] = element;
	}
}
