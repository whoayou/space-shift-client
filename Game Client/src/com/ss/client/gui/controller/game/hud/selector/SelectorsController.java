package com.ss.client.gui.controller.game.hud.selector;

import java.util.Properties;

import rlib.logging.Logger;
import rlib.logging.Loggers;
import rlib.util.array.Array;
import rlib.util.array.Arrays;

import com.jme3.input.InputManager;
import com.jme3.input.controls.ActionListener;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import com.jme3.renderer.Camera;
import com.ss.client.Game;
import com.ss.client.gui.controller.camera.CameraController;
import com.ss.client.gui.controller.game.GameUIController;
import com.ss.client.gui.controller.game.event.impl.SelectedObjectEvent;
import com.ss.client.gui.controller.game.hud.AbstractHudComponent;
import com.ss.client.gui.controller.game.hud.HudController;
import com.ss.client.gui.controller.game.hud.event.HudEventListener;
import com.ss.client.gui.controller.game.hud.indicator.SelectIndicatorEvent;
import com.ss.client.gui.model.InputMode;
import com.ss.client.gui.model.listeners.AddRemoveObjectListener;
import com.ss.client.manager.UpdateUIManager;
import com.ss.client.model.SpaceObject;
import com.ss.client.model.SpaceObjectView;
import com.ss.client.model.ship.SpaceShip;
import com.ss.client.model.ship.player.PlayerShip;
import com.ss.client.model.space.SpaceLocation;
import com.ss.client.network.Network;
import com.ss.client.network.packet.client.RequestForceSyncObject;
import com.ss.client.network.packet.client.RequestSelectTarget;
import com.ss.client.settings.InputSetting;
import com.ss.client.util.GameUtil;
import com.ss.client.util.LocalObjects;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.xml.xpp3.Attributes;

/**
 * Контролер выделений объектов наведения.
 * 
 * @author Ronn
 */
public class SelectorsController extends AbstractHudComponent implements AddRemoveObjectListener {

	public static final int SQUARED_DISTANCE_TO_VECTOR = 30 * 30;

	protected static final Logger LOGGER = Loggers.getLogger(SelectorsController.class);

	public static final String BASE_PANEL_ID = "#selector_panel";
	public static final String SELECTOR_ID = "#object_selector";

	private static final SpaceLocation SPACE_LOCATION = SpaceLocation.getInstance();
	private static final UpdateUIManager UPDATE_UI_MANAGER = UpdateUIManager.getInstance();
	private static final Network NETWORK = Network.getInstance();
	private static final Game GAME = Game.getInstance();

	private static final String SELECTOR_ACTIVATE_KEY = InputSetting.SELECTOR_ACTIVATE.name();
	private static final String MOVE_CAMERA_KEY = InputSetting.MOVE_CAMERA.name();
	private static final String SWITCH_SELECTOR_KEY = InputSetting.SWITCH_SELECTOR.name();

	private static final int SYNC_INTERVAL = 10000;

	/** список окружующих объектов */
	private final Array<SpaceObject> aroundObjects;
	/** список уже ранее выбранных объектов */
	private final Array<SpaceObject> selectedObjects;

	/** слушатель нажатий на кнопки */
	private final ActionListener actionListener = new ActionListener() {

		@Override
		public void onAction(String name, boolean isPressed, float tpf) {

			if(isPressed) {
				return;
			}

			InputMode inputMode = getInputMode();
			LocalObjects local = LocalObjects.get();

			if(name == SWITCH_SELECTOR_KEY && inputMode == InputMode.CONTROL_MODE) {
				switchSelector(local);
			} else if(name == MOVE_CAMERA_KEY && inputMode == InputMode.CONTROL_MODE) {

				CameraController controller = CameraController.getInstance();
				ObjectSelector selector = getSelector();

				SpaceShip currentShip = controller.getSpaceShip();

				if(currentShip != getPlayerShip()) {
					controller.setSpaceShip(getPlayerShip());
				} else if(selector != null) {

					SpaceObject object = selector.getTarget();

					if(object.isSpaceShip()) {
						controller.setSpaceShip(object.getSpaceShip());
					}
				}

			} else if(name == SELECTOR_ACTIVATE_KEY && inputMode == InputMode.CONTROL_MODE) {

				ObjectSelector selector = getSelector();

				if(selector != null) {
					selector.acivate();
				}
			}
		}
	};;

	/** слушатель выбора индикатора для его селекта */
	private final HudEventListener<SelectIndicatorEvent> selectIndicatorListener = new HudEventListener<SelectIndicatorEvent>() {

		@Override
		public void notify(SelectIndicatorEvent event) {
			selectTarget(event.getObject(), LocalObjects.get());
		}
	};

	/** вктор вертикального направления корабля игрока */
	private final Vector3f up;

	/** текущий активный селектор */
	private ObjectSelector selector;

	/** время последней синхронизации */
	private long lastSync;

	public SelectorsController() {
		this.aroundObjects = Arrays.toArraySet(SpaceObject.class);
		this.selectedObjects = Arrays.toArraySet(SpaceObject.class);
		this.up = new Vector3f();
	}

	/**
	 * Активировать взаимодействие с индикатором.
	 */
	public void activate() {

		ObjectSelector selector = getSelector();

		if(selector != null) {
			selector.acivate();
		}
	}

	@Override
	public void addObject(SpaceObject object) {

		SelectorType selectorType = object.getSelectorType();

		if(selectorType == SelectorType.NONE || object == getPlayerShip()) {
			return;
		}

		Array<SpaceObject> objects = getAroundObjects();

		if(!objects.contains(object)) {
			objects.add(object);
		}
	}

	@Override
	public void bind(final Nifty nifty, final Screen screen, final Element element, final Properties parameter, final Attributes controlDefinitionAttributes) {
		super.bind(nifty, screen, element, parameter, controlDefinitionAttributes);

		SPACE_LOCATION.addListener(this);
		UPDATE_UI_MANAGER.addElement(this);
	}

	/**
	 * Построение селектора для указанного объекта.
	 * 
	 * @param target целевой объектю
	 * @return седектор этого объекта.
	 */
	private ObjectSelector buildSelector(SpaceObject target) {

		SelectorType selectorType = target.getSelectorType();

		ObjectSelector selector = selectorType.build(target, getNifty(), getScreen(), getBasePanel());

		if(selector == null) {
			return selector;
		}

		Element element = selector.getElement();
		element.hide();

		return selector;
	}

	/**
	 * Поиск следующей цели для индикатора.
	 * 
	 * @param local контейнер локальных объектов.
	 * @param ship корабль игрока.
	 * @param currentTime текущее время.
	 * @return следующий объект для индикации.
	 */
	private SpaceObject findNext(LocalObjects local, final PlayerShip ship, final long currentTime) {

		if(ship == null) {
			return null;
		}

		Array<SpaceObject> selectedObjects = getSelectedObjects();
		Array<SpaceObject> aroundObjects = getAroundObjects();

		// удаляем объекты, которые уже не могут больше быть выделенными
		selectedObjects.retainAll(aroundObjects);

		if(aroundObjects.isEmpty()) {
			return null;
		}

		Camera camera = GAME.getCamera();
		Vector3f position = local.getNextVector();
		Vector2f vector = ship.getVectorRotation();

		synchronized(camera) {

			// ищем еще из не селекнутых
			for(SpaceObject object : aroundObjects.array()) {

				if(object == null) {
					break;
				}

				if(!object.isVisible() || selectedObjects.contains(object)) {
					continue;
				}

				SpaceObjectView view = object.getView();

				if(view == null) {
					continue;
				}

				position = camera.getScreenCoordinates(view.getLocation(), position);

				if(!GameUtil.isVisibleOnScreen(position, camera)) {
					continue;
				}

				if(vector.distanceSquared(position.getX(), position.getY()) > SQUARED_DISTANCE_TO_VECTOR) {
					continue;
				}

				return object;
			}

			// ищем среди ранее селекнутых
			for(SpaceObject object : selectedObjects.array()) {

				if(object == null) {
					break;
				}

				if(!object.isVisible()) {
					continue;
				}

				SpaceObjectView view = object.getView();

				if(view == null) {
					continue;
				}

				position = camera.getScreenCoordinates(view.getLocation(), position);

				if(!GameUtil.isVisibleOnScreen(position, camera)) {
					continue;
				}

				if(vector.distanceSquared(position.getX(), position.getY()) > SQUARED_DISTANCE_TO_VECTOR) {
					continue;
				}

				selectedObjects.clear();
				return object;
			}
		}

		return null;
	}

	@Override
	public void finish() {
		super.finish();
		onExit(getSelector());
	}

	/**
	 * @return слушатель нажатий на кнопки.
	 */
	public ActionListener getActionListener() {
		return actionListener;
	}

	/**
	 * @return список окружающих объектов.
	 */
	private Array<SpaceObject> getAroundObjects() {
		return aroundObjects;
	}

	/**
	 * @return время последней синхронизации.
	 */
	public long getLastSync() {
		return lastSync;
	}

	/**
	 * @return список уже ранее выбранных объектов.
	 */
	private Array<SpaceObject> getSelectedObjects() {
		return selectedObjects;
	}

	/**
	 * @return слушатель выбора индикатора для его селекта.
	 */
	public HudEventListener<SelectIndicatorEvent> getSelectIndicatorListener() {
		return selectIndicatorListener;
	}

	/**
	 * @return текущий активный селектор.
	 */
	private ObjectSelector getSelector() {
		return selector;
	}

	/**
	 * @return вктор вертикального направления корабля игрока.
	 */
	public Vector3f getUp() {
		return up;
	}

	@Override
	public boolean isComplexElement() {
		return false;
	}

	/**
	 * Обработка отмены селектора.
	 * 
	 * @param selector сброшенный селектор.
	 */
	public void onExit(final ObjectSelector selector) {

		if(selector == null) {
			return;
		}

		selector.setTarget(null);
		selector.setPlayerShip(null);

		Element element = selector.getElement();
		element.markForRemoval();

		setSelector(null);

		PlayerShip playerShip = getPlayerShip();

		if(playerShip == null) {
			return;
		}

		playerShip.setTarget(null);
	}

	@Override
	public void register(HudController controller) {
		super.register(controller);
		controller.registerListener(SelectIndicatorEvent.class, getSelectIndicatorListener());
	}

	@Override
	public void register(InputManager inputManager) {
		super.register(inputManager);
		inputManager.addListener(getActionListener(), MOVE_CAMERA_KEY, SWITCH_SELECTOR_KEY, SELECTOR_ACTIVATE_KEY);
	}

	@Override
	public void removeObject(SpaceObject object) {

		SelectorType selectorType = object.getSelectorType();

		if(selectorType == SelectorType.NONE) {
			return;
		}

		Array<SpaceObject> selectedObjects = getSelectedObjects();
		selectedObjects.fastRemove(object);

		Array<SpaceObject> aroudObjects = getAroundObjects();
		aroudObjects.fastRemove(object);

		CameraController controller = CameraController.getInstance();

		if(controller.getSpaceShip() == object) {
			controller.setSpaceShip(getPlayerShip());
		}

		ObjectSelector selector = getSelector();

		if(selector != null && selector.getTarget() == object) {
			onExit(selector);
			return;
		}
	}

	/**
	 * Выбор цели мышкой.
	 */
	private void selectTarget(SpaceObject object, LocalObjects local) {

		PlayerShip playerShip = getPlayerShip();

		if(playerShip == null) {
			return;
		}

		ObjectSelector selector = getSelector();

		if(selector != null && selector.getTarget() == object) {
			return;
		}

		Array<SpaceObject> selectedObjects = getSelectedObjects();
		Array<SpaceObject> aroundObjects = getAroundObjects();

		selectedObjects.clear();
		selectedObjects.addAll(aroundObjects);
		selectedObjects.fastRemove(object);
		switchSelector(local);
	}

	/**
	 * @param lastSync время последней синхронизации.
	 */
	public void setLastSync(long lastSync) {
		this.lastSync = lastSync;
	}

	/**
	 * @param selector текущий активный селектор.
	 */
	private void setSelector(final ObjectSelector selector) {
		this.selector = selector;

		RequestSelectTarget packet = RequestSelectTarget.getInstance(selector == null ? null : selector.getTarget());
		NETWORK.sendPacketToGameServer(packet);
	}

	/**
	 * Переключение седектора на другой объект.
	 * 
	 * @param local контейнер локальных объектов.
	 */
	public void switchSelector(LocalObjects local) {

		ObjectSelector selector = getSelector();

		if(selector != null) {

			SpaceObject object = selector.getTarget();

			if(object != null) {
				getSelectedObjects().add(object);
			}

			SpaceObject next = findNext(local, selector.getPlayerShip(), System.currentTimeMillis());

			if(next == object) {
				return;
			}

			onExit(selector);
		}
	}

	@Override
	public boolean update(LocalObjects local, long currentTime) {

		PlayerShip playerShip = getPlayerShip();

		if(playerShip == null) {
			return false;
		}

		ObjectSelector selector = getSelector();

		if(selector != null) {

			final SpaceObject object = selector.getTarget();

			if(object == null || !object.isVisible()) {
				onExit(selector);
				return false;
			}

			selector.update(currentTime);

			if(currentTime - getLastSync() > SYNC_INTERVAL) {
				NETWORK.sendPacketToGameServer(RequestForceSyncObject.getInstance(object));
				setLastSync(currentTime);
			}

			return false;
		}

		SpaceObject target = findNext(local, playerShip, currentTime);

		if(target != null) {

			selector = buildSelector(target);

			if(selector == null) {
				return false;
			}

			selector.setTarget(target);
			selector.setPlayerShip(playerShip);
			selector.start();

			playerShip.setTarget(target);
			setSelector(selector);

			GameUIController controller = GameUIController.getInstance();
			controller.notifyEvent(SelectedObjectEvent.get(target));

			NETWORK.sendPacketToGameServer(RequestForceSyncObject.getInstance(target));
			setLastSync(currentTime);
		}

		return false;
	}
}
