package com.ss.client.gui.controller.game.panel.fast;

import java.util.Comparator;
import java.util.Properties;

import rlib.util.Strings;
import rlib.util.array.Array;
import rlib.util.array.ArrayComparator;
import rlib.util.array.Arrays;
import rlib.util.table.Table;
import rlib.util.table.Tables;

import com.jme3.input.InputManager;
import com.jme3.input.KeyInput;
import com.jme3.input.MouseInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.input.controls.MouseButtonTrigger;
import com.jme3.input.controls.Trigger;
import com.ss.client.gui.controller.game.GameUIComponent;
import com.ss.client.gui.controller.game.GameUIController;
import com.ss.client.gui.controller.game.GameUIEventListener;
import com.ss.client.gui.controller.game.event.GameUIEvent;
import com.ss.client.gui.controller.game.event.impl.UpdateFastPanelCellEvent;
import com.ss.client.gui.element.DraggableElementUI;
import com.ss.client.gui.element.DroppableElementUI;
import com.ss.client.gui.element.event.ElementUIEvent;
import com.ss.client.gui.element.event.impl.DraggableStopEvent;
import com.ss.client.gui.element.event.impl.DroppableDropedEvent;
import com.ss.client.gui.element.event.impl.SecondaryClickEvent;
import com.ss.client.gui.element.filter.DroppableDropFilter;
import com.ss.client.gui.element.impl.DraggableElementUIImpl;
import com.ss.client.gui.element.listener.ElementUIEventListener;
import com.ss.client.gui.model.ElementUtils;
import com.ss.client.gui.model.InputMode;
import com.ss.client.model.ship.player.PlayerShip;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.input.NiftyInputEvent;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.tools.Color;
import de.lessvoid.xml.xpp3.Attributes;

/**
 * Реализация контролера панели быстрого доступа.
 * 
 * @author Ronn
 */
public class FastPanelUIController extends DraggableElementUIImpl implements GameUIComponent, DroppableDropFilter {

	public static final String ACTION_INPUT_MAPPING_PREFIX = "game_ui_fast_panel_use";

	public static final String BACKGROUND_IMAGE = "ui/game/fast_panel/background.png";
	public static final String SPLIT_IMAGE = "ui/game/fast_panel/split.png";
	public static final String CELL_BACKGROUND_IMAGE = "ui/game/fast_panel/cell.png";

	public static final String PANEL_ID = "#base_fast_panel";

	public static final String BACKGROUND_ID = "#background";
	public static final String SPLIT_CELLS_ID = "#split_cells";
	public static final String CONTENT_ID = "#cell_table";
	public static final String CELL_KEY_ICON_ID = "#icon";
	public static final String CELL_KEY_LABEL_ID = "#key_label";
	public static final String CELL_CONTAINER_ID = "#container";
	public static final String CELL_PANEL_ID = "#cell_";

	public static final String PAGE_NEXT_ID = "#page_next";
	public static final String PAGE_NUM_ID = "#page_num";
	public static final String PAGE_PREV_ID = "#page_prev";
	public static final String PAGINATION_PANEL_ID = "#pagination_panel";
	public static final String DRAGGED_PANEL_ID = "#dragged_panel";

	public static final Color TEXT_COLOR = Color.WHITE;

	private static final int[] KEY_TABLE = {
		KeyInput.KEY_1,
		KeyInput.KEY_2,
		KeyInput.KEY_3,
		KeyInput.KEY_4,
		KeyInput.KEY_5,
		KeyInput.KEY_6,
		KeyInput.KEY_7,
		KeyInput.KEY_8,
	};

	private static final Comparator<Element> CELL_COMPARATOR = new ArrayComparator<Element>() {

		@Override
		protected int compareImpl(Element first, Element second) {

			String firstId = first.getParent().getId();
			String secondId = second.getParent().getId();

			int firstValue = Integer.parseInt(firstId.replace(CELL_PANEL_ID, Strings.EMPTY));
			int secondValue = Integer.parseInt(secondId.replace(CELL_PANEL_ID, Strings.EMPTY));

			return Integer.compare(firstValue, secondValue);
		}
	};

	private final ElementUIEventListener<DraggableElementUI> dragStopEventListener = new ElementUIEventListener<DraggableElementUI>() {

		@Override
		public void notifyEvent(ElementUIEvent<DraggableElementUI> event) {

			DraggableStopEvent stopEvent = (DraggableStopEvent) event;

			if(stopEvent.isNotFound()) {
				remove((FastPanelItem) stopEvent.getElement());
			}
		}
	};

	private final ElementUIEventListener<DroppableElementUI> dropEventListener = new ElementUIEventListener<DroppableElementUI>() {

		@Override
		public void notifyEvent(ElementUIEvent<DroppableElementUI> event) {

			DroppableElementUI droppable = event.getElement();

			if(event.getEventType() == DroppableDropedEvent.EVENT_TYPE) {

				DraggableElementUI draggable = droppable.getDraggable();

				if(!(draggable instanceof FastPanelItem)) {
					return;
				}

				FastPanelItem item = (FastPanelItem) draggable;
				item.updateToServer(Arrays.indexOf(getCells(), droppable.getElement()));

				Array<FastPanelItem> items = getItems();

				if(!items.contains(item)) {
					draggable.addElementListener(SecondaryClickEvent.EVENT_TYPE, clickEventListener);
					draggable.addElementListener(DraggableStopEvent.DRAGGABLE_STOP_EVENT, dragStopEventListener);
					items.add(item);
				}
			}
		}
	};

	private final ElementUIEventListener<DraggableElementUI> clickEventListener = new ElementUIEventListener<DraggableElementUI>() {

		@Override
		public void notifyEvent(ElementUIEvent<DraggableElementUI> event) {

			DraggableElementUI draggable = event.getElement();
			DroppableElementUI droppable = draggable.getDroppable();

			if(droppable != null && Arrays.contains(getCells(), droppable.getElement())) {
				activate(droppable);
			}
		}
	};

	/** слушатель нажатий пользователя */
	private final ActionListener inputListener = new ActionListener() {

		@Override
		public void onAction(String name, boolean isPressed, float tpf) {

			if(getInputMode() != InputMode.CONTROL_MODE || isPressed) {
				return;
			}

			DroppableElementUI activated = keyBindTable.get(name);

			if(activated != null) {
				activate(activated);
			}
		}
	};

	/** слушатель событий связанных с элементами на панели */
	private final GameUIEventListener fastPanelItemEventListener = new GameUIEventListener() {

		@Override
		public void notifyEvent(GameUIEvent event) {

			FastPanelItemEvent itemEvent = (FastPanelItemEvent) event;
			Array<FastPanelItem> items = getItems();

			for(FastPanelItem item : items.array()) {

				if(item == null) {
					break;
				}

				if(item.processEvent(itemEvent)) {
					break;
				}
			}
		}
	};

	/** слушатель обновления позиций элементов на панели */
	private final GameUIEventListener updateCellListener = new GameUIEventListener() {

		@Override
		public void notifyEvent(GameUIEvent event) {

			UpdateFastPanelCellEvent cellEvent = (UpdateFastPanelCellEvent) event;
			Element cell = getCell(cellEvent.getOrder());

			if(cell == null) {
				return;
			}

			FastPanelItem item = cellEvent.getItem();
			item.setIndex(cellEvent.getOrder());

			DroppableElementUI droppable = cell.getControl(DroppableElementUI.class);
			droppable.accept(item.getDroppable(), item);

			if(item.getDroppable() != droppable) {
				item.setIndex(-1);
			}
		}
	};

	/** таблица биндинга кнопок на ячейки */
	private final Table<String, DroppableElementUI> keyBindTable;

	/** текущий набор элемнтов на панели */
	private final Array<FastPanelItem> items;

	/** массив ячеяк панели */
	private Element[] cells;

	/** контрол UI */
	private Nifty nifty;
	/** экран этого элемента */
	private Screen screen;

	/** менеджер ввода */
	private InputManager inputManager;

	/** режим ввода */
	private InputMode inputMode;

	public FastPanelUIController() {
		this.keyBindTable = Tables.newObjectTable();
		this.items = Arrays.toArray(FastPanelItem.class);
	}

	@Override
	public boolean accept(DroppableElementUI source, DraggableElementUI draggable, DroppableElementUI target) {

		if(!(draggable instanceof FastPanelItem)) {
			return false;
		}

		FastPanelItem current = (FastPanelItem) target.getDraggable();
		FastPanelItem item = (FastPanelItem) draggable;

		if(source != null && Arrays.contains(getCells(), source.getElement())) {

			if(current != null) {
				source.drop(current, null);
			}

			return true;

		} else {

			if(current != null) {
				remove(current);
			}

			DraggableElementUI result = item.createFrom(target.getElement(), getScreen(), getNifty());
			target.drop(result, null);
		}

		return false;
	}

	/**
	 * Активация элемента в ячейки.
	 */
	public void activate(DroppableElementUI droppable) {

		FastPanelItem item = (FastPanelItem) droppable.getDraggable();

		if(item == null) {
			return;
		}

		item.activate();
	}

	@Override
	public void bind(Nifty nifty, Screen screen, Element element, Properties properties, Attributes attributes) {
		super.bind(nifty, screen, element, properties, attributes);

		this.nifty = nifty;
		this.screen = screen;

		// подготовка списка ячеяк
		{
			final Array<Element> cells = Arrays.toArray(Element.class);

			// формируем список всех ячейках
			ElementUtils.addElements(cells, element, CELL_CONTAINER_ID);

			cells.sort(CELL_COMPARATOR);

			for(Element cell : cells) {
				DroppableElementUI droppable = cell.getControl(DroppableElementUI.class);
				droppable.addElementListener(DroppableDropedEvent.EVENT_TYPE, dropEventListener);
				droppable.addFilter(this);
			}

			this.cells = cells.toArray(new Element[cells.size()]);
		}
	}

	@Override
	public void finish() {

		Array<String> mappingNames = Arrays.toArray(String.class);

		Table<String, DroppableElementUI> keyBindTable = getKeyBindTable();
		keyBindTable.keyArray(mappingNames);
		keyBindTable.clear();

		InputManager inputManager = getInputManager();

		for(String name : mappingNames) {
			inputManager.deleteMapping(name);
		}

		inputManager.removeListener(getInputListener());

		Element[] cells = getCells();

		for(Element cell : cells) {

			DroppableElementUI droppable = cell.getControl(DroppableElementUI.class);
			droppable.removeElementListener(DroppableDropedEvent.EVENT_TYPE, dropEventListener);
			droppable.removeFilter(this);

			DraggableElementUI draggable = droppable.getDraggable();

			if(draggable != null) {
				draggable.removeElementListener(SecondaryClickEvent.EVENT_TYPE, clickEventListener);
			}
		}

		Arrays.clear(cells);
	}

	/**
	 * Получение ячейки по указанному индексу.
	 * 
	 * @param index индекс ячейки.
	 * @return сама ячейка.
	 */
	public Element getCell(final int index) {

		if(index < 0 || index >= cells.length) {
			return null;
		}

		return cells[index];
	}

	/**
	 * @return чписок доступных ячеяк.
	 */
	public Element[] getCells() {
		return cells;
	}

	/**
	 * @return слушатель событий связанных с элементами на панели.
	 */
	public GameUIEventListener getFastPanelItemEventListener() {
		return fastPanelItemEventListener;
	}

	/**
	 * @return слушатель нажатий пользователя.
	 */
	public ActionListener getInputListener() {
		return inputListener;
	}

	/**
	 * @return менеджер ввода.
	 */
	public InputManager getInputManager() {
		return inputManager;
	}

	/**
	 * @return режим ввода.
	 */
	public InputMode getInputMode() {
		return inputMode;
	}

	/**
	 * @return текущий набор элемнтов на панели.
	 */
	public Array<FastPanelItem> getItems() {
		return items;
	}

	/**
	 * @return таблица биндинга кнопок на ячейки.
	 */
	public Table<String, DroppableElementUI> getKeyBindTable() {
		return keyBindTable;
	}

	/**
	 * @return контрол UI.
	 */
	public Nifty getNifty() {
		return nifty;
	}

	/**
	 * @return экран этого элемента.
	 */
	public Screen getScreen() {
		return screen;
	}

	/**
	 * @return слушатель обновления позиций элементов на панели.
	 */
	public GameUIEventListener getUpdateCellListener() {
		return updateCellListener;
	}

	@Override
	public boolean inputEvent(NiftyInputEvent inputEvent) {
		return false;
	}

	@Override
	public void onStartScreen() {
	}

	@Override
	public void register(GameUIController controller) {
		controller.registerListener(FastPanelItemEvent.EVENT_TYPE, getFastPanelItemEventListener());
		controller.registerListener(UpdateFastPanelCellEvent.EVENT_TYPE, getUpdateCellListener());
	}

	@Override
	public void register(InputManager inputManager) {

		Element[] cells = getCells();

		for(int i = 0, length = KEY_TABLE.length; i < length; i++) {
			register(inputManager, new KeyTrigger(KEY_TABLE[i]), cells[i + 2]);
		}

		register(inputManager, new MouseButtonTrigger(MouseInput.BUTTON_LEFT), cells[0]);
		register(inputManager, new MouseButtonTrigger(MouseInput.BUTTON_RIGHT), cells[1]);
	}

	/**
	 * Регистрация прослушивания ввода пользователя для активации ячеяк.
	 * 
	 * @param inputManager менеджер ввода.
	 * @param trigger тригер активации.
	 * @param element активируемая ячейка.
	 */
	private void register(InputManager inputManager, Trigger trigger, Element element) {

		Element parent = element.getParent();

		String name = ACTION_INPUT_MAPPING_PREFIX + parent.getId();

		inputManager.addMapping(name, trigger);
		inputManager.addListener(getInputListener(), name);

		getKeyBindTable().put(name, element.getControl(DroppableElementUI.class));
	}

	/**
	 * Удаление итема панели.
	 */
	private void remove(FastPanelItem item) {
		item.removeElementListener(SecondaryClickEvent.EVENT_TYPE, clickEventListener);
		item.removeElementListener(DraggableStopEvent.DRAGGABLE_STOP_EVENT, dragStopEventListener);
		item.updateToServer(-1);
		getItems().fastRemove(item);
		item.remove();
	}

	/**
	 * @param inputManager менеджер ввода.
	 */
	public void setInputManager(InputManager inputManager) {
		this.inputManager = inputManager;
	}

	@Override
	public void setInputMode(InputMode inputMode) {
		this.inputMode = inputMode;
	}

	@Override
	public void setPlayerShip(PlayerShip playerShip) {
	}
}
