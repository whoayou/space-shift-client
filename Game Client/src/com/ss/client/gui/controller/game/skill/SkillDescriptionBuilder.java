package com.ss.client.gui.controller.game.skill;

import com.ss.client.gui.element.effect.builder.EffectUIBuilder;
import com.ss.client.gui.element.effect.builder.impl.HoverHintEffectUIBuilder;

/**
 * Реализация конструктора подсказки о умении.
 * 
 * @author Ronn
 */
public class SkillDescriptionBuilder extends HoverHintEffectUIBuilder {

	static {
		EffectUIBuilder builder = new SkillDescriptionBuilder();
		NIFTY.registerEffect(builder.getEffectName(), SkillDescriptionEffect.class.getName());
	}

}
