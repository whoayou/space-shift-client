package com.ss.client.gui.controller.game;

import com.jme3.input.InputManager;
import com.ss.client.gui.model.InputMode;
import com.ss.client.model.ship.player.PlayerShip;

import de.lessvoid.nifty.controls.Controller;

/**
 * Интерфейс для реализации компонента игрового UI.
 * 
 * @author Ronn
 */
public interface GameUIComponent extends Controller {

	/**
	 * Завершение работы компонента.
	 */
	public void finish();

	/**
	 * Регистрация у контролера.
	 * 
	 * @param controller контролер игрового УИ.
	 */
	public void register(final GameUIController controller);

	/**
	 * Регистрация прослушки ввода.
	 * 
	 * @param inputManager менеджер ввода.
	 */
	public void register(final InputManager inputManager);

	/**
	 * Уведомление о смене режима ввода.
	 * 
	 * @param inputMode новый режим ввода.
	 */
	public void setInputMode(InputMode inputMode);

	/**
	 * @param playerShip корабль игрока.
	 */
	public void setPlayerShip(PlayerShip playerShip);
}
