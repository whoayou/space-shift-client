package com.ss.client.gui.controller.game.panel.status;

import java.util.Properties;

import rlib.util.array.Array;

import com.jme3.input.InputManager;
import com.ss.client.gui.controller.game.GameUIComponent;
import com.ss.client.gui.controller.game.GameUIController;
import com.ss.client.gui.element.LabelUI;
import com.ss.client.gui.element.RegulatorUI;
import com.ss.client.gui.element.event.ElementUIEvent;
import com.ss.client.gui.element.event.impl.ChangeElementUIEvent;
import com.ss.client.gui.element.listener.ElementUIEventListener;
import com.ss.client.gui.model.ElementUtils;
import com.ss.client.gui.model.InputMode;
import com.ss.client.gui.util.UpdateUIElement;
import com.ss.client.manager.UpdateUIManager;
import com.ss.client.model.ship.player.PlayerShip;
import com.ss.client.network.Network;
import com.ss.client.network.packet.client.RequestCurrentEnergyBalance;
import com.ss.client.network.packet.client.RequestEnergyBalance;
import com.ss.client.util.GameUtil;
import com.ss.client.util.LocalObjects;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.controls.AbstractController;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.input.NiftyInputEvent;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.tools.Color;
import de.lessvoid.xml.xpp3.Attributes;

/**
 * Реализация контролера панели статуса корабля.
 * 
 * @author Ronn
 */
public class ShipStatusController extends AbstractController implements GameUIComponent, UpdateUIElement {

	public static final String ID = "#ship_status_base_panel";

	public static final String STATE_PANEL_ID = "#state_panel";
	public static final String LABEL_PANEL_ID = "#label_panel";
	public static final String STRENGTH_LABEL_ID = "#strength_label";
	public static final String ENERGY_LABEL_ID = "#energy_label";
	public static final String INDICATOR_PANEL_ID = "#indicator_panel";
	public static final String STRENGTH_INDICATOR_ID = "#strength_indicator";
	public static final String ENERGY_INDICATOR_ID = "#energy_indicator";
	public static final String ICON_PANEL_ID = "#icon_panel";
	public static final String STRENGTH_ICON_ID = "#strength_icon";
	public static final String ENERGY_ICON_ID = "#energy_icon";
	public static final String CONTROL_PANEL_ID = "#control_panel";
	public static final String ENGINE_PANEL_ID = "#engine_panel";
	public static final String ENGINE_LABEL_ID = "#engine_label";
	public static final String ENGINE_CONTROL_ID = "#engine_control";
	public static final String SHIELD_PANEL_ID = "#shield_panel";
	public static final String SHIELD_LABEL_ID = "#shield_label";
	public static final String SHIELD_CONTROL_ID = "#shield_control";
	public static final String WEAPON_PANEL_ID = "#weapon_panel";
	public static final String WEAPON_LABEL_ID = "#weapon_label";
	public static final String WEAPON_CONTROL_ID = "#weapon_control";
	public static final String BUFFER_PANEL_ID = "#buffer_panel";

	public static final String IMAGE_REGULATOR_ADD = "game/horizontal-regulator/regulator_add.png";
	public static final String IMAGE_REGULATOR_SUB = "game/horizontal-regulator/regulator_sub.png";
	public static final String SHIP_STATUS_STRENGTH_IMAGE = "game/ship_status/strength.png";
	public static final String SHIP_STATUS_ENERGY_IMAGE = "game/ship_status/energy.png";
	public static final String REGULATOR_WEAPON_IMAGE = "game/ship_status/energy_weapon.png";
	public static final String REGULATOR_SHIELD_IMAGE = "game/ship_status/energy_shield.png";
	public static final String REGULATOR_ENGINE_IMAGE = "game/ship_status/energy_engine.png";

	public static final Color STRENGTH_INDICATOR_COLOR = new Color("#35c804");
	public static final Color ENERGY_INDICATOR_COLOR = new Color("#29abe2");
	public static final Color REGULATOR_VALUE_COLOR = new Color("#35c804");
	public static final Color REGULATOR_BASE_COLOR = new Color("#29abe2");

	public static final String REGULATOR_BUTTON_SIZE = "17px";
	public static final String REGULATOR_HEIGHT = "18px";
	public static final String REGULATOR_PANEL_HEIGHT = "6px";
	public static final String REGULATOR_PANEL_WIDTH = "110px";

	public static final String ENERGY_CONTROL_WIDTH = "200px";
	public static final String ENERGY_CONTROL_HEIGHT = "25px";

	public static final int INDICATOR_WIDTH = 271;
	public static final int CONTROL_WIDTH = 150;

	private static final UpdateUIManager UPDATE_UI_MANAGER = UpdateUIManager.getInstance();
	private static final Network NETWORK = Network.getInstance();

	private final ElementUIEventListener<RegulatorUI> regulatorEventListener = new ElementUIEventListener<RegulatorUI>() {

		@Override
		public void notifyEvent(ElementUIEvent<RegulatorUI> event) {

			if(!isIgnoreEvents()) {
				requestEnergyBalance();
			}
		}
	};

	/** индикатор энергии */
	private Element energyIndicator;
	/** индикатор прочности */
	private Element strengthIndicator;
	/** панель регуляторов */
	private Element controlPanel;

	/** надпись со значением энергии */
	private LabelUI energyLabel;
	/** надпись со значением прочности */
	private LabelUI strengthLabel;

	/** контролер отдачи энергии на оружие */
	private RegulatorUI weaponControl;
	/** контролер отдачи энергии на щит */
	private RegulatorUI shieldControl;
	/** контролер отдачи энергии на двигатели */
	private RegulatorUI engineControl;

	/** корабль игрока */
	private PlayerShip playerShip;

	/** последний обновленный процент энергии */
	private float lastEnergyPercent;
	/** последний обновленный процент прочности */
	private float lastStrengthPercent;

	/** последний уровень энергии */
	private int lastEnergy;
	/** последний уровень прочности */
	private int lastStrength;

	/** игорировать ли события контролов */
	private boolean ignoreEvents;

	@Override
	public void bind(Array<UpdateUIElement> container) {
	}

	@Override
	public void bind(Nifty nifty, Screen screen, Element element, Properties parameter, Attributes controlDefinitionAttributes) {
		this.energyIndicator = element.findElementByName(ENERGY_INDICATOR_ID);
		this.strengthIndicator = element.findElementByName(STRENGTH_INDICATOR_ID);
		this.controlPanel = element.findElementByName(CONTROL_PANEL_ID);
		this.energyLabel = element.findNiftyControl(ENERGY_LABEL_ID, LabelUI.class);
		this.strengthLabel = element.findNiftyControl(STRENGTH_LABEL_ID, LabelUI.class);
		this.weaponControl = element.findNiftyControl(WEAPON_CONTROL_ID, RegulatorUI.class);
		this.shieldControl = element.findNiftyControl(SHIELD_CONTROL_ID, RegulatorUI.class);
		this.engineControl = element.findNiftyControl(ENGINE_CONTROL_ID, RegulatorUI.class);

		weaponControl.addElementListener(ChangeElementUIEvent.CHANGE_ELEMENT_UI_EVENT, regulatorEventListener);
		shieldControl.addElementListener(ChangeElementUIEvent.CHANGE_ELEMENT_UI_EVENT, regulatorEventListener);
		engineControl.addElementListener(ChangeElementUIEvent.CHANGE_ELEMENT_UI_EVENT, regulatorEventListener);
	}

	@Override
	public void finish() {
		setPlayerShip(null);
	}

	/**
	 * @return панель регуляторов.
	 */
	public Element getControlPanel() {
		return controlPanel;
	}

	/**
	 * @return индикатор энергии.
	 */
	public Element getEnergyIndicator() {
		return energyIndicator;
	}

	/**
	 * @return надпись со значением энергии.
	 */
	public LabelUI getEnergyLabel() {
		return energyLabel;
	}

	/**
	 * @return контролер отдачи энергии на двигатели.
	 */
	public RegulatorUI getEngineControl() {
		return engineControl;
	}

	/**
	 * @return последний уровень энергии.
	 */
	public int getLastEnergy() {
		return lastEnergy;
	}

	/**
	 * @return последний обновленный процент энергии.
	 */
	public float getLastEnergyPercent() {
		return lastEnergyPercent;
	}

	/**
	 * @return последний уровень прочности.
	 */
	public int getLastStrength() {
		return lastStrength;
	}

	/**
	 * @return последний обновленный процент прочности.
	 */
	public float getLastStrengthPercent() {
		return lastStrengthPercent;
	}

	/**
	 * @return корабль игрока.
	 */
	public PlayerShip getPlayerShip() {
		return playerShip;
	}

	/**
	 * @return контролер отдачи энергии на щит.
	 */
	public RegulatorUI getShieldControl() {
		return shieldControl;
	}

	/**
	 * @return индикатор прочности.
	 */
	public Element getStrengthIndicator() {
		return strengthIndicator;
	}

	/**
	 * @return надпись со значением прочности.
	 */
	public LabelUI getStrengthLabel() {
		return strengthLabel;
	}

	/**
	 * @return контролер отдачи энергии на оружие.
	 */
	public RegulatorUI getWeaponControl() {
		return weaponControl;
	}

	@Override
	public boolean inputEvent(NiftyInputEvent inputEvent) {
		return false;
	}

	@Override
	public boolean isComplexElement() {
		return false;
	}

	/**
	 * @return игорировать ли события контролов.
	 */
	private boolean isIgnoreEvents() {
		return ignoreEvents;
	}

	@Override
	public void onStartScreen() {
		UPDATE_UI_MANAGER.addElement(this);
	}

	@Override
	public void register(GameUIController controller) {
		// TODO Auto-generated method stub

	}

	@Override
	public void register(InputManager inputManager) {
	}

	/**
	 * Запрос на обновление баланса энергии.
	 */
	private void requestEnergyBalance() {

		RegulatorUI weapon = getWeaponControl();
		RegulatorUI shield = getShieldControl();
		RegulatorUI engine = getEngineControl();

		NETWORK.sendPacketToGameServer(RequestEnergyBalance.getInstance(weapon.getCurrent(), shield.getCurrent(), engine.getCurrent()));
	}

	/**
	 * @param ignoreEvents игорировать ли события контролов.
	 */
	private void setIgnoreEvents(boolean ignoreEvents) {
		this.ignoreEvents = ignoreEvents;
	}

	@Override
	public void setInputMode(InputMode inputMode) {
		getControlPanel().setVisible(inputMode == InputMode.INTERFACE_MODE);
	}

	/**
	 * @param lastEnergy последний уровень энергии.
	 */
	public void setLastEnergy(int lastEnergy) {
		this.lastEnergy = lastEnergy;
	}

	/**
	 * @param lastEnergyPercent последний обновленный процент энергии.
	 */
	public void setLastEnergyPercent(float lastEnergyPercent) {
		this.lastEnergyPercent = lastEnergyPercent;
	}

	/**
	 * @param lastStrength последний уровень прочности.
	 */
	public void setLastStrength(int lastStrength) {
		this.lastStrength = lastStrength;
	}

	/**
	 * @param lastStrengthPercent последний обновленный процент прочности.
	 */
	public void setLastStrengthPercent(float lastStrengthPercent) {
		this.lastStrengthPercent = lastStrengthPercent;
	}

	@Override
	public void setPlayerShip(PlayerShip playerShip) {
		this.playerShip = playerShip;

		if(playerShip != null) {
			NETWORK.sendPacketToGameServer(RequestCurrentEnergyBalance.getInstance());
		}
	}

	/**
	 * обновление параметров распраделения.
	 * 
	 * @param weapon распределение на оружие.
	 * @param shield распределение на щит.
	 * @param engine распределение на двигатели.
	 */
	public void update(float weapon, float shield, float engine) {

		setIgnoreEvents(true);
		try {
			getWeaponControl().setCurrent(weapon);
			getShieldControl().setCurrent(shield);
			getEngineControl().setCurrent(engine);
		} finally {
			setIgnoreEvents(false);
		}
	}

	@Override
	public boolean update(LocalObjects local, long currentTime) {

		PlayerShip playerShip = getPlayerShip();

		if(playerShip == null) {
			return false;
		}

		float energyPercent = playerShip.getEnergyPercent();
		float strengthPercent = playerShip.getStrengthPercent();

		if(getLastEnergyPercent() != energyPercent || getLastStrengthPercent() != strengthPercent) {

			Element energyIndicator = getEnergyIndicator();
			energyIndicator.setConstraintWidth(GameUtil.getPixelSize((int) (energyPercent * INDICATOR_WIDTH)));

			Element strengthIndicator = getStrengthIndicator();
			strengthIndicator.setConstraintWidth(GameUtil.getPixelSize((int) (strengthPercent * INDICATOR_WIDTH)));

			ElementUtils.updateLayout(energyIndicator.getParent());

			setLastEnergyPercent(energyPercent);
			setLastStrengthPercent(strengthPercent);
		}

		int currentEnergy = playerShip.getCurrentEnergy();
		int currentStrength = playerShip.getCurrentStrength();

		if(getLastEnergy() != currentEnergy || getLastStrength() != currentStrength) {

			LabelUI energyLabel = getEnergyLabel();
			energyLabel.setText(String.valueOf(currentEnergy));
			LabelUI strengthLabel = getStrengthLabel();
			strengthLabel.setText(String.valueOf(currentStrength));

			setLastEnergy(currentEnergy);
			setLastStrength(currentStrength);
		}

		return false;
	}
}
