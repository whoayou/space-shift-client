package com.ss.client.gui.controller.game.window.quest;

/**
 * Набор констант связанных с UI текстом окна журнала заданий.
 * 
 * @author Ronn
 */
public interface QuestWindowTextIds {

	public static final String LOCALIZATION_FILE = "/data/localization/game_interface/quest_window.xml";

	public static final String WINDOW_TITLE = "@quest:windowTitle@";
	public static final String DESCRIPTION_CONTAINER_TITLE = "@quest:descriptionContainer@";
	public static final String REWARDS_CONTAINER_TITLE = "@quest:rewardsContainer@";
	public static final String COUNTERS_CONTAINER_TITLE = "@quest:countersContainer@";
	public static final String BUTTONS_CONTAINER_TITLE = "@quest:buttonsContainery@";
	public static final String QUEST_NAME_LABEL = "@quest:nameLbel@";
	public static final String QUEST_STATUS_LABEL = "@quest:statusLabel@";

}
