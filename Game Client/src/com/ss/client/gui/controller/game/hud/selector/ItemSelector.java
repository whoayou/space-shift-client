package com.ss.client.gui.controller.game.hud.selector;

import java.util.Properties;

import com.ss.client.gui.element.LabelUI;
import com.ss.client.model.SpaceObject;
import com.ss.client.model.item.SpaceItem;
import com.ss.client.network.Network;
import com.ss.client.network.packet.client.RequestTeleportItem;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.xml.xpp3.Attributes;

/**
 * Реализация селектора предметов.
 * 
 * @author Ronn
 */
public class ItemSelector extends AbstractObjectSelector<SpaceItem> {

	public static final String SELECTOR_ID = "#object_selector";

	public static final String SELECTOR_ICON = "game/hud/selector/default.png";

	public static final String INDICATOR_HEIGHT = "46px";
	public static final String INDICATOR_WIDTH = "46px";

	@Override
	public void acivate() {

		SpaceItem item = getTarget();

		if(item != null) {
			Network network = Network.getInstance();
			network.sendPacketToGameServer(RequestTeleportItem.getInstance(item));
		}
	}

	@Override
	public void bind(Nifty nifty, Screen screen, Element element, Properties parameter, Attributes controlDefinitionAttributes) {
		super.bind(nifty, screen, element, parameter, controlDefinitionAttributes);
	}

	@Override
	public void setTarget(SpaceObject target) {
		super.setTarget(target);

		SpaceItem item = getTarget();

		if(item != null) {

			LabelUI label = getNameLabel();
			String name = item.getName();

			long itemCount = item.getItemCount();

			if(itemCount == 1) {
				label.setText(name);
			} else {
				label.setText(name + "(" + itemCount + ")");
			}
		}
	}
}
