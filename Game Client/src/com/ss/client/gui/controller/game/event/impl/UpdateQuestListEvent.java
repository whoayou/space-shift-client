package com.ss.client.gui.controller.game.event.impl;

import rlib.util.array.Array;
import rlib.util.array.Arrays;

import com.ss.client.gui.controller.game.event.GameUIEventType;
import com.ss.client.gui.model.quest.QuestState;

/**
 * Реализация события полного обновления списка заданий.
 * 
 * @author Ronn
 */
public class UpdateQuestListEvent extends AbstractGameUIEvent {

	public static final GameUIEventType EVENT_TYPE = new GameUIEventType() {
	};

	private static final ThreadLocal<UpdateQuestListEvent> LOCAL_EVENT = new ThreadLocal<UpdateQuestListEvent>() {

		@Override
		protected UpdateQuestListEvent initialValue() {
			return new UpdateQuestListEvent();
		}
	};

	public static UpdateQuestListEvent get(Array<QuestState> container) {

		UpdateQuestListEvent event = LOCAL_EVENT.get();
		event.container.addAll(container);

		return event;
	}

	/** контейнер с заданиями */
	private final Array<QuestState> container;

	public UpdateQuestListEvent() {
		this.container = Arrays.toArray(QuestState.class);
	}

	/**
	 * @return контейнер с заданиями.
	 */
	public Array<QuestState> getContainer() {
		return container;
	}

	@Override
	public GameUIEventType getEventType() {
		return EVENT_TYPE;
	}
}
