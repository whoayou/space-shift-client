package com.ss.client.gui.controller.game.event.impl;

import com.ss.client.gui.controller.game.event.GameUIEventType;
import com.ss.client.model.SpaceObject;

/**
 * Событие о смене статуса дружелюбности объекта.
 * 
 * @author Ronn
 */
public class ObjectChangeFriendStatusEvent extends AbstractGameUIEvent {

	public static final GameUIEventType EVENT_TYPE = new GameUIEventType() {
	};

	private static final ThreadLocal<ObjectChangeFriendStatusEvent> LOCAL_EVENT = new ThreadLocal<ObjectChangeFriendStatusEvent>() {

		@Override
		protected ObjectChangeFriendStatusEvent initialValue() {
			return new ObjectChangeFriendStatusEvent();
		}
	};

	public static final ObjectChangeFriendStatusEvent get(SpaceObject object) {

		ObjectChangeFriendStatusEvent event = LOCAL_EVENT.get();
		event.object = object;

		return event;
	}

	/** объект изменивший статус */
	private SpaceObject object;

	@Override
	public GameUIEventType getEventType() {
		return EVENT_TYPE;
	}

	/**
	 * @return объект изменивший статус.
	 */
	public SpaceObject getObject() {
		return object;
	}
}
