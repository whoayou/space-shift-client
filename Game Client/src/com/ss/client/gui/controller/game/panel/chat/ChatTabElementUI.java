package com.ss.client.gui.controller.game.panel.chat;

import java.util.Properties;
import java.util.concurrent.atomic.AtomicInteger;

import rlib.util.linkedlist.LinkedList;
import rlib.util.linkedlist.LinkedLists;
import rlib.util.linkedlist.Node;

import com.jme3.network.Message;
import com.ss.client.gui.builder.game.factory.ChatElementUIFactory;
import com.ss.client.gui.element.LabelUI;
import com.ss.client.gui.element.ScrollPanelUI;
import com.ss.client.gui.element.impl.ButtonUIImpl;
import com.ss.client.gui.model.ElementUtils;
import com.ss.client.util.GameUtil;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.tools.Color;
import de.lessvoid.xml.xpp3.Attributes;

/**
 * Реализация контроллера таба чата.
 * 
 * @author Ronn
 */
public class ChatTabElementUI extends ButtonUIImpl {

	public static final String TAB_ID = "#tab_";
	public static final String TAB_SPLIT_ID = "#tab_split_";
	public static final String MESSAGE_VIEW_ID = "#message_view";
	public static final String MESSAGE_CONTAINER_ID = "#message_container";

	public static final String TAB_SPLIT_IMAGE = "ui/game/chat_panel/split.png";

	public static final String PROPERTY_TAB_TYPE = "tab_type";

	public static final Color NO_ACTIVE_COLOR = new Color("#4e4e4e");
	public static final Color ACTIVE_COLOR = Color.WHITE;

	/** список сообщений таба */
	private final LinkedList<ChatMessage> messages;
	/** список UI элементов сообщений таба */
	private final LinkedList<Element> messageElements;

	/** счетчик индексов элементов */
	private final AtomicInteger messageIndex;

	/** скролируемая панель отображающая сообщения */
	private ScrollPanelUI messageView;

	/** панель для размещения элемента */
	private Element messagePanel;

	/** название таба */
	private LabelUI title;

	/** тип таба чата */
	private ChatTabType tabType;

	public ChatTabElementUI() {
		this.messages = LinkedLists.newLinkedList(Message.class);
		this.messageElements = LinkedLists.newLinkedList(Element.class);
		this.messageIndex = new AtomicInteger();
	}

	/**
	 * Добавление сообщения в таб.
	 * 
	 * @param message сообщение.
	 */
	public synchronized void addMessage(ChatMessage message) {

		ChatTabType tabType = getTabType();

		if(!tabType.isVisible(message)) {
			return;
		}

		LinkedList<ChatMessage> messages = getMessages();
		LinkedList<Element> elements = getMessageElements();

		if(messages.size() > 10) {

			messages.poll();

			Element element = elements.poll();
			element.markForRemoval();
		}

		Element messagePanel = getMessagePanel();
		Element messageElement = ChatElementUIFactory.build(message, messagePanel, getScreen(), getNifty(), getMessageIndex());

		messages.add(message);
		elements.add(messageElement);

		updateHeight(messagePanel, elements);
		getMessageView().layoutCallback();
	}

	@Override
	public void bind(Nifty nifty, Screen screen, Element element, Properties parameter, Attributes attributes) {
		super.bind(nifty, screen, element, parameter, attributes);

		this.tabType = ChatTabType.valueOf(parameter.getProperty(PROPERTY_TAB_TYPE));
		this.title = element.findControl(TEXT_ELEMENT_ID, LabelUI.class);
		this.messagePanel = element.findElementByName(MESSAGE_CONTAINER_ID);
		this.messageView = element.findControl(MESSAGE_VIEW_ID, ScrollPanelUI.class);
	}

	/**
	 * @return список UI элементов сообщений таба.
	 */
	public LinkedList<Element> getMessageElements() {
		return messageElements;
	}

	/**
	 * @return счетчик индексов элементов.
	 */
	public AtomicInteger getMessageIndex() {
		return messageIndex;
	}

	/**
	 * @return панель для размещения сообщений.
	 */
	public Element getMessagePanel() {
		return messagePanel;
	}

	/**
	 * @return список сообщений таба.
	 */
	public LinkedList<ChatMessage> getMessages() {
		return messages;
	}

	/**
	 * @return панель отображающая сообщения.
	 */
	public ScrollPanelUI getMessageView() {
		return messageView;
	}

	/**
	 * @return тип таба чата.
	 */
	public ChatTabType getTabType() {
		return tabType;
	}

	/**
	 * @return название таба.
	 */
	public LabelUI getTitle() {
		return title;
	}

	/**
	 * Инициализация таба.
	 * 
	 * @param controller контроллер панели чата.
	 * @param messagePanel панель для размещения сообщений.
	 */
	public void init(ChatPanelUIController controller, Element messagePanel) {
		ScrollPanelUI messageView = getMessageView();
		Element element = messageView.getElement();
		element.markForMove(messagePanel);
	}

	/**
	 * @return видимы ли сообщения таба.
	 */
	public boolean isVisibleMessages() {
		return getMessageView().isVisible();
	}

	/**
	 * @param visible видимость сообщений этого таба.
	 */
	public void setVisibleMessages(boolean visible) {

		ScrollPanelUI messageView = getMessageView();
		messageView.setVisible(visible);

		LabelUI title = getTitle();
		title.setColor(visible ? ACTIVE_COLOR : NO_ACTIVE_COLOR);
	}

	/**
	 * Обновленин высоты панели сообщений.
	 */
	private void updateHeight(Element element, LinkedList<Element> messages) {

		int height = 0;

		for(Node<Element> node = messages.getFirstNode(); node != null; node = node.getNext()) {
			Element child = node.getItem();
			height += child.getHeight() + 1;
		}

		element.setConstraintHeight(GameUtil.getPixelSize(height));

		ElementUtils.updateLayout(element.getParent());
	}
}
