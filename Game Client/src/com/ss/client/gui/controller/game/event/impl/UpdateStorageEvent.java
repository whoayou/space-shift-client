package com.ss.client.gui.controller.game.event.impl;

import rlib.util.array.Array;
import rlib.util.array.Arrays;

import com.ss.client.gui.controller.game.event.GameUIEventType;
import com.ss.client.gui.controller.game.window.storage.CellInfo;

/**
 * Реализация события полного обновления хранилища корабля.
 * 
 * @author Ronn
 */
public class UpdateStorageEvent extends AbstractGameUIEvent {

	public static final GameUIEventType EVENT_TYPE = new GameUIEventType() {
	};

	private static final ThreadLocal<UpdateStorageEvent> LOCAL_EVENT = new ThreadLocal<UpdateStorageEvent>() {

		@Override
		protected UpdateStorageEvent initialValue() {
			return new UpdateStorageEvent();
		}
	};

	public static UpdateStorageEvent get(Array<CellInfo> container) {

		UpdateStorageEvent event = LOCAL_EVENT.get();
		event.container.addAll(container);

		return event;
	}

	/** контейнер информаций о ячейках */
	private final Array<CellInfo> container;

	public UpdateStorageEvent() {
		this.container = Arrays.toArray(CellInfo.class);
	}

	/**
	 * @return контейнер информаций о ячейках.
	 */
	public Array<CellInfo> getContainer() {
		return container;
	}

	@Override
	public GameUIEventType getEventType() {
		return EVENT_TYPE;
	}
}
