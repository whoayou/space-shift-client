package com.ss.client.gui.controller.game.quest.reward.impl;

import com.ss.client.gui.model.quest.reward.impl.ItemQuestReward;

/**
 * Реализация контроллера награды в виде предметов.
 * 
 * @author Ronn
 */
public class ItemQuestRewardController extends AbstractQuestRewardElementController {

	@Override
	public void refresh() {
		ItemQuestReward reward = (ItemQuestReward) getQuestReward();
		// TODO обновить иконку, и раместить строку n items
		getName().setText("itemId " + reward.getId() + " itemCount " + reward.getCount());
	}
}
