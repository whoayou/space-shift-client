package com.ss.client.gui.controller.game.item;

import com.ss.client.gui.controller.game.event.GameUIEventType;
import com.ss.client.gui.controller.game.event.impl.AbstractGameUIEvent;
import com.ss.client.gui.controller.game.panel.fast.FastPanelItemEvent;

/**
 * Событие о обновлении предмета.
 * 
 * @author Ronn
 */
public class UpdateItemEvent extends AbstractGameUIEvent implements FastPanelItemEvent {

	private static final ThreadLocal<UpdateItemEvent> LOCAL_EVENT = new ThreadLocal<UpdateItemEvent>() {

		@Override
		protected UpdateItemEvent initialValue() {
			return new UpdateItemEvent();
		}
	};

	public static final UpdateItemEvent get(long objectId, long itemCount) {

		UpdateItemEvent event = LOCAL_EVENT.get();
		event.objectId = objectId;
		event.itemCount = itemCount;

		return event;
	}

	/** дата старта отката */
	private long itemCount;
	/** уникальный ид предмета */
	private long objectId;

	@Override
	public GameUIEventType getEventType() {
		return EVENT_TYPE;
	}

	/**
	 * @return дата старта отката.
	 */
	public long getItemCount() {
		return itemCount;
	}

	/**
	 * @return уникальный ид предмета.
	 */
	public long getObjectId() {
		return objectId;
	}
}
