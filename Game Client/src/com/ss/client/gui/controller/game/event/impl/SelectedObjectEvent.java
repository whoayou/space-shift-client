package com.ss.client.gui.controller.game.event.impl;

import com.ss.client.gui.controller.game.event.GameUIEventType;
import com.ss.client.model.SpaceObject;

/**
 * События о выборе новой цели.
 * 
 * @author Ronn
 */
public class SelectedObjectEvent extends AbstractGameUIEvent {

	public static final GameUIEventType EVENT_TYPE = new GameUIEventType() {
	};

	private static final ThreadLocal<SelectedObjectEvent> LOCAL_EVENT = new ThreadLocal<SelectedObjectEvent>() {

		@Override
		protected SelectedObjectEvent initialValue() {
			return new SelectedObjectEvent();
		}
	};

	public static final SelectedObjectEvent get(SpaceObject object) {

		SelectedObjectEvent event = LOCAL_EVENT.get();
		event.object = object;

		return event;
	}

	/** выбранный в цель объект */
	private SpaceObject object;

	@Override
	public GameUIEventType getEventType() {
		return EVENT_TYPE;
	}

	/**
	 * @return выбранный в цель объект.
	 */
	public SpaceObject getObject() {
		return object;
	}
}
