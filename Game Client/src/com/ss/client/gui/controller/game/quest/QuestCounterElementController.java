package com.ss.client.gui.controller.game.quest;

import java.util.Properties;

import com.ss.client.gui.element.LabelUI;
import com.ss.client.gui.element.MessageElementUI;
import com.ss.client.gui.element.impl.DynamicPanelUIImpl;
import com.ss.client.gui.model.quest.QuestCounter;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.xml.xpp3.Attributes;

/**
 * Реализация контроллера отображения счетчика задания.
 * 
 * @author Ronn
 */
public class QuestCounterElementController extends DynamicPanelUIImpl implements QuestStructElementController {

	public static final String ID = "#quest_counter_element_";
	public static final String QUEST_COUNTER_STATUS_ID = "#quest_counter_status";
	public static final String QUEST_COUNTER_DESCRIPTION_ID = "#quest_counter_description";

	/** счетчик задания */
	private QuestCounter counter;

	/** описание счетчика */
	private MessageElementUI description;
	/** статус счетчика */
	private LabelUI status;

	@Override
	public void bind(Nifty nifty, Screen screen, Element element, Properties parameter, Attributes attributes) {
		super.bind(nifty, screen, element, parameter, attributes);

		this.description = element.findControl(QUEST_COUNTER_DESCRIPTION_ID, MessageElementUI.class);
		this.status = element.findControl(QUEST_COUNTER_STATUS_ID, LabelUI.class);
	}

	/**
	 * @return счетчик задания.
	 */
	public QuestCounter getCounter() {
		return counter;
	}

	/**
	 * @return описание счетчика.
	 */
	public MessageElementUI getDescription() {
		return description;
	}

	/**
	 * @return статус счетчика.
	 */
	public LabelUI getStatus() {
		return status;
	}

	@Override
	public void refresh() {

		QuestCounter counter = getCounter();

		getDescription().setText(counter.getDescription());
		getStatus().setText("(" + counter.getValue() + "/" + counter.getLimit() + ")");

		updateSize(true, true);
	}

	/**
	 * @param counter счетчик задания.
	 */
	public void setCounter(QuestCounter counter) {
		this.counter = counter;
	}
}
