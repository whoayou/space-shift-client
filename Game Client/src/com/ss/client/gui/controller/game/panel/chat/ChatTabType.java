package com.ss.client.gui.controller.game.panel.chat;

import rlib.util.array.Arrays;

import com.ss.client.gui.model.MessageType;
import com.ss.client.table.LangTable;

/**
 * Перечисление типов табов чата.
 * 
 * @author Ronn
 */
public enum ChatTabType {

	MAIN("@interface:chatTabMain@", MessageType.MAIN, MessageType.MAIN, MessageType.ANNOUNCE, MessageType.FEDERATION, MessageType.FRACTION, MessageType.PARTY, MessageType.SYSTEM, MessageType.PRIVATE),
	PARTY("@interface:chatTabParty@", MessageType.PARTY, MessageType.PARTY, MessageType.ANNOUNCE),
	FRACTION("@interface:chatTabFraction@", MessageType.FRACTION, MessageType.FRACTION, MessageType.ANNOUNCE),
	FEDERATION("@interface:chatTabFederation@", MessageType.FEDERATION, MessageType.FEDERATION, MessageType.ANNOUNCE), ;

	private static final ChatTabType[] VALUES = values();

	public static final int LENGTH = VALUES.length;

	public static final ChatTabType valueOf(int index) {
		return VALUES[index];
	}

	/** название таба чата */
	private String tabName;

	/** от отправляемых сообщений табом */
	private MessageType messageType;

	/** список отображаемых типов сообщений */
	private MessageType[] types;

	private ChatTabType(String name, MessageType messageType, MessageType... types) {

		LangTable langTable = LangTable.getInstance();

		this.messageType = messageType;
		this.tabName = langTable.getText(name);
		this.types = types;
	}

	/**
	 * @return от отправляемых сообщений табом.
	 */
	public MessageType getMessageType() {
		return messageType;
	}

	/**
	 * @return название таба чата.
	 */
	public String getTabName() {
		return tabName;
	}

	/**
	 * @return список отображаемых типов сообщений.
	 */
	public MessageType[] getTypes() {
		return types;
	}

	/**
	 * Отображается ли указанный тип сообщения в таком типе таба.
	 * 
	 * @param message сообщение.
	 * @return отображается ли такое сообщение.
	 */
	public boolean isVisible(ChatMessage message) {
		MessageType messageType = message.getMessageType();
		return Arrays.contains(getTypes(), messageType);
	}
}
