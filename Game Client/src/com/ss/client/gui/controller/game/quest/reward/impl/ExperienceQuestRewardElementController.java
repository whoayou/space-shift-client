package com.ss.client.gui.controller.game.quest.reward.impl;

import java.util.Properties;

import com.ss.client.gui.model.quest.reward.impl.ExperienceQuestReward;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.xml.xpp3.Attributes;

/**
 * Реализация контроллера награды в виде опыта.
 * 
 * @author Ronn
 */
public class ExperienceQuestRewardElementController extends AbstractQuestRewardElementController {

	@Override
	public void bind(Nifty nifty, Screen screen, Element element, Properties parameter, Attributes attributes) {
		super.bind(nifty, screen, element, parameter, attributes);

		// TODO вставка иконки опыта
		getIcon();
	}

	@Override
	public void refresh() {
		ExperienceQuestReward reward = (ExperienceQuestReward) getQuestReward();
		// TODO вставка строки "n опыта"
		getName().setText("Exp " + reward.getExp());
	}
}
