package com.ss.client.gui.controller.game.window.skills;

import java.util.Comparator;
import java.util.Properties;

import rlib.util.Strings;
import rlib.util.array.Array;
import rlib.util.array.ArrayComparator;
import rlib.util.array.Arrays;

import com.jme3.input.InputManager;
import com.ss.client.gui.controller.game.GameUIComponent;
import com.ss.client.gui.controller.game.GameUIController;
import com.ss.client.gui.controller.game.GameUIEventListener;
import com.ss.client.gui.controller.game.event.GameUIEvent;
import com.ss.client.gui.controller.game.event.GameUIEventType;
import com.ss.client.gui.controller.game.event.impl.ObjectReuseEvent;
import com.ss.client.gui.controller.game.event.impl.UpdateFastPanelCellEvent;
import com.ss.client.gui.controller.game.event.impl.UpdateSkillListEvent;
import com.ss.client.gui.controller.game.event.impl.UpdateSkillPanelEvent;
import com.ss.client.gui.controller.game.panel.fast.FastPanelItem;
import com.ss.client.gui.controller.game.panel.fast.FastPanelItemEvent;
import com.ss.client.gui.controller.game.skill.SkillElementReuseEvent;
import com.ss.client.gui.controller.game.skill.SkillElementUIController;
import com.ss.client.gui.element.DraggableElementUI;
import com.ss.client.gui.element.DroppableElementUI;
import com.ss.client.gui.element.ElementUI;
import com.ss.client.gui.element.event.ElementUIEvent;
import com.ss.client.gui.element.event.impl.PrimaryClickEvent;
import com.ss.client.gui.element.filter.DroppableDropFilter;
import com.ss.client.gui.element.impl.DraggableElementUIImpl;
import com.ss.client.gui.element.listener.ElementUIEventListener;
import com.ss.client.gui.model.ElementUtils;
import com.ss.client.gui.model.InputMode;
import com.ss.client.gui.model.SkillPanelInfo;
import com.ss.client.model.ship.player.PlayerShip;
import com.ss.client.model.skills.Skill;
import com.ss.client.network.Network;
import com.ss.client.network.packet.client.RequestSkillList;
import com.ss.client.network.packet.client.RequestSkillPanel;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.input.NiftyInputEvent;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.tools.Color;
import de.lessvoid.xml.xpp3.Attributes;

/**
 * Реализация контроллера окна умений.
 * 
 * @author Ronn
 */
public class SkillWindowUIController extends DraggableElementUIImpl implements GameUIComponent, DroppableDropFilter {

	private static final Network NETWORK = Network.getInstance();

	public static final String WINDOW_BACKGROUND_IMAGE = "ui/game/skill_window/background.png";
	public static final String WINDOW_CLOSE_BUTTON_IMAGE = "ui/game/skill_window/close_button.png";
	public static final String WINDOW_SPLIT_IMAGE = "ui/game/skill_window/split.png";
	public static final String WINDOW_CELL_IMAGE = "ui/game/skill_window/cell.png";
	public static final String WINDOW_LINE_IMAGE = "ui/game/skill_window/line.png";

	public static final String WINDOW_ID = "#skill_window";
	public static final String WINDOW_HEADER_ID = "#header";
	public static final String WINDOW_BUTTON_CLOSE_ID = "#close";
	public static final String WINDOW_TITLE_ID = "#title";
	public static final String WINDOW_CONTENT_ID = "#content";
	public static final String WINDOW_CELL_ID = "#cell_";
	public static final String WINDOW_CELL_ROW_ID = "#row_";
	public static final String WINDOW_TITLE_COUNTER_ID = "#title_counter";
	public static final String WINDOW_TITLE_SPLIT_ID = "#title_split";
	public static final String WINDOW_LINE_ID = "#window_line";

	public static final Color TITLE_TEXT_COLOR = new Color("#5f9ea5");

	private static final Comparator<Element> CELL_COMPARATOR = new ArrayComparator<Element>() {

		@Override
		protected int compareImpl(Element first, Element second) {

			String firstId = first.getId();
			String secondId = second.getId();

			int firstValue = Integer.parseInt(firstId.replace(WINDOW_CELL_ID, Strings.EMPTY));
			int secondValue = Integer.parseInt(secondId.replace(WINDOW_CELL_ID, Strings.EMPTY));

			return Integer.compare(firstValue, secondValue);
		}
	};

	/** слушатель событий игрового УИ */
	private final GameUIEventListener eventListener = new GameUIEventListener() {

		@Override
		public void notifyEvent(GameUIEvent event) {

			GameUIEventType eventType = event.getEventType();

			if(eventType == UpdateSkillListEvent.EVENT_TYPE) {
				UpdateSkillListEvent skillListEvent = (UpdateSkillListEvent) event;
				updateSkillList(skillListEvent.getSkills());
			} else if(eventType == UpdateSkillPanelEvent.EVENT_TYPE) {
				processEvent((UpdateSkillPanelEvent) event);
			} else if(eventType == ObjectReuseEvent.EVENT_TYPE) {
				processReuseEvent(event);
			} else if(eventType == FastPanelItemEvent.EVENT_TYPE) {
				processItemEvent(event);
			}
		}
	};

	/** слушатель нажатий на кнопку закрыть */
	private final ElementUIEventListener<ElementUI> closeEventListener = new ElementUIEventListener<ElementUI>() {

		@Override
		public void notifyEvent(ElementUIEvent<ElementUI> event) {
			setVisible(false);
		}
	};

	/** список умений корабля */
	private final Array<Skill> skills;

	/** массив ячеяк в окне умений */
	private SkillCellUIController[] cells;

	public SkillWindowUIController() {
		this.skills = Arrays.toArray(Skill.class);
	}

	@Override
	public boolean accept(DroppableElementUI source, DraggableElementUI draggable, DroppableElementUI target) {
		return false;
	}

	@Override
	public void bind(Nifty nifty, Screen screen, Element element, Properties properties, Attributes attributes) {
		super.bind(nifty, screen, element, properties, attributes);

		ElementUI close = element.findControl(WINDOW_BUTTON_CLOSE_ID, ElementUI.class);
		close.addElementListener(PrimaryClickEvent.EVENT_TYPE, getCloseEventListener());

		// подготовка списка ячеяк
		{
			final Array<Element> cells = Arrays.toArray(Element.class);

			// формируем список всех ячейках
			ElementUtils.addElements(cells, element, WINDOW_CELL_ID);

			cells.sort(CELL_COMPARATOR);

			Array<SkillCellUIController> controls = Arrays.toArray(SkillCellUIController.class);

			for(Element cell : cells) {
				SkillCellUIController control = cell.getControl(SkillCellUIController.class);
				control.addFilter(this);
				controls.add(control);
			}

			this.cells = controls.toArray(new SkillCellUIController[controls.size()]);
		}

		NETWORK.sendPacketToGameServer(RequestSkillList.getInstance());
	}

	/**
	 * Поиск соотвествующего умения.
	 */
	public FastPanelItem findItem(SkillPanelInfo info) {

		for(SkillCellUIController cell : getCells()) {

			Skill skill = cell.getSkill();

			if(skill.getId() != info.getSkillId() || skill.getObjectId() != info.getObjectId()) {
				continue;
			}

			Element element = cell.getSkillElement();
			return element.getControl(FastPanelItem.class);
		}

		return null;
	}

	@Override
	public void finish() {
	}

	/**
	 * @return список доступных ячеяк для умений.
	 */
	public SkillCellUIController[] getCells() {
		return cells;
	}

	/**
	 * @return слушатель нажатий на кнопку закрыть.
	 */
	public ElementUIEventListener<ElementUI> getCloseEventListener() {
		return closeEventListener;
	}

	/**
	 * @return слушатель событий игрового УИ.
	 */
	private GameUIEventListener getEventListener() {
		return eventListener;
	}

	/**
	 * @return список умений корабля.
	 */
	private Array<Skill> getSkills() {
		return skills;
	}

	@Override
	public boolean inputEvent(NiftyInputEvent inputEvent) {
		return false;
	}

	@Override
	public void onStartScreen() {
	}

	private void processEvent(UpdateSkillPanelEvent skillPanelEvent) {

		Array<SkillPanelInfo> skillPanelInfo = skillPanelEvent.getSkillPanelInfo();

		if(skillPanelInfo.isEmpty()) {
			return;
		}

		UpdateFastPanelCellEvent event = UpdateFastPanelCellEvent.get();
		GameUIController controller = GameUIController.getInstance();

		for(SkillPanelInfo info : skillPanelInfo.array()) {

			if(info == null) {
				break;
			}

			FastPanelItem item = findItem(info);

			if(item == null) {
				continue;
			}

			event.setItem(item);
			event.setOrder(info.getOrder());

			controller.notifyEvent(event);
		}
	}

	/**
	 * Обработка события связанного с элементом панели.
	 */
	private void processItemEvent(GameUIEvent event) {

		FastPanelItemEvent itemEvent = (FastPanelItemEvent) event;

		for(SkillCellUIController cell : getCells()) {

			Element skillElement = cell.getSkillElement();

			if(skillElement == null) {
				break;
			}

			FastPanelItem controller = skillElement.getControl(FastPanelItem.class);

			if(controller != null && controller.processEvent(itemEvent)) {
				break;
			}
		}
	}

	/**
	 * Обработка события с откатом умения.
	 */
	private void processReuseEvent(GameUIEvent event) {

		ObjectReuseEvent objectEvent = (ObjectReuseEvent) event;

		if(objectEvent.getObjectType() != SkillElementUIController.OBJECT_TYPE_SKILL) {
			return;
		}

		GameUIController controller = GameUIController.getInstance();
		controller.notifyEvent(SkillElementReuseEvent.get(objectEvent.getObjectId(), objectEvent.getTemplateId(), objectEvent.getReuseTime(), objectEvent.getStartReuseTime()));
	}

	@Override
	public void register(GameUIController controller) {
		controller.registerListener(UpdateSkillListEvent.EVENT_TYPE, getEventListener());
		controller.registerListener(UpdateSkillPanelEvent.EVENT_TYPE, getEventListener());
		controller.registerListener(ObjectReuseEvent.EVENT_TYPE, getEventListener());
		controller.registerListener(FastPanelItemEvent.EVENT_TYPE, getEventListener());
	}

	@Override
	public void register(InputManager inputManager) {
	}

	@Override
	public void setInputMode(InputMode inputMode) {

		if(inputMode == InputMode.CONTROL_MODE) {
			setVisible(false);
		}
	}

	@Override
	public void setPlayerShip(PlayerShip playerShip) {
	}

	/**
	 * Обновление списка умений модулей корабля.
	 * 
	 * @param newList новый список.
	 */
	public void updateSkillList(final Array<Skill> newList) {

		final Array<Skill> skills = getSkills();
		skills.clear();

		final SkillCellUIController[] cells = getCells();

		for(int i = 0, length = newList.size(); i < length; i++) {

			final SkillCellUIController cell = cells[i];
			final Skill skill = newList.get(i);

			cell.setSkill(skill);
		}

		skills.addAll(newList);

		NETWORK.sendPacketToGameServer(RequestSkillPanel.getInstance());
	}
}
