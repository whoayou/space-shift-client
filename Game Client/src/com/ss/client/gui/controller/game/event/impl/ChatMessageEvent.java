package com.ss.client.gui.controller.game.event.impl;

import com.ss.client.gui.controller.game.event.GameUIEventType;
import com.ss.client.gui.controller.game.panel.chat.ChatMessage;

/**
 * Событие об поступлении нового сообщения в чат.
 * 
 * @author Ronn
 */
public class ChatMessageEvent extends AbstractGameUIEvent {

	public static final GameUIEventType EVENT_TYPE = new GameUIEventType() {
	};

	private static final ThreadLocal<ChatMessageEvent> LOCAL_EVENT = new ThreadLocal<ChatMessageEvent>() {

		@Override
		protected ChatMessageEvent initialValue() {
			return new ChatMessageEvent();
		}
	};

	public static final ChatMessageEvent get() {
		return LOCAL_EVENT.get();
	}

	/** сообщение в чат */
	private ChatMessage message;

	@Override
	public GameUIEventType getEventType() {
		return EVENT_TYPE;
	}

	/**
	 * @return сообщение в чат.
	 */
	public ChatMessage getMessage() {
		return message;
	}

	/**
	 * @param message сообщение в чат.
	 */
	public void setMessage(ChatMessage message) {
		this.message = message;
	}
}
