package com.ss.client.gui.controller.game.window.skills;

import java.util.Properties;

import com.ss.client.gui.builder.game.factory.SkillElementUIFactory;
import com.ss.client.gui.element.impl.DroppableElementUIImpl;
import com.ss.client.model.skills.Skill;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.input.NiftyInputEvent;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.xml.xpp3.Attributes;

/**
 * Реализация контроллера ячейки умения в окне умений.
 * 
 * @author Ronn
 */
public class SkillCellUIController extends DroppableElementUIImpl {

	/** контролер UI */
	private Nifty nifty;
	/** экран элемента */
	private Screen screen;
	/** элемент скила */
	private Element skillElement;

	/** скил ячейки */
	private Skill skill;

	@Override
	public void bind(Nifty nifty, Screen screen, Element element, Properties parameter, Attributes attributes) {
		super.bind(nifty, screen, element, parameter, attributes);

		this.nifty = nifty;
		this.screen = screen;
	}

	/**
	 * @return контролер UI.
	 */
	public Nifty getNifty() {
		return nifty;
	}

	/**
	 * @return экран элемента.
	 */
	public Screen getScreen() {
		return screen;
	}

	/**
	 * @return скил ячейки.
	 */
	public Skill getSkill() {
		return skill;
	}

	/**
	 * @return элемент скила.
	 */
	public Element getSkillElement() {
		return skillElement;
	}

	@Override
	public boolean inputEvent(NiftyInputEvent inputEvent) {
		return false;
	}

	@Override
	public void onStartScreen() {
	}

	/**
	 * Установка скила в ячейку.
	 * 
	 * @param skill устонавливаемый скил.
	 */
	public void setSkill(Skill skill) {

		Element element = getElement();
		element.setVisible(true);

		setSkillElement(SkillElementUIFactory.build(element, skill, getNifty(), getScreen()));

		this.skill = skill;
	}

	/**
	 * @param skillElement элемент скила.
	 */
	public void setSkillElement(Element skillElement) {
		this.skillElement = skillElement;
	}
}
