package com.ss.client.gui.controller.game.skill;

import java.util.Properties;

import com.ss.client.gui.builder.game.factory.SkillElementUIFactory;
import com.ss.client.gui.controller.game.GameUIController;
import com.ss.client.gui.controller.game.panel.fast.FastPanelItem;
import com.ss.client.gui.controller.game.panel.fast.FastPanelItemEvent;
import com.ss.client.gui.element.DraggableElementUI;
import com.ss.client.gui.element.DroppableElementUI;
import com.ss.client.gui.element.impl.DraggableElementUIImpl;
import com.ss.client.model.skills.Skill;
import com.ss.client.network.Network;
import com.ss.client.network.packet.client.RequestSkillPanelUpdate;
import com.ss.client.network.packet.client.RequestUseSkill;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.xml.xpp3.Attributes;

/**
 * Реализация контролера элемента умения.
 * 
 * @author Ronn
 */
public class SkillElementUIController extends DraggableElementUIImpl implements FastPanelItem {

	public static final String REUSE_PANEL_ID = "#reuse_panel";

	private static final Network NETWORK = Network.getInstance();

	/** контроллер отображения отката */
	private SkillReuseUIController reuseUIController;

	/** скил элемента */
	private Skill skill;

	/** индекс элемента */
	private int index;

	public SkillElementUIController() {
		this.index = -1;
	}

	@Override
	public void activate() {

		final Skill skill = getSkill();

		if(skill == null) {
			return;
		}

		GameUIController controller = GameUIController.getInstance();

		NETWORK.sendPacketToGameServer(RequestUseSkill.getInstance(controller.getPlayerShip(), skill.getObjectId(), skill.getId()));
	}

	@Override
	public void bind(Nifty nifty, Screen screen, Element element, Properties properties, Attributes attributes) {
		super.bind(nifty, screen, element, properties, attributes);

		this.reuseUIController = element.findControl(REUSE_PANEL_ID, SkillReuseUIController.class);
	}

	@Override
	public DraggableElementUI createFrom(Element parent, Screen screen, Nifty nifty) {

		Element result = SkillElementUIFactory.build(parent, getSkill(), nifty, screen);

		SkillElementUIController element = result.getControl(SkillElementUIController.class);
		element.setIndex(getIndex());

		return element;
	}

	@Override
	public int getIndex() {
		return index;
	}

	/**
	 * @return умение.
	 */
	public Skill getSkill() {
		return skill;
	}

	@Override
	public boolean matches(FastPanelItem item) {

		if(item.getClass() != getClass()) {
			return false;
		}

		SkillElementUIController controller = (SkillElementUIController) item;
		return getSkill().equals(controller.getSkill());
	}

	@Override
	public boolean processEvent(FastPanelItemEvent event) {

		Skill skill = getSkill();

		if(event.getClass() == SkillElementReuseEvent.class) {

			SkillElementReuseEvent reuseEvent = (SkillElementReuseEvent) event;

			if(reuseEvent.getObjectId() != skill.getObjectId() || reuseEvent.getTemplateId() != skill.getTemplateId()) {
				return false;
			}

			SkillReuseUIController reuseUIController = getReuseUIController();
			reuseUIController.startReuse(reuseEvent.getStartReuseTime(), reuseEvent.getReuseTime());
		}

		return false;
	}

	@Override
	public void remove() {

		DroppableElementUI droppable = getDroppable();
		droppable.setDraggable(null);

		getElement().markForRemoval();
	}

	@Override
	public void setIndex(int index) {
		this.index = index;
	}

	/**
	 * @param skill умение.
	 */
	public void setSkill(Skill skill) {
		this.skill = skill;
	}

	@Override
	public void updateToServer(final int index) {

		if(getIndex() == index) {
			return;
		}

		final Skill skill = getSkill();

		if(skill == null) {
			return;
		}

		NETWORK.sendPacketToGameServer(RequestSkillPanelUpdate.getInstance(skill, getIndex(), index));
		setIndex(index);
	}

	/**
	 * @return контроллер отображения отката.
	 */
	public SkillReuseUIController getReuseUIController() {
		return reuseUIController;
	}
}
