package com.ss.client.gui.controller.game.window.quest;

import java.util.Properties;

import rlib.util.array.Array;
import rlib.util.linkedlist.LinkedList;
import rlib.util.linkedlist.LinkedLists;

import com.ss.client.gui.controller.game.quest.QuestStructContainerController;
import com.ss.client.gui.controller.game.quest.QuestStructElementController;
import com.ss.client.gui.element.LabelUI;
import com.ss.client.gui.element.MessageElementUI;
import com.ss.client.gui.element.impl.PanelUIImpl;
import com.ss.client.gui.model.ElementUtils;
import com.ss.client.gui.model.quest.QuestButton;
import com.ss.client.gui.model.quest.QuestCounter;
import com.ss.client.gui.model.quest.QuestState;
import com.ss.client.gui.model.quest.reward.QuestReward;
import com.ss.client.table.LangTable;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.xml.xpp3.Attributes;

/**
 * Реализация контрола, который отображает полную информацию о задании.
 * 
 * @author Ronn
 */
public class QuestDetailsElementController extends PanelUIImpl {

	public static final String QUEST_DESCRIPTION_CONTAINER_ID = "quest_description_container";
	public static final String QUEST_BUTTON_CONTAINER_ID = "#quest_button_container";
	public static final String QUEST_COUNTER_CONTAINER_ID = "#quest_counter_container";
	public static final String QUEST_REWARD_CONTAINER_ID = "#quest_reward_container";
	public static final String QUEST_DESCRIPTION_ID = "#quest_description";
	public static final String QUEST_STATUS_ID = "#quest_status";
	public static final String QUEST_NAME_ID = "#quest_name";

	private static final LangTable LANG_TABLE = LangTable.getInstance();

	/** список отображаемых элементов наград */
	private final LinkedList<Element> rewards;
	/** список отображаемых элементов счетчиков */
	private final LinkedList<Element> counters;
	/** список отображаемых кнопок */
	private final LinkedList<Element> buttons;

	/** контроллер UI */
	private Nifty nifty;
	/** текущий экран */
	private Screen screen;

	/** контейнер описания текущей стадии задания */
	private QuestStructContainerController descriptionContainer;
	/** контейнер панель для наград */
	private QuestStructContainerController rewardContainer;
	/** контейнер панель для счетчиков */
	private QuestStructContainerController counterContainer;
	/** контейнер панель для кнопок */
	private QuestStructContainerController buttonContainer;

	/** название задания */
	private LabelUI questName;
	/** статус задания */
	private LabelUI questStatus;

	/** описание текущей стадии задания */
	private MessageElementUI description;

	/** отображаемое задание */
	private QuestState quest;

	public QuestDetailsElementController() {
		this.rewards = LinkedLists.newLinkedList(Element.class);
		this.counters = LinkedLists.newLinkedList(Element.class);
		this.buttons = LinkedLists.newLinkedList(Element.class);
	}

	@Override
	public void bind(Nifty nifty, Screen screen, Element element, Properties parameter, Attributes attributes) {
		super.bind(nifty, screen, element, parameter, attributes);

		this.nifty = nifty;
		this.screen = screen;
		this.questName = element.findControl(QUEST_NAME_ID, LabelUI.class);
		this.questStatus = element.findControl(QUEST_STATUS_ID, LabelUI.class);
		this.description = element.findControl(QUEST_DESCRIPTION_ID, MessageElementUI.class);
		this.descriptionContainer = element.findControl(QUEST_DESCRIPTION_CONTAINER_ID, QuestStructContainerController.class);
		this.rewardContainer = element.findControl(QUEST_REWARD_CONTAINER_ID, QuestStructContainerController.class);
		this.counterContainer = element.findControl(QUEST_COUNTER_CONTAINER_ID, QuestStructContainerController.class);
		this.buttonContainer = element.findControl(QUEST_BUTTON_CONTAINER_ID, QuestStructContainerController.class);
	}

	/**
	 * Очистка данных от предыдущего задания.
	 */
	protected void clear() {

		LinkedList<Element> rewards = getRewards();
		ElementUtils.remove(rewards);
		rewards.clear();

		LinkedList<Element> counters = getCounters();
		ElementUtils.remove(counters);
		counters.clear();

		LinkedList<Element> buttons = getButtons();
		ElementUtils.remove(buttons);
		buttons.clear();

		getRewardContainer().setVisible(false);
		getButtonContainer().setVisible(false);
		getCounterContainer().setVisible(false);
		getDescriptionContainer().setVisible(false);
		getQuestName().setVisible(false);
		getQuestStatus().setVisible(false);
	}

	/**
	 * @return контейнер панель для кнопок.
	 */
	public QuestStructContainerController getButtonContainer() {
		return buttonContainer;
	}

	/**
	 * @return список отображаемых кнопок.
	 */
	public LinkedList<Element> getButtons() {
		return buttons;
	}

	/**
	 * @return контейнер панель для счетчиков.
	 */
	public QuestStructContainerController getCounterContainer() {
		return counterContainer;
	}

	/**
	 * @return список отображаемых элементов счетчиков.
	 */
	public LinkedList<Element> getCounters() {
		return counters;
	}

	/**
	 * @return описание текущей стадии задания.
	 */
	public MessageElementUI getDescription() {
		return description;
	}

	/**
	 * @return контейнер описания текущей стадии задания.
	 */
	public QuestStructContainerController getDescriptionContainer() {
		return descriptionContainer;
	}

	/**
	 * @return контроллер UI.
	 */
	public Nifty getNifty() {
		return nifty;
	}

	/**
	 * @return отображаемое задание.
	 */
	public QuestState getQuest() {
		return quest;
	}

	/**
	 * @return название задания.
	 */
	public LabelUI getQuestName() {
		return questName;
	}

	/**
	 * @return статус задания.
	 */
	public LabelUI getQuestStatus() {
		return questStatus;
	}

	/**
	 * @return контейнер панель для наград.
	 */
	public QuestStructContainerController getRewardContainer() {
		return rewardContainer;
	}

	/**
	 * @return список отображаемых элементов наград.
	 */
	public LinkedList<Element> getRewards() {
		return rewards;
	}

	/**
	 * @return текущий экран.
	 */
	public Screen getScreen() {
		return screen;
	}

	/**
	 * @param quest отображаемое задание.
	 */
	public void setQuest(QuestState quest) {
		this.quest = quest;
	}

	/**
	 * Отобразить указанное задание.
	 * 
	 * @param quest новое задание.
	 */
	public void showQuest(QuestState quest) {

		QuestState current = getQuest();

		if(current == quest) {
			return;
		}

		setQuest(quest);
		clear();

		if(quest == null) {
			return;
		}

		LabelUI questName = getQuestName();
		questName.setText(LANG_TABLE.getText(QuestWindowTextIds.QUEST_NAME_LABEL) + ": " + quest.getName());
		questName.setVisible(true);

		QuestStructContainerController descriptionContainer = getDescriptionContainer();

		descriptionContainer.updateSize(true, false);
		descriptionContainer.setVisible(true);

		ElementUtils.updateLayout(descriptionContainer.getElement());

		getDescription().setText(quest.getDescription());

		descriptionContainer.updateSize(true, false);
		descriptionContainer.setVisible(true);

		ElementUtils.updateLayout(descriptionContainer.getElement());

		int order = 0;

		Array<QuestReward> rewards = quest.getRewards();
		QuestStructContainerController container = getRewardContainer();

		Element element = container.getElement();
		Nifty nifty = getNifty();
		Screen screen = getScreen();

		if(rewards.isEmpty()) {
			container.setVisible(false);
		} else {

			container.setVisible(true);

			for(QuestReward reward : rewards) {
				getRewards().add(reward.buildTo(element, nifty, screen, order++));
			}

			container.updateSize(true, false);
		}

		order = 0;

		Array<QuestCounter> counters = quest.getCounters();
		container = getCounterContainer();
		element = container.getElement();

		if(counters.isEmpty()) {
			container.setVisible(false);
		} else {

			container.setVisible(true);

			for(QuestCounter counter : counters) {
				getCounters().add(counter.buildElement(element, nifty, screen, order++));
			}

			container.updateSize(true, false);
		}

		order = 0;

		Array<QuestButton> buttons = quest.getButtons();
		container = getButtonContainer();
		element = container.getElement();

		if(buttons.isEmpty()) {
			container.setVisible(false);
		} else {

			container.setVisible(true);

			for(QuestButton button : buttons) {
				getButtons().add(button.buildElement(element, nifty, screen, order++));
			}

			container.updateSize(true, false);
		}
	}

	/**
	 * Обновление видимости элементов.
	 */
	public void updateVisible() {

		if(getButtons().isEmpty()) {
			getButtonContainer().setVisible(false);
		}

		if(getRewards().isEmpty()) {
			getRewardContainer().setVisible(false);
		}

		if(getCounters().isEmpty()) {
			getCounterContainer().setVisible(false);
		}

		if(getQuest() == null) {
			getDescriptionContainer().setVisible(false);
			getQuestName().setVisible(false);
			getQuestStatus().setVisible(false);
		}
	}

	/**
	 * Обновить содержимое без перестройки.
	 */
	public void update() {

		LinkedList<Element> counters = getCounters();

		if(!counters.isEmpty()) {
			for(Element counter : counters) {
				QuestStructElementController controller = counter.getControl(QuestStructElementController.class);
				controller.refresh();
			}
		}
	}
}
