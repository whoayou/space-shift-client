package com.ss.client.gui.controller.game.hud.selector;

import com.ss.client.gui.builder.game.SelectorUIBuilder;
import com.ss.client.model.SpaceObject;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.screen.Screen;

/**
 * Перечисление типов селекторов.
 * 
 * @author Ronn
 */
public enum SelectorType {

	NONE(false),
	ITEM(false) {

		@Override
		public ObjectSelector build(SpaceObject object, Nifty nifty, Screen screen, Element parent) {
			return SelectorUIBuilder.buildItemSelector(object, nifty, screen, parent);
		}
	},

	SPACE_SHIP(true) {

		@Override
		public ObjectSelector build(SpaceObject object, Nifty nifty, Screen screen, Element parent) {
			return SelectorUIBuilder.buildSpaceShipSelector(object, nifty, screen, parent);
		}
	},

	;

	/** нужен ли прицел дял такого селектора */
	private boolean needAIM;

	private SelectorType(boolean needAIM) {
		this.needAIM = needAIM;
	}

	/**
	 * Построение селектора для указанного объекта.
	 * 
	 * @param object объект для которого нужно построить селектор.
	 * @param nifty контроллер UI.
	 * @param screen текущий экран UI.
	 * @param parent место размещения селектора.
	 * @return новый селектор.
	 */
	public ObjectSelector build(SpaceObject object, Nifty nifty, Screen screen, Element parent) {
		return null;
	}

	/**
	 * @return нужно ли отображать прицел
	 */
	public boolean isNeedAIM() {
		return needAIM;
	}
}
