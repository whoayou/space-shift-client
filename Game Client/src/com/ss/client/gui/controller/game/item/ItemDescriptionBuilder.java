package com.ss.client.gui.controller.game.item;

import com.ss.client.gui.element.effect.builder.EffectUIBuilder;
import com.ss.client.gui.element.effect.builder.impl.HoverHintEffectUIBuilder;

/**
 * Реализация конструктора подсказки о предмете.
 * 
 * @author Ronn
 */
public class ItemDescriptionBuilder extends HoverHintEffectUIBuilder {

	static {
		EffectUIBuilder builder = new ItemDescriptionBuilder();
		NIFTY.registerEffect(builder.getEffectName(), ItemDescriptionEffect.class.getName());
	}

}
