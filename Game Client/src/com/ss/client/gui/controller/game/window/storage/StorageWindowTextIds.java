package com.ss.client.gui.controller.game.window.storage;

/**
 * Набор констант связанных с UI текстом окна хранилища.
 * 
 * @author Ronn
 */
public interface StorageWindowTextIds {

	public static final String LOCALIZATION_FILE = "/data/localization/game_interface/storage_window.xml";

	public static final String WINDOW_TITLE = "@interface:storageWindowTitle@";
	public static final String WINDOW_DESTROY_AREA = "@interface:storageWindowDestroyArea@";
}
