package com.ss.client.gui.controller.game.hud.selector;

import java.util.Properties;

import com.ss.client.Game;
import com.ss.client.gui.model.ElementUtils;
import com.ss.client.model.ship.SpaceShip;
import com.ss.client.model.ship.player.PlayerShip;
import com.ss.client.network.Network;
import com.ss.client.network.packet.client.RequestShipStatus;
import com.ss.client.util.GameUtil;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.xml.xpp3.Attributes;

/**
 * Реализация селектора для космического корабля.
 * 
 * @author Ronn
 */
public class SpaceShipSelector extends AbstractObjectSelector<SpaceShip> {

	public static final String STRENGTH_INDICATOR_ID = "#strength";
	public static final String ENERGY_INDICATOR_ID = "#energy";

	private static final Network NETWORK = Network.getInstance();

	/** элемент энергии */
	private Element energyElement;
	/** энергии прочности */
	private Element strengthElement;

	/** дата последнего запроса статуса */
	private long lastStatusUpdate;

	/** последняя прочность */
	private float lastStrength;
	/** последняя энергия */
	private float lastEnergy;

	@Override
	public void acivate() {
	}

	@Override
	public void bind(Nifty nifty, Screen screen, Element element, Properties parameter, Attributes controlDefinitionAttributes) {
		super.bind(nifty, screen, element, parameter, controlDefinitionAttributes);
		this.energyElement = element.findElementByName(ENERGY_INDICATOR_ID);
		this.strengthElement = element.findElementByName(STRENGTH_INDICATOR_ID);
	}

	/**
	 * @return элемент энергии.
	 */
	public Element getEnergyElement() {
		return energyElement;
	}

	/**
	 * @return последняя энергия.
	 */
	public float getLastEnergy() {
		return lastEnergy;
	}

	/**
	 * @return дата последнего запроса статуса.
	 */
	public long getLastStatusUpdate() {
		return lastStatusUpdate;
	}

	/**
	 * @return последняя прочность.
	 */
	public float getLastStrength() {
		return lastStrength;
	}

	/**
	 * @return энергии прочности.
	 */
	public Element getStrengthElement() {
		return strengthElement;
	}

	/**
	 * @param lastEnergy последняя энергия.
	 */
	public void setLastEnergy(float lastEnergy) {
		this.lastEnergy = lastEnergy;
	}

	/**
	 * @param lastStatusUpdate дата последнего запроса статуса.
	 */
	public void setLastStatusUpdate(long lastStatusUpdate) {
		this.lastStatusUpdate = lastStatusUpdate;
	}

	/**
	 * @param lastStrength последняя прочность.
	 */
	public void setLastStrength(float lastStrength) {
		this.lastStrength = lastStrength;
	}

	@Override
	public void start() {
		super.start();
		setLastStatusUpdate(Game.getCurrentTime());
		setLastEnergy(-1F);
		setLastStrength(-1F);
		NETWORK.sendPacketToGameServer(RequestShipStatus.getInstance(getTarget()));
	}

	@Override
	public void update(long currentTime) {
		super.update(currentTime);

		final SpaceShip target = getTarget();
		final PlayerShip playerShip = getPlayerShip();

		if(playerShip == null || target == null) {
			return;
		}

		float currentEnergy = target.getEnergyPercent();
		float currentStrength = target.getStrengthPercent();

		if(currentEnergy != getLastEnergy() || currentStrength != getLastStrength()) {

			Element energyElement = getEnergyElement();
			Element strengthElement = getStrengthElement();

			energyElement.setConstraintWidth(GameUtil.getPercentSize((int) (100 * currentEnergy)));
			strengthElement.setConstraintWidth(GameUtil.getPercentSize((int) (100 * currentStrength)));

			setLastEnergy(currentEnergy);
			setLastStrength(currentStrength);

			ElementUtils.updateLayout(energyElement.getParent());
		}

		if(currentTime - getLastStatusUpdate() > 1000) {
			NETWORK.sendPacketToGameServer(RequestShipStatus.getInstance(target));
			setLastStatusUpdate(currentTime);
		}
	}
}
