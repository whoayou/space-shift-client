package com.ss.client.gui.controller.game.panel.chat;

import com.ss.client.gui.model.MessageType;

/**
 * Реализация сообщения чата.
 * 
 * @author Ronn
 */
public class ChatMessage {

	/** тип сообщения */
	private final MessageType messageType;

	/** название отправителя */
	private final String name;
	/** сообщение */
	private final String message;

	public ChatMessage(MessageType messageType, String name, String message) {
		this.messageType = messageType;
		this.name = name;
		this.message = message;
	}

	/**
	 * @return сообщение.
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @return тип сообщения.
	 */
	public MessageType getMessageType() {
		return messageType;
	}

	/**
	 * @return название отправителя.
	 */
	public String getName() {
		return name;
	}
}
