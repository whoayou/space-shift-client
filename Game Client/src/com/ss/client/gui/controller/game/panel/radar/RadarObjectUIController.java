package com.ss.client.gui.controller.game.panel.radar;

import java.util.Properties;

import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import com.jme3.renderer.Camera;
import com.ss.client.gui.builder.game.factory.RadarElementUIFactory;
import com.ss.client.gui.element.impl.ImageUIImpl;
import com.ss.client.model.SpaceObject;
import com.ss.client.model.SpaceObjectView;
import com.ss.client.util.GameUtil;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.xml.xpp3.Attributes;

/**
 * Реализация контрола объекта на радаре.
 * 
 * @author Ronn
 */
public class RadarObjectUIController extends ImageUIImpl {

	public static final String OBJECT_ID = "#object_";

	/** позиция на панели */
	private final Vector2f position;

	/** отображаемый объект */
	private SpaceObject object;

	/** выбран ли этот объект целью */
	private boolean selected;

	public RadarObjectUIController() {
		this.position = new Vector2f();
	}

	@Override
	public void bind(Nifty nifty, Screen screen, Element element, Properties parameter, Attributes attributes) {
		super.bind(nifty, screen, element, parameter, attributes);
	}

	/**
	 * @return отображаемый объект.
	 */
	public SpaceObject getObject() {
		return object;
	}

	/**
	 * @return позиция на панели.
	 */
	public Vector2f getPosition() {
		return position;
	}

	/**
	 * @return выбран ли этот объект целью.
	 */
	public boolean isSelected() {
		return selected;
	}

	/**
	 * Удаление объекта с радара.
	 */
	public void remove() {
		getElement().markForRemoval();
	}

	/**
	 * @param object отображаемый объект.
	 */
	public void setObject(SpaceObject object) {
		this.object = object;
	}

	/**
	 * @param selected выбран ли этот объект целью.
	 */
	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	/**
	 * Обновление позиции объекта на радаре.
	 * 
	 * @param camera камера с видом сверху на корабль игрока.
	 * @param cameraPosition позиция объекта на камере.
	 * @param center позиция центра на панели.
	 * @param mod модификатор координат.
	 * @param max максимальный радиус от центра.
	 * @param offsetX отступ по ширине.
	 * @param offsetY отступ по высоте.
	 * @return было ли обновление позиции элемента.
	 */
	public boolean update(Camera camera, Vector3f cameraPosition, Vector2f center, float mod, float max, int offsetX, int offsetY) {

		SpaceObject object = getObject();

		if(object == null) {
			return false;
		}

		SpaceObjectView view = object.getView();

		if(view == null) {
			return false;
		}

		camera.getScreenCoordinates(view.getLocation(), cameraPosition);

		Element element = getElement();

		float x = (int) (cameraPosition.getX() - element.getWidth() / 2);
		x *= mod;
		x += offsetX;

		float y = (int) (camera.getHeight() - cameraPosition.getY() - element.getHeight() / 2);
		y *= mod;
		y += offsetY;

		Vector2f position = getPosition();
		position.set(x, y);

		float distance = position.distance(center);

		boolean visible = distance < max;

		if(isVisible() != visible) {
			setVisible(visible);
		}

		if(visible) {

			int newX = (int) x;
			int newY = (int) y;

			if(newX != element.getX() || newY != element.getY()) {
				element.setConstraintX(GameUtil.getPixelSize(newX));
				element.setConstraintY(GameUtil.getPixelSize(newY));
				return true;
			}
		}

		return false;
	}

	/**
	 * Обновление иконки объекта.
	 */
	public void updateIcon() {
		updateIcon(isSelected());
	}

	/**
	 * Обновление иконки объекта.
	 */
	private void updateIcon(boolean selected) {
		String imagePath = RadarElementUIFactory.getPath(getObject().getFriendStatus(), selected);
		setImage(imagePath);
	}
}
