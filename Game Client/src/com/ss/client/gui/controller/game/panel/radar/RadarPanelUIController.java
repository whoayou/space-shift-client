package com.ss.client.gui.controller.game.panel.radar;

import java.util.Properties;
import java.util.concurrent.atomic.AtomicInteger;

import rlib.util.array.Array;
import rlib.util.array.Arrays;
import rlib.util.table.FuncValue;
import rlib.util.table.LongKey;
import rlib.util.table.Table;
import rlib.util.table.Tables;

import com.jme3.input.InputManager;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import com.jme3.renderer.Camera;
import com.ss.client.Game;
import com.ss.client.gui.builder.game.factory.RadarElementUIFactory;
import com.ss.client.gui.controller.game.GameUIComponent;
import com.ss.client.gui.controller.game.GameUIController;
import com.ss.client.gui.controller.game.GameUIEventListener;
import com.ss.client.gui.controller.game.event.GameUIEvent;
import com.ss.client.gui.controller.game.event.impl.ObjectChangeFriendStatusEvent;
import com.ss.client.gui.controller.game.event.impl.SelectedObjectEvent;
import com.ss.client.gui.element.ElementUI;
import com.ss.client.gui.element.event.ElementUIEvent;
import com.ss.client.gui.element.event.impl.PrimaryClickEvent;
import com.ss.client.gui.element.impl.DraggableElementUIImpl;
import com.ss.client.gui.element.listener.ElementUIEventListener;
import com.ss.client.gui.model.ElementUtils;
import com.ss.client.gui.model.InputMode;
import com.ss.client.gui.model.listeners.AddRemoveObjectListener;
import com.ss.client.gui.util.UpdateUIElement;
import com.ss.client.manager.UpdateUIManager;
import com.ss.client.model.SpaceObject;
import com.ss.client.model.SpaceObjectView;
import com.ss.client.model.ship.player.PlayerShip;
import com.ss.client.model.space.SpaceLocation;
import com.ss.client.util.LocalObjects;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.xml.xpp3.Attributes;

/**
 * Реализация контрола панели радара.
 * 
 * @author Ronn
 */
public class RadarPanelUIController extends DraggableElementUIImpl implements GameUIComponent, UpdateUIElement, AddRemoveObjectListener {

	public static final String ZOOM_IN_ID = "#zoom_in";
	public static final String ZOOM_PANEL_ID = "#zoom_panel";
	public static final String ZOOM_OUT_ID = "#zoom_out";
	public static final String OBJECT_PANEL_ID = "#object_panel";
	public static final String PANEL_ID = "#radar_panel";

	public static final String BACKGROUND_IMAGE = "ui/game/radar_panel/background.png";
	public static final String ZOOM_IN_IMAGE = "ui/game/radar_panel/zoom_in.png";
	public static final String ZOOM_OUT_IMAGE = "ui/game/radar_panel/zoom_out.png";

	public static final int RADAR_RADIUS = 3000;

	private static final Game GAME = Game.getInstance();
	private static final Camera CAMERA = GAME.getCamera().clone();
	private static final SpaceLocation SPACE_LOCATION = SpaceLocation.getInstance();

	private static final FuncValue<RadarObjectUIController> FUNC_CLEAR_SELECTED = new FuncValue<RadarObjectUIController>() {

		@Override
		public void apply(RadarObjectUIController controller) {
			if(controller.isSelected()) {
				controller.setSelected(false);
				controller.updateIcon();
			}
		}
	};

	/** слушатель событий игрового UI */
	private final GameUIEventListener eventListener = new GameUIEventListener() {

		@Override
		public void notifyEvent(GameUIEvent event) {
			processEvent(event);
		}
	};

	/** слушатель приближения радара */
	private final ElementUIEventListener<ElementUI> zoomInEventListener = new ElementUIEventListener<ElementUI>() {

		@Override
		public void notifyEvent(ElementUIEvent<ElementUI> event) {
			zoom = Math.max(zoom - 0.1F, 0.1F);
		}
	};

	/** слушатель отдаления радара */
	private final ElementUIEventListener<ElementUI> zoomOutEventListener = new ElementUIEventListener<ElementUI>() {

		@Override
		public void notifyEvent(ElementUIEvent<ElementUI> event) {
			zoom = Math.min(zoom + 0.1F, 1F);
		}
	};

	/** таблица контроллеров */
	private final Table<LongKey, RadarObjectUIController> controllers;
	/** список объектов */
	private final Array<SpaceObject> objects;
	/** список обновляемых объектов */
	private final Array<RadarObjectUIController> updated;

	/** оригинальный разворот камеры */
	private final Quaternion originalRotation;
	/** разворот камеры для обновления объектов */
	private final Quaternion updateRotation;

	/** оригинальная позиция камеры */
	private final Vector3f originalPosition;
	/** позиция камеры для обновления объектов */
	private final Vector3f updatePosition;
	/** координаты центральной точки */
	private final Vector2f center;

	/** счетчик объектов на радаре */
	private final AtomicInteger objectIndex;

	/** контроллер UI */
	private Nifty nifty;
	/** текущий экран элемента */
	private Screen screen;
	/** панель для размещения объектов */
	private Element objectPanel;

	/** корабль игрока */
	private PlayerShip playerShip;

	/** модификатор координат */
	private float coordinatMod;
	/** приближение радара */
	private float zoom;

	/** отступ по горизонтали */
	private int offsetX;
	/** отступ по вертикали */
	private int offsetY;

	public RadarPanelUIController() {
		this.controllers = Tables.newLongTable();
		this.objects = Arrays.toArray(SpaceObject.class);
		this.updated = Arrays.toArray(RadarObjectUIController.class);
		this.originalPosition = new Vector3f();
		this.originalRotation = new Quaternion();
		this.updatePosition = new Vector3f();
		this.updateRotation = new Quaternion();
		this.objectIndex = new AtomicInteger();
		this.center = new Vector2f();
		this.zoom = 1F;
	}

	@Override
	public void addObject(SpaceObject object) {

		if(!object.isSpaceShip()) {
			return;
		}

		Table<LongKey, RadarObjectUIController> controllers = getControllers();

		if(controllers.containsKey(object.getObjectId())) {
			return;
		}

		RadarObjectUIController controller = RadarElementUIFactory.build(object, getObjectIndex(), getObjectPanel(), getScreen(), getNifty(), object == getPlayerShip());
		controllers.put(object.getObjectId(), controller);
	}

	@Override
	public void bind(Array<UpdateUIElement> container) {
	}

	@Override
	public void bind(Nifty nifty, Screen screen, Element element, Properties properties, Attributes attributes) {
		super.bind(nifty, screen, element, properties, attributes);

		this.objectPanel = element.findElementByName(OBJECT_PANEL_ID);

		ElementUI zoomIn = element.findControl(ZOOM_IN_ID, ElementUI.class);
		zoomIn.addElementListener(PrimaryClickEvent.EVENT_TYPE, getZoomInEventListener());

		ElementUI zoomOut = element.findControl(ZOOM_OUT_ID, ElementUI.class);
		zoomOut.addElementListener(PrimaryClickEvent.EVENT_TYPE, getZoomOutEventListener());

		float radius = Math.max(objectPanel.getHeight(), objectPanel.getWidth()) - 10;
		float cameraSize = Math.max(CAMERA.getHeight(), CAMERA.getWidth());
		float sizeMod = radius / cameraSize;

		getCenter().set(objectPanel.getWidth() / 2, objectPanel.getHeight() / 2);

		float diff = (objectPanel.getHeight() - CAMERA.getHeight() * sizeMod) / 2;
		this.offsetY = (int) diff;

		diff = (objectPanel.getWidth() - CAMERA.getWidth() * sizeMod) / 2;
		this.offsetX = (int) diff;

		this.coordinatMod = sizeMod;
		this.screen = screen;
		this.nifty = nifty;
	}

	@Override
	public void finish() {
	}

	/**
	 * @return координаты центральной точки.
	 */
	public Vector2f getCenter() {
		return center;
	}

	/**
	 * @return таблица контроллеров.
	 */
	public Table<LongKey, RadarObjectUIController> getControllers() {
		return controllers;
	}

	/**
	 * @return модификатор координат.
	 */
	public float getCoordinatMod() {
		return coordinatMod;
	}

	/**
	 * @return слушатель событий игрового UI.
	 */
	public GameUIEventListener getEventListener() {
		return eventListener;
	}

	/**
	 * @return контроллер UI.
	 */
	public Nifty getNifty() {
		return nifty;
	}

	/**
	 * @return счетчик объектов на радаре.
	 */
	public AtomicInteger getObjectIndex() {
		return objectIndex;
	}

	/**
	 * @return панель для размещения объектов.
	 */
	public Element getObjectPanel() {
		return objectPanel;
	}

	/**
	 * @return список объектов.
	 */
	public Array<SpaceObject> getObjects() {
		return objects;
	}

	/**
	 * @return отступ по горизонтали.
	 */
	public int getOffsetX() {
		return offsetX;
	}

	/**
	 * @return отступ по вертикали.
	 */
	public int getOffsetY() {
		return offsetY;
	}

	/**
	 * @return оригинальная позиция камеры.
	 */
	public Vector3f getOriginalPosition() {
		return originalPosition;
	}

	/**
	 * @return оригинальный разворот камеры.
	 */
	public Quaternion getOriginalRotation() {
		return originalRotation;
	}

	/**
	 * @return корабль игрока.
	 */
	public PlayerShip getPlayerShip() {
		return playerShip;
	}

	/**
	 * @return текущий экран элемента.
	 */
	public Screen getScreen() {
		return screen;
	}

	/**
	 * @return список обновляемых объектов.
	 */
	public Array<RadarObjectUIController> getUpdated() {
		return updated.clear();
	}

	/**
	 * @return позиция камеры для обновления объектов.
	 */
	public Vector3f getUpdatePosition() {
		return updatePosition;
	}

	/**
	 * @return разворот камеры для обновления объектов.
	 */
	public Quaternion getUpdateRotation() {
		return updateRotation;
	}

	/**
	 * @return приближение радара.
	 */
	public float getZoom() {
		return zoom;
	}

	/**
	 * @return слушатель приближения радара.
	 */
	public ElementUIEventListener<ElementUI> getZoomInEventListener() {
		return zoomInEventListener;
	}

	/**
	 * @return слушатель отдаления радара.
	 */
	public ElementUIEventListener<ElementUI> getZoomOutEventListener() {
		return zoomOutEventListener;
	}

	@Override
	public boolean isComplexElement() {
		return true;
	}

	@Override
	public void onStartScreen() {

		UpdateUIManager manager = UpdateUIManager.getInstance();
		manager.addElement(this);

		SPACE_LOCATION.addListener(this);
	}

	/**
	 * Обрабортка события игрового UI.
	 */
	private void processEvent(GameUIEvent event) {

		Table<LongKey, RadarObjectUIController> controllers = getControllers();

		if(event.getEventType() == ObjectChangeFriendStatusEvent.EVENT_TYPE) {

			ObjectChangeFriendStatusEvent friendStatusEvent = (ObjectChangeFriendStatusEvent) event;
			SpaceObject object = friendStatusEvent.getObject();

			if(object == getPlayerShip()) {
				return;
			}

			RadarObjectUIController controller = controllers.get(object.getObjectId());

			if(controller != null) {
				controller.updateIcon();
			}

		} else if(event.getEventType() == SelectedObjectEvent.EVENT_TYPE) {

			SelectedObjectEvent objectEvent = (SelectedObjectEvent) event;
			SpaceObject object = objectEvent.getObject();

			if(object == getPlayerShip()) {
				return;
			}

			controllers.apply(FUNC_CLEAR_SELECTED);

			RadarObjectUIController controller = controllers.get(object.getObjectId());

			if(controller != null) {
				controller.setSelected(true);
				controller.updateIcon();
			}
		}
	}

	@Override
	public void register(GameUIController controller) {
		controller.registerListener(ObjectChangeFriendStatusEvent.EVENT_TYPE, getEventListener());
		controller.registerListener(SelectedObjectEvent.EVENT_TYPE, getEventListener());
	}

	@Override
	public void register(InputManager inputManager) {
	}

	@Override
	public void removeObject(SpaceObject object) {

		if(!object.isSpaceShip()) {
			return;
		}

		Table<LongKey, RadarObjectUIController> controllers = getControllers();

		if(!controllers.containsKey(object.getObjectId())) {
			return;
		}

		RadarObjectUIController controller = controllers.remove(object.getObjectId());
		controller.remove();
	}

	@Override
	public void setInputMode(InputMode inputMode) {
	}

	@Override
	public void setPlayerShip(PlayerShip playerShip) {
		this.playerShip = playerShip;
	}

	@Override
	public boolean update(LocalObjects local, long currentTime) {

		PlayerShip playerShip = getPlayerShip();

		if(playerShip == null) {
			return false;
		}

		SpaceObjectView view = playerShip.getView();

		if(view == null) {
			return false;
		}

		Table<LongKey, RadarObjectUIController> controllers = getControllers();

		if(controllers.isEmpty()) {
			return false;
		}

		Array<RadarObjectUIController> updated = controllers.values(getUpdated());

		Camera camera = CAMERA;

		int counter = 0;

		Vector3f originalPosition = getOriginalPosition();
		originalPosition.set(camera.getLocation());

		Quaternion originalRotation = getOriginalRotation();
		originalRotation.set(camera.getRotation());

		Quaternion rotation = view.getRotation();

		Vector3f up = rotation.getRotationColumn(2, local.getNextVector());
		Vector3f position = rotation.getRotationColumn(1, getUpdatePosition());
		position.multLocal(RADAR_RADIUS * getZoom());
		position.addLocal(view.getLocation());

		Vector3f direction = local.getNextVector();
		direction.set(view.getLocation());
		direction.subtractLocal(position);
		direction.normalizeLocal();

		Quaternion updateRotation = getUpdateRotation();
		updateRotation.lookAt(direction, up);

		camera.setRotation(updateRotation);
		camera.setLocation(position);

		Vector3f objectPosition = local.getNextVector();

		float mod = getCoordinatMod();

		int offsetX = getOffsetX();
		int offsetY = getOffsetY();

		Vector2f center = getCenter();

		for(RadarObjectUIController controller : updated.array()) {

			if(controller == null) {
				break;
			}

			if(controller.update(camera, objectPosition, center, mod, 90, offsetX, offsetY)) {
				counter++;
			}
		}

		camera.setRotation(originalRotation);
		camera.setLocation(originalPosition);

		if(counter > 0) {
			ElementUtils.updateLayout(getObjectPanel());
		}

		return false;
	}
}
