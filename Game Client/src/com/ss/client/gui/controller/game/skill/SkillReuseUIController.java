package com.ss.client.gui.controller.game.skill;

import rlib.util.array.Array;

import com.ss.client.gui.element.impl.PanelUIImpl;
import com.ss.client.gui.model.ElementUtils;
import com.ss.client.gui.util.UpdateUIElement;
import com.ss.client.manager.UpdateUIManager;
import com.ss.client.util.GameUtil;
import com.ss.client.util.LocalObjects;

import de.lessvoid.nifty.elements.Element;

/**
 * Контроллер отображения отката умения.
 * 
 * @author Ronn
 */
public class SkillReuseUIController extends PanelUIImpl implements UpdateUIElement {

	public static final String REUSE_IMAGE = "ui/game/fast_panel/active_cell.png";
	public static final String REUSE_IMAGE_ID = "#reuse_image";

	/** контейнер, в котором находится элемент для обновления */
	private Array<UpdateUIElement> container;

	/** дата старта отката */
	private long startReuseTime;

	/** время отката */
	private int reuseTime;

	@Override
	public void bind(Array<UpdateUIElement> container) {
		setContainer(container);
	}

	@Override
	public void finish() {
		setContainer(null);
		setVisible(false);
	}

	@Override
	public boolean isComplexElement() {
		return false;
	}

	@Override
	public boolean update(LocalObjects local, long currentTime) {

		long diff = currentTime - getStartReuseTime();

		int percent = (int) (100 - (diff * 100 / getReuseTime()));
		percent = Math.min(percent, 100);
		percent = Math.max(0, percent);

		if(percent > 0) {

			Element element = getElement();
			element.setConstraintHeight(GameUtil.getPercentSize(percent));

			ElementUtils.updateLayout(element.getParent());
		}

		return percent < 1;
	}

	/**
	 * @return контейнер, в котором находится элемент для обновления.
	 */
	public Array<UpdateUIElement> getContainer() {
		return container;
	}

	/**
	 * Запуск отображения отката.
	 * 
	 * @param startTime время старта отката.
	 * @param reuseTime общее время отката.
	 */
	public void startReuse(long startTime, int reuseTime) {

		Array<UpdateUIElement> container = getContainer();

		if(container == null) {
			UpdateUIManager manager = UpdateUIManager.getInstance();
			manager.addElement(this);
		}

		setStartReuseTime(startTime);
		setReuseTime(reuseTime);
		update(LocalObjects.get(), System.currentTimeMillis());
		setVisible(true);
	}

	/**
	 * @param startReuseTime дата старта отката.
	 */
	public void setStartReuseTime(long startReuseTime) {
		this.startReuseTime = startReuseTime;
	}

	/**
	 * @param reuseTime время отката.
	 */
	public void setReuseTime(int reuseTime) {
		this.reuseTime = reuseTime;
	}

	/**
	 * @return дата старта отката.
	 */
	public long getStartReuseTime() {
		return startReuseTime;
	}

	/**
	 * @return длительность отката.
	 */
	public int getReuseTime() {
		return reuseTime;
	}

	/**
	 * @param container контейнер, в котором находится элемент для обновления.
	 */
	private void setContainer(Array<UpdateUIElement> container) {
		this.container = container;
	}
}
