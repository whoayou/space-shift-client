package com.ss.client.gui.controller.game.window.skills;

/**
 * Набор констант связанных с UI текстом окна умений.
 * 
 * @author Ronn
 */
public interface SkillWindowTextIds {

	public static final String LOCALIZATION_FILE = "/data/localization/game_interface/skill_window.xml";

	public static final String WINDOW_TITLE = "@interface:skillWindowTitle@";
}
