package com.ss.client.gui.controller.game.event.impl;

import com.ss.client.gui.controller.game.event.GameUIEventType;
import com.ss.client.gui.controller.game.panel.fast.FastPanelItem;

/**
 * Событие об обновлении ячейки на панели быстрого доступа.
 * 
 * @author Ronn
 */
public class UpdateFastPanelCellEvent extends AbstractGameUIEvent {

	public static final GameUIEventType EVENT_TYPE = new GameUIEventType() {
	};

	private static final ThreadLocal<UpdateFastPanelCellEvent> LOCAL_EVENT = new ThreadLocal<UpdateFastPanelCellEvent>() {

		@Override
		protected UpdateFastPanelCellEvent initialValue() {
			return new UpdateFastPanelCellEvent();
		}
	};

	public static final UpdateFastPanelCellEvent get() {
		return LOCAL_EVENT.get();
	}

	/** новый итем для ячейки */
	private FastPanelItem item;

	/** позиция итема */
	private int order;

	@Override
	public GameUIEventType getEventType() {
		return EVENT_TYPE;
	}

	/**
	 * @return новый итем для ячейки.
	 */
	public FastPanelItem getItem() {
		return item;
	}

	/**
	 * @return позиция итема.
	 */
	public int getOrder() {
		return order;
	}

	/**
	 * @param item новый итем для ячейки.
	 */
	public void setItem(FastPanelItem item) {
		this.item = item;
	}

	/**
	 * @param order позиция итема.
	 */
	public void setOrder(int order) {
		this.order = order;
	}
}
