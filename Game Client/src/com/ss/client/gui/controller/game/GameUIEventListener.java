package com.ss.client.gui.controller.game;

import com.ss.client.gui.controller.game.event.GameUIEvent;

/**
 * Интерфейс для реализации прослушки игровых событий интерфейса.
 * 
 * @author Ronn
 */
public interface GameUIEventListener {

	/**
	 * Уведомление о событии.
	 * 
	 * @param event событие.
	 */
	public void notifyEvent(GameUIEvent event);
}
