package com.ss.client.gui.controller.game.window.ship;

/**
 * Набор констант связанных с UI текстом окна корабля.
 * 
 * @author Ronn
 */
public interface ShipWindowTextIds {

	public static final String LOCALIZATION_FILE = "/data/localization/game_interface/ship_window.xml";

	public static final String STATS_FLY_ACCEL = "@interface:shipWindowStatsFlyAccel@";
	public static final String STATS_FLY_SPEED = "@interface:shipWindowStatsFlySpeed@";
	public static final String STATS_ENERGY_GEN = "@interface:shipWindowStatsEnergyGen@";
	public static final String STATS_ENERGY = "@interface:shipWindowStatsEnergy@";
	public static final String STATS_STRENGTH = "@interface:shipWindowStatsStrength@";
	public static final String STATS_EXP = "@interface:shipWindowStatsExp@";
	public static final String STATS_LEVEL = "@interface:shipWindowStatsLevel@";

	public static final String WINDOW_TITLE = "@interface:shipWindowTitle@";
	public static final String STATS_TITLE = "@interface:shipWindowStatsTitle@";
	public static final String MODULES_TITLE = "@interface:shipWindowModulesTitle@";
	public static final String SHIP_NAME_LABEL = "@interface:shipWindowShipName@";

}
