package com.ss.client.gui.controller.game.panel;

import java.util.Properties;

import rlib.util.array.Array;
import rlib.util.array.Arrays;
import rlib.util.table.Table;
import rlib.util.table.Tables;

import com.jme3.input.InputManager;
import com.ss.client.gui.controller.game.GameUIComponent;
import com.ss.client.gui.controller.game.GameUIController;
import com.ss.client.gui.controller.game.window.quest.QuestWindowUIController;
import com.ss.client.gui.controller.game.window.ship.ShipWindowUIController;
import com.ss.client.gui.controller.game.window.skills.SkillWindowUIController;
import com.ss.client.gui.controller.game.window.storage.StorageWindowUIController;
import com.ss.client.gui.element.ButtonUI;
import com.ss.client.gui.element.ElementUI;
import com.ss.client.gui.element.event.ElementUIEvent;
import com.ss.client.gui.element.event.impl.PrimaryClickEvent;
import com.ss.client.gui.element.impl.DraggableElementUIImpl;
import com.ss.client.gui.element.listener.ElementUIEventListener;
import com.ss.client.gui.model.InputMode;
import com.ss.client.model.ship.player.PlayerShip;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.xml.xpp3.Attributes;

/**
 * Реализация контроллера главной панели игрового UI.
 * 
 * @author Ronn
 */
public class MainPanelController extends DraggableElementUIImpl implements GameUIComponent {

	private static final Table<String, String> MAPPING = Tables.newObjectTable();

	public static final String BUTTON_OPEN_WIDWOW_SKILL_ID = "#button_open_window_skill";
	public static final String BUTTON_OPEN_WIDWOW_STORAGE_ID = "#button_open_window_storage";
	public static final String BUTTON_OPEN_WIDWOW_SHIP_ID = "#button_open_window_ship";
	public static final String BUTTON_OPEN_WIDWOW_QUEST_ID = "#button_open_window_quest";

	public static final String PANEL_ID = "#base_main_panel";

	static {
		MAPPING.put(BUTTON_OPEN_WIDWOW_SKILL_ID, SkillWindowUIController.WINDOW_ID);
		MAPPING.put(BUTTON_OPEN_WIDWOW_STORAGE_ID, StorageWindowUIController.WINDOW_ID);
		MAPPING.put(BUTTON_OPEN_WIDWOW_SHIP_ID, ShipWindowUIController.WINDOW_ID);
		MAPPING.put(BUTTON_OPEN_WIDWOW_QUEST_ID, QuestWindowUIController.WINDOW_ID);
	}

	/** слушатель нажатий на иконки */
	private final ElementUIEventListener<ButtonUI> activateListener = new ElementUIEventListener<ButtonUI>() {

		@Override
		public void notifyEvent(ElementUIEvent<ButtonUI> event) {
			activate(event.getElement());
		}
	};

	/** набор кнопок панели */
	private ButtonUI[] buttons;

	/** экран, на котором размещена панель */
	private Screen screen;

	private void activate(ButtonUI button) {

		String targetID = MAPPING.get(button.getId());
		Element target = getScreen().findElementByName(targetID);

		if(target != null && !target.isVisible()) {

			ElementUI element = target.getControl(ElementUI.class);

			if(element != null) {
				element.setVisible(true);
			}
		}
	}

	@Override
	public void bind(Nifty nifty, Screen screen, Element element, Properties properties, Attributes attributes) {
		super.bind(nifty, screen, element, properties, attributes);

		// сбор вставленных кнопок
		{
			Array<ButtonUI> buttons = Arrays.toArray(ButtonUI.class);

			for(Element child : element.getElements()) {

				ButtonUI button = child.getControl(ButtonUI.class);

				if(button != null) {
					button.addElementListener(PrimaryClickEvent.EVENT_TYPE, getActivateListener());
					buttons.add(button);
				}
			}

			this.buttons = buttons.toArray(new ButtonUI[buttons.size()]);
		}

		this.screen = screen;
	}

	@Override
	public void finish() {
	}

	/**
	 * @return слушатель нажатий на иконки.
	 */
	private ElementUIEventListener<ButtonUI> getActivateListener() {
		return activateListener;
	}

	/**
	 * @return набор кнопок панели.
	 */
	public ButtonUI[] getButtons() {
		return buttons;
	}

	/**
	 * @return экран, на котором размещена панель.
	 */
	private Screen getScreen() {
		return screen;
	}

	@Override
	public void register(GameUIController controller) {
	}

	@Override
	public void register(InputManager inputManager) {
	}

	@Override
	public void setInputMode(InputMode inputMode) {
		Element element = getElement();
		element.setVisible(inputMode == InputMode.INTERFACE_MODE);
	}

	@Override
	public void setPlayerShip(PlayerShip playerShip) {
	}
}
