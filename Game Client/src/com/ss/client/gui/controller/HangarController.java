package com.ss.client.gui.controller;

import java.util.concurrent.ScheduledFuture;

import rlib.logging.Logger;
import rlib.logging.Loggers;
import rlib.util.SafeTask;
import rlib.util.Strings;
import rlib.util.array.Array;

import com.ss.client.Game;
import com.ss.client.gui.ElementId;
import com.ss.client.manager.ExecutorManager;
import com.ss.client.model.ServerInfo;
import com.ss.client.model.ship.player.PlayerShip;
import com.ss.client.model.ship.player.PlayerShipClass;
import com.ss.client.model.state.HangarState;
import com.ss.client.model.state.StateId;
import com.ss.client.network.Network;
import com.ss.client.network.model.NetServer;
import com.ss.client.network.packet.client.RequestCreateShip;
import com.ss.client.network.packet.client.RequestDeleteShip;
import com.ss.client.network.packet.client.RequestSelectShip;
import com.ss.client.table.LangTable;
import com.ss.client.util.Initializable;
import com.ss.client.util.ReflectionMethod;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.NiftyEventSubscriber;
import de.lessvoid.nifty.controls.Button;
import de.lessvoid.nifty.controls.DropDown;
import de.lessvoid.nifty.controls.DropDownSelectionChangedEvent;
import de.lessvoid.nifty.controls.Label;
import de.lessvoid.nifty.controls.TextField;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.input.NiftyInputEvent;
import de.lessvoid.nifty.screen.DefaultScreenController;
import de.lessvoid.nifty.screen.Screen;

/**
 * Модель основного меню.
 * 
 * @author Ronn
 */
public class HangarController extends DefaultScreenController implements Initializable {

	public static enum ScreenState {
		WAIT,
		CREATE_SHIP,
		SELECTED_SHIP,
		DELETE_SHIP,
	}

	private static final Logger log = Loggers.getLogger(HangarController.class);

	private static final ExecutorManager EXECUTOR_MANAGER = ExecutorManager.getInstance();
	private static final LangTable LANG_TABLE = LangTable.getInstance();
	private static final Network NETWORK = Network.getInstance();
	private static final Game GAME = Game.getInstance();
	private static final Nifty NIFTY = GAME.getNifty();

	private static final String INTERFACE_HANGAR_LABEL_SERVER_TYPE = LANG_TABLE.getText("@interface:hangarLabelServerType@");
	private static final String INTERFACE_HANGAR_LABEL_SERVER_NAME = LANG_TABLE.getText("@interface:hangarLabelServerName@");

	/** экземпляр контролера */
	private static HangarController instance;

	public static HangarController getInstance() {
		if(instance == null)
			instance = new HangarController();

		return instance;
	}

	/** надпись с инфой сервера */
	private Label serverInfoLabel;
	/** сообщение окна внимания */
	private Label warning;

	/** название нового корабля */
	private TextField createName;

	/** кнопка создания корабля */
	private Button createShip;
	/** кнопка входа в мир */
	private Button enterShip;
	/** кнопка удаления корабля */
	private Button deleteShip;

	/** список кораблей на аккаунте */
	private DropDown<String> enterShipList;
	/** список кораблей на аккаунте */
	private DropDown<String> deleteShipList;

	/** слой создания корабля */
	private Element createShipLayer;
	/** слой предупреждения */
	private Element warningLayer;
	/** слой выбора корабля */
	private Element selectedShipLayer;
	/** слой удаления корабля */
	private Element deleteShipLayer;

	/** таск авторазблокировки */
	private SafeTask unblock;

	/** таск авторазблокировки */
	private ScheduledFuture<SafeTask> unblockSchedule;

	/** стадия ангара */
	private ScreenState state;
	/** стадия ангара */
	private ScreenState nextState;

	/** иницилазирован ли контролер */
	private boolean initialized;

	@NiftyEventSubscriber(pattern = "hangar.*")
	public void changeSelectedShip(final String id, final DropDownSelectionChangedEvent<String> event) {
		final ElementId element = ElementId.findById(id);

		GAME.syncLock();
		try {
			if(element == ElementId.HANGAR_ENTER_DROP_DOWN_SELECTED) {
				final HangarState hangar = getHangarState();

				if(hangar == null || !hangar.isInitialized())
					return;

				hangar.selectedPlayerShip(enterShipList.getSelectedIndex());
				enterShip.setFocus();
			} else if(element == ElementId.HANGAR_DELETE_DROP_DOWN_SELECTED) {
				final HangarState hangar = getHangarState();

				if(hangar == null || !hangar.isInitialized())
					return;

				hangar.selectedPlayerShip(deleteShipList.getSelectedIndex());
				deleteShip.setFocus();
			}
		} finally {
			GAME.syncUnlock();
		}
	}

	/**
	 * Открыть окно создания корабля.
	 */
	@ReflectionMethod
	public void createShip() {
		if(warningLayer.isVisible())
			return;

		GAME.syncLock();
		try {
			switch(getState()) {
				case CREATE_SHIP:
					return;
				case DELETE_SHIP:
					deleteShipCancel();
					break;
				case SELECTED_SHIP:
					selectedShipCancel();
					break;
				default:
					break;
			}

			if(getState() != ScreenState.WAIT)
				return;

			toState(ScreenState.CREATE_SHIP);
		} finally {
			GAME.syncUnlock();
		}
	}

	/**
	 * Отмена создания корабля.
	 */
	@ReflectionMethod
	public void createShipCancel() {
		if(getState() != ScreenState.CREATE_SHIP)
			return;

		GAME.syncLock();
		try {
			createName.setText(Strings.EMPTY);

			toState(ScreenState.WAIT);
		} finally {
			GAME.syncUnlock();
		}
	}

	/**
	 * Создание корабля.
	 */
	@ReflectionMethod
	public void createShipCreate() {
		if(getState() != ScreenState.CREATE_SHIP)
			return;

		GAME.syncLock();
		try {
			final PlayerShipClass type = PlayerShipClass.EXPLORATION;

			final String name = createName.getDisplayedText();
			final NetServer server = NETWORK.getGameServer();

			if(name == null || type == null || server == null) {
				log.warning(new Exception("not found name or type or server."));
				return;
			}

			server.sendPacket(RequestCreateShip.getInstance(name, type));

			disable(createShipLayer);

			unblockSchedule = EXECUTOR_MANAGER.scheduleGeneral(unblock, 5000);
		} finally {
			GAME.syncUnlock();
		}
	}

	/**
	 * Спрятать слой создания корабля.
	 */
	private void createShipHide() {
		createShipLayer.hide();
	}

	/**
	 * Отобразить слой создания корабля.
	 */
	private void createShipShow() {
		createShipLayer.show();
		createName.setFocus();
	}

	/**
	 * Переход к окну удаления звездалета.
	 */
	@ReflectionMethod
	public void deleteShip() {
		if(warningLayer.isVisible())
			return;

		GAME.syncLock();
		try {
			switch(getState()) {
				case DELETE_SHIP:
					return;
				case CREATE_SHIP:
					createShipCancel();
					break;
				case SELECTED_SHIP:
					selectedShipCancel();
					break;
				default:
					break;
			}

			if(getState() != ScreenState.WAIT)
				return;

			final HangarState hangar = getHangarState();

			if(hangar == null || !hangar.isInitialized()) {
				log.warning("not found or not initialized hangar.");
				return;
			}

			final Array<PlayerShip> ships = hangar.getPlayerShips();

			if(ships.isEmpty())
				return;

			final DropDown<String> deleteShipList = getDeleteShipList();

			deleteShipList.clear();

			for(final PlayerShip ship : ships)
				deleteShipList.addItem(ship.getName());

			deleteShipList.selectItemByIndex(0);

			hangar.selectedPlayerShip(0);

			toState(ScreenState.DELETE_SHIP);
		} finally {
			GAME.syncUnlock();
		}
	}

	/**
	 * Отменая удаления корабля.
	 */
	@ReflectionMethod
	public void deleteShipCancel() {
		if(getState() != ScreenState.DELETE_SHIP)
			return;

		GAME.syncLock();
		try {
			deleteShipList.clear();

			final HangarState hangar = getHangarState();

			if(hangar == null || !hangar.isInitialized()) {
				log.warning(new Exception("not found or not initialized hangar."));
				return;
			}

			hangar.selectedPlayerShip(-1);

			toState(ScreenState.WAIT);
		} finally {
			GAME.syncUnlock();
		}
	}

	/**
	 * Удаление корабля.
	 */
	@ReflectionMethod
	public void deleteShipDelete() {
		if(getState() != ScreenState.DELETE_SHIP)
			return;

		GAME.syncLock();
		try {
			final String name = deleteShipList.getSelection();

			final NetServer server = NETWORK.getGameServer();

			if(name == null || server == null) {
				log.warning(new Exception("not found name or server."));
				toState(ScreenState.WAIT);
				return;
			}

			final HangarState hangar = getHangarState();

			if(hangar == null || !hangar.isInitialized()) {
				log.warning(new Exception("not found or not initialized hangar."));
				return;
			}

			hangar.selectedPlayerShip(-1);

			server.sendPacket(RequestDeleteShip.getInstance(name));

			toState(ScreenState.WAIT);
		} finally {
			GAME.syncUnlock();
		}
	}

	/**
	 * Спрятать слой удаления корабля.
	 */
	private void deleteShipHide() {
		deleteShipLayer.hide();
	}

	/**
	 * Отобразить слой удаление корабля.
	 */
	private void deleteShipShow() {
		deleteShipLayer.show();
		deleteShipList.setFocus();
	}

	/**
	 * Отключение панели.
	 */
	private void disable(final Element layer) {
		final Array<Element> elements = layer.getFastElements();

		layer.disable();

		for(final Element element : elements)
			element.disable();
	}

	/**
	 * Включение панели.
	 */
	private void enable(final Element layer) {
		final Array<Element> elements = layer.getFastElements();

		layer.enable();

		for(final Element element : elements)
			element.enable();
	}

	/**
	 * Выход из сервера.
	 */
	@ReflectionMethod
	public void exit() {
		if(warningLayer.isVisible())
			return;

		GAME.syncLock();
		try {
			switch(getState()) {
				case CREATE_SHIP:
					createShipCancel();
					break;
				case DELETE_SHIP:
					deleteShipCancel();
					break;
				case SELECTED_SHIP:
					selectedShipCancel();
					break;
				default:
					break;
			}

			if(getState() != ScreenState.WAIT)
				return;

			final NetServer server = NETWORK.getGameServer();

			if(server != null)
				server.close();

			GAME.gotoState(StateId.LOGIN_STATE);
		} finally {
			GAME.syncUnlock();
		}
	}

	/**
	 * @return список кораблей на удаление.
	 */
	public DropDown<String> getDeleteShipList() {
		return deleteShipList;
	}

	/**
	 * @return текущий набор кораблей для входа.
	 */
	public DropDown<String> getEnterShipList() {
		return enterShipList;
	}

	/**
	 * @return стадия ангара.
	 */
	private HangarState getHangarState() {
		return GAME.getStateManager().getState(HangarState.class);
	}

	/**
	 * @return след. стадия.
	 */
	public ScreenState getNextState() {
		return nextState;
	}

	/**
	 * @return стадия меню.
	 */
	public final ScreenState getState() {
		return state;
	}

	@NiftyEventSubscriber(pattern = "hangar.*")
	public void inputKey(final String id, final NiftyInputEvent event) {
		if(event == NiftyInputEvent.SubmitText && ElementId.HANGAR_CREATE_FIELD_NAME.getId().equals(id))
			createShip.setFocus();
	}

	@Override
	public boolean isInitialized() {
		return initialized;
	}

	@Override
	public void onEndScreen() {
		super.onEndScreen();

		if(unblockSchedule != null) {
			unblockSchedule.cancel(false);
			unblockSchedule = null;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void onStartScreen() {
		if(!isInitialized()) {
			final Screen screen = NIFTY.getScreen(ElementId.HANGAR_SCREEN.getId());

			serverInfoLabel = screen.findNiftyControl(ElementId.HANGAR_SERVER_LABEL_INFO.getId(), Label.class);
			createName = screen.findNiftyControl(ElementId.HANGAR_CREATE_FIELD_NAME.getId(), TextField.class);
			enterShipList = screen.findNiftyControl(ElementId.HANGAR_ENTER_DROP_DOWN_SELECTED.getId(), DropDown.class);
			deleteShipList = screen.findNiftyControl(ElementId.HANGAR_DELETE_DROP_DOWN_SELECTED.getId(), DropDown.class);
			warning = screen.findNiftyControl(ElementId.HANGAR_WARNING_LABEL.getId(), Label.class);

			createShipLayer = screen.findElementByName(ElementId.HANGAR_CREATE_LAYER.getId());
			createShipLayer.hide();

			warningLayer = screen.findElementByName(ElementId.HANGAR_WARNING_LAYER.getId());
			warningLayer.hide();

			selectedShipLayer = screen.findElementByName(ElementId.HANGAR_ENTER_LAYER.getId());
			selectedShipLayer.hide();

			deleteShipLayer = screen.findElementByName(ElementId.HANGAR_DELETE_LAYER.getId());
			deleteShipLayer.hide();

			createShip = screen.findNiftyControl(ElementId.HANGAR_CREATE_BUTTON_CREATE.getId(), Button.class);
			enterShip = screen.findNiftyControl(ElementId.HANGAR_ENTER_BUTTON_ENTER.getId(), Button.class);
			deleteShip = screen.findNiftyControl(ElementId.HANGAR_DELETE_BUTTON_DELETE.getId(), Button.class);

			unblock = new SafeTask() {

				@Override
				protected void runImpl() {
					unblocking();
				}
			};

			final NetServer server = NETWORK.getGameServer();

			if(server == null) {
				Thread.dumpStack();
				System.exit(0);
			}

			final ServerInfo info = server.getInfo();

			if(info == null) {
				Thread.dumpStack();
				System.exit(0);
			}

			serverInfoLabel.setText(INTERFACE_HANGAR_LABEL_SERVER_NAME + " \"" + info.getName() + "\" " + " " + INTERFACE_HANGAR_LABEL_SERVER_TYPE + " \"" + info.getType() + "\"   ");

			setState(ScreenState.WAIT);
			setInitialized(true);
		}

		super.onStartScreen();
	}

	/**
	 * Переход к окну выбора звездалета.
	 */
	@ReflectionMethod
	public void selectedShip() {
		if(warningLayer.isVisible())
			return;

		GAME.syncLock();
		try {
			switch(getState()) {
				case SELECTED_SHIP:
					return;
				case CREATE_SHIP:
					createShipCancel();
					break;
				case DELETE_SHIP:
					deleteShipCancel();
					break;
				default:
					break;
			}

			if(getState() != ScreenState.WAIT)
				return;

			final HangarState hangar = getHangarState();

			if(hangar == null || !hangar.isInitialized()) {
				log.warning(new Exception("not found or not initialized hangar."));
				return;
			}

			final Array<PlayerShip> ships = hangar.getPlayerShips();

			if(ships.isEmpty())
				return;

			final DropDown<String> enterShipList = getEnterShipList();

			enterShipList.clear();

			for(final PlayerShip ship : ships)
				enterShipList.addItem(ship.getName());

			enterShipList.selectItemByIndex(0);

			hangar.selectedPlayerShip(0);

			toState(ScreenState.SELECTED_SHIP);
		} finally {
			GAME.syncUnlock();
		}
	}

	/**
	 * Отмена выбора корабля.
	 */
	@ReflectionMethod
	public void selectedShipCancel() {
		if(getState() != ScreenState.SELECTED_SHIP)
			return;

		GAME.syncLock();
		try {
			enterShipList.clear();

			final HangarState hangar = getHangarState();

			if(hangar == null || !hangar.isInitialized()) {
				log.warning(new Exception("not found or not initialized hangar."));
				return;
			}

			hangar.selectedPlayerShip(-1);

			toState(ScreenState.WAIT);
		} finally {
			GAME.syncUnlock();
		}
	}

	/**
	 * Вход в мир с выбранным кораблем.
	 */
	@ReflectionMethod
	public void selectedShipEnter() {
		if(getState() != ScreenState.SELECTED_SHIP)
			return;

		GAME.syncLock();
		try {
			final int index = enterShipList.getSelectedIndex();

			if(index < 0)
				return;

			final HangarState hangar = getHangarState();

			if(hangar == null || !hangar.isInitialized()) {
				log.warning("not found or not initialized hangar.");
				return;
			}

			final Array<PlayerShip> playerShips = hangar.getPlayerShips();

			if(playerShips.size() < index)
				return;

			final PlayerShip playerShip = playerShips.get(index);

			final NetServer server = NETWORK.getGameServer();

			if(server == null) {
				log.warning(new Exception("not found server."));
				return;
			}

			server.sendPacket(RequestSelectShip.getInstance(playerShip.getObjectId()));
		} finally {
			GAME.syncUnlock();
		}
	}

	/**
	 * Спрятать слой выбора корабля.
	 */
	private void selectedShipHide() {
		selectedShipLayer.hide();
	}

	/**
	 * Отобразить слой выбора корабля.
	 */
	private void selectedShipShow() {
		selectedShipLayer.show();
		enterShipList.setFocus();
	}

	public void setInitialized(final boolean initialized) {
		this.initialized = initialized;
	}

	/**
	 * @param next следующее состояние.
	 */
	public void setNextState(final ScreenState next) {
		this.nextState = next;
	}

	/**
	 * @param state стадия меню.
	 */
	public final void setState(final ScreenState state) {
		this.state = state;
	}

	/**
	 * Перейти к указанному состоянию.
	 * 
	 * @param state новое состояние.
	 */
	public void toState(final ScreenState state) {
		GAME.syncLock();
		try {
			setState(state);

			switch(state) {
				case WAIT: {
					createShipHide();
					selectedShipHide();
					deleteShipHide();

					break;
				}
				case CREATE_SHIP: {
					selectedShipHide();
					deleteShipHide();
					createShipShow();

					break;
				}
				case SELECTED_SHIP: {
					createShipHide();
					deleteShipHide();
					selectedShipShow();

					break;
				}
				case DELETE_SHIP: {
					createShipHide();
					selectedShipHide();
					deleteShipShow();
				}
			}
		} finally {
			GAME.syncUnlock();
		}
	}

	/**
	 * Разблокировка меню.
	 */
	public void unblocking() {
		GAME.syncLock();
		try {
			if(unblockSchedule != null) {
				unblockSchedule.cancel(false);
				unblockSchedule = null;
			}

			switch(getState()) {
				case CREATE_SHIP:
					enable(createShipLayer);
					break;
				case SELECTED_SHIP:
					enable(selectedShipLayer);
					break;
				case DELETE_SHIP:
					enable(deleteShipLayer);
					break;
				default:
					break;
			}
		} finally {
			GAME.syncUnlock();
		}
	}

	/**
	 * Нажать на ОК в предупреждении.
	 */
	@ReflectionMethod
	public void warningCancel() {
		GAME.syncLock();
		try {
			switch(getState()) {
				case CREATE_SHIP:
					enable(createShipLayer);
					break;
				case DELETE_SHIP:
					enable(deleteShipLayer);
					break;
				case SELECTED_SHIP:
					enable(selectedShipLayer);
					break;
				default:
					break;
			}

			warningHide();

			final ScreenState nextState = getNextState();

			if(nextState != null)
				toState(nextState);
		} finally {
			GAME.syncUnlock();
		}
	}

	/**
	 * Спрятать предупреждение.
	 */
	public void warningHide() {
		warningLayer.hide();
	}

	/**
	 * Отобразить предупреждение.
	 */
	public void warningShow(final String text, final ScreenState nextState) {
		GAME.syncLock();
		try {
			switch(getState()) {
				case CREATE_SHIP:
					disable(createShipLayer);
					break;
				case DELETE_SHIP:
					disable(deleteShipLayer);
					break;
				case SELECTED_SHIP:
					disable(selectedShipLayer);
					break;
				default:
					break;
			}

			setNextState(nextState);

			warning.setText(text);
			warningLayer.show();
		} finally {
			GAME.syncUnlock();
		}
	}
}
