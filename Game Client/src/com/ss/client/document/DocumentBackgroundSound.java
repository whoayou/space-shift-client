package com.ss.client.document;

import java.io.InputStream;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

import rlib.data.AbstractStreamDocument;
import rlib.util.VarTable;
import rlib.util.array.Array;
import rlib.util.array.Arrays;

import com.jme3.asset.AssetManager;
import com.ss.client.Game;
import com.ss.client.gui.model.BackgroundSound;

/**
 * Парсер фоновых звуков с xml.
 * 
 * @author Ronn
 */
public final class DocumentBackgroundSound extends AbstractStreamDocument<Array<BackgroundSound>> {

	private static final Game GAME = Game.getInstance();

	public DocumentBackgroundSound(final InputStream stream) {
		super(stream);
	}

	@Override
	protected Array<BackgroundSound> create() {
		return Arrays.toArray(BackgroundSound.class);
	}

	@Override
	protected void parse(final Document doc) {
		final AssetManager asset = GAME.getAssetManager();

		final VarTable vars = VarTable.newInstance();

		for(Node node = doc.getFirstChild(); node != null; node = node.getNextSibling())
			if("list".equals(node.getNodeName()))
				for(Node child = node.getFirstChild(); child != null; child = child.getNextSibling())
					if(child.getNodeType() == Node.ELEMENT_NODE && "sound".equals(child.getNodeName())) {
						vars.parse(child);

						result.add(new BackgroundSound(asset, vars.getString("name"), vars.getString("path"), vars.getBoolean("effect", false)));
					}
	}
}
