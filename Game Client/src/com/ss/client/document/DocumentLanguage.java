package com.ss.client.document;

import java.io.InputStream;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

import com.ss.client.gui.model.Language;

import rlib.data.AbstractStreamDocument;
import rlib.util.VarTable;
import rlib.util.array.Arrays;

/**
 * Парсер конфига с xml.
 * 
 * @author Ronn
 */
public final class DocumentLanguage extends AbstractStreamDocument<Language[]> {

	public DocumentLanguage(final InputStream stream) {
		super(stream);
	}

	@Override
	protected Language[] create() {
		return new Language[0];
	}

	@Override
	protected void parse(final Document doc) {
		final VarTable vars = VarTable.newInstance();

		for(Node list = doc.getFirstChild(); list != null; list = list.getNextSibling())
			if("list".equals(list.getNodeName()))
				for(Node child = list.getFirstChild(); child != null; child = child.getNextSibling())
					if(child.getNodeType() == Node.ELEMENT_NODE && "lang".equals(child.getNodeName())) {
						vars.parse(child);

						result = Arrays.addToArray(result, new Language(vars.getString("name"), vars.getInteger("ordinal")), Language.class);
					}
	}
}
