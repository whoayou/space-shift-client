package com.ss.client.document;

import java.io.File;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

import rlib.data.AbstractFileDocument;
import rlib.util.VarTable;

/**
 * Парсер конфига с xml.
 * 
 * @author Ronn
 */
public final class DocumentConfig extends AbstractFileDocument<VarTable> {

	public DocumentConfig(final File file) {
		super(file);
	}

	@Override
	protected VarTable create() {
		return VarTable.newInstance();
	}

	@Override
	protected void parse(final Document doc) {
		for(Node child = doc.getFirstChild(); child != null; child = child.getNextSibling())
			if("list".equals(child.getNodeName()))
				result.parse(child, "set", "name", "value");
	}
}
