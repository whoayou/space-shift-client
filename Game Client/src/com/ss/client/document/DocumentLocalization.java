package com.ss.client.document;

import java.io.InputStream;
import java.util.Arrays;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

import rlib.data.AbstractStreamDocument;
import rlib.util.VarTable;
import rlib.util.table.Table;
import rlib.util.table.Tables;

/**
 * Парсер конфига с xml.
 * 
 * @author Ronn
 */
public final class DocumentLocalization extends AbstractStreamDocument<Table<String, String>[]> {

	private final Table<String, String>[] container;

	public DocumentLocalization(final Table<String, String>[] container, final InputStream stream) {
		super(stream);

		this.container = container;
	}

	@Override
	protected Table<String, String>[] create() {
		return container;
	}

	@Override
	protected void parse(final Document doc) {
		for(Node list = doc.getFirstChild(); list != null; list = list.getNextSibling())
			if("list".equals(list.getNodeName()))
				for(Node child = list.getFirstChild(); child != null; child = child.getNextSibling())
					if(child.getNodeType() == Node.ELEMENT_NODE && "label".equals(child.getNodeName()))
						parseLabel(child);
	}

	private void parseLabel(final Node node) {
		final VarTable vars = VarTable.newInstance(node);

		final String id = vars.getString("id");

		for(Node child = node.getFirstChild(); child != null; child = child.getNextSibling())
			if(child.getNodeType() == Node.ELEMENT_NODE && "text".equals(child.getNodeName())) {
				vars.parse(child);

				final int lang = vars.getInteger("lang");

				if(result.length <= lang)
					result = Arrays.copyOf(result, lang + 1);

				Table<String, String> table = result[lang];

				if(table == null) {
					table = Tables.newObjectTable();
					result[lang] = table;
				}

				if(table.containsKey(id))
					LOGGER.warning(this, "found duplicate text for id " + id);

				table.put(id, vars.getString("value"));
			}
	}
}
