package com.ss.client.util;

import rlib.util.array.Array;
import rlib.util.array.Arrays;

import com.jme3.math.Quaternion;
import com.jme3.math.Ray;
import com.jme3.math.Vector3f;
import com.ss.client.GameThread;
import com.ss.client.model.module.Module;

/**
 * Контейнер локальных объектов.
 * 
 * @author Ronn
 */
public class LocalObjects {

	private static final int SIZE = 20;
	private static final int LIMIT = SIZE - 1;

	public static final LocalObjects get() {
		return ((GameThread) Thread.currentThread()).getLocalObects();
	}

	/** буффер векторов */
	private final Vector3f[] vectorBuffer;
	/** буфер кватернионов */
	private final Quaternion[] rotationBuffer;
	/** буфер лучей */
	private final Ray[] rayBuffer;

	/** буфер списков модулей */
	private final Array<Module>[] moduleLists;

	/** индекс след. свободного вектора */
	private int vectorIndex;
	/** иднекс след. свободного кватерниона */
	private int rotationIndex;
	/** индекс следующего луча */
	private int rayIndex;

	/** индекс следующего свободного списка модулей */
	private int moduleListIndex;

	@SuppressWarnings("unchecked")
	public LocalObjects() {

		this.vectorBuffer = new Vector3f[SIZE];
		this.rotationBuffer = new Quaternion[SIZE];
		this.rayBuffer = new Ray[SIZE];
		this.moduleLists = new Array[SIZE];

		for(int i = 0, length = SIZE; i < length; i++) {
			rotationBuffer[i] = new Quaternion();
			vectorBuffer[i] = new Vector3f();
			rayBuffer[i] = new Ray();
			moduleLists[i] = Arrays.toArray(Module.class);
		}
	}

	/**
	 * @return получаем след. свободный квантернион.
	 */
	public Quaternion getNextRotation() {

		if(rotationIndex == LIMIT) {
			rotationIndex = 0;
		}

		return rotationBuffer[rotationIndex++];
	}

	/**
	 * @return получаем след. свободнвый вектор.
	 */
	public Vector3f getNextVector() {

		if(vectorIndex == LIMIT) {
			vectorIndex = 0;
		}

		return vectorBuffer[vectorIndex++];
	}

	/**
	 * @return получаем след. свободного луча.
	 */
	public Ray getNextRay() {

		if(rayIndex == LIMIT) {
			rayIndex = 0;
		}

		return rayBuffer[rayIndex++];
	}

	/**
	 * @return получаем след. свободный список модулей.
	 */
	public Array<Module> getNextModuleList() {

		if(moduleListIndex == LIMIT) {
			moduleListIndex = 0;
		}

		return moduleLists[moduleListIndex++];
	}
}
