package com.ss.client.util;

import rlib.util.pools.Foldable;

/**
 * Интерфейс для реализации задачи пакета с сохранением в пул объекта.
 * 
 * @author Ronn
 */
public interface FoldablePacketTask extends PacketTask, Foldable {

	/**
	 * Складировать в пул.
	 */
	public void fold();
}
