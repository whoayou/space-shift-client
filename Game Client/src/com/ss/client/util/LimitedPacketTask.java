package com.ss.client.util;

import rlib.logging.Logger;
import rlib.logging.Loggers;

/**
 * Целкическая ограниченная задача.
 * 
 * @author Ronn
 */
public abstract class LimitedPacketTask implements PacketTask {

	protected static final Logger LOGGER = Loggers.getLogger(PacketTask.class);

	/** максимальное кол-во раз исполнений */
	private int limit;

	/** счетчик исполнений */
	private int counter;

	public LimitedPacketTask(int limit) {
		this.limit = limit;
	}

	/**
	 * @param counter счетчик исполнений.
	 */
	public void setCounter(int counter) {
		this.counter = counter;
	}

	/**
	 * @param limit максимальное кол-во раз исполнений.
	 */
	public void setLimit(int limit) {
		this.limit = limit;
	}

	/**
	 * @return счетчик исполнений.
	 */
	public int getCounter() {
		return counter;
	}

	/**
	 * @return максимальное кол-во раз исполнений.
	 */
	public int getLimit() {
		return limit;
	}

	protected abstract boolean executeImpl(LocalObjects local, long currentTime);

	@Override
	public boolean execute(LocalObjects local, long currentTime) {

		try {
			return executeImpl(local, currentTime) || ++counter >= getLimit();
		} catch(Exception e) {
			LOGGER.warning(e);
		}

		return true;
	}

	@Override
	public boolean isMatches(Object object) {
		return false;
	}
}
