package com.ss.client;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.concurrent.locks.Lock;
import java.util.logging.Level;

import rlib.concurrent.ConcurrentUtils;
import rlib.concurrent.Locks;
import rlib.concurrent.atomic.AtomicInteger;
import rlib.logging.Logger;
import rlib.logging.Loggers;
import rlib.manager.InitializeManager;
import rlib.util.array.Array;
import rlib.util.array.Arrays;

import com.jme3.app.SimpleApplication;
import com.jme3.font.BitmapFont;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.niftygui.NiftyJmeDisplay;
import com.jme3.post.FilterPostProcessor;
import com.jme3.renderer.Camera;
import com.jme3.system.AppSettings;
import com.jme3.system.JmeContext.Type;
import com.ss.client.gui.ElementId;
import com.ss.client.gui.InterfaceId;
import com.ss.client.gui.controller.camera.CameraController;
import com.ss.client.gui.model.ScreenSize;
import com.ss.client.jme.post.PostEffect;
import com.ss.client.jme.post.effect.BloomEffect;
import com.ss.client.jme.post.effect.StartLightEffect;
import com.ss.client.manager.ExecutorManager;
import com.ss.client.manager.PacketTaskManager;
import com.ss.client.manager.UpdateGraficEffectManager;
import com.ss.client.manager.UpdateObjectManager;
import com.ss.client.manager.UpdateShotManager;
import com.ss.client.manager.UpdateUIManager;
import com.ss.client.model.Account;
import com.ss.client.model.ServerInfo;
import com.ss.client.model.state.StateId;
import com.ss.client.network.Network;
import com.ss.client.table.BackgroundSoundTable;
import com.ss.client.table.GravityObjectTable;
import com.ss.client.table.ItemTable;
import com.ss.client.table.LangTable;
import com.ss.client.table.LocationObjectTable;
import com.ss.client.table.LocationTable;
import com.ss.client.table.ModuleTable;
import com.ss.client.table.ShipTable;
import com.ss.client.table.SkillTable;
import com.ss.client.table.StationTable;
import com.ss.client.util.LocalObjects;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.screen.Screen;

/**
 * Главный класс игры.
 * 
 * @author Ronn
 */
public final class Game extends SimpleApplication {

	private static final int WAIT_FINISH_UPDATE_UI = 2;
	private static final int WAIT_FINISH_UPDATE_GEOMETRY = 1;
	private static final int RUNNING = 0;

	private static final Logger LOGGER = Loggers.getLogger(Game.class);

	private static final int TEXTURE_SIZE = 2048;

	private static final String OBJECT_NAME_FNT = "object_name.fnt";
	private static final String NIFTY_DEFAULT_CONTROLS_XML = "nifty-default-controls.xml";
	private static final String NIFTY_DEFAULT_STYLES_XML = "nifty-default-styles.xml";

	/** ссылка на игру */
	private static final Game GAME = new Game();

	/** отсуп в получении текущего времени */
	private static long timeOffset = 0;

	/**
	 * @return текущее время в соотвествии с сервером.
	 */
	public static final long getCurrentTime() {
		return System.currentTimeMillis();
	}

	/**
	 * @return ссылка на игру.
	 */
	public static Game getInstance() {
		return GAME;
	}

	public static void main(final String[] args) throws IOException {

		// выключаем стандартный логер
		java.util.logging.Logger.getLogger("").setLevel(Level.SEVERE);

		try {

			ScreenSize.init();
			LangTable.getInstance();
			Config.init();

			if(args.length > 0) {
				Config.LOGIN_HOST = new InetSocketAddress(args[0], Config.LOGIN_SERVER_PORT);
			}

			if(args.length > 1) {
				Config.LOCAL_MODE = "local".equals(args[1]);
			}

			LOGGER.info("address login server " + Config.LOGIN_HOST);
			LOGGER.info("is local mode " + Config.LOCAL_MODE);

			final AppSettings settings = GameConfig.loadSettings();

			ElementId.init();

			GAME.setSettings(settings);
			GAME.setShowSettings(false);
			GAME.setDisplayStatView(false);
			GAME.setDisplayFps(true);
			GAME.start();

		} catch(final Exception e) {
			LOGGER.warning(e);
		}
	}

	public static void setTimeOffset(long timeOffset) {
		Game.timeOffset = timeOffset;
	}

	/** список пост эффектов в игре */
	private final Array<PostEffect> postEffects;

	/** синхронизатор */
	private final Lock lock;
	/** аккаунт пользователя */
	private final Account account;

	/** счетчик ожидающих потоков для обновлениния геометрии */
	private final AtomicInteger waitUpdateGeometrySync;
	/** счетчик исполняющих потоков обновления геометрии */
	private final AtomicInteger executeUpdateGeometrySync;
	/** счетчик ожидающих потоков дял обновления UI */
	private final AtomicInteger waitUpdateUISync;
	/** счетчик исполняющих потоков обновления UI */
	private final AtomicInteger executeUpdateUISync;
	/** синхронизатор ожидания */
	private final AtomicInteger waitState;

	/** текущая стадия игры */
	private StateId currentState;

	/** ссылка на интерфейс */
	private Nifty nifty;

	/** информацию о текущем гейм сервере */
	private ServerInfo gameServer;

	public Game() {
		this.postEffects = Arrays.toArray(PostEffect.class);
		this.account = new Account();
		this.lock = Locks.newLock();
		this.waitUpdateGeometrySync = new AtomicInteger();
		this.waitUpdateUISync = new AtomicInteger();
		this.executeUpdateGeometrySync = new AtomicInteger();
		this.executeUpdateUISync = new AtomicInteger();
		this.waitState = new AtomicInteger();
	}

	@Override
	public void destroy() {
		super.destroy();
		System.exit(0);
	}

	/**
	 * @return аккаунт пользователя.
	 */
	public final Account getAccount() {
		return account;
	}

	@Override
	public Camera getCamera() {
		// TODO Auto-generated method stub
		return super.getCamera();
	}

	/**
	 * @return текущее состояние игры.
	 */
	public final StateId getCurrentState() {
		return currentState;
	}

	/**
	 * @return счетчик исполняющих потоков обновления геометрии.
	 */
	public AtomicInteger getExecuteUpdateGeometrySync() {
		return executeUpdateGeometrySync;
	}

	/**
	 * @return счетчик исполняющих потоков обновления UI.
	 */
	public AtomicInteger getExecuteUpdateUISync() {
		return executeUpdateUISync;
	}

	/**
	 * @return текущий гейм сервер.
	 */
	public final ServerInfo getGameServer() {
		return gameServer;
	}

	/**
	 * @return шрифт для ГУИ.
	 */
	public BitmapFont getGuiFont() {
		return guiFont;
	}

	/**
	 * @return ссылка на интерфейс игры.
	 */
	public Nifty getNifty() {
		return nifty;
	}

	/**
	 * @return список пост эффектов в игре.
	 */
	public Array<PostEffect> getPostEffects() {
		return postEffects;
	}

	/**
	 * @return синхронизатор ожидания.
	 */
	public AtomicInteger getWaitState() {
		return waitState;
	}

	/**
	 * @return счетчик ожидающих потоков для обновлениния геометрии.
	 */
	public AtomicInteger getWaitUpdateGeometrySync() {
		return waitUpdateGeometrySync;
	}

	/**
	 * @return счетчик ожидающих потоков дял обновления UI.
	 */
	public AtomicInteger getWaitUpdateUISync() {
		return waitUpdateUISync;
	}

	/**
	 * Переход к указанному интерфейсу.
	 */
	public void gotoScreen(final InterfaceId builder) {

		final Screen screen = builder.newInstance(nifty);

		nifty.addScreen(screen.getScreenId(), screen);
		nifty.gotoScreen(screen.getScreenId());

		System.gc();
	}

	/**
	 * Переходим на указанную стадию.
	 * 
	 * @param state ид стадии игры.
	 */
	public void gotoState(final StateId state) {
		syncLock();
		try {

			final StateId currentState = getCurrentState();

			if(currentState == state) {
				return;
			}

			if(currentState != null) {
				stateManager.detach(currentState.getState());
			}

			stateManager.attach(state.getState());

			setCurrentState(state);

		} finally {
			syncUnlock();
		}
	}

	@Deprecated
	public boolean isHeldByCurrentThread() {
		// ReentrantReadWriteLock reentrantLock = lock;
		// return reentrantLock.isWriteLockedByCurrentThread();
		return true;
	}

	/**
	 * @param currentState текущее состояние игры.
	 */
	public final void setCurrentState(final StateId currentState) {
		this.currentState = currentState;
	}

	/**
	 * @param gameServer текущий гейм сервер.
	 */
	public final void setGameServer(final ServerInfo gameServer) {
		this.gameServer = gameServer;
	}

	@Override
	public void simpleInitApp() {

		guiFont = assetManager.loadFont(OBJECT_NAME_FNT);
		guiNode.detachAllChildren();

		// устанавливаем нужный нам обзор в 55 градусов
		cam.setFrustumPerspective(65f, (float) cam.getWidth() / cam.getHeight(), 1f, GameConfig.VIEW_DISTANCE);

		flyCam.setDragToRotate(true);
		flyCam.setEnabled(false);

		final NiftyJmeDisplay niftyDisplay = new NiftyJmeDisplay(assetManager, inputManager, audioRenderer, guiViewPort);
		guiViewPort.addProcessor(niftyDisplay);

		FilterPostProcessor postProcessor = new FilterPostProcessor(assetManager);
		postProcessor.initialize(renderManager, viewPort);
		viewPort.addProcessor(postProcessor);

		Array<PostEffect> postEffects = getPostEffects();
		postEffects.add(BloomEffect.bind(postProcessor));
		postEffects.add(StartLightEffect.bind(postProcessor));
		// postEffects.add(MotionBlurEffect.bind(postProcessor));
		// postEffects.add(DepthOfFieldEffect.bind(postProcessor));
		postEffects.trimToSize();

		nifty = niftyDisplay.getNifty();
		nifty.loadStyleFile(NIFTY_DEFAULT_STYLES_XML);
		nifty.loadControlFile(NIFTY_DEFAULT_CONTROLS_XML);
		nifty.loadControlFile("control/space-shift-controls.xml");
		// nifty.setDebugOptionPanelColors(true);

		// Create a new cam for the gui
		Camera uiCam = new Camera(settings.getWidth(), settings.getHeight());
		uiCam.setFrustumPerspective(45f, (float) cam.getWidth() / cam.getHeight(), 1f, 1000f);
		uiCam.setLocation(new Vector3f(0, 0, -10));
		uiCam.setRotation(Quaternion.DIRECTION_Z);

		// uiNode.setQueueBucket(Bucket.Gui);
		// uiNode.setCullHint(CullHint.Never);

		ExecutorManager.getInstance();

		InitializeManager.register(PacketTaskManager.class);
		InitializeManager.register(BackgroundSoundTable.class);
		InitializeManager.register(UpdateShotManager.class);
		InitializeManager.register(UpdateUIManager.class);
		InitializeManager.register(UpdateObjectManager.class);
		InitializeManager.register(UpdateGraficEffectManager.class);
		InitializeManager.register(ShipTable.class);
		InitializeManager.register(ModuleTable.class);
		InitializeManager.register(ItemTable.class);
		InitializeManager.register(GravityObjectTable.class);
		InitializeManager.register(LocationObjectTable.class);
		InitializeManager.register(LocationTable.class);
		InitializeManager.register(StationTable.class);
		InitializeManager.register(SkillTable.class);
		InitializeManager.register(Network.class);

		InitializeManager.initialize();

		assetManager.addAssetEventListener(GameConfig.getInstance());
		// inputManager.deleteMapping(SimpleApplication.INPUT_MAPPING_EXIT);

		gotoState(StateId.LOGIN_STATE);
	}

	@Override
	public void start(final Type contextType) {
		settings.setRenderer("CUSTOM" + GameContext.class.getName());
		super.start(contextType);
	}

	public final void syncLock() {
		lock.lock();
	}

	public final void syncUnlock() {
		lock.unlock();
	}

	@Override
	public void update() {

		AtomicInteger waitUpdateGeometrySync = getWaitUpdateGeometrySync();
		AtomicInteger executeUpdateGeometrySync = getExecuteUpdateGeometrySync();
		AtomicInteger waitUpdateUISync = getWaitUpdateUISync();
		AtomicInteger executeUpdateUISync = getExecuteUpdateUISync();
		AtomicInteger waitState = getWaitState();

		synchronized(waitState) {

			if(waitUpdateGeometrySync.get() > 0) {

				synchronized(waitUpdateGeometrySync) {
					executeUpdateGeometrySync.getAndSet(waitUpdateGeometrySync.getAndSet(0));
					ConcurrentUtils.notifyAllInSynchronize(waitUpdateGeometrySync);
				}

				ConcurrentUtils.waitInSynchronize(waitState);
			}

			CameraController controller = CameraController.getInstance();
			controller.update(LocalObjects.get(), System.currentTimeMillis());

			if(waitUpdateUISync.get() > 0) {

				synchronized(waitUpdateUISync) {
					executeUpdateUISync.getAndSet(waitUpdateUISync.getAndSet(0));
					ConcurrentUtils.notifyAllInSynchronize(waitUpdateUISync);
				}

				ConcurrentUtils.waitInSynchronize(waitState);
			}
		}

		syncLock();
		try {
			// System.err.println("start render");
			super.update();
			// System.err.println("finish render");
		} catch(NullPointerException npe) {
			LOGGER.warning(npe);
			System.exit(1);
		} catch(IllegalStateException e) {
			LOGGER.warning(e);
		} finally {
			syncUnlock();
		}
	}

	/**
	 * Уведомлпение о завершении обновлении геометрии.
	 */
	public void updateGeomEnd() {

		AtomicInteger executeUpdateGeometrySync = getExecuteUpdateGeometrySync();

		if(executeUpdateGeometrySync.decrementAndGet() == 0) {
			ConcurrentUtils.notifyAll(getWaitState());
		}
	}

	/**
	 * Ожидание возможности начать обновлять геометрию.
	 */
	public void updateGeomStart() {

		AtomicInteger waitUpdateGeometrySync = getWaitUpdateGeometrySync();

		synchronized(waitUpdateGeometrySync) {
			waitUpdateGeometrySync.incrementAndGet();
			ConcurrentUtils.waitInSynchronize(waitUpdateGeometrySync);
		}
	}

	/**
	 * Уведомлпение о завершении обновлении UI.
	 */
	public void updateUIEnd() {

		AtomicInteger executeUpdateUISync = getExecuteUpdateUISync();

		if(executeUpdateUISync.decrementAndGet() == 0) {
			ConcurrentUtils.notifyAll(getWaitState());
		}
	}

	/**
	 * Ожидание возможности начать обновлять UI.
	 */
	public void updateUIStart() {

		AtomicInteger waitUpdateUISync = getWaitUpdateUISync();

		synchronized(waitUpdateUISync) {
			waitUpdateUISync.incrementAndGet();
			ConcurrentUtils.waitInSynchronize(waitUpdateUISync);
		}
	}
}
