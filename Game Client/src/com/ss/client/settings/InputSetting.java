package com.ss.client.settings;

import com.jme3.input.KeyInput;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.input.controls.Trigger;

/**
 * Перечисление прослушиваемого ввода.
 * 
 * @author Ronn
 */
public enum InputSetting {

	SWITCH_SELECTOR(new KeyTrigger(KeyInput.KEY_TAB)),
	SELECTOR_ACTIVATE(new KeyTrigger(KeyInput.KEY_E)),
	MOVE_CAMERA(new KeyTrigger(KeyInput.KEY_V)), ;

	/** список триггеров */
	private Trigger[] triggers;

	/**
	 * @param triggers список триггеров.
	 */
	private InputSetting(Trigger... triggers) {
		this.triggers = triggers;
	}

	/**
	 * @return список триггеров.
	 */
	public Trigger[] getTriggers() {
		return triggers;
	}

	/**
	 * @param triggers список триггеров.
	 */
	public void setTriggers(Trigger[] triggers) {
		this.triggers = triggers;
	}
}
