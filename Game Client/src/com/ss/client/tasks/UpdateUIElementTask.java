package com.ss.client.tasks;

import rlib.concurrent.ConcurrentUtils;
import rlib.logging.Logger;
import rlib.logging.Loggers;
import rlib.util.array.Array;
import rlib.util.array.Arrays;
import rlib.util.array.FuncElement;

import com.ss.client.Game;
import com.ss.client.GameThread;
import com.ss.client.gui.util.UpdateUIElement;
import com.ss.client.util.LocalObjects;

/**
 * Реализация задачи по асинхронному обновлению UI элементов в контейнерах.
 * 
 * @author Ronn
 */
public class UpdateUIElementTask extends GameThread {

	private static final Logger LOGGER = Loggers.getLogger(UpdateUIElementTask.class);
	private static final Game GAME = Game.getInstance();

	private static final FuncElement<UpdateUIElement> FINISH_ELEMENT_FUNC = new FuncElement<UpdateUIElement>() {

		@Override
		public void apply(UpdateUIElement element) {
			element.finish();
		}
	};

	/** контейнер обновляемых выстрелов */
	private final Array<UpdateUIElement> container;

	/** обслуживает ли задача комплексный элемент */
	private final boolean complex;

	public UpdateUIElementTask(Array<UpdateUIElement> container, int order, boolean complex) {
		this.container = container;
		this.complex = complex;
		setDaemon(true);
		setName(getClass().getSimpleName() + (order < 0 ? "" : "-" + order));
	}

	/**
	 * @return контейнер обновляемых элементов.
	 */
	public Array<UpdateUIElement> getContainer() {
		return container;
	}

	/**
	 * @return обслуживает ли задача комплексный элемент.
	 */
	public boolean isComplex() {
		return complex;
	}

	@Override
	public void run() {

		Array<UpdateUIElement> update = Arrays.toArray(UpdateUIElement.class);
		Array<UpdateUIElement> finished = Arrays.toArray(UpdateUIElement.class);
		Array<UpdateUIElement> container = getContainer();

		LocalObjects local = LocalObjects.get();
		boolean complex = isComplex();

		while(true) {

			update.clear();
			finished.clear();

			try {

				synchronized(container) {

					if(container.isEmpty()) {
						ConcurrentUtils.waitInSynchronize(container);
					}

					update.addAll(container);
				}

				GAME.updateUIStart();
				try {

					long currentTime = System.currentTimeMillis();

					for(UpdateUIElement element : update.array()) {

						if(element == null) {
							break;
						}

						if(element.update(local, currentTime)) {
							finished.add(element);
						}
					}

				} finally {
					GAME.updateUIEnd();
				}

				if(!finished.isEmpty()) {

					synchronized(container) {
						container.removeAll(finished);
					}

					finished.apply(FINISH_ELEMENT_FUNC);

					if(complex) {
						return;
					}
				}

			} catch(Exception e) {
				LOGGER.warning(e);
			}
		}
	}
}
