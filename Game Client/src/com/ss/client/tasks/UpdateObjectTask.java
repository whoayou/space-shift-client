package com.ss.client.tasks;

import rlib.concurrent.ConcurrentUtils;
import rlib.logging.Logger;
import rlib.logging.Loggers;
import rlib.util.array.Array;
import rlib.util.array.Arrays;
import rlib.util.array.FuncElement;

import com.ss.client.Game;
import com.ss.client.GameThread;
import com.ss.client.model.util.UpdateableObject;
import com.ss.client.util.LocalObjects;

/**
 * Реализация задачи по обновлению объектов в контейнерах.
 * 
 * @author Ronn
 */
public class UpdateObjectTask extends GameThread {

	private static final Logger LOGGER = Loggers.getLogger(UpdateObjectTask.class);
	private static final Game GAME = Game.getInstance();

	private static final FuncElement<UpdateableObject> FINISH_OBJECT_FUNC = new FuncElement<UpdateableObject>() {

		@Override
		public void apply(UpdateableObject object) {
			object.finish();
		}
	};

	/** контейнер обновляемых объектов */
	private final Array<UpdateableObject> container;

	public UpdateObjectTask(Array<UpdateableObject> container, int order) {
		this.container = container;
		setDaemon(true);
		setName(getClass().getSimpleName() + "-" + order);
		start();
	}

	/**
	 * @return контейнер обновляемых объектов.
	 */
	public Array<UpdateableObject> getContainer() {
		return container;
	}

	@Override
	public void run() {

		Array<UpdateableObject> update = Arrays.toArray(UpdateableObject.class);
		Array<UpdateableObject> finished = Arrays.toArray(UpdateableObject.class);
		Array<UpdateableObject> container = getContainer();

		LocalObjects local = LocalObjects.get();

		while(true) {

			update.clear();
			finished.clear();

			try {

				synchronized(container) {

					if(container.isEmpty()) {
						ConcurrentUtils.waitInSynchronize(container);
					}

					update.addAll(container);
				}

				GAME.updateGeomStart();
				try {

					long currentTime = System.currentTimeMillis();

					for(UpdateableObject object : update.array()) {

						if(object == null) {
							break;
						}

						if(object.update(local, currentTime)) {
							finished.add(object);
						}
					}

				} finally {
					GAME.updateGeomEnd();
				}

				if(!finished.isEmpty()) {

					synchronized(container) {
						container.removeAll(finished);
					}

					finished.apply(FINISH_OBJECT_FUNC);
				}

			} catch(Exception e) {
				LOGGER.warning(e);
			}
		}
	}
}
