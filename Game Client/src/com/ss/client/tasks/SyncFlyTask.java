package com.ss.client.tasks;

import java.util.concurrent.atomic.AtomicBoolean;

import com.jme3.math.Vector3f;
import com.ss.client.model.ship.SpaceShip;
import com.ss.client.model.ship.SpaceShipView;
import com.ss.client.util.LocalObjects;

/**
 * Реализация задачи по синхронизации клиентского полета с серверным.
 * 
 * @author Ronn
 */
public class SyncFlyTask {

	/** летающий корабль */
	private final SpaceShip ship;
	/** вектор разницы между серверным и клиентским */
	private final Vector3f diff;
	/** флаг активированности */
	private final AtomicBoolean enabled;

	/** время последнего обновления */
	private long lastUpdate;

	public SyncFlyTask(SpaceShip ship) {
		this.ship = ship;
		this.diff = new Vector3f();
		this.enabled = new AtomicBoolean();
	}

	/**
	 * @return вектор разницы между серверным и клиентским.
	 */
	public Vector3f getDiff() {
		return diff;
	}

	/**
	 * @return время последнего обновления.
	 */
	public long getLastUpdate() {
		return lastUpdate;
	}

	/**
	 * @return летающий корабль.
	 */
	public SpaceShip getShip() {
		return ship;
	}

	/**
	 * @return флаг активированности.
	 */
	public boolean isEnabled() {
		return enabled.get();
	}

	public void reinit(long currentTime) {
		synchronized(this) {
			setEnabled(true);
			setLastUpdate(currentTime);
		}
	}

	/**
	 * @param enabled флаг активированности.
	 */
	public void setEnabled(boolean enabled) {
		this.enabled.set(enabled);
	}

	/**
	 * @param lastUpdate время последнего обновления.
	 */
	public void setLastUpdate(long lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	public void stop() {
		synchronized(this) {
			setEnabled(false);
		}
	}

	public void update(LocalObjects local, long currentTime) {

		if(!isEnabled()) {
			return;
		}

		SpaceShip ship = getShip();
		SpaceShipView view = ship.getView();

		try {

			if(view == null) {
				return;
			}

			synchronized(this) {

				if(!isEnabled()) {
					return;
				}

				Vector3f server = ship.getLocation();
				Vector3f client = view.getLocation();

				float distance = server.distance(client);

				if(distance < 1) {
					return;
				}

				float percent = (currentTime - getLastUpdate()) / 1000F;

				Vector3f diff = server.subtract(client, getDiff());
				diff.multLocal(percent);
				diff.addLocal(client);

				view.setLocation(diff);
			}

		} finally {
			setLastUpdate(currentTime);
		}
	}
}
