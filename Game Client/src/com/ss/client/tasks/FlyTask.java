package com.ss.client.tasks;

import java.util.concurrent.atomic.AtomicBoolean;

import com.jme3.math.Vector3f;
import com.ss.client.Config;
import com.ss.client.Game;
import com.ss.client.model.ship.SpaceShip;
import com.ss.client.model.ship.SpaceShipView;
import com.ss.client.util.LocalObjects;

/**
 * Модель полета корабля.
 * 
 * @author Ronn
 */
public class FlyTask {

	private static final int DEFAULT_ACCEL = -20;

	/** флаг активности */
	private final AtomicBoolean running;
	/** летающий корабль */
	private final SpaceShip spaceShip;

	/** текущая позиция */
	private final Vector3f location;
	/** вектор полета */
	private final Vector3f flyVector;
	/** вектор изменения */
	private final Vector3f changed;

	/** ускорение */
	private float accel;
	/** максимальная скорость */
	private float maxSpeed;
	/** максимальная текущая скорость */
	private float maxCurrentSpeed;

	/** последняя скорость модели */
	private float lastSpeed;
	/** последняя скорость вьюхи */
	private float lastViewSpeed;

	/** последнее обновление модели корабля */
	private long lastUpdate;
	/** последнее обновление вьюхи корабля */
	private long lastViewUpdate;

	/** активны ли двигатели */
	private boolean active;
	/** нужно ли синхронизировать позицию модели и вьюхи */
	private boolean needSyncLocation;

	public FlyTask(final SpaceShip spaceShip) {
		this.spaceShip = spaceShip;
		this.flyVector = new Vector3f();
		this.changed = new Vector3f();
		this.location = new Vector3f();
		this.running = new AtomicBoolean();
	}

	/**
	 * @return accel текущее ускорение.
	 */
	public final float getAccel() {
		return accel;
	}

	/**
	 * @return вектор изменения.
	 */
	public Vector3f getChanged() {
		return changed;
	}

	/**
	 * @return вектор полета.
	 */
	public Vector3f getFlyVector() {
		return flyVector;
	}

	/**
	 * @return последняя скорость модели.
	 */
	public final float getLastSpeed() {
		return lastSpeed;
	}

	/**
	 * @return последнее обновление модели корабля.
	 */
	public final long getLastUpdate() {
		return lastUpdate;
	}

	/**
	 * @return последняя скорость вьюхи.
	 */
	public float getLastViewSpeed() {
		return lastViewSpeed;
	}

	/**
	 * @return последнее обновление вьюхи корабля.
	 */
	public long getLastViewUpdate() {
		return lastViewUpdate;
	}

	/**
	 * @return текущая позиция.
	 */
	public Vector3f getLocation() {
		return location;
	}

	/**
	 * @return максимальная текущая скорость.
	 */
	public float getMaxCurrentSpeed() {
		return maxCurrentSpeed;
	}

	/**
	 * @return maxSpeed максимальная скорость.
	 */
	public final float getMaxSpeed() {
		return maxSpeed;
	}

	/**
	 * @return летающий корабль.
	 */
	public SpaceShip getSpaceShip() {
		return spaceShip;
	}

	/**
	 * @return активны ли двигатели.
	 */
	public boolean isActive() {
		return active;
	}

	/**
	 * @return нужно ли синхронизировать позицию модели и вьюхи.
	 */
	public boolean isNeedSyncLocation() {
		return needSyncLocation;
	}

	/**
	 * @return запущена ли задача полета.
	 */
	public boolean isRunning() {
		return running.get();
	}

	/**
	 * Запуск.
	 */
	public void reinit(long currentTime) {

		SpaceShip ship = getSpaceShip();

		setRunning(true);
		setLastUpdate(currentTime);
		setLastViewUpdate(currentTime);
		setLocation(ship.getLocation());
	}

	/**
	 * @param accel текущее ускорение.
	 */
	public final void setAccel(final float accel) {
		this.accel = accel;
	}

	/**
	 * @param active активны ли двигатели.
	 */
	public void setActive(boolean active) {
		this.active = active;
	}

	/**
	 * @param lastSpeed последняя скорость модели.
	 */
	public final void setLastSpeed(final float lastSpeed) {
		this.lastSpeed = lastSpeed;
	}

	/**
	 * @param lastUpdate последнее обновление модели корабля.
	 */
	public final void setLastUpdate(final long lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	/**
	 * @param lastViewSpeed последняя скорость вьюхи.
	 */
	public void setLastViewSpeed(float lastViewSpeed) {
		this.lastViewSpeed = lastViewSpeed;
	}

	/**
	 * @param lastViewUpdate последнее обновление вьюхи корабля.
	 */
	public void setLastViewUpdate(long lastViewUpdate) {
		this.lastViewUpdate = lastViewUpdate;
	}

	/**
	 * @param location текущая позиция.
	 */
	public void setLocation(Vector3f location) {
		this.location.set(location);
	}

	/**
	 * @param maxCurrentSpeed максимальная текущая скорость.
	 */
	public void setMaxCurrentSpeed(float maxCurrentSpeed) {
		this.maxCurrentSpeed = maxCurrentSpeed;
	}

	/**
	 * @param maxSpeed максимальная скорость.
	 */
	public final void setMaxSpeed(final float maxSpeed) {
		this.maxSpeed = maxSpeed;
	}

	/**
	 * @param needSyncLocation нужно ли синхронизировать позицию модели и вьюхи.
	 */
	public void setNeedSyncLocation(boolean needSyncLocation) {
		this.needSyncLocation = needSyncLocation;
	}

	/**
	 * @param running запущена ли задача полета.
	 */
	public void setRunning(boolean running) {
		this.running.set(running);
	}

	/**
	 * Остановка.
	 */
	public void stop() {
		synchronized(this) {
			setRunning(false);
			setAccel(0);
			setLastSpeed(0);
			setMaxCurrentSpeed(0);
			setMaxSpeed(0);
		}
	}

	@Override
	public String toString() {
		return "FlyTask accel = " + accel + ",  maxSpeed = " + maxSpeed + ",  maxCurrentSpeed = " + maxCurrentSpeed + ",  active = " + active;
	}

	public boolean update(LocalObjects local, long currentTime) {

		final SpaceShip ship = getSpaceShip();
		final SpaceShipView view = ship.getView();

		synchronized(this) {

			if(!isRunning()) {
				return false;
			}

			float lastViewSpeed = getLastViewSpeed();
			float lastSpeed = getLastSpeed();
			float accel = getAccel();

			try {

				if(accel < 1 && lastSpeed < 1) {
					return false;
				}

				final Vector3f flyVector = getFlyVector();

				// обновление положения вьюхи
				{
					final long diff = currentTime - getLastViewUpdate();

					if(diff < 1) {
						return false;
					}

					float maxCurrentSpeed = getMaxCurrentSpeed();

					if(maxCurrentSpeed < lastViewSpeed) {
						accel = DEFAULT_ACCEL;
					}

					float oldSpeed = lastViewSpeed;
					float newSpeed = Math.max(Math.min(lastViewSpeed + diff / 1000F * accel, getMaxSpeed()), 0);

					if(oldSpeed <= maxCurrentSpeed && newSpeed > maxCurrentSpeed) {
						lastViewSpeed = maxCurrentSpeed;
					} else {
						lastViewSpeed = newSpeed;
					}

					ship.setFlyCurrentSpeed(lastViewSpeed);

					float dist = diff * lastViewSpeed / 1000F;

					flyVector.set(ship.getDirection());
					flyVector.multLocal(dist);
					flyVector.addLocal(view.getLocation());

					view.setLocation(flyVector);
				}

				// обновление положения модели
				if(currentTime - getLastUpdate() > Config.WORLD_UPDATE_OBJECT_INTERVAL) {

					final long diff = currentTime - getLastUpdate();

					if(diff < 1) {
						return false;
					}

					float maxCurrentSpeed = getMaxCurrentSpeed();

					if(maxCurrentSpeed < lastSpeed) {
						accel = DEFAULT_ACCEL;
					}

					float oldSpeed = lastSpeed;
					float newSpeed = Math.max(Math.min(lastSpeed + diff / 1000F * accel, getMaxSpeed()), 0);

					if(oldSpeed <= maxCurrentSpeed && newSpeed > maxCurrentSpeed) {
						lastSpeed = maxCurrentSpeed;
					} else {
						lastSpeed = newSpeed;
					}

					float dist = diff * lastSpeed / 1000F;

					final Vector3f location = getLocation();

					flyVector.set(ship.getDirection());
					flyVector.multLocal(dist);
					flyVector.addLocal(location);

					final Vector3f changed = getChanged();
					changed.set(flyVector).subtractLocal(ship.getLocation());

					ship.setLocation(flyVector, changed);

					setLocation(flyVector);
					setLastUpdate(currentTime);
				}

				return false;

			} finally {
				setLastSpeed(lastSpeed);
				setLastViewSpeed(lastViewSpeed);
				setLastViewUpdate(currentTime);
			}
		}
	}

	/**
	 * Обновление работы таска.
	 * 
	 * @param location текущая позиция на сервере.
	 * @param accel ускорение текущее на сервере.
	 * @param speed скорость на сервере.
	 * @param maxSpeed максимальная скоростьна сервере.
	 * @param maxCurrentSpeed максимальная текущая скорость.
	 * @param active активны ли двигатели.
	 * @param force принудительная ли синхронизация.
	 */

	public void update(final Vector3f location, final float accel, final float speed, final float maxSpeed, float maxCurrentSpeed, boolean active, boolean force) {

		final SpaceShip ship = getSpaceShip();

		synchronized(this) {

			ship.setFlyAccel(accel);
			ship.setFlyCurrentSpeed(speed);
			ship.setFlyMaxSpeed(maxCurrentSpeed);

			long currentTime = Game.getCurrentTime();

			setAccel(accel);
			setActive(active);
			setLastSpeed(speed);
			setLocation(location);
			setMaxSpeed(maxSpeed);
			setLastUpdate(currentTime);
			setLastViewUpdate(currentTime);
			setMaxCurrentSpeed(maxCurrentSpeed);
		}
	}
}
