package com.ss.client.model.state;


import com.jme3.app.Application;
import com.jme3.app.state.AppStateManager;
import com.ss.client.Game;
import com.ss.client.gui.InterfaceId;
import com.ss.client.gui.model.BackgroundSound;
import com.ss.client.model.Account;
import com.ss.client.network.Network;
import com.ss.client.network.model.NetServer;
import com.ss.client.table.BackgroundSoundTable;

/**
 * Стадия авторизации пользователя на логин сервере.
 * 
 * @author Ronn
 */
public class LoginState extends AbstractState {

	private static final Game GAME = Game.getInstance();
	private static final Network NETWORK = Network.getInstance();

	/** фоновая музыка в меню авторизации */
	private BackgroundSound backgroundSound;

	@Override
	public void cleanup() {
		try {

			super.cleanup();

			final BackgroundSound backgroundSound = getBackgroundSound();

			if(backgroundSound != null) {
				backgroundSound.stop();
				setBackgroundSound(null);
			}

		} catch(final Exception e) {
			LOGGER.warning(e);
		}
	}

	/**
	 * @return фоновый звук.
	 */
	public BackgroundSound getBackgroundSound() {
		return backgroundSound;
	}

	@Override
	public void initialize(final AppStateManager stateManager, final Application app) {
		try {

			super.initialize(stateManager, app);

			final NetServer gameServer = NETWORK.getGameServer();

			if(gameServer != null) {
				gameServer.close();
			}

			final Account account = GAME.getAccount();
			account.clear();

			final BackgroundSoundTable soundTable = BackgroundSoundTable.getInstance();
			final BackgroundSound backgroundSound = soundTable.getNextBackground();

			if(backgroundSound != null) {
				backgroundSound.play();
				setBackgroundSound(backgroundSound);
			}

			GAME.gotoScreen(InterfaceId.LOGIN_MENU);

		} catch(final Exception e) {
			LOGGER.warning(e);
		}
	}

	/**
	 * @param backgroundSound фоновый звук.
	 */
	public void setBackgroundSound(final BackgroundSound backgroundSound) {
		this.backgroundSound = backgroundSound;
	}
}
