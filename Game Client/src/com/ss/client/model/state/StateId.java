package com.ss.client.model.state;

import rlib.logging.Loggers;

import com.jme3.app.state.AppState;

/**
 * Набор стадий игры.
 * 
 * @author Ronn
 */
public enum StateId {
	GAME_STATE(GameState.class),
	HANGAR_STATE(HangarState.class),
	LOGIN_STATE(LoginState.class);

	/** экземпляр стадии игры */
	private AppState state;

	/** тип стадии */
	private Class<? extends AppState> type;

	private StateId(final Class<? extends AppState> type) {
		this.type = type;
	}

	/**
	 * @return сталия игры.
	 */
	@SuppressWarnings("unchecked")
	public final <T extends AppState> T getState() {
		if(state == null)
			try {
				state = type.newInstance();
			} catch(InstantiationException | IllegalAccessException e) {
				Loggers.warning(this, e);
			}

		return (T) state;
	}
}
