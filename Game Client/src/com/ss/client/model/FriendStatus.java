package com.ss.client.model;

/**
 * Перечисление статуса дружелюбности объекта в игроку.
 * 
 * @author Ronn
 */
public enum FriendStatus {
	PARTY,
	CLAN,
	ALIANCE,
	NEUTRAL,
	AGGRESSOR;

	private static final FriendStatus[] VALUES = values();

	public static final FriendStatus valueOf(int status) {
		return VALUES[status];
	}
}
