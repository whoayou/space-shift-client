package com.ss.client.model.module.view;

import com.jme3.asset.AssetManager;
import com.jme3.scene.Spatial;
import com.ss.client.model.effect.GraficEffect;
import com.ss.client.model.module.classes.BlasterModule;
import com.ss.client.model.module.info.RocketShotInfo;
import com.ss.client.template.ModuleTemplate;
import com.ss.client.template.grafic.effect.GraficEffectTemplate;

/**
 * Модель внешности бластерного модуля.
 * 
 * @author Ronn
 */
public class RocketModuleView extends AbstractModuleView<BlasterModule> {

	/** эффект взрыва ракеты */
	private GraficEffectTemplate explosion;

	/** модель ракеты */
	private Spatial rocket;

	public RocketModuleView(final Spatial spatial, final ModuleTemplate template) {
		super(spatial, template);

		final AssetManager assetManager = GAME.getAssetManager();

		RocketShotInfo rocketInfo = template.getRocketShotInfo();

		if(rocketInfo == null) {
			LOGGER.warning("not found rocket info for " + template);
			return;
		}

		setRocket(rocketInfo.createModel(assetManager));
		setExplosion(rocketInfo.getExplosion());
	}

	/**
	 * Создание эффекты взрыва от ракеты.
	 * 
	 * @return модель эффекта.
	 */
	public GraficEffect createExplosion() {
		return explosion.takeInstance();
	}

	/**
	 * Модель для выстрела ракетой.
	 * 
	 * @return модель бластера.
	 */
	public Spatial getModel() {
		return rocket.clone();
	}

	/**
	 * @param explosion эффект взрыва ракеты.
	 */
	private void setExplosion(GraficEffectTemplate explosion) {
		this.explosion = explosion;
	}

	/**
	 * @param rocket модель ракеты.
	 */
	private void setRocket(Spatial rocket) {
		this.rocket = rocket;
	}
}
