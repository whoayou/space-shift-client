package com.ss.client.model.module;

import com.ss.client.model.SpaceObject;
import com.ss.client.model.SpaceObjectType;
import com.ss.client.model.SpaceObjectView;
import com.ss.client.model.module.classes.BlasterModule;
import com.ss.client.model.module.classes.DefaultModule;
import com.ss.client.model.module.classes.EngineModule;
import com.ss.client.model.module.classes.ForceShieldModule;
import com.ss.client.model.module.classes.RocketModule;
import com.ss.client.model.module.view.BlasterModuleView;
import com.ss.client.model.module.view.DefaultModuleView;
import com.ss.client.model.module.view.EngineModuleView;
import com.ss.client.model.module.view.ForceShieldModuleView;
import com.ss.client.model.module.view.RocketModuleView;

/**
 * Перечисление типов модулей.
 * 
 * @author Ronn
 */
public enum ModuleType implements SpaceObjectType {
	ROCKET("@interface:moduleRocket@", RocketModule.class, RocketModuleView.class),
	FORCE_SHIELD("@interface:moduleForceShield@", ForceShieldModule.class, ForceShieldModuleView.class),
	ENERGY_GENERATOR("@interface:moduleTypeEnergyGenerator@", DefaultModule.class, DefaultModuleView.class),
	BLASTER("@interface:moduleTypeWeapon@", BlasterModule.class, BlasterModuleView.class),
	ENGINE("@interface:moduleTypeEngine@", EngineModule.class, EngineModuleView.class);

	private static final ModuleType[] VALUES = values();

	public static ModuleType[] getValues() {
		return VALUES;
	}

	/**
	 * Определение типа модуля по его индексу.
	 * 
	 * @param index индекс типа модуля.
	 * @return тип модуля.
	 */
	public static ModuleType valueOf(final int index) {
		return VALUES[index];
	}

	/** название типа слота */
	private String typeName;

	/** клас объекта */
	private Class<? extends Module> instanceClass;
	/** класс внешнего вдиа */
	private Class<? extends ModuleView> appearanceClass;

	private ModuleType(final String typeName, final Class<? extends Module> instanceClass, final Class<? extends ModuleView> appearanceClass) {
		this.typeName = typeName;
		this.instanceClass = instanceClass;
		this.appearanceClass = appearanceClass;
	}

	@Override
	public Class<? extends SpaceObject> getInstanceClass() {
		return instanceClass;
	}

	/**
	 * @return название типа модуля.
	 */
	public String getTypeName() {
		return typeName;
	}

	@Override
	public Class<? extends SpaceObjectView> getViewClass() {
		return appearanceClass;
	}
}
