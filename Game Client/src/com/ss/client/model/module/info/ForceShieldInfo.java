package com.ss.client.model.module.info;

import rlib.util.VarTable;

import com.jme3.math.ColorRGBA;

/**
 * Информация о силовом щите.
 * 
 * @author Ronn
 */
public class ForceShieldInfo {

	public static final String EFFECT_COLOR = "effectColor";
	public static final String TEXTURE_KEY = "textureKey";
	public static final String MODEL_KEY = "modelKey";
	public static final String EFFECT_SIZE = "effectSize";

	/** цвет эффекта щита */
	private final ColorRGBA effectColor;

	/** ключ для модели щита */
	private final String modelKey;
	/** ключ текстуры щита */
	private final String textureKey;

	/** размер эффекта попадания в щит */
	private final float effectSize;

	public ForceShieldInfo(VarTable vars) {
		this.effectSize = vars.getFloat(EFFECT_SIZE, 1F);
		this.modelKey = vars.getString(MODEL_KEY);
		this.textureKey = vars.getString(TEXTURE_KEY);
		this.effectColor = vars.getGeneric(EFFECT_COLOR, ColorRGBA.class);
	}

	/**
	 * @return цвет эффекта щита.
	 */
	public ColorRGBA getEffectColor() {
		return effectColor;
	}

	/**
	 * @return размер эффекта попадания в щит.
	 */
	public float getEffectSize() {
		return effectSize;
	}

	/**
	 * @return ключ для модели щита.
	 */
	public String getModelKey() {
		return modelKey;
	}

	/**
	 * @return ключ текстуры щита.
	 */
	public String getTextureKey() {
		return textureKey;
	}
}
