package com.ss.client.model.module.info;

import rlib.util.VarTable;

import com.jme3.asset.AssetManager;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.ss.client.template.grafic.effect.GraficEffectTemplate;

/**
 * Модель информации, описывающего ракету.
 * 
 * @author Ronn
 */
public final class RocketShotInfo {

	public static final String ENGINE_INFO = "engineInfo";
	public static final String EXPLOSION = "explosion";
	public static final String SCALE = "scale";
	public static final String MODEL = "model";

	/** путь к модели ракеты */
	private final String model;

	/** маштаб модели ракеты */
	private final Vector3f scale;

	/** шаблон эффекта взрыва от ракеты */
	private final GraficEffectTemplate explosion;

	/** описание двигателя ракеты */
	private final EngineFireInfo engineFireInfo;

	public RocketShotInfo(final VarTable vars) {
		this.model = vars.getString(MODEL);
		this.engineFireInfo = vars.getGeneric(ENGINE_INFO, EngineFireInfo.class);
		this.scale = vars.getGeneric(SCALE, Vector3f.class);
		this.explosion = vars.get(EXPLOSION, GraficEffectTemplate.class, null);
	}

	/**
	 * Создание новой модели ракеты.
	 * 
	 * @return новая модель ракеты.
	 */
	public Spatial createModel(AssetManager assetManager) {

		Node rocket = (Node) assetManager.loadModel(getModel());
		rocket.setLocalScale(getScale());
		rocket.attachChild(getEngineFireInfo().createModel(assetManager));

		return rocket;
	}

	/**
	 * @return описание двигателя ракеты.
	 */
	public EngineFireInfo getEngineFireInfo() {
		return engineFireInfo;
	}

	/**
	 * @return шаблон эффекта взрыва от бластера.
	 */
	public GraficEffectTemplate getExplosion() {
		return explosion;
	}

	/**
	 * @return адресс модели.
	 */
	public String getModel() {
		return model;
	}

	/**
	 * @return маштаб.
	 */
	public Vector3f getScale() {
		return scale;
	}
}