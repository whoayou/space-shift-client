package com.ss.client.model.module.system;


import com.jme3.math.Vector3f;
import com.ss.client.model.module.ModuleType;

/**
 * Информация для слота модуля.
 * 
 * @author Ronn
 */
public class SlotInfo {

	/** тип модуля */
	private final ModuleType type;

	/** позиция сокета */
	private final Vector3f offset;

	public SlotInfo(final ModuleType type, final Vector3f offset) {
		this.type = type;
		this.offset = offset;
	}

	/**
	 * @return позиция модуля.
	 */
	public final Vector3f getOffset() {
		return offset;
	}

	/**
	 * @return тип модуля.
	 */
	public final ModuleType getType() {
		return type;
	}
}
