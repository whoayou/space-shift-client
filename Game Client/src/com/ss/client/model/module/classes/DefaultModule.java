package com.ss.client.model.module.classes;

import com.ss.client.model.module.AbstractModule;
import com.ss.client.model.module.view.DefaultModuleView;
import com.ss.client.template.ModuleTemplate;

/**
 * Стандартная реаизация модуля.
 * 
 * @author Ronn
 */
public class DefaultModule extends AbstractModule<DefaultModuleView> {

	public DefaultModule(long objectId, ModuleTemplate template) {
		super(objectId, template);
	}
}
