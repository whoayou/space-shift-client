package com.ss.client.model.gravity;


import com.jme3.scene.Spatial;
import com.ss.client.template.GravityObjectTemplate;

/**
 * Модель внешнсоти планеты.
 * 
 * @author Ronn
 */
public class PlanetView extends AbstractGravityObjectView<Planet, GravityObjectTemplate> {

	public PlanetView(final Spatial spatial, final GravityObjectTemplate template) {
		super(spatial, template);
	}
}
