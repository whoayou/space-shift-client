package com.ss.client.model.gravity;

import rlib.util.array.Array;
import rlib.util.array.Arrays;

import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.ss.client.model.AbstractSpaceObject;
import com.ss.client.model.SpaceObject;
import com.ss.client.model.ship.player.PlayerShip;
import com.ss.client.model.space.VisibleType;
import com.ss.client.tasks.GravityFlyTask;
import com.ss.client.template.GravityObjectTemplate;
import com.ss.client.util.LocalObjects;

/**
 * Базовая модель гравитационного объекта.
 * 
 * @author Ronn
 */
public abstract class AbstractGravityObject<E extends GravityObjectTemplate, T extends GravityObjectView> extends AbstractSpaceObject<E, T> implements GravityObject {

	/** список объектов в грави поле */
	private final Array<GravityObject> childs;
	/** список внутренних объектов */
	private final Array<SpaceObject> objects;

	/** обработчик полета */
	private final GravityFlyTask gravityFly;
	/** наклок орбиты */
	private final Quaternion orbitalRotation;

	/** объект, вокруг которого вращается этот */
	private volatile GravityObject parent;

	/** дистанция до центра */
	private int distance;
	/** время разворота вокруг оси */
	private int turnTime;
	/** радиус объекта */
	private int radius;

	/** время разворота вокруг центра */
	private long turnAroundTime;

	public AbstractGravityObject(final long objectId, final E template) {
		super(objectId, template);

		this.orbitalRotation = new Quaternion();
		this.gravityFly = new GravityFlyTask(this);
		this.childs = Arrays.toConcurrentArray(GravityObject.class);
		this.objects = Arrays.toConcurrentArray(SpaceObject.class);
	}

	@Override
	public void add(final SpaceObject object) {
		objects.add(object);
	}

	@Override
	public void addChild(final GravityObject object) {
		childs.add(object);
	}

	@Override
	public boolean contains(final SpaceObject object) {
		return objects.contains(object);
	}

	@Override
	public void deleteMe() {
		super.deleteMe();
		gravityFly.stop();
	}

	@Override
	public void fly(final Vector3f changed) {

		final Array<SpaceObject> objects = getObjects();

		if(objects.isEmpty()) {
			return;
		}

		objects.readLock();
		try {

			final SpaceObject[] array = objects.array();

			for(int i = 0, length = objects.size(); i < length; i++) {

				final SpaceObject object = array[i];

				final Vector3f location = object.getLocation();
				location.addLocal(changed);

				object.setLocation(location, changed);
			}

		} finally {
			objects.readUnlock();
		}
	}

	@Override
	public Array<GravityObject> getChilds() {
		return childs;
	}

	@Override
	public int getDistance() {
		return distance;
	}

	@Override
	public Array<SpaceObject> getObjects() {
		return objects;
	}

	@Override
	public Quaternion getOrbitalRotation() {
		return orbitalRotation;
	}

	@Override
	public GravityObject getParent() {
		return parent;
	}

	@Override
	public int getRadius() {
		return radius;
	}

	@Override
	public long getTurnAroundTime() {
		return turnAroundTime;
	}

	@Override
	public int getTurnTime() {
		return turnTime;
	}

	@Override
	public VisibleType getVisibleType() {
		return VisibleType.GLOBAL;
	}

	@Override
	public boolean isVisibleOnMap() {
		return true;
	}

	@Override
	public void remove(final SpaceObject object) {
		objects.fastRemove(object);
	}

	@Override
	public void removeChild(final GravityObject object) {
		childs.fastRemove(object);
	}

	@Override
	public void setLocation(final Vector3f point, final Vector3f changed) {
		super.setLocation(point, changed);
		fly(changed);
	}

	@Override
	public void setOrbit(final int radius, final long turnAroundTime, final int turnTime, final int distance, final Quaternion orbitalRotation) {
		this.radius = radius;
		this.turnAroundTime = turnAroundTime;
		this.turnTime = turnTime;
		this.distance = distance;
		this.orbitalRotation.set(orbitalRotation);
	}

	@Override
	public void setParent(final GravityObject object) {
		this.parent = object;
	}

	@Override
	public void spawnMe() {
		super.spawnMe();
		gravityFly.start();
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + " getObjectId() = " + getObjectId() + ", getLocation() = " + getLocation();
	}

	@Override
	public void update(final LocalObjects local, final long currentTime, final float tpf) {
		gravityFly.update(currentTime);

		final Array<GravityObject> childs = getChilds();

		if(childs.isEmpty()) {
			return;
		}

		for(final GravityObject child : childs.array()) {

			if(child == null) {
				break;
			}

			child.update(local, currentTime, tpf);
		}
	}

	@Override
	public void updatePosition(double done) {
		gravityFly.update(done);
	}

	@Override
	public void updateVisibleTo(PlayerShip ship) {
	}
}
