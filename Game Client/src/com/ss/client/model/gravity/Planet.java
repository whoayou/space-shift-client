package com.ss.client.model.gravity;

import com.ss.client.template.GravityObjectTemplate;

/**
 * Модель планеты.
 * 
 * @author Ronn
 */
public class Planet extends AbstractGravityObject<GravityObjectTemplate, PlanetView> {

	public Planet(final long objectId, final GravityObjectTemplate template) {
		super(objectId, template);
	}
}
