package com.ss.client.model.gravity;


import com.jme3.scene.Spatial;
import com.ss.client.template.GravityObjectTemplate;

/**
 * Модель внешности астеройда.
 * 
 * @author Ronn
 */
public class AsteroidView extends AbstractGravityObjectView<Asteroid, GravityObjectTemplate> {

	public AsteroidView(Spatial spatial, GravityObjectTemplate template) {
		super(spatial, template);
	}

}
