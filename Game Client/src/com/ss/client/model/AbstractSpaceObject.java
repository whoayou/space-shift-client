package com.ss.client.model;

import rlib.logging.Logger;
import rlib.logging.Loggers;
import rlib.util.array.Array;

import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.ss.client.Game;
import com.ss.client.gui.controller.game.hud.selector.SelectorType;
import com.ss.client.model.item.SpaceItem;
import com.ss.client.model.module.Module;
import com.ss.client.model.ship.SpaceShip;
import com.ss.client.model.ship.player.PlayerShip;
import com.ss.client.model.space.SpaceLocation;
import com.ss.client.model.space.SpaceSector;
import com.ss.client.model.space.VisibleType;
import com.ss.client.model.state.StateId;
import com.ss.client.model.util.UpdateableObject;
import com.ss.client.template.ObjectTemplate;
import com.ss.client.util.LocalObjects;

/**
 * Базовая модель космического объекта.
 * 
 * @author Ronn
 */
public abstract class AbstractSpaceObject<T extends ObjectTemplate, A extends SpaceObjectView> implements SpaceObject {

	protected static final Logger LOGGER = Loggers.getLogger(SpaceObject.class);

	protected static final StateId GAME_STATE = StateId.GAME_STATE;
	protected static final Game GAME = Game.getInstance();
	protected static final SpaceLocation SPACE_LOCATION = SpaceLocation.getInstance();

	/** темплейт объекта */
	protected final T template;

	/** координаты объекта в мире */
	protected final Vector3f location;
	/** рахворот объекта в мире */
	protected final Quaternion rotation;

	/** контейнер обновляемых объектов, в котором находится этот объект */
	protected Array<UpdateableObject> container;

	/** отображение объекта */
	protected A view;

	/** пространство, в котором находится объект */
	protected Node parentNode;

	/** текущий сектор объекта */
	protected SpaceSector currentSector;

	/** статус дружественности объекта к игроку */
	protected FriendStatus friendStatus;

	/** название объекта */
	protected String name;

	/** уникальный ид объекта */
	protected long objectId;

	/** уровень объекта */
	protected int level;

	/** был ли отспавнен объект */
	protected boolean spawned;
	/** отображается ли объект */
	protected boolean visible;

	@SuppressWarnings("unchecked")
	public AbstractSpaceObject(final long objectId, final T template) {
		try {
			this.objectId = objectId;
			this.template = template;
			this.location = new Vector3f();
			this.rotation = new Quaternion(0, 0, 0, 1F);
			this.view = (A) template.takeView();
			this.view.setOwner(this);
			this.friendStatus = FriendStatus.NEUTRAL;
		} catch(final Exception e) {
			LOGGER.warning(this, e);
			throw e;
		}
	}

	@Override
	public void bind(Array<UpdateableObject> container) {
		setContainer(container);
	}

	@Override
	public int compareTo(final SpaceObject object) {
		return (int) objectId - (int) object.getObjectId();
	}

	@Override
	public void decayMe() {
		if(isSpawned()) {
			SPACE_LOCATION.removeVisibleObject(this);
			hide();
			setSpawned(false);
		}
	}

	@Override
	public void deleteMe() {

		if(isSpawned()) {
			SPACE_LOCATION.removeOldObject(this);
		}

		Array<UpdateableObject> container = getContainer();

		if(container != null) {
			synchronized(container) {
				container.fastRemove(this);
			}
		}

		decayMe();
		finish();
		template.putInstance(this);
	}

	@Override
	public void destruct() {
		A view = getView();
		view.destruct();
	}

	@Override
	public float distanceTo(final SpaceObject object) {
		return getLocation().distance(object.getLocation());
	}

	@Override
	public float distanceTo(Vector3f location) {
		return getLocation().distance(location);
	}

	@Override
	public void finalyze() {
		template.putAppearance(view);
		view = null;
		parentNode = null;
		name = null;
	}

	@Override
	public void finish() {
		setContainer(null);
	}

	/**
	 * @return контейнер обновляемых объектов, в котором находится этот объект.
	 */
	protected Array<UpdateableObject> getContainer() {
		return container;
	}

	@Override
	public SpaceSector getCurrentSector() {
		return currentSector;
	}

	@Override
	@Deprecated
	public Spatial getDebug() {
		return getView().getDebug();
	}

	@Override
	public FriendStatus getFriendStatus() {
		return friendStatus;
	}

	@Override
	public int getLevel() {
		return level;
	}

	@Override
	public final Vector3f getLocation() {
		return location;
	}

	@Override
	public int getMaxSize() {
		return Math.max(Math.max(getSizeX(), getSizeY()), getSizeZ());
	}

	@Override
	public Module getModule() {
		return null;
	}

	@Override
	public final String getName() {
		return name;
	}

	@Override
	public final long getObjectId() {
		return objectId;
	}

	@Override
	public final Node getParentNode() {
		return parentNode;
	}

	@Override
	public PlayerShip getPlayerShip() {
		return null;
	}

	@Override
	public final Quaternion getRotation() {
		return rotation;
	}

	@Override
	public SelectorType getSelectorType() {
		return SelectorType.NONE;
	}

	@Override
	public final int getSizeX() {
		return template.getSizeX();
	}

	@Override
	public final int getSizeY() {
		return template.getSizeY();
	}

	@Override
	public final int getSizeZ() {
		return template.getSizeZ();
	}

	@Override
	public SpaceItem getSpaceItem() {
		return null;
	}

	@Override
	public SpaceShip getSpaceShip() {
		return null;
	}

	@SuppressWarnings({
		"hiding",
		"unchecked"
	})
	@Override
	public <T extends ObjectTemplate> T getTemplate() {
		return (T) template;
	}

	@Override
	public int getTemplateId() {
		return template.getId();
	}

	@Override
	public final A getView() {
		return view;
	}

	@Override
	public int getVisibleRange() {
		return template.getVisibleRange();
	}

	@Override
	public VisibleType getVisibleType() {
		return VisibleType.NONE;
	}

	@Override
	public void hide() {
		view.removeToNode(parentNode);
		setVisible(false);
	}

	@Override
	public boolean isInRange(final SpaceObject object, final int range) {
		return isInRange(object.getLocation(), range);
	}

	@Override
	public boolean isInRange(final Vector3f point, final int range) {
		return location.distanceSquared(point) < range * range;
	}

	@Override
	public boolean isModule() {
		return false;
	}

	@Override
	public boolean isNeedIndicator() {
		return true;
	}

	@Override
	public boolean isPlayerShip() {
		return false;
	}

	@Override
	public boolean isSpaceItem() {
		return false;
	}

	@Override
	public boolean isSpaceShip() {
		return false;
	}

	@Override
	public final boolean isSpawned() {
		return spawned;
	}

	@Override
	public boolean isVisible() {
		return visible;
	}

	@Override
	public boolean isVisibleOnMap() {
		return false;
	}

	@Override
	@SuppressWarnings("unchecked")
	public void reinit() {
		this.view = (A) template.takeView();
		this.view.setOwner(this);
	}

	/**
	 * @param container контейнер обновляемых объектов, в котором находится этот
	 * объект.
	 */
	protected void setContainer(Array<UpdateableObject> container) {
		this.container = container;
	}

	@Override
	public void setCurrentSector(final SpaceSector currentSector) {
		this.currentSector = currentSector;
	}

	@Override
	public void setFriendStatus(FriendStatus friendStatus) {
		this.friendStatus = friendStatus;
	}

	@Override
	public void setLevel(final int level) {
		this.level = level;
	}

	@Override
	public void setLocation(final Vector3f point, final Vector3f changed) {

		location.set(point);

		if(isSpawned()) {
			SPACE_LOCATION.addVisibleObject(this);
		}
	}

	@Override
	public final void setName(final String newName) {
		name = newName;
	}

	@Override
	public void setObjectId(final long objectId) {
		this.objectId = objectId;
	}

	@Override
	public final void setParentNode(final Node stateNode) {
		this.parentNode = stateNode;
	}

	@Override
	public void setRotation(final Quaternion rotate) {
		rotation.set(rotate);
	}

	/**
	 * @param spawned задаваемое spawned
	 */
	protected final void setSpawned(final boolean spawned) {
		this.spawned = spawned;
	}

	@Override
	public void setVisible(final boolean visible) {
		this.visible = visible;
	}

	@Override
	public void show() {
		view.addToNode(parentNode);
		setVisible(true);
	}

	@Override
	public void spawnMe() {
		SPACE_LOCATION.addNewObject(this);
		show();
		setSpawned(true);

		Vector3f location = getLocation();
		Quaternion rotation = getRotation();

		setLocation(location, Vector3f.ZERO);
		setRotation(rotation);

		A view = getView();
		view.setLocation(location);
		view.setRotation(rotation);
	}

	@Override
	public void teleportTo(Vector3f position, Quaternion rotation, LocalObjects local) {

		setLocation(position, Vector3f.ZERO);
		setRotation(rotation);

		A view = getView();

		if(view != null) {
			view.setLocation(position);
			view.setRotation(rotation);
		}
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + " objectId = " + objectId;
	}

	@Override
	public boolean update(LocalObjects local, long currentTime) {
		return false;
	}

	@Override
	public void update(final LocalObjects local, final long currentTime, final float tpf) {
	}

	@Override
	public void updateVisibleTo(PlayerShip ship) {

		boolean visible = isInRange(ship, getVisibleRange());

		if(visible != isVisible()) {
			if(visible) {
				show();
			} else {
				hide();
			}
		}
	}
}
