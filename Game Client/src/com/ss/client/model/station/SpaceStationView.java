package com.ss.client.model.station;

import com.ss.client.model.gravity.GravityObjectView;

/**
 * Интерфейс для реализации внешности космической станции.
 * 
 * @author Ronn
 */
public interface SpaceStationView extends GravityObjectView {

}
