package com.ss.client.model.station;

import com.ss.client.template.StationTemplate;

/**
 * Модель базовой космической станции.
 * 
 * @author Ronn
 */
public class BaseStation extends AbstractSpaceStation<BaseStationAppearance> {

	public BaseStation(final long objectId, final StationTemplate template) {
		super(objectId, template);
	}

	@Override
	public String toString() {
		return "BaseStation name = " + name;
	}
}
