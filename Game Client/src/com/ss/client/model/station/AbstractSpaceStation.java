package com.ss.client.model.station;

import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.ss.client.model.gravity.AbstractGravityObject;
import com.ss.client.model.space.VisibleType;
import com.ss.client.template.StationTemplate;

/**
 * Базовая модель космической станции.
 * 
 * @author Ronn
 */
public abstract class AbstractSpaceStation<T extends SpaceStationView> extends AbstractGravityObject<StationTemplate, T> implements SpaceStation {

	public AbstractSpaceStation(final long objectId, final StationTemplate template) {
		super(objectId, template);
	}

	@Override
	public VisibleType getVisibleType() {
		return VisibleType.REGION;
	}

	@Override
	public void setLocation(Vector3f point, Vector3f changed) {
		super.setLocation(point, changed);
		getView().setLocation(point);
	}

	@Override
	public void setRotation(Quaternion rotate) {
		super.setRotation(rotate);
		getView().setRotation(rotate);
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + " getObjectId() = " + getObjectId() + ", getName() = " + getName();
	}
}
