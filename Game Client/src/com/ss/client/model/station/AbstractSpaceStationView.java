package com.ss.client.model.station;

import com.jme3.material.Material;
import com.jme3.scene.Geometry;
import com.jme3.scene.Spatial;
import com.jme3.scene.shape.Box;
import com.ss.client.model.gravity.AbstractGravityObjectView;
import com.ss.client.template.StationTemplate;

/**
 * Базовая модель внешности космической станции.
 * 
 * @author Ronn
 */
public abstract class AbstractSpaceStationView<T extends SpaceStation> extends AbstractGravityObjectView<T, StationTemplate> implements SpaceStationView {

	public AbstractSpaceStationView(final Spatial spatial, final StationTemplate template) {
		super(spatial, template);

		// debug
		{
			Box box = new Box(template.getSizeX(), template.getSizeY(), template.getSizeZ());
			debug = new Geometry("debug", box);
			debug.setMaterial(new Material(GAME.getAssetManager(), "Common/MatDefs/Light/Lighting.j3md"));
		}
	}
}
