package com.ss.client.model;

import rlib.util.array.Array;

import com.jme3.math.Vector3f;

/**
 * Интерфейс для реализаци контейнера объектов.
 * 
 * @author Ronn
 */
public interface ObjectContainer {

	/**
	 * @param object добавляемый объект.
	 */
	public void add(SpaceObject object);

	/**
	 * Находится ли объект в контейнере.
	 * 
	 * @param object проверяемый объект.
	 * @return содержится ли объект.
	 */
	public boolean contains(SpaceObject object);

	/**
	 * Переместить содержимые объекты.
	 * 
	 * @param changed вектор смещения.
	 */
	public void fly(Vector3f changed);

	/**
	 * @return содержимые объекты.
	 */
	public Array<SpaceObject> getObjects();

	/**
	 * @param object удаляемый объект.
	 */
	public void remove(SpaceObject object);
}
