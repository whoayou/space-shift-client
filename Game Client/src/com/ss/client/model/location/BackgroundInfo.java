package com.ss.client.model.location;

import java.util.Arrays;

import rlib.util.VarTable;

import com.jme3.asset.TextureKey;

/**
 * Модель описание текстуры фона локации.
 * 
 * @author Ronn
 */
public final class BackgroundInfo {

	/**
	 * Перечисление сторон фона.
	 * 
	 * @author Ronn
	 */
	public static enum BackgroundSide {
		WEST,
		EAST,
		NORTH,
		SOUTH,
		UP,
		DOWN;

		public static final BackgroundSide[] VALUES = values();

		public static final BackgroundSide valueOf(final int index) {
			return VALUES[index];
		}
	}

	/** набор ключей для загрузки текстур */
	private final TextureKey[] keys;

	public BackgroundInfo(final VarTable vars) {
		keys = new TextureKey[BackgroundSide.VALUES.length];

		for(final BackgroundSide side : BackgroundSide.VALUES) {
			final TextureKey key = new TextureKey(vars.getString(side.name()));

			key.setGenerateMips(true);
			key.setAsCube(true);

			keys[side.ordinal()] = key;
		}
	}

	/**
	 * Получить ключ текстуры по стороне на фоне.
	 * 
	 * @param side сторона фона.
	 * @return ключ текстуры.
	 */
	public TextureKey getKey(final BackgroundSide side) {
		return keys[side.ordinal()];
	}

	@Override
	public String toString() {
		return "BackgroundInfo keys = " + Arrays.toString(keys);
	}
}
