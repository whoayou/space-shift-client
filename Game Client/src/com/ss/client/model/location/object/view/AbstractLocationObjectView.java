package com.ss.client.model.location.object.view;

import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.scene.Spatial;
import com.ss.client.model.AbstractSpaceObjectView;
import com.ss.client.model.location.object.LocationObject;
import com.ss.client.model.location.object.LocationObjectView;
import com.ss.client.template.LocationObjectTemplate;

/**
 * Базовая реализация внешности локационного объекта.
 * 
 * @author Ronn
 */
public abstract class AbstractLocationObjectView<O extends LocationObject> extends AbstractSpaceObjectView<LocationObjectTemplate, O> implements LocationObjectView {

	public AbstractLocationObjectView(Spatial model, LocationObjectTemplate template) {
		super(model, template);
	}

	@Override
	public void setLocation(Vector3f loc) {
		super.setLocation(loc);
		debug.setLocalTranslation(loc);
	}

	@Override
	public void setRotation(Quaternion rotate) {
		super.setRotation(rotate);
		debug.setLocalRotation(rotate);
	}
}
