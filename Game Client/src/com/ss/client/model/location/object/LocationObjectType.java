package com.ss.client.model.location.object;

import com.ss.client.model.SpaceObject;
import com.ss.client.model.SpaceObjectType;
import com.ss.client.model.SpaceObjectView;
import com.ss.client.model.location.object.classes.Asteroid;
import com.ss.client.model.location.object.view.AsteroidView;

/**
 * Перечисление типов локационных объектов.
 * 
 * @author Ronn
 */
public enum LocationObjectType implements SpaceObjectType {
	ASTEROID(Asteroid.class, AsteroidView.class), ;

	/** список типов грави объектов */
	private static final LocationObjectType[] VALUES = values();

	/**
	 * Получение типа локационного объекта по его индексу.
	 * 
	 * @param index индекс типа локационного объекта.
	 * @return тип локационного объекта.
	 */
	public static final LocationObjectType valueOf(final int index) {
		return VALUES[index];
	}

	/** класс реализации локационного объекта */
	private final Class<? extends LocationObject> instanceClass;
	/** класс реализации внешности */
	private final Class<? extends LocationObjectView> viewClass;

	private LocationObjectType(Class<? extends LocationObject> instanceClass, Class<? extends LocationObjectView> viewClass) {
		this.instanceClass = instanceClass;
		this.viewClass = viewClass;
	}

	@Override
	public Class<? extends SpaceObject> getInstanceClass() {
		return instanceClass;
	}

	@Override
	public Class<? extends SpaceObjectView> getViewClass() {
		return viewClass;
	}
}
