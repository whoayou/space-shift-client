package com.ss.client.model.location.object.view;

import com.jme3.scene.Spatial;
import com.ss.client.model.location.object.classes.Asteroid;
import com.ss.client.template.LocationObjectTemplate;

/**
 * Реализация внешности локационного астеройда.
 * 
 * @author Ronn
 */
public class AsteroidView extends AbstractLocationObjectView<Asteroid> {

	public AsteroidView(Spatial model, LocationObjectTemplate template) {
		super(model, template);
	}

}
