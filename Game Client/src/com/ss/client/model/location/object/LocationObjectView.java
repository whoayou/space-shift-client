package com.ss.client.model.location.object;

import com.ss.client.model.SpaceObjectView;

/**
 * Интерфейс для реализации внешности локационного объекта.
 * 
 * @author Ronn
 */
public interface LocationObjectView extends SpaceObjectView {

}
