package com.ss.client.model.location;

import rlib.util.VarTable;

import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;

/**
 * Контейнер информации о источнике света в локации.
 * 
 * @author Ronn
 */
public final class LightInfo {

	public static enum LightType {
		AMBIENT,
		SPOT,
		POINT,
		DIRECTION;

		public static final LightType[] VALUES = values();
	}
	public static final String MAIN = "main";
	public static final String TYPE = "type";
	public static final String COLOR = "color";
	public static final String POSITION = "position";

	public static final String RADIUS = "radius";

	/** позиция */
	private final Vector3f position;
	/** цвет света */
	private final ColorRGBA color;
	/** тип света */
	private final LightType lightType;

	/** радиус света */
	private final int radius;

	/** является ли основным источником в локации */
	private final boolean main;

	public LightInfo(final VarTable vars) {
		this.radius = vars.getInteger(RADIUS);
		this.position = vars.getGeneric(POSITION, Vector3f.class);
		this.lightType = vars.getEnum(TYPE, LightType.class);
		this.color = vars.getGeneric(COLOR, ColorRGBA.class);
		this.main = vars.getBoolean(MAIN);
	}

	/**
	 * @return цвет освещения.
	 */
	public final ColorRGBA getColor() {
		return color;
	}

	/**
	 * @return тип света.
	 */
	public LightType getLightType() {
		return lightType;
	}

	/**
	 * @return позиция освещения.
	 */
	public final Vector3f getPosition() {
		return position;
	}

	/**
	 * @return радиус освещения.
	 */
	public final int getRadius() {
		return radius;
	}

	/**
	 * @return является ли основным источником в локации.
	 */
	public boolean isMain() {
		return main;
	}

	@Override
	public String toString() {
		return "LightInfo loc = " + position + ",  color = " + color + ",  radius = " + radius;
	}
}
