package com.ss.client.model;

import rlib.util.pools.Foldable;

import com.jme3.math.Quaternion;
import com.jme3.math.Ray;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.Control;
import com.ss.client.util.LocalObjects;

/**
 * Интерфейс для реализации внешности 3Д объекта.
 * 
 * @author Ronn
 */
public interface SpaceObjectView extends Foldable, Control {

	public static final String NAME_OBJECT_LAYER = "#name_object_layer";

	/**
	 * Добавить модель к модели.
	 */
	public void addSpatial(Spatial spatial);

	/**
	 * Добавить модели к модели.
	 */
	public void addSpatial(Spatial[] spatials);

	/**
	 * Добавление в пространство отображения модели.
	 * 
	 * @param rootNode новое пространство.
	 */
	public void addToNode(Node rootNode);

	/**
	 * Отображение разрушения корабля.
	 */
	public void destruct();

	/**
	 * Получение точки пересечения луча с моделью объекта.
	 * 
	 * @param ray луч.
	 * @param result контейнер результата.
	 * @param targetNode название целевого узла.
	 * @return название целевой геометрии.
	 */
	public String getCollision(Ray ray, Vector3f result, String targetNode);

	@Deprecated
	public Geometry getDebug();

	/**
	 * @return позиция отображения корабля в сцене.
	 */
	public Vector3f getLocation();

	/**
	 * @return путь к иконке для отображения на карте.
	 */
	public String getMapIcon();

	/**
	 * @return ядро 3Д модели.
	 */
	public Node getNode();

	/**
	 * @return владелец внешности.
	 */
	public SpaceObject getOwner();

	/**
	 * @return разворот модели объекта.
	 */
	public Quaternion getRotation();

	/**
	 * Уведомление внешности объекта о попадании в него.
	 * 
	 * @param point точка попадания.
	 * @param target название целевого объекта.
	 * @param local контейнер локальных объектов.
	 */
	public void onHit(Vector3f point, String target, LocalObjects local);

	/**
	 * Удалить модель из модeли.
	 */
	public void removeSpatial(Spatial spatial);

	/**
	 * Удалить модели из модeли.
	 */
	public void removeSpatial(Spatial[] spatials);

	/**
	 * Удаление из пространства отображения объекта.
	 * 
	 * @param rootNode пространство.
	 */
	public void removeToNode(Node rootNode);

	/**
	 * Установка нового положения модели в пространстве.
	 * 
	 * @param loc новая позиция.
	 */
	public void setLocation(Vector3f loc);

	/**
	 * @param owner владелец внешности.
	 */
	public void setOwner(SpaceObject owner);

	/**
	 * Разворот 3Д модели в нужном направлении.
	 * 
	 * @param rotate нужное направление.
	 */
	public void setRotation(Quaternion rotate);
}
