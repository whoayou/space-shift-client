package com.ss.client.model;

import java.net.InetSocketAddress;

import rlib.util.pools.Foldable;
import rlib.util.pools.FoldablePool;
import rlib.util.pools.Pools;

/**
 * Контейнер информации о сервере.
 * 
 * @author Ronn
 */
public final class ServerInfo implements Foldable {

	private static final FoldablePool<ServerInfo> pool = Pools.newConcurrentFoldablePool(ServerInfo.class);

	public static ServerInfo newInstance(final String name, final String type, final String host, final int port, final float loading, final int online) {
		ServerInfo info = pool.take();

		if(info == null)
			info = new ServerInfo();

		info.name = name;
		info.type = type;
		info.adress = new InetSocketAddress(host, port);
		info.loading = loading;
		info.online = online;
		info.string = "[" + name + "] [" + type + "] [" + loading + "] [" + online + "]";

		return info;
	}

	/** название сервера */
	private String name;
	/** тип сервера */
	private String type;
	/** текстовое предстовление контейнера */
	private String string;

	/** адресс сервера */
	private InetSocketAddress adress;

	/** загруженность сервера */
	private float loading;
	/** онлаин сервера */
	private int online;

	@Override
	public boolean equals(Object obj) {
		if(this == obj)
			return true;
		if(obj == null)
			return false;
		if(getClass() != obj.getClass())
			return false;
		ServerInfo other = (ServerInfo) obj;
		if(adress == null) {
			if(other.adress != null)
				return false;
		} else if(!adress.equals(other.adress))
			return false;
		return true;
	}

	@Override
	public void finalyze() {
		adress = null;
		loading = 0F;
		name = null;
		online = 0;
		type = null;
	}

	/**
	 * Сложить в пул.
	 */
	public void fold() {
		pool.put(this);
	}

	/**
	 * @return адресс сервера.
	 */
	public final InetSocketAddress getAdress() {
		return adress;
	}

	/**
	 * @return загруженность сервера.
	 */
	public final float getLoading() {
		return loading;
	}

	/**
	 * @return название сервера.
	 */
	public final String getName() {
		return name;
	}

	/**
	 * @return онлаин сервера.
	 */
	public final int getOnline() {
		return online;
	}

	/**
	 * @return тип сервера.
	 */
	public final String getType() {
		return type;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((adress == null) ? 0 : adress.hashCode());
		return result;
	}

	@Override
	public void reinit() {
	}

	@Override
	public String toString() {
		return getName();
	}
}
