package com.ss.client.model.effect;

import com.jme3.asset.AssetManager;
import com.jme3.effect.ParticleEmitter;
import com.jme3.effect.ParticleMesh.Type;
import com.jme3.effect.influencers.ParticleInfluencer;
import com.jme3.effect.shapes.EmitterSphereShape;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import com.ss.client.template.grafic.effect.ItemTeleportEffectTemplate;
import com.ss.client.util.LocalObjects;

/**
 * Реализация эффекта телепорта предмета.
 * 
 * @author Ronn
 */
public class ItemTeleportEffect extends AbstractGraficEffect {

	public static final String PARTICLE_EMITER_ROUND_SPARK_NAME = "RoundSpark";
	public static final String PARTICLE_EMITER_FLASH_NAME = "Flash";

	public static final String PARTICLE_EMITER_MATERIAL = "Common/MatDefs/Misc/Particle.j3md";
	public static final String PARTICLE_EMITTER_TEXTURE = "Texture";

	/** частицы вспышки */
	private ParticleEmitter flash;
	/** остаточные частицы взрыва */
	private ParticleEmitter roundSpark;

	/** время эффекта */
	private final int timeEffect;

	public ItemTeleportEffect(ItemTeleportEffectTemplate template) {
		super(template);

		Node effectNode = getEffectNode();
		AssetManager assetManager = GAME.getAssetManager();

		if(template.isHasFlash()) {
			createFlash(assetManager, effectNode, template);
		}

		if(template.isHasRoundSpark()) {
			createRoundSpark(assetManager, effectNode, template);
		}

		this.timeEffect = template.getTimeEffect();
	}

	/**
	 * Создание источника вспышки.
	 */
	private void createFlash(AssetManager assetManager, Node effectNode, ItemTeleportEffectTemplate template) {

		Material material = new Material(assetManager, PARTICLE_EMITER_MATERIAL);
		material.setTexture(PARTICLE_EMITTER_TEXTURE, assetManager.loadTexture(template.getFlashTexture()));
		material.setBoolean("PointSprite", true);

		ColorRGBA startColor = template.getFlashStarColor();
		ColorRGBA endColor = template.getFlashEndColor();

		ParticleEmitter flash = new ParticleEmitter(PARTICLE_EMITER_FLASH_NAME, Type.Point, (int) (24 * template.getCountFactor()));
		flash.setSelectRandomImage(true);
		flash.setStartColor(new ColorRGBA(startColor.getRed(), startColor.getGreen(), startColor.getBlue(), 1F));
		flash.setEndColor(new ColorRGBA(endColor.getRed(), endColor.getGreen(), endColor.getBlue(), 0F));
		flash.setStartSize(template.getFlashStartSize());
		flash.setEndSize(template.getFlashEndSize());
		flash.setShape(new EmitterSphereShape(Vector3f.ZERO, 0.05F));
		flash.setParticlesPerSec(0);
		flash.setGravity(0, 0, 0);
		flash.setLowLife(template.getFlashMinLife());
		flash.setHighLife(template.getFlashMaxLife());
		flash.setImagesX(2);
		flash.setImagesY(2);
		flash.setMaterial(material);

		ParticleInfluencer particleInfluencer = flash.getParticleInfluencer();
		particleInfluencer.setInitialVelocity(new Vector3f(0, 0, 0));
		particleInfluencer.setVelocityVariation(1F);

		setFlash(flash);

		effectNode.attachChild(flash);
	}

	/**
	 * Создание источника остаточных частиц.
	 */
	private void createRoundSpark(AssetManager assetManager, Node effectNode, ItemTeleportEffectTemplate template) {

		Material material = new Material(assetManager, PARTICLE_EMITER_MATERIAL);
		material.setTexture(PARTICLE_EMITTER_TEXTURE, assetManager.loadTexture(template.getRoundSparkTexture()));
		material.setBoolean("PointSprite", true);

		ColorRGBA startColor = template.getRoundSparkStarColor();
		ColorRGBA endColor = template.getRoundSparkEndColor();

		ParticleEmitter roundSpark = new ParticleEmitter(PARTICLE_EMITER_ROUND_SPARK_NAME, Type.Point, (int) (200 * template.getCountFactor()));
		roundSpark.setStartColor(new ColorRGBA(startColor.getRed(), startColor.getGreen(), startColor.getBlue(), 1F));
		roundSpark.setEndColor(new ColorRGBA(endColor.getRed(), endColor.getGreen(), endColor.getBlue(), 0F));
		roundSpark.setStartSize(template.getRoundSparkStartSize());
		roundSpark.setEndSize(template.getRoundSparkEndSize());
		roundSpark.setShape(new EmitterSphereShape(Vector3f.ZERO, 1.0F));
		roundSpark.setParticlesPerSec(0);
		roundSpark.setFacingVelocity(true);
		roundSpark.setGravity(0, 0, 0);
		roundSpark.setLowLife(template.getRoundSparkMinLife());
		roundSpark.setHighLife(template.getRoundSparkMaxLife());
		roundSpark.setImagesX(1);
		roundSpark.setImagesY(1);
		roundSpark.setMaterial(material);

		ParticleInfluencer particleInfluencer = roundSpark.getParticleInfluencer();
		particleInfluencer.setInitialVelocity(new Vector3f(0, 0, 0));
		particleInfluencer.setVelocityVariation(0);

		setRoundSpark(roundSpark);

		effectNode.attachChild(roundSpark);
	}

	/**
	 * @return частицы вспышки.
	 */
	public ParticleEmitter getFlash() {
		return flash;
	}

	/**
	 * @return остаточные частицы взрыва.
	 */
	public ParticleEmitter getRoundSpark() {
		return roundSpark;
	}

	/**
	 * @return длительность эффекта.
	 */
	public int getTimeEffect() {
		return timeEffect;
	}

	/**
	 * @param flash частицы вспышки.
	 */
	public void setFlash(ParticleEmitter flash) {
		this.flash = flash;
	}

	/**
	 * @param roundSpark остаточные частицы взрыва.
	 */
	public void setRoundSpark(ParticleEmitter roundSpark) {
		this.roundSpark = roundSpark;
	}

	@Override
	public boolean update(LocalObjects local, long currentTime) {

		ParticleEmitter flash = getFlash();
		ParticleEmitter roundSpark = getRoundSpark();

		int diff = (int) (currentTime - getStartTime());
		int state = getState();

		if(state == EFFECT_STATE_START) {

			if(flash != null) {
				flash.emitAllParticles();
			}

			if(roundSpark != null) {
				roundSpark.emitAllParticles();
			}

			setState(1);

		} else if(state == 1 && diff > getTimeEffect()) {

			if(flash != null) {
				flash.killAllParticles();
			}

			if(roundSpark != null) {
				roundSpark.killAllParticles();
			}

			return true;
		}

		return false;
	}
}
