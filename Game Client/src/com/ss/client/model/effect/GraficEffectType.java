package com.ss.client.model.effect;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import com.ss.client.network.ServerPacket;
import com.ss.client.template.grafic.effect.DestructExplosionEffectTemplate;
import com.ss.client.template.grafic.effect.ExplosionEffectTemplate;
import com.ss.client.template.grafic.effect.GraficEffectTemplate;
import com.ss.client.template.grafic.effect.ItemTeleportEffectTemplate;

import rlib.logging.Loggers;

/**
 * Перечисление типов графических эффектов.
 * 
 * @author Ronn
 */
public enum GraficEffectType {

	/** обычный взрыв */
	EXPLOSION(ExplosionEffect.class, ExplosionEffectTemplate.class),
	/** эффект телепорта */
	ITEM_TELEPORT(ItemTeleportEffect.class, ItemTeleportEffectTemplate.class),
	/** взрыв разрушения */
	DESTRUCT_EXPLOSION(DestructExplosionEffect.class, DestructExplosionEffectTemplate.class), ;

	public static final GraficEffectType[] TYPES = values();

	@SuppressWarnings("unchecked")
	public static <T extends GraficEffectTemplate> T newTemplate(ServerPacket packet) {

		int index = packet.readByte();

		GraficEffectType type = TYPES[index];
		Constructor<? extends GraficEffectTemplate> constructor = type.getTemplateConstructor();

		try {
			return (T) constructor.newInstance(packet);
		} catch(InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			Loggers.warning(type, e);
		}

		return null;
	}

	/** класс реализации эффекта */
	private Class<? extends GraficEffect> instanceClass;
	/** констрктор шаблона эффекта */
	private Constructor<? extends GraficEffectTemplate> templateConstructor;

	private GraficEffectType(Class<? extends GraficEffect> instanceClass, Class<? extends GraficEffectTemplate> templateClass) {
		this.instanceClass = instanceClass;
		try {
			this.templateConstructor = templateClass.getConstructor(ServerPacket.class);
		} catch(NoSuchMethodException | SecurityException e) {
			Loggers.warning(this, e);
		}
	}

	/**
	 * @return класс реализации эффекта.
	 */
	public Class<? extends GraficEffect> getInstanceClass() {
		return instanceClass;
	}

	/**
	 * @return конструктор шаблона.
	 */
	public Constructor<? extends GraficEffectTemplate> getTemplateConstructor() {
		return templateConstructor;
	}
}
