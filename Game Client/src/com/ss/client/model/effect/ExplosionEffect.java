package com.ss.client.model.effect;


import com.jme3.asset.AssetManager;
import com.jme3.effect.ParticleEmitter;
import com.jme3.effect.ParticleMesh.Type;
import com.jme3.effect.influencers.ParticleInfluencer;
import com.jme3.effect.shapes.EmitterSphereShape;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.FastMath;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import com.ss.client.template.grafic.effect.ExplosionEffectTemplate;
import com.ss.client.util.LocalObjects;

/**
 * Базовая реализация эффекта взрыва.
 * 
 * @author Ronn
 */
public class ExplosionEffect extends AbstractGraficEffect {

	public static final String PARTICLE_EMITER_SHOCK_WAVE_NAME = "Shockwave";
	public static final String PARTICLE_EMITER_DEBRIS_NAME = "Debris";
	public static final String PARTICLE_EMITER_SMOK_TRAIL_NAME = "SmokeTrail";
	public static final String PARTICLE_EMITER_SPARK_NAME = "Spark";
	public static final String PARTICLE_EMITER_ROUND_SPARK_NAME = "RoundSpark";
	public static final String PARTICLE_EMITER_FLASH_NAME = "Flash";
	public static final String PARTICLE_EMITER_FLAME_NAME = "Flame";

	public static final String PARTICLE_EMITER_MATERIAL = "Common/MatDefs/Misc/Particle.j3md";

	public static final String PARTICLE_EMITTER_TEXTURE = "Texture";

	public static final int EXPLOSION_FINISH_STATE = 2;
	public static final int EXPLOSION_FLAME_STATE = 1;

	private static final float COUNT_FACTOR_F = 1f;

	/** частицы огня */
	private ParticleEmitter flame;
	/** частицы вспышки */
	private ParticleEmitter flash;
	/** частицы разлетающихся искр */
	private ParticleEmitter spark;
	/** остаточные частицы взрыва */
	private ParticleEmitter roundSpark;
	/** частицы разлетающегося дыма */
	private ParticleEmitter smokeTrail;
	/** частицы разломаных деталей */
	private ParticleEmitter debris;
	/** взрывная волна */
	private ParticleEmitter shockWave;

	public ExplosionEffect(ExplosionEffectTemplate template) {
		super(template);

		Node effectNode = getEffectNode();

		AssetManager assetManager = GAME.getAssetManager();

		if(template.isHasFlame()) {
			createFlame(assetManager, effectNode, template);
		}

		if(template.isHasFlash()) {
			createFlash(assetManager, effectNode, template);
		}

		if(template.isHasSpark()) {
			createSpark(assetManager, effectNode, template);
		}

		if(template.isHasRoundSpark()) {
			createRoundSpark(assetManager, effectNode, template);
		}

		if(template.isHasSmokeTrail()) {
			createSmokeTrail(assetManager, effectNode, template);
		}

		if(template.isHasDebris()) {
			createDebris(assetManager, effectNode, template);
		}

		if(template.isHasShockWave()) {
			createShockWave(assetManager, effectNode, template);
		}
	}

	/**
	 * Частицы разломаных деталей.
	 */
	private void createDebris(AssetManager assetManager, Node effectNode, ExplosionEffectTemplate template) {

		Material material = new Material(assetManager, PARTICLE_EMITER_MATERIAL);
		material.setTexture(PARTICLE_EMITTER_TEXTURE, assetManager.loadTexture(template.getDebrisTexture()));

		ParticleEmitter debris = new ParticleEmitter(PARTICLE_EMITER_DEBRIS_NAME, Type.Triangle, (int) (15 * template.getCountFactor()));
		debris.setSelectRandomImage(true);
		debris.setRandomAngle(true);
		debris.setRotateSpeed(FastMath.TWO_PI * 4);
		debris.setStartColor(new ColorRGBA(1F, 0.59F, 0.28F, 1.0F / COUNT_FACTOR_F));
		debris.setEndColor(new ColorRGBA(0.5F, 0.5F, 0.5F, 0F));
		debris.setStartSize(1.2f);
		debris.setEndSize(4.2f);

		// debris.setShape(new EmitterSphereShape(Vector3f.ZERO, .05f));
		debris.setParticlesPerSec(0);
		debris.setGravity(10, 0, -10);
		debris.setLowLife(1.4F);
		debris.setHighLife(3.0F);
		debris.setImagesX(3);
		debris.setImagesY(3);
		debris.setMaterial(material);

		ParticleInfluencer particleInfluencer = debris.getParticleInfluencer();
		particleInfluencer.setInitialVelocity(new Vector3f(-10, 0, 10));
		particleInfluencer.setVelocityVariation(0);

		setDebris(debris);

		effectNode.attachChild(debris);
	}

	/**
	 * Создание источника пламени.
	 */
	private void createFlame(AssetManager assetManager, Node effectNode, ExplosionEffectTemplate template) {

		Material material = new Material(assetManager, PARTICLE_EMITER_MATERIAL);
		material.setTexture(PARTICLE_EMITTER_TEXTURE, assetManager.loadTexture(template.getFlameTexture()));
		material.setBoolean("PointSprite", true);

		ColorRGBA startColor = template.getFlameStarColor();
		ColorRGBA endColor = template.getFlameEndColor();

		ParticleEmitter flame = new ParticleEmitter(PARTICLE_EMITER_FLAME_NAME, Type.Point, (int) (32 * template.getCountFactor()));
		flame.setSelectRandomImage(true);
		flame.setStartColor(new ColorRGBA(startColor.getRed(), startColor.getGreen(), startColor.getBlue(), 1F));
		flame.setEndColor(new ColorRGBA(endColor.getRed(), endColor.getGreen(), endColor.getBlue(), 0F));
		flame.setStartSize(template.getFlameStartSize());
		flame.setEndSize(template.getFlameEndSize());
		flame.setShape(new EmitterSphereShape(Vector3f.ZERO, 1f));
		flame.setParticlesPerSec(0);
		flame.setGravity(0, 0, 0);
		flame.setLowLife(template.getFlameMinLife());
		flame.setHighLife(template.getFlameMaxLife());
		flame.setImagesX(2);
		flame.setImagesY(2);
		flame.setMaterial(material);

		ParticleInfluencer particleInfluencer = flame.getParticleInfluencer();
		particleInfluencer.setInitialVelocity(new Vector3f(0, 7, 0));
		particleInfluencer.setVelocityVariation(1F);

		setFlame(flame);

		effectNode.attachChild(flame);
	}

	/**
	 * Создание источника вспышки.
	 */
	private void createFlash(AssetManager assetManager, Node effectNode, ExplosionEffectTemplate template) {

		Material material = new Material(assetManager, PARTICLE_EMITER_MATERIAL);
		material.setTexture(PARTICLE_EMITTER_TEXTURE, assetManager.loadTexture(template.getFlashTexture()));
		material.setBoolean("PointSprite", true);

		ColorRGBA startColor = template.getFlashStarColor();
		ColorRGBA endColor = template.getFlashEndColor();

		ParticleEmitter flash = new ParticleEmitter(PARTICLE_EMITER_FLASH_NAME, Type.Point, (int) (24 * template.getCountFactor()));
		flash.setSelectRandomImage(true);
		flash.setStartColor(new ColorRGBA(startColor.getRed(), startColor.getGreen(), startColor.getBlue(), 1F));
		flash.setEndColor(new ColorRGBA(endColor.getRed(), endColor.getGreen(), endColor.getBlue(), 0F));
		flash.setStartSize(template.getFlashStartSize());
		flash.setEndSize(template.getFlashEndSize());
		flash.setShape(new EmitterSphereShape(Vector3f.ZERO, 0.05F));
		flash.setParticlesPerSec(0);
		flash.setGravity(0, 0, 0);
		flash.setLowLife(template.getFlashMinLife());
		flash.setHighLife(template.getFlashMaxLife());
		flash.setImagesX(2);
		flash.setImagesY(2);
		flash.setMaterial(material);

		ParticleInfluencer particleInfluencer = flash.getParticleInfluencer();
		particleInfluencer.setInitialVelocity(new Vector3f(0, 5F, 0));
		particleInfluencer.setVelocityVariation(1F);

		setFlash(flash);

		effectNode.attachChild(flash);
	}

	/**
	 * Создание источника остаточных частиц.
	 */
	private void createRoundSpark(AssetManager assetManager, Node effectNode, ExplosionEffectTemplate template) {

		Material material = new Material(assetManager, PARTICLE_EMITER_MATERIAL);
		material.setTexture(PARTICLE_EMITTER_TEXTURE, assetManager.loadTexture(template.getRoundSparkTexture()));
		material.setBoolean("PointSprite", true);

		ParticleEmitter roundSpark = new ParticleEmitter(PARTICLE_EMITER_ROUND_SPARK_NAME, Type.Point, (int) (20 * template.getCountFactor()));
		roundSpark.setStartColor(new ColorRGBA(1F, 0.29F, 0.34F, (float) (1.0 / COUNT_FACTOR_F)));
		roundSpark.setEndColor(new ColorRGBA(0, 0, 0, 0.5F / COUNT_FACTOR_F));
		roundSpark.setStartSize(template.getRoundSparkStartSize());
		roundSpark.setEndSize(template.getRoundSparkEndSize());
		roundSpark.setShape(new EmitterSphereShape(Vector3f.ZERO, 2F));
		roundSpark.setParticlesPerSec(0);
		roundSpark.setGravity(0, 0, 0);
		roundSpark.setLowLife(template.getRoundSparkMinLife());
		roundSpark.setHighLife(template.getRoundSparkMaxLife());
		roundSpark.setImagesX(1);
		roundSpark.setImagesY(1);
		roundSpark.setMaterial(material);

		ParticleInfluencer particleInfluencer = roundSpark.getParticleInfluencer();
		particleInfluencer.setInitialVelocity(new Vector3f(0, 0, 0));
		particleInfluencer.setVelocityVariation(0.5F);

		setRoundSpark(roundSpark);

		effectNode.attachChild(roundSpark);
	}

	/**
	 * Создание истчника взрывной волны.
	 */
	private void createShockWave(AssetManager assetManager, Node effectNode, ExplosionEffectTemplate template) {

		Material material = new Material(assetManager, PARTICLE_EMITER_MATERIAL);
		material.setTexture(PARTICLE_EMITTER_TEXTURE, assetManager.loadTexture(template.getShockWaveTexture()));

		ColorRGBA startColor = template.getShockWaveStarColor();
		ColorRGBA endColor = template.getShockWaveEndColor();

		ParticleEmitter shockWave = new ParticleEmitter(PARTICLE_EMITER_SHOCK_WAVE_NAME, Type.Triangle, (int) (1 * template.getCountFactor()));
		shockWave.setRandomAngle(true);
		shockWave.setStartColor(new ColorRGBA(startColor.getRed(), startColor.getGreen(), startColor.getBlue(), 1F));
		shockWave.setEndColor(new ColorRGBA(endColor.getRed(), endColor.getGreen(), endColor.getBlue(), 0F));
		shockWave.setStartSize(template.getShockWaveStartSize());
		shockWave.setEndSize(template.getShockWaveEndSize());
		shockWave.setParticlesPerSec(0);
		shockWave.setGravity(0, 0, 0);
		shockWave.setLowLife(template.getShockWaveMinLife());
		shockWave.setHighLife(template.getShockWaveMaxLife());
		shockWave.setImagesX(1);
		shockWave.setImagesY(1);
		shockWave.setMaterial(material);

		ParticleInfluencer particleInfluencer = shockWave.getParticleInfluencer();
		particleInfluencer.setInitialVelocity(new Vector3f(0, 0, 0));
		particleInfluencer.setVelocityVariation(0);

		setShockWave(shockWave);

		effectNode.attachChild(shockWave);
	}

	/**
	 * Создание источника дымовых частиц.
	 */
	private void createSmokeTrail(AssetManager assetManager, Node effectNode, ExplosionEffectTemplate template) {

		Material material = new Material(assetManager, PARTICLE_EMITER_MATERIAL);
		material.setTexture(PARTICLE_EMITTER_TEXTURE, assetManager.loadTexture(template.getSmokeTrailTexture()));

		ParticleEmitter smokeTrail = new ParticleEmitter(PARTICLE_EMITER_SMOK_TRAIL_NAME, Type.Triangle, (int) (22 * template.getCountFactor()));
		smokeTrail.setStartColor(new ColorRGBA(1F, 0.8F, 0.36F, 1.0F / COUNT_FACTOR_F));
		smokeTrail.setEndColor(new ColorRGBA(1F, 0.8F, 0.36F, 0F));
		smokeTrail.setStartSize(0.2F);
		smokeTrail.setEndSize(1F);

		// smoketrail.setShape(new EmitterSphereShape(Vector3f.ZERO, 1f));
		smokeTrail.setFacingVelocity(true);
		smokeTrail.setParticlesPerSec(0);
		smokeTrail.setGravity(0, 1, 0);
		smokeTrail.setLowLife(0.4F);
		smokeTrail.setHighLife(0.5F);
		smokeTrail.setImagesX(1);
		smokeTrail.setImagesY(3);
		smokeTrail.setMaterial(material);

		ParticleInfluencer particleInfluencer = smokeTrail.getParticleInfluencer();
		particleInfluencer.setInitialVelocity(new Vector3f(0, 12F, 0));
		particleInfluencer.setVelocityVariation(1F);

		setSmokeTrail(smokeTrail);

		effectNode.attachChild(smokeTrail);
	}

	/**
	 * Создание источника частиц разлетающихся искр.
	 */
	private void createSpark(AssetManager assetManager, Node effectNode, ExplosionEffectTemplate template) {

		Material material = new Material(assetManager, PARTICLE_EMITER_MATERIAL);
		material.setTexture(PARTICLE_EMITTER_TEXTURE, assetManager.loadTexture(template.getSparkTexture()));

		ParticleEmitter spark = new ParticleEmitter(PARTICLE_EMITER_SPARK_NAME, Type.Triangle, (int) (30 * template.getCountFactor()));

		ColorRGBA color = template.getSparkColor();

		// цвет искр
		spark.setStartColor(new ColorRGBA(color.getRed(), color.getGreen(), color.getBlue(), 1F));
		spark.setEndColor(new ColorRGBA(color.getRed(), color.getGreen(), color.getBlue(), 0F));

		spark.setStartSize(template.getSparkStartSize());
		spark.setEndSize(template.getSparkEndSize());
		spark.setFacingVelocity(true);
		spark.setParticlesPerSec(0);

		Vector3f gravity = template.getSparkGravity();

		// направление искр
		spark.setGravity(gravity.getX(), gravity.getY(), gravity.getZ());
		// время жизни искр
		spark.setLowLife(template.getSparkMinLife());
		spark.setHighLife(template.getSparkMaxLife());

		spark.setImagesX(1);
		spark.setImagesY(1);
		spark.setMaterial(material);

		ParticleInfluencer particleInfluencer = spark.getParticleInfluencer();
		particleInfluencer.setInitialVelocity(new Vector3f(0, 20, 0));
		particleInfluencer.setVelocityVariation(1F);

		setSpark(spark);

		effectNode.attachChild(spark);
	}

	/**
	 * @return частицы разломаных деталей.
	 */
	public ParticleEmitter getDebris() {
		return debris;
	}

	/**
	 * @return частицы огня.
	 */
	public ParticleEmitter getFlame() {
		return flame;
	}

	/**
	 * @return частицы вспышки.
	 */
	public ParticleEmitter getFlash() {
		return flash;
	}

	/**
	 * @return остаточные частицы взрыва.
	 */
	public ParticleEmitter getRoundSpark() {
		return roundSpark;
	}

	/**
	 * @return взрывная волна.
	 */
	public ParticleEmitter getShockWave() {
		return shockWave;
	}

	/**
	 * @return частицы разлетающегося дыма.
	 */
	public ParticleEmitter getSmokeTrail() {
		return smokeTrail;
	}

	/**
	 * @return частицы разлетающихся искр.
	 */
	public ParticleEmitter getSpark() {
		return spark;
	}

	/**
	 * @param debris частицы разломаных деталей.
	 */
	public void setDebris(ParticleEmitter debris) {
		this.debris = debris;
	}

	/**
	 * @param flame частицы огня.
	 */
	public void setFlame(ParticleEmitter flame) {
		this.flame = flame;
	}

	/**
	 * @param flash частицы вспышки.
	 */
	public void setFlash(ParticleEmitter flash) {
		this.flash = flash;
	}

	/**
	 * @param roundSpark остаточные частицы взрыва.
	 */
	public void setRoundSpark(ParticleEmitter roundSpark) {
		this.roundSpark = roundSpark;
	}

	/**
	 * @param shockWave взрывная волна.
	 */
	public void setShockWave(ParticleEmitter shockWave) {
		this.shockWave = shockWave;
	}

	/**
	 * @param smokeTrail частицы разлетающегося дыма.
	 */
	public void setSmokeTrail(ParticleEmitter smokeTrail) {
		this.smokeTrail = smokeTrail;
	}

	/**
	 * @param spark частицы разлетающихся искр.
	 */
	public void setSpark(ParticleEmitter spark) {
		this.spark = spark;
	}

	@Override
	public boolean update(LocalObjects local, long currentTime) {

		ParticleEmitter flash = getFlash();
		ParticleEmitter flame = getFlame();
		ParticleEmitter spark = getSpark();
		ParticleEmitter smokeTrail = getSmokeTrail();
		ParticleEmitter debris = getDebris();
		ParticleEmitter shockWave = getShockWave();
		ParticleEmitter roundSpark = getRoundSpark();

		int diff = (int) (currentTime - getStartTime());
		int state = getState();

		if(state == EFFECT_STATE_START) {

			if(flash != null) {
				flash.emitAllParticles();
			}

			if(spark != null) {
				spark.emitAllParticles();
			}

			if(smokeTrail != null) {
				smokeTrail.emitAllParticles();
			}

			if(debris != null) {
				debris.emitAllParticles();
			}

			if(shockWave != null) {
				shockWave.emitAllParticles();
			}

			setState(EXPLOSION_FLAME_STATE);

		} else if(state == EXPLOSION_FLAME_STATE) {

			if(flame != null) {
				flame.emitAllParticles();
			}

			if(roundSpark != null) {
				roundSpark.emitAllParticles();
			}

			setState(EXPLOSION_FINISH_STATE);

		} else if(state == EXPLOSION_FINISH_STATE && diff > 7000) {

			if(flash != null) {
				flash.killAllParticles();
			}

			if(spark != null) {
				spark.killAllParticles();
			}

			if(smokeTrail != null) {
				smokeTrail.killAllParticles();
			}

			if(debris != null) {
				debris.killAllParticles();
			}

			if(flame != null) {
				flame.killAllParticles();
			}

			if(roundSpark != null) {
				roundSpark.killAllParticles();
			}

			if(shockWave != null) {
				shockWave.killAllParticles();
			}

			return true;
		}

		return false;
	}
}
