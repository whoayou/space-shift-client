package com.ss.client.model.item.common;


import com.jme3.scene.Spatial;
import com.ss.client.model.item.AbstractSpaceItemView;
import com.ss.client.template.item.CommonItemTemplate;

/**
 * Модель внешности разлиных предметов.
 * 
 * @author Ronn
 */
public class CommonItemView extends AbstractSpaceItemView<CommonItemTemplate, CommonItem> {

	public CommonItemView(final Spatial spatial, final CommonItemTemplate template) {
		super(spatial, template);
	}
}
