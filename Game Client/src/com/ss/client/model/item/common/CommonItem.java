package com.ss.client.model.item.common;

import com.ss.client.model.item.AbstractSpaceItem;
import com.ss.client.model.item.SpaceItemView;
import com.ss.client.template.item.CommonItemTemplate;

/**
 * Базовая реализация различных предметов.
 * 
 * @author Ronn
 */
public abstract class CommonItem extends AbstractSpaceItem<CommonItemTemplate, SpaceItemView> {

	public CommonItem(final long objectId, final CommonItemTemplate template) {
		super(objectId, template);
	}
}
