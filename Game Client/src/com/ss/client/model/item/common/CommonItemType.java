package com.ss.client.model.item.common;

import com.ss.client.model.SpaceObject;
import com.ss.client.model.SpaceObjectType;
import com.ss.client.model.SpaceObjectView;

/**
 * Перечисление типов предметов.
 * 
 * @author Ronn
 */
public enum CommonItemType implements SpaceObjectType {
	OTHER(OtherItem.class),

	;

	private static final CommonItemType[] VALUES = values();

	public static final CommonItemType valueOf(final int index) {
		return VALUES[index];
	}

	/** тип экземпляров предметов */
	private Class<? extends CommonItem> instanceClass;

	private CommonItemType(final Class<? extends CommonItem> instanceClass) {
		this.instanceClass = instanceClass;
	}

	@Override
	public Class<? extends SpaceObject> getInstanceClass() {
		return instanceClass;
	}

	@Override
	public Class<? extends SpaceObjectView> getViewClass() {
		return CommonItemView.class;
	}
}
