package com.ss.client.model.item;

import com.ss.client.model.SpaceObject;

/**
 * Интерфейс для реализации модели космического предмета.
 * 
 * @author ronn
 */
public interface SpaceItem extends SpaceObject {

	public static final int CREADITS_ITEM_ID = 1;

	/**
	 * @return кол-во предметов.
	 */
	public long getItemCount();

	@Override
	public SpaceItemView getView();

	/**
	 * @return стакуемый ли придмет.
	 */
	public boolean isStackable();

	/**
	 * @param itemCount кол-во предметов.
	 */
	public void setItemCount(long itemCount);
}
