package com.ss.client.model.item;

import com.ss.client.table.ItemTable;
import com.ss.client.template.item.ItemTemplate;

/**
 * Набор утильных методов по работе с предметами.
 * 
 * @author Ronn
 */
public class ItemUtils {

	public static SpaceItem createItem(int templateId, long objectId, long itemCount) {

		ItemTable itemTable = ItemTable.getInstance();

		ItemTemplate template = itemTable.getTemplate(templateId);

		if(template == null) {
			return null;
		}

		SpaceItem item = template.takeInstance(objectId);
		item.setItemCount(itemCount);
		item.hide();

		return item;
	}

	private ItemUtils() {
		throw new RuntimeException();
	}
}
