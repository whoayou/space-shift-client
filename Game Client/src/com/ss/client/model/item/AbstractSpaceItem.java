package com.ss.client.model.item;

import com.ss.client.gui.controller.game.hud.selector.SelectorType;
import com.ss.client.model.AbstractSpaceObject;
import com.ss.client.model.space.VisibleType;
import com.ss.client.template.item.ItemTemplate;

/**
 * Базовая модель космического предмета.
 * 
 * @author Ronn
 */
public abstract class AbstractSpaceItem<T extends ItemTemplate, V extends SpaceItemView> extends AbstractSpaceObject<T, V> implements SpaceItem {

	/** кол-во предметов */
	protected long itemCount;

	public AbstractSpaceItem(final long objectId, final T template) {
		super(objectId, template);
		setName(template.getName());
	}

	@Override
	public long getItemCount() {
		return itemCount;
	}

	@Override
	public SelectorType getSelectorType() {
		return SelectorType.ITEM;
	}

	@Override
	public VisibleType getVisibleType() {
		return VisibleType.LOCAL;
	}

	@Override
	public boolean isSpaceItem() {
		return true;
	}

	@Override
	public boolean isStackable() {
		return template.isStackable();
	}

	@Override
	public void reinit() {
		super.reinit();
		setName(template.getName());
	}

	@Override
	public void setItemCount(final long itemCount) {
		this.itemCount = itemCount;
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + " itemCount = " + itemCount + ",  objectId = " + objectId;
	}
}
