package com.ss.client.model.listeners;

import com.ss.client.model.module.classes.EngineModule;

/**
 * Интерфейс для реализации прослушки работы двигателя.
 * 
 * @author Ronn
 */
public interface EngineListener {

	/**
	 * Активация модуля двигателя.
	 * 
	 * @param module активированный модуль.
	 */
	public void onActivate(EngineModule module);

	/**
	 * Деактивация модуля двигателя.
	 * 
	 * @param module деактивированный модуль.
	 */
	public void onDeactivate(EngineModule module);
}
