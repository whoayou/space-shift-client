package com.ss.client.model.space;

/**
 * Перечисление типов дистанций отображения объектов.
 * 
 * @author Ronn
 */
public enum VisibleType {
	/** всегда видно */
	GLOBAL(SpaceLocation.GLOBAL_SECTOR_SIZE),
	/** средняя дистанция видимости */
	REGION(SpaceLocation.REGION_SECTOR_SIZE),
	/** ближняя дистанция видимости */
	LOCAL(SpaceLocation.LOCAL_SECTOR_SIZE),
	/** не видимый */
	NONE(0);

	/** размер сектора для соотвествующегоо типа дистанции */
	private int sectorSize;

	private VisibleType(final int sectorSize) {
		this.sectorSize = sectorSize;
	}

	/**
	 * @return размер сектора.
	 */
	public int getSectorSize() {
		return sectorSize;
	}
}
