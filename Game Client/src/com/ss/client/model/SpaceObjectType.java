package com.ss.client.model;

/**
 * Интерфейс для реализации типа космического объекта.
 * 
 * @author Ronn
 */
public interface SpaceObjectType {

	/**
	 * @return класс объекта.
	 */
	public abstract Class<? extends SpaceObject> getInstanceClass();

	/**
	 * @return класс внешности объектов.
	 */
	public abstract Class<? extends SpaceObjectView> getViewClass();
}
