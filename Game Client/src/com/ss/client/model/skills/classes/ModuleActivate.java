package com.ss.client.model.skills.classes;

import com.ss.client.model.module.Module;
import com.ss.client.template.SkillTemplate;

/**
 * Модель скила по активации модуля.
 * 
 * @author Ronn
 */
public class ModuleActivate extends AbstractSkill {

	public ModuleActivate(final SkillTemplate template) {
		super(template);
	}

	@Override
	public void useSkill(final Module module, final int active) {
		super.useSkill(module, active);
		module.active(active);
	}
}
