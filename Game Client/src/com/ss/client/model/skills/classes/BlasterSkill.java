package com.ss.client.model.skills.classes;

import com.ss.client.template.SkillTemplate;

/**
 * @author Ronn
 */
public class BlasterSkill extends AbstractSkill {

	public BlasterSkill(SkillTemplate template) {
		super(template);
	}

}
