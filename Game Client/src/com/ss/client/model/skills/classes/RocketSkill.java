package com.ss.client.model.skills.classes;

import com.ss.client.template.SkillTemplate;

/**
 * @author Ronn
 */
public class RocketSkill extends AbstractSkill {

	public RocketSkill(SkillTemplate template) {
		super(template);
	}

}
