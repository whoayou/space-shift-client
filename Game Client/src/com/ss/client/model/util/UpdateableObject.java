package com.ss.client.model.util;

import com.ss.client.util.LocalObjects;

import rlib.util.array.Array;

/**
 * Интерфейс дял реализации обновляемого объекта.
 * 
 * @author Ronn
 */
public interface UpdateableObject {

	/**
	 * Запоминание контейнера, в котором лежит объект для обновления.
	 * 
	 * @param container контейнер обновляемых объектов.
	 */
	public void bind(Array<UpdateableObject> container);

	/**
	 * Завершение обновлений объекта.
	 */
	public void finish();

	/**
	 * Обновление объекта.
	 * 
	 * @param local контейнер локальных объектов.
	 * @param currentTime текущее время.
	 * @return завершил ли объект задачи по обновлению.
	 */
	public boolean update(LocalObjects local, long currentTime);
}
