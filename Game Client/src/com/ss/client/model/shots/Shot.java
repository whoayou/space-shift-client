package com.ss.client.model.shots;

import rlib.util.array.Array;
import rlib.util.pools.Foldable;
import rlib.util.pools.FoldablePool;

import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.ss.client.model.module.ShotModule;
import com.ss.client.model.ship.SpaceShip;
import com.ss.client.model.skills.Skill;
import com.ss.client.util.LocalObjects;

/**
 * Интерфейс для реализации выстрелов бластеров/ракет в космосе.
 * 
 * @author Ronn
 */
public interface Shot extends Foldable {

	/**
	 * Запоминание в какой контейнер выстрел был положен.
	 * 
	 * @param updateShots контейнер обновляемых выстрелов.
	 */
	public void bind(Array<Shot> updateShots);

	/**
	 * Бинд пула, в который выстрел сложится после выполнения задачи.
	 * 
	 * @param pool пул выстрелов.
	 */
	public void bind(FoldablePool<Shot> pool);

	/**
	 * Обработка заверщения выстрела.
	 */
	public void finish();

	/**
	 * @return пройденного расстояния от максимального.
	 */
	public float getDone();

	/**
	 * @return индекс ствола, из котороо был пуск.
	 */
	public int getIndex();

	/**
	 * @return время последнего обновления.
	 */
	public long getLastTime();

	/**
	 * @return позиция снаряда.
	 */
	public Vector3f getLocation();

	/**
	 * @return максимальная дальность полета выстрела.
	 */
	public int getMaxDistance();

	/**
	 * @return модель выстрела.
	 */
	public Spatial getModel();

	/**
	 * @return модуль, из которого был пуск.
	 */
	public ShotModule getModule();

	/**
	 * @return узел, в котором находится выстрел.
	 */
	public Node getNode();

	/**
	 * @return уникальный ид выстрела.
	 */
	public int getObjectId();

	/**
	 * @return разворот выстрела.
	 */
	public Quaternion getRotation();

	/**
	 * @return выстреливший корабль.
	 */
	public SpaceShip getShooter();

	/**
	 * @return скил, с помощью которого был произведен выстрел.
	 */
	public Skill getSkill();

	/**
	 * @return скорость выстрела.
	 */
	public float getSpeed();

	/**
	 * @return стартовая точка выстрела.
	 */
	public Vector3f getStartLoc();

	/**
	 * @return время старта выстрела.
	 */
	public long getStartTime();

	/**
	 * @param index индекс ствола, из котороо был пуск.
	 */
	public void setIndex(int index);

	/**
	 * @param maxDistance максимальная дистанция выстрела.
	 */
	public void setMaxDistance(int maxDistance);

	/**
	 * @param model модель выстрела.
	 */
	public void setModel(Spatial model);

	/**
	 * @param module модуль, из которого был пуск.
	 */
	public void setModule(ShotModule module);

	/**
	 * @param node узел, в котором находится выстрел.
	 */
	public void setNode(Node node);

	/**
	 * @param objectId уникальный ид выстрела.
	 */
	public void setObjectId(int objectId);

	/**
	 * @param rotation разворот выстрела.
	 */
	public void setRotation(Quaternion rotation);

	/**
	 * @param shooter стреляющий корабль.
	 */
	public void setShooter(SpaceShip shooter);

	/**
	 * @param skill скил, с помощью которого был произведен выстрел.
	 */
	public void setSkill(Skill skill);

	/**
	 * @param speed текущая скорость выстрела.
	 */
	public void setSpeed(float speed);

	/**
	 * @param startLoc точна старта выстрела.
	 */
	public void setStartLoc(Vector3f startLoc);

	/**
	 * Запуск выстрела.
	 */
	public void start();

	/**
	 * Остановка выстрела.
	 */
	public void stop();

	/**
	 * Обновить состояние выстрела.
	 * 
	 * @param curreentTime текущее время.
	 * @param local локальные объекты.
	 * @param завершен ли выстрел.
	 */
	public boolean update(long curreentTime, LocalObjects local);
}
