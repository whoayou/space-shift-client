package com.ss.client.model.shots;

import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.ss.client.Game;
import com.ss.client.model.SpaceObject;
import com.ss.client.model.SpaceObjectView;
import com.ss.client.util.LocalObjects;

/**
 * Реалиация выстрела ракетой.
 * 
 * @author Ronn
 */
public class RocketShot extends AbstractShot {

	public static final int GUIDANCE_INTERVAL = 500;

	/** целевой разворот ракеты */
	private final Quaternion targetRotation;
	/** стартовый разворот ракеты */
	private final Quaternion startRotation;
	/** итоговый текущий разворот ракеты */
	private final Quaternion resultRotation;
	/** буферный разворот для вычислений */
	private final Quaternion bufferRotation;

	/** текущее направление ракеты */
	private final Vector3f direction;

	/** цель ракеты */
	private SpaceObject target;

	/** время последнего обновления наведения */
	private long lastGuidanceUpdate;

	/** скорость разворота ракеты */
	private float rotationSpeed;
	/** выполненость разворота ракеты */
	private float rotatinDone;
	/** шаг разворота */
	private float rotationStep;

	/** максимальная скорость ракеты */
	private int maxSpeed;
	/** ускорение ракеты */
	private int accel;

	public RocketShot() {
		this.targetRotation = new Quaternion();
		this.startRotation = new Quaternion();
		this.resultRotation = new Quaternion();
		this.bufferRotation = new Quaternion();
		this.direction = new Vector3f();
	}

	/**
	 * @return ускорение ракеты.
	 */
	public int getAccel() {
		return accel;
	}

	/**
	 * @return буферный разворот для вычислений.
	 */
	private Quaternion getBufferRotation() {
		return bufferRotation;
	}

	/**
	 * @return текущее направление ракеты.
	 */
	private Vector3f getDirection() {
		return direction;
	}

	/**
	 * @return время последнего обновления наведения.
	 */
	private long getLastGuidanceUpdate() {
		return lastGuidanceUpdate;
	}

	/**
	 * @return максимальная скорость ракеты.
	 */
	public int getMaxSpeed() {
		return maxSpeed;
	}

	/**
	 * @return итоговый текущий разворот ракеты.
	 */
	private Quaternion getResultRotation() {
		return resultRotation;
	}

	/**
	 * @return выполненость разворота ракеты.
	 */
	private float getRotatinDone() {
		return rotatinDone;
	}

	/**
	 * @return скорость разворота ракеты.
	 */
	public float getRotationSpeed() {
		return rotationSpeed;
	}

	/**
	 * @return шаг разворота.
	 */
	private float getRotationStep() {
		return rotationStep;
	}

	/**
	 * @return стартовый разворот ракеты.
	 */
	private Quaternion getStartRotation() {
		return startRotation;
	}

	/**
	 * @return цель ракеты.
	 */
	public SpaceObject getTarget() {
		return target;
	}

	/**
	 * @return целевой разворот ракеты.
	 */
	private Quaternion getTargetRotation() {
		return targetRotation;
	}

	/**
	 * Обработка полета ракеты.
	 * 
	 * @param currentTime текущее время.
	 * @param diff разница времени с последним обновлением.
	 * @param local контейнер локальных объектов.
	 */
	private void processFly(long currentTime, long diff, LocalObjects local) {

		float done = getDone();
		float speed = getSpeed();
		float accel = getAccel();

		try {

			// считаем ускорение
			if(accel < 1 && speed < 1) {
				return;
			}

			speed = Math.min(speed + diff / 1000F * accel, getMaxSpeed());

			final float dist = diff * speed / 1000F;
			final Vector3f direction = local.getNextVector();
			final Vector3f location = getLocation();

			done += dist;

			direction.set(getDirection());
			direction.multLocal(dist);
			direction.addLocal(getLocation());

			location.set(direction);

			Spatial model = getModel();
			model.setLocalTranslation(location);

		} finally {
			setDone(done);
			setSpeed(speed);
		}
	}

	/**
	 * Процесс наведения ракеты на цель.
	 * 
	 * @param target цель ракеты.
	 * @param local контейнер локальных объектов.
	 */
	private void processGuidance(SpaceObject target, long currentTime, LocalObjects local) {

		long diff = currentTime - getLastGuidanceUpdate();

		if(diff > GUIDANCE_INTERVAL) {

			Quaternion targetRotation = getTargetRotation();
			Quaternion current = getRotation();

			Vector3f location = getLocation();
			Vector3f direction = local.getNextVector();

			float distance = target.distanceTo(location);

			targetRotation.getRotationColumn(2, direction);

			direction.multLocal(distance);
			direction.addLocal(location);

			SpaceObjectView view = target.getView();

			if(view == null) {
				return;
			}

			Node node = view.getNode();

			if(node == null) {
				return;
			}

			Vector3f up = current.getRotationColumn(1, local.getNextVector());
			direction.set(target.getLocation());

			// if(target.isSpaceShip()) {
			//
			// SpaceShip ship = target.getSpaceShip();
			//
			// // определяем время полета скила до текущего положения
			// // корабля
			// float time = distance / getSpeed();
			//
			// // определяем время полета скила, до положение корабля, на
			// // котором
			// // он будет во время подлета скила
			// distance += ship.getCurrentSpeed() * time;
			// time = distance / getSpeed();
			//
			// // определяем дистанцию смещения за время подлета
			// distance = ship.getCurrentSpeed() * time;
			//
			// direction.set(ship.getDirection());
			// direction.multLocal(distance);
			// direction.addLocal(ship.getLocation());
			// }

			direction.subtractLocal(getLocation());
			direction.normalizeLocal();

			targetRotation.lookAt(direction, up);
			updateRotation(targetRotation, local);
			setLastGuidanceUpdate(currentTime);
		}
	}

	/**
	 * Обработка разворота ракеты.
	 * 
	 * @param currentTime текущее время.
	 * @param diff разница времени с последним обновлением.
	 * @param local контейнер локальных объектов.
	 */
	private void processRotation(long currentTime, long diff, LocalObjects local) {

		float done = getRotatinDone();

		try {

			if(done >= 1F) {
				return;
			}

			done += getRotationStep() * diff / 1000F;

			Quaternion rotation = getRotation();
			Quaternion start = getStartRotation();
			Quaternion target = getTargetRotation();
			Quaternion result = getResultRotation();

			if(done >= 1F) {
				result = target;
			}

			if(result != target) {

				final Quaternion buffer = getBufferRotation();
				buffer.set(target);

				result = result.slerp(start, buffer, done);
			}

			result.getRotationColumn(2, getDirection());
			rotation.set(result);

			Spatial model = getModel();
			model.setLocalRotation(rotation);

		} finally {
			setRotatinDone(done);
		}
	}

	/**
	 * @param accel ускорение ракеты.
	 */
	public void setAccel(int accel) {
		this.accel = accel;
	}

	/**
	 * @param lastGuidanceUpdate время последнего обновления наведения.
	 */
	private void setLastGuidanceUpdate(long lastGuidanceUpdate) {
		this.lastGuidanceUpdate = lastGuidanceUpdate;
	}

	/**
	 * @param maxSpeed максимальная скорость ракеты.
	 */
	public void setMaxSpeed(int maxSpeed) {
		this.maxSpeed = maxSpeed;
	}

	/**
	 * @param rotatinDone выполненость разворота ракеты.
	 */
	private void setRotatinDone(float rotatinDone) {
		this.rotatinDone = rotatinDone;
	}

	/**
	 * @param rotationSpeed скорость разворота ракеты.
	 */
	public void setRotationSpeed(float rotationSpeed) {
		this.rotationSpeed = rotationSpeed;
	}

	/**
	 * @param rotationStep шаг разворота.
	 */
	private void setRotationStep(float rotationStep) {
		this.rotationStep = rotationStep;
	}

	/**
	 * @param rotation стартовый разворот ракеты.
	 */
	private void setStartRotation(Quaternion rotation) {
		this.startRotation.set(rotation);
	}

	/**
	 * @param target цель ракеты.
	 */
	public void setTarget(SpaceObject target) {
		this.target = target;
	}

	/**
	 * @param rotation целевой разворот ракеты.
	 */
	private void setTargetRotation(Quaternion rotation) {
		this.targetRotation.set(targetRotation);
	}

	@Override
	public void start() {

		Quaternion rotation = getRotation();
		rotation.getRotationColumn(2, getDirection());

		setStartRotation(rotation);
		setTargetRotation(rotation);
		setRotatinDone(1);
		setLastGuidanceUpdate(Game.getCurrentTime() + GUIDANCE_INTERVAL);

		super.start();
	}

	@Override
	public boolean update(long currentTime, LocalObjects local) {

		float done = getDone();

		try {

			if(done > getMaxDistance() * 3) {
				return true;
			}

			SpaceObject target = getTarget();

			if(target == null) {
				return true;
			}

			processGuidance(target, currentTime, local);

			final long diff = currentTime - getLastTime();

			processRotation(currentTime, diff, local);
			processFly(currentTime, diff, local);

			return false;
		} finally {
			setLastTime(currentTime);
		}
	}

	/**
	 * Обновление целевого направления.
	 * 
	 * @param target новое конечное направление.
	 * @param local контейнер локальных объектов.
	 */
	public void updateRotation(final Quaternion target, LocalObjects local) {

		float speed = getRotationSpeed();

		if(speed <= 0) {
			return;
		}

		Quaternion current = getRotation();

		float dot = Math.abs(current.dot(target));

		if(dot > 0.99999F) {
			setDone(1F);
			return;
		}

		final float radians = (float) Math.acos(dot);

		final float count = radians / speed;
		final float step = 1 / count;

		setStartRotation(current);
		setTargetRotation(target);
		setRotationStep(step);
		setRotatinDone(0);
	}
}
