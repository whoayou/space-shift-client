package com.ss.client.model;

/**
 * Аккаунт пользователя.
 * 
 * @author Ronn
 */
public final class Account {

	/** логин аккаунта */
	private String name;
	/** пароль аккаунта */
	private String password;

	/**
	 * Очистка.
	 */
	public final void clear() {
		this.name = null;
		this.password = null;
	}

	/**
	 * @return имя аккаунта.
	 */
	public final String getName() {
		return name;
	}

	/**
	 * @return пароль.
	 */
	public final String getPassword() {
		return password;
	}

	/**
	 * @param name имя аккаунта.
	 */
	public final void setName(final String name) {
		this.name = name;
	}

	/**
	 * @param password пароль.
	 */
	public final void setPassword(final String password) {
		this.password = password;
	}
}
