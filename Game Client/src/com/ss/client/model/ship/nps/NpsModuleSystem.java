package com.ss.client.model.ship.nps;

import com.ss.client.model.module.system.AbstractModuleSystem;

/**
 * Реализация системы модулей дял Nps.
 * 
 * @author Ronn
 */
public class NpsModuleSystem extends AbstractModuleSystem<Nps> {

}
