package com.ss.client.model.ship.nps;

import com.ss.client.model.ship.SpaceShipView;

/**
 * Интерфейс для реализации внешности Nps.
 * 
 * @author Ronn
 */
public interface NpsView extends SpaceShipView {

}
