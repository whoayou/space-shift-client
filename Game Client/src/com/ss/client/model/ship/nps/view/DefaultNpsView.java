package com.ss.client.model.ship.nps.view;


import com.jme3.scene.Spatial;
import com.ss.client.template.ship.NpsTemplate;

/**
 * Стандартная реализация внешности Nps.
 * 
 * @author Ronn
 */
public class DefaultNpsView extends AbstractNpsView {

	public DefaultNpsView(Spatial spatial, NpsTemplate template) {
		super(spatial, template);
	}
}
