package com.ss.client.model.ship.nps.classes;

import com.ss.client.template.ship.NpsTemplate;

/**
 * Реализация боевого дрона.
 * 
 * @author Ronn
 */
public class EnemyDron extends DefaultNps {

	public EnemyDron(long objectId, NpsTemplate template) {
		super(objectId, template);
	}
}
