package com.ss.client.model.ship.nps.view;

import com.jme3.scene.Spatial;
import com.ss.client.template.ship.NpsTemplate;

/**
 * Реализация внешности для боевого дрона.
 * 
 * @author Ronn
 */
public class EnemyDronView extends DefaultNpsView {

	public EnemyDronView(Spatial spatial, NpsTemplate template) {
		super(spatial, template);
	}

}
