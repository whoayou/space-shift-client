package com.ss.client.model.ship;

import com.ss.client.model.SpaceObject;
import com.ss.client.model.SpaceObjectType;
import com.ss.client.model.SpaceObjectView;
import com.ss.client.model.ship.nps.classes.DefaultNps;
import com.ss.client.model.ship.nps.classes.EnemyDron;
import com.ss.client.model.ship.nps.classes.FriendlyDron;
import com.ss.client.model.ship.nps.view.DefaultNpsView;
import com.ss.client.model.ship.nps.view.EnemyDronView;
import com.ss.client.model.ship.nps.view.FriendlyDronView;
import com.ss.client.model.ship.player.PlayerShip;
import com.ss.client.model.ship.player.PlayerShipView;

/**
 * Перечисление типов космических кораблей.
 * 
 * @author Ronn
 */
public enum SpaceShipType implements SpaceObjectType {
	NPS(DefaultNps.class, DefaultNpsView.class),
	PLAYER_SHIP(PlayerShip.class, PlayerShipView.class),
	ENEMY_DRON(EnemyDron.class, EnemyDronView.class),
	FRIENDLY_DRON(FriendlyDron.class, FriendlyDronView.class), ;

	/** список типов кораблей */
	private static final SpaceShipType[] VALUES = values();

	/**
	 * Получение типа корабля по индексу типа.
	 * 
	 * @param index индекс типа корабля.
	 * @return тип корабля.
	 */
	public static final SpaceShipType valueOf(final int index) {
		return VALUES[index];
	}

	/** тип объекта корабля */
	private Class<? extends SpaceShip> instanceClass;
	/** тип внешности корабля */
	private Class<? extends SpaceShipView> appearanceClass;

	private SpaceShipType(final Class<? extends SpaceShip> instanceClass, final Class<? extends SpaceShipView> appearanceClass) {
		this.instanceClass = instanceClass;
		this.appearanceClass = appearanceClass;
	}

	@Override
	public Class<? extends SpaceObject> getInstanceClass() {
		return instanceClass;
	}

	@Override
	public Class<? extends SpaceObjectView> getViewClass() {
		return appearanceClass;
	}
}
