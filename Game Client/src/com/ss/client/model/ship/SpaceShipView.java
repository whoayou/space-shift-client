package com.ss.client.model.ship;

import com.ss.client.model.SpaceObjectView;

/**
 * Интерфейс для реализации внешности кораблей.
 * 
 * @author Ronn
 */
public interface SpaceShipView extends SpaceObjectView {

	public static final String SPACE_SHIP_NODE = "SpaceShip";

}
