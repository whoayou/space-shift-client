package com.ss.client.model.ship;

import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.ss.client.model.SpaceObject;
import com.ss.client.model.module.system.ModuleSystem;
import com.ss.client.util.LocalObjects;

/**
 * Интерфейс для реализации корабля.
 * 
 * @author Ronn
 */
public interface SpaceShip extends SpaceObject {

	/**
	 * Полет по указанным параметрам.
	 * 
	 * @param location текущая позиция на сервере.
	 * @param accel ускорение текущее на сервере.
	 * @param speed скорость на сервере.
	 * @param maxSpeed максимальная скоростьна сервере.
	 * @param maxCurrentSpeed максимальная текущая скорость.
	 * @param active активны ли двигатели.
	 * @param force принудительная ли синхронизация.
	 */
	public void fly(Vector3f location, float accel, float speed, float maxSpeed, float maxCurrentSpeed, boolean active, boolean force);

	/**
	 * @return вектор направления.
	 */
	public Vector3f getDirection();

	/**
	 * @return процент энергии корабля.
	 */
	public float getEnergyPercent();

	/**
	 * @return ускорение корабля.
	 */
	public float getFlyAccel();

	/**
	 * @return текущая скорость полета корабля.
	 */
	public float getFlyCurrentSpeed();

	/**
	 * @return максимальная скорость корабля.
	 */
	public float getFlyMaxSpeed();

	/**
	 * @return система модулей.
	 */
	public ModuleSystem getModuleSystem();

	/**
	 * @return скорость разворота.
	 */
	public float getRotateSpeed();

	/**
	 * @return процент прочности корабля.
	 */
	public float getStrengthPercent();

	/**
	 * @return целевой разворот.
	 */
	public Quaternion getTargetRotation();

	@Override
	public SpaceShipView getView();

	/**
	 * @return активированы ли двигатели.
	 */
	public boolean isActiveEngine();

	/**
	 * @return в процессе ли вращения сейчас корабль.
	 */
	public boolean isRotation();

	/**
	 * Разворот по указанным параметрам.
	 * 
	 * @param local контейнер локальных объектв.
	 * @param start стартовый разворот.
	 * @param target целевой развор получаем владельца
	 * @param step шаг разворота.
	 * @param done выполненность разворота.
	 * @param infinity бесконечно ли разворачиваться.
	 * @param force принудительная ли синхронизация.
	 */
	public void rotate(LocalObjects local, Quaternion start, final Quaternion target, float step, float done, boolean infinity, boolean force);

	/**
	 * Запуск бесконечного разворота корабля по заданному направлению.
	 * 
	 * @param target направление разворота.
	 * @param modiff модификатор скорости разворота.
	 */
	public void rotate(Quaternion target, float modiff);

	/**
	 * @param percent процент энергии корабля.
	 */
	public void setEnergyPercent(float percent);

	/**
	 * @param accel ускорение корабля.
	 */
	public void setFlyAccel(float accel);

	/**
	 * @param currentSpeed текущая скорость полета корабля.
	 */
	public void setFlyCurrentSpeed(float currentSpeed);

	/**
	 * @param flyMaxSpeed максимальная скорость корабля.
	 */
	public void setFlyMaxSpeed(float flyMaxSpeed);

	/**
	 * @param rotateSpeed скорость разворота.
	 */
	public void setRotateSpeed(float rotateSpeed);

	/**
	 * @param percent процент прочности корабля.
	 */
	public void setStrengthPercent(float percent);

	/**
	 * Остановка вращения корабля.
	 */
	public void stopRotate();
}
