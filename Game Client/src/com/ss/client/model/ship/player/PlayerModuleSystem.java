package com.ss.client.model.ship.player;

import com.ss.client.model.module.system.AbstractModuleSystem;

/**
 * Система модулей игрока.
 * 
 * @author Ronn
 */
public class PlayerModuleSystem extends AbstractModuleSystem<PlayerShip> {

}
