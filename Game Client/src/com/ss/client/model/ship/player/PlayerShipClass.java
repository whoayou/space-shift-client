package com.ss.client.model.ship.player;

import com.ss.client.table.LangTable;

/**
 * Перечисление классов кораблей игроков.
 * 
 * @author Ronn
 */
public enum PlayerShipClass {
	EXPLORATION("HANGAR_DROP_DOWN_EXPLORATION_SHIP"), ;

	/** список классов кораблей игрока */
	private static final PlayerShipClass[] VALUES = values();

	/**
	 * Получение класса корабля по его индексу.
	 * 
	 * @param index индекс класса корабля.
	 * @return класс корабля.
	 */
	public static final PlayerShipClass valueOf(final int index) {
		return VALUES[index];
	}

	/** название класса */
	private String name;

	private PlayerShipClass(final String name) {
		// получаем таблицу локализации
		final LangTable langTable = LangTable.getInstance();

		this.name = langTable.getText(name);
	}

	@Override
	public String toString() {
		return name;
	}
}
