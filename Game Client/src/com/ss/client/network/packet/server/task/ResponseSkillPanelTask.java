package com.ss.client.network.packet.server.task;

import rlib.util.array.Array;
import rlib.util.array.Arrays;
import rlib.util.array.FuncElement;
import rlib.util.pools.FoldablePool;
import rlib.util.pools.Pools;

import com.ss.client.gui.controller.game.GameUIController;
import com.ss.client.gui.controller.game.event.impl.UpdateSkillPanelEvent;
import com.ss.client.gui.model.SkillPanelInfo;
import com.ss.client.manager.PacketTaskManager;
import com.ss.client.util.FoldablePacketTask;
import com.ss.client.util.LimitedPacketTask;
import com.ss.client.util.LocalObjects;

/**
 * Пакетная задача для обновления размещения умений на панели умения.
 * 
 * @author Ronn
 */
public class ResponseSkillPanelTask extends LimitedPacketTask implements FoldablePacketTask {

	private static final FoldablePool<ResponseSkillPanelTask> POOL = Pools.newConcurrentFoldablePool(ResponseSkillPanelTask.class);

	private static final FuncElement<SkillPanelInfo> INFO_FOLD = new FuncElement<SkillPanelInfo>() {

		@Override
		public void apply(final SkillPanelInfo info) {
			info.fold();
		}
	};

	public static final ResponseSkillPanelTask getInstance(Array<SkillPanelInfo> skillPanelInfo) {

		ResponseSkillPanelTask task = POOL.take();

		if(task == null) {
			task = new ResponseSkillPanelTask();
		}

		task.skillPanelInfo.addAll(skillPanelInfo);

		return task;
	}

	/** список размещений умений на панели */
	private final Array<SkillPanelInfo> skillPanelInfo;

	public ResponseSkillPanelTask() {
		super(PacketTaskManager.EXECUTE_LIMIT);
		this.skillPanelInfo = Arrays.toArray(SkillPanelInfo.class);
	}

	@Override
	protected boolean executeImpl(LocalObjects local, long currentTime) {

		final GameUIController controler = GameUIController.getInstance();
		controler.notifyEvent(UpdateSkillPanelEvent.get(skillPanelInfo));

		return true;
	}

	@Override
	public void finalyze() {
		skillPanelInfo.apply(INFO_FOLD);
		skillPanelInfo.clear();
	}

	@Override
	public void fold() {
		POOL.put(this);
	}

	@Override
	public void reinit() {
	}
}
