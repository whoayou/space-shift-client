package com.ss.client.network.packet.server;

import com.ss.client.manager.PacketTaskManager;
import com.ss.client.model.FriendStatus;
import com.ss.client.model.SpaceObject;
import com.ss.client.model.space.SpaceLocation;
import com.ss.client.network.ServerPacket;
import com.ss.client.network.packet.server.task.ObjectFriendStatusTask;

/**
 * Пакет со статусом дружелбности объекта.
 * 
 * @author Ronn
 */
public class ObjectFriendStatus extends ServerPacket {

	private static final SpaceLocation SPACE_LOCATION = SpaceLocation.getInstance();
	private static final PacketTaskManager TASK_MANAGER = PacketTaskManager.getInstance();

	/** статус дружелюбности */
	private FriendStatus status;

	/** уникальный ид объекта */
	private long objectId;

	@Override
	protected void readImpl() {
		objectId = readLong();
		status = FriendStatus.valueOf(readByte());
	}

	@Override
	protected void runImpl() {

		final SpaceObject object = SPACE_LOCATION.getObject(objectId);

		if(object == null) {
			return;
		}

		object.setFriendStatus(status);

		TASK_MANAGER.addUITask(ObjectFriendStatusTask.getInstance(object));
	}
}
