package com.ss.client.network.packet.server;

import com.ss.client.manager.PacketTaskManager;
import com.ss.client.network.ServerPacket;
import com.ss.client.network.packet.server.task.QuestCounterTask;

/**
 * Серверный пакет с новым значением счетчика.
 * 
 * @author Ronn
 */
public class QuestCounterInfo extends ServerPacket {

	private static final PacketTaskManager TASK_MANAGER = PacketTaskManager.getInstance();

	/** ид задания */
	private int questId;
	/** уникальный ид задания */
	private int objectId;
	/** ид счетчика */
	private int counterId;
	/** текущее значение */
	private int value;

	@Override
	protected void readImpl() {
		objectId = readInt();
		questId = readInt();
		counterId = readInt();
		value = readInt();
	}

	@Override
	protected void runImpl() {
		TASK_MANAGER.addUITask(QuestCounterTask.get(questId, objectId, counterId, value));
	}
}
