package com.ss.client.network.packet.server;

import rlib.util.array.Array;
import rlib.util.array.Arrays;

import com.ss.client.gui.controller.game.window.storage.CellInfo;
import com.ss.client.manager.PacketTaskManager;
import com.ss.client.network.ServerPacket;
import com.ss.client.network.packet.server.task.StorageInfoTask;
import com.ss.client.table.ItemTable;

/**
 * Информация о содержании хранилища корабля игрока.
 * 
 * @author Ronn
 */
public class StorageInfo extends ServerPacket {

	private static final PacketTaskManager TASK_MANAGER = PacketTaskManager.getInstance();
	private static final ItemTable ITEM_TABLE = ItemTable.getInstance();

	/** контейнер информаций о ячейках */
	private final Array<CellInfo> container;

	public StorageInfo() {
		this.container = Arrays.toArray(CellInfo.class);
	}

	@Override
	public void finalyze() {
		container.clear();
	}

	/**
	 * @return контейнер информаций о ячейках.
	 */
	private Array<CellInfo> getContainer() {
		return container;
	}

	@Override
	public boolean isSynchronized() {
		return true;
	}

	@Override
	protected void readImpl() {

		Array<CellInfo> container = getContainer();

		int size = readInt();

		for(int i = 0; i < size; i++) {

			CellInfo info = CellInfo.newInstance();
			info.setIndex(i);

			container.add(info);

			if(readByte() == 0) {
				continue;
			}

			info.setObjectId(readLong());
			info.setTemplateId(readInt());
			info.setItemCount(readLong());
		}
	}

	@Override
	protected void runImpl() {

		for(CellInfo info : getContainer().array()) {

			if(info == null) {
				break;
			}

			if(info.getTemplateId() == 0) {
				continue;
			}

			ITEM_TABLE.getTemplate(info.getTemplateId());
		}

		TASK_MANAGER.addUITask(StorageInfoTask.getInstance(getContainer()));
	}
}
