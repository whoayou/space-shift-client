package com.ss.client.network.packet.server.task;

import rlib.util.pools.FoldablePool;
import rlib.util.pools.Pools;

import com.ss.client.manager.PacketTaskManager;
import com.ss.client.model.module.ShotModule;
import com.ss.client.model.shots.Shot;
import com.ss.client.model.space.SpaceLocation;
import com.ss.client.util.FoldablePacketTask;
import com.ss.client.util.LimitedPacketTask;
import com.ss.client.util.LocalObjects;

/**
 * Пакетная задача для удаления выстрела.
 * 
 * @author Ronn
 */
public class ShotEndTask extends LimitedPacketTask implements FoldablePacketTask {

	private static final SpaceLocation SPACE_LOCATION = SpaceLocation.getInstance();

	private static final FoldablePool<ShotEndTask> POOL = Pools.newConcurrentFoldablePool(ShotEndTask.class);

	public static final ShotEndTask getInstance(int objectId, boolean destruct) {

		ShotEndTask task = POOL.take();

		if(task == null) {
			task = new ShotEndTask();
		}

		task.objectId = objectId;
		task.destruct = destruct;

		return task;
	}

	/** уникальный ид выстрела */
	private int objectId;

	/** уничтожение выстрела */
	private boolean destruct;

	public ShotEndTask() {
		super(PacketTaskManager.EXECUTE_LIMIT);
	}

	@Override
	protected boolean executeImpl(LocalObjects local, long currentTime) {

		Shot shot = SPACE_LOCATION.getShot(objectId);

		if(shot == null) {
			return true;
		}

		if(destruct) {
			ShotModule module = shot.getModule();
			module.destruct(shot, LocalObjects.get());
		}

		shot.stop();
		return true;
	}

	@Override
	public void finalyze() {
	}

	@Override
	public void fold() {
		POOL.put(this);
	}

	@Override
	public void reinit() {
	}
}
