package com.ss.client.network.packet.server;

import rlib.concurrent.ConcurrentUtils;
import rlib.util.VarTable;

import com.ss.client.table.GravityObjectTable;
import com.ss.client.template.GravityObjectTemplate;

/**
 * Серверный пакет с инфой о шаблоне гравитационного объекта.
 * 
 * @author Ronn
 */
public class ResponseGravityObjectTemplate extends ResponseObjectTemplate {

	@Override
	protected void readImpl() {
		super.readImpl();

		final VarTable vars = getVars();
		vars.set(GravityObjectTemplate.BRIGHTNESS, readInt());
		vars.set(GravityObjectTemplate.RADIUS, readInt());
	}

	@Override
	protected void runImpl() {

		GravityObjectTable objectTable = GravityObjectTable.getInstance();
		objectTable.addTemplate(new GravityObjectTemplate(getVars()));

		ConcurrentUtils.notifyAll(objectTable);
	}
}
