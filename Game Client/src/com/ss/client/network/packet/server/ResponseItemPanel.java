package com.ss.client.network.packet.server;

import rlib.util.array.Array;
import rlib.util.array.Arrays;

import com.ss.client.gui.model.ItemPanelInfo;
import com.ss.client.manager.PacketTaskManager;
import com.ss.client.network.ServerPacket;
import com.ss.client.network.packet.server.task.ResponseItemPanelTask;

/**
 * Пакет с настройками расположения предметов на панели.
 * 
 * @author Ronn
 */
public class ResponseItemPanel extends ServerPacket {

	private static final PacketTaskManager TASK_MANAGER = PacketTaskManager.getInstance();

	private final Array<ItemPanelInfo> itemPanelInfo;

	public ResponseItemPanel() {
		this.itemPanelInfo = Arrays.toArray(ItemPanelInfo.class);
	}

	@Override
	public void finalyze() {
		itemPanelInfo.clear();
	}

	@Override
	protected void readImpl() {

		final int size = readByte();

		for(int i = 0; i < size; i++) {
			itemPanelInfo.add(ItemPanelInfo.newInstance(readLong(), readByte()));
		}
	}

	@Override
	protected void runImpl() {
		if(!itemPanelInfo.isEmpty()) {
			TASK_MANAGER.addUITask(ResponseItemPanelTask.getInstance(itemPanelInfo));
		}
	}
}
