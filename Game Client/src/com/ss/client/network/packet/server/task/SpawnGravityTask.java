package com.ss.client.network.packet.server.task;

import rlib.util.pools.FoldablePool;
import rlib.util.pools.Pools;

import com.ss.client.Game;
import com.ss.client.manager.PacketTaskManager;
import com.ss.client.model.gravity.GravityObject;
import com.ss.client.model.space.SpaceLocation;
import com.ss.client.model.state.GameState;
import com.ss.client.model.state.StateId;
import com.ss.client.network.Network;
import com.ss.client.network.packet.client.RequestForceSyncObject;
import com.ss.client.util.FoldablePacketTask;
import com.ss.client.util.LimitedPacketTask;
import com.ss.client.util.LocalObjects;

/**
 * Задача по отложенному спавну гравитационного объекта.
 * 
 * @author Ronn
 */
public class SpawnGravityTask extends LimitedPacketTask implements FoldablePacketTask {

	private static final FoldablePool<SpawnGravityTask> POOL = Pools.newConcurrentFoldablePool(SpawnGravityTask.class);

	private static final StateId GAME_STATE = StateId.GAME_STATE;

	private static final SpaceLocation SPACE_LOCATION = SpaceLocation.getInstance();
	private static final Game GAME = Game.getInstance();

	public static final SpawnGravityTask getInstance(GravityObject object, long parentId) {

		SpawnGravityTask task = POOL.take();

		if(task == null) {
			task = new SpawnGravityTask();
		}

		task.object = object;
		task.parentId = parentId;

		return task;
	}

	/** гравитационный объект */
	private GravityObject object;

	/** ид объекта, вокруг которого вращается этот гравитационный объект */
	private long parentId;

	public SpawnGravityTask() {
		super(PacketTaskManager.EXECUTE_LIMIT);
	}

	@Override
	protected boolean executeImpl(LocalObjects local, long currentTime) {

		final GravityObject parent = SPACE_LOCATION.getObject(getParentId());

		if(parent == null) {
			return false;
		}

		GameState state = GAME_STATE.getState();
		Network network = Network.getInstance();

		GravityObject object = getObject();

		parent.addChild(object);
		object.setParent(parent);
		object.setParentNode(state.getGravityState());

		GAME.syncLock();
		try {
			object.spawnMe();
		} finally {
			GAME.syncUnlock();
		}

		network.sendPacketToGameServer(RequestForceSyncObject.getInstance(object));
		return true;
	}

	@Override
	public void finalyze() {
		this.object = null;
	}

	@Override
	public void fold() {
		POOL.put(this);
	}

	/**
	 * @return гравитационный объект.
	 */
	public GravityObject getObject() {
		return object;
	}

	/**
	 * @return ид объекта, вокруг которого вращается этот гравитационный объект.
	 */
	public long getParentId() {
		return parentId;
	}

	@Override
	public void reinit() {
	}
}
