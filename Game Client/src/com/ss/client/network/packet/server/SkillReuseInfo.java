package com.ss.client.network.packet.server;

import com.ss.client.Game;
import com.ss.client.manager.PacketTaskManager;
import com.ss.client.network.ServerPacket;
import com.ss.client.network.packet.server.task.SkillReuseInfoTask;

/**
 * Пакет с информацией о откатсе скила.
 * 
 * @author Ronn
 */
public class SkillReuseInfo extends ServerPacket {

	private static final PacketTaskManager TASK_MANAGER = PacketTaskManager.getInstance();

	/** уникальный ид скила */
	private int objectId;
	/** ид шаблона скила */
	private int templateId;
	/** время отката скила */
	private int reuseTime;

	@Override
	protected void readImpl() {
		objectId = readInt();
		templateId = readInt();
		reuseTime = readInt();
	}

	@Override
	protected void runImpl() {
		TASK_MANAGER.addUITask(SkillReuseInfoTask.getInstance(Game.getCurrentTime(), objectId, templateId, reuseTime));
	}
}
