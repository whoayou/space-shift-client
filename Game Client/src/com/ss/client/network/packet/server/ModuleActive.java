package com.ss.client.network.packet.server;

import com.ss.client.manager.PacketTaskManager;
import com.ss.client.model.skills.Skill;
import com.ss.client.network.ServerPacket;
import com.ss.client.network.packet.server.task.ModuleActivateTask;
import com.ss.client.table.SkillTable;

/**
 * Пакет с уведомлением о изменении активности модуля.
 * 
 * @author Ronn
 */
public class ModuleActive extends ServerPacket {

	private static final PacketTaskManager TASK_MANAGER = PacketTaskManager.getInstance();
	private static final SkillTable SKILL_TABLE = SkillTable.getInstance();

	/** ид корабля */
	private long shipId;
	/** ид модуля */
	private long moduleId;

	/** ид скила */
	private int skillId;
	/** активность модуля */
	private int active;

	@Override
	public boolean isSynchronized() {
		return true;
	}

	@Override
	protected void readImpl() {
		shipId = readLong();
		moduleId = readLong();
		skillId = readInt();
		active = readByte();
	}

	@Override
	protected void runImpl() {

		final Skill skill = SKILL_TABLE.getInstance(skillId);

		if(skill == null) {
			return;
		}

		TASK_MANAGER.addSyncTask(ModuleActivateTask.getInstance(shipId, moduleId, skillId, active));
	}
}
