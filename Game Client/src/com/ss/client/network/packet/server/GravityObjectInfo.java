package com.ss.client.network.packet.server;

import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.ss.client.Game;
import com.ss.client.manager.PacketTaskManager;
import com.ss.client.model.gravity.GravityObject;
import com.ss.client.model.space.SpaceLocation;
import com.ss.client.model.state.GameState;
import com.ss.client.model.state.StateId;
import com.ss.client.network.Network;
import com.ss.client.network.ServerPacket;
import com.ss.client.network.packet.client.RequestForceSyncObject;
import com.ss.client.network.packet.server.task.SpawnGravityTask;
import com.ss.client.table.GravityObjectTable;
import com.ss.client.template.GravityObjectTemplate;

/**
 * Пакет с информацией о гравитационных объектах..
 * 
 * @author Ronn
 */
public class GravityObjectInfo extends ServerPacket {

	private static final StateId GAME_STATE = StateId.GAME_STATE;

	private static final PacketTaskManager TASK_MANAGER = PacketTaskManager.getInstance();
	private static final SpaceLocation SPACE_LOCATION = SpaceLocation.getInstance();
	private static final GravityObjectTable GRAVITY_OBJECT_TABLE = GravityObjectTable.getInstance();
	private static final Game GAME = Game.getInstance();

	/** позиция объекта */
	private final Vector3f location;
	/** разворот орбиты */
	private final Quaternion rotation;

	/** уникальный ид станции */
	private long objectId;
	/** ид владельца объекта */
	private long parentId;

	/** ид темплейта */
	private int templateId;
	/** радиус */
	private int radius;
	/** дистанция от центра */
	private int distance;
	/** время разворота вокруг своей оси */
	private int turnTime;

	/** время разворота вокруг орбиты */
	private long turnAroundTime;

	public GravityObjectInfo() {
		this.location = new Vector3f();
		this.rotation = new Quaternion();
	}

	@Override
	public boolean isSynchronized() {
		return true;
	}

	@Override
	protected void readImpl() {
		objectId = readLong();
		templateId = readInt();
		parentId = readLong();
		radius = readInt();
		distance = readInt();
		turnTime = readInt();
		turnAroundTime = readLong();

		location.set(readFloat(), readFloat(), readFloat());
		rotation.set(readFloat(), readFloat(), readFloat(), readFloat());
	}

	@Override
	protected void runImpl() {

		final Network network = Network.getInstance();
		final GameState state = GAME_STATE.getState();

		if(!state.isInitialized()) {
			return;
		}

		final GravityObjectTemplate template = GRAVITY_OBJECT_TABLE.getTemplate(templateId);

		if(template == null) {
			LOGGER.warning(this, "not found template " + templateId);
			return;
		}

		final GravityObject object = template.takeInstance(objectId);

		object.setOrbit(radius, turnAroundTime, turnTime, distance, rotation);
		object.setLocation(location, Vector3f.ZERO);
		object.setRotation(rotation);

		if(parentId > 0) {

			final GravityObject parent = SPACE_LOCATION.getObject(parentId);

			if(parent == null) {
				LOGGER.warning(getClass(), "not found parent for objectId " + objectId);
				TASK_MANAGER.addGeomTask(SpawnGravityTask.getInstance(object, parentId));
				return;
			}

			parent.addChild(object);
			object.setParent(parent);
		}

		object.setParentNode(state.getGravityState());

		GAME.syncLock();
		try {
			object.spawnMe();
		} finally {
			GAME.syncUnlock();
		}

		network.sendPacketToGameServer(RequestForceSyncObject.getInstance(object));
	}

	@Override
	public String toString() {
		return "GravityObjectInfo location = " + location + ",  rotation = " + rotation + ",  objectId = " + objectId + ",  parentId = " + parentId + ",  templateId = " + templateId + ",  radius = "
				+ radius + ",  distance = " + distance + ",  turnTime = " + turnTime + ",  turnAroundTime = " + turnAroundTime;
	}
}
