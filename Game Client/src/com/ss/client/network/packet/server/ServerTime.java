package com.ss.client.network.packet.server;

import com.ss.client.Game;
import com.ss.client.network.ServerPacket;

/**
 * Пакет в текущим серверным временем, что бы синхронизоваться с клиентом.
 * 
 * @author Ronn
 */
public class ServerTime extends ServerPacket {

	/** текущее серврное время */
	private long serverTime;

	@Override
	protected void readImpl() {
		serverTime = readLong();
	}

	@Override
	protected void runImpl() {
		Game.setTimeOffset(System.currentTimeMillis() - serverTime);
	}
}
