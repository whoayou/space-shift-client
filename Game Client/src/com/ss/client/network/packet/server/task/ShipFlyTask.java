package com.ss.client.network.packet.server.task;

import rlib.util.pools.FoldablePool;
import rlib.util.pools.Pools;

import com.jme3.math.Vector3f;
import com.ss.client.manager.PacketTaskManager;
import com.ss.client.model.ship.SpaceShip;
import com.ss.client.model.space.SpaceLocation;
import com.ss.client.network.packet.server.ShipFly;
import com.ss.client.util.FoldablePacketTask;
import com.ss.client.util.LimitedPacketTask;
import com.ss.client.util.LocalObjects;

/**
 * Пакетная задача для обновления движения корабля.
 * 
 * @author Ronn
 */
public class ShipFlyTask extends LimitedPacketTask implements FoldablePacketTask {

	private static final SpaceLocation SPACE_LOCATION = SpaceLocation.getInstance();

	private static final FoldablePool<ShipFlyTask> POOL = Pools.newConcurrentFoldablePool(ShipFlyTask.class);

	public static final ShipFlyTask getInstance(ShipFly packet) {

		ShipFlyTask task = POOL.take();

		if(task == null) {
			task = new ShipFlyTask();
		}

		task.location.set(packet.getLocation());

		task.objectId = packet.getObjectId();
		task.accel = packet.getAccel();
		task.speed = packet.getSpeed();
		task.maxSpeed = packet.getMaxSpeed();
		task.maxCurrentSpeed = packet.getMaxCurrentSpeed();
		task.active = packet.isActive();
		task.force = packet.isForce();

		return task;
	}

	/** позиция корабля */
	private final Vector3f location;

	/** ид корабля */
	private long objectId;

	/** ускорение */
	private float accel;
	/** текущая скорость */
	private float speed;
	/** максимальная скорость */
	private float maxSpeed;
	/** максимальная текущая скорость */
	private float maxCurrentSpeed;

	/** активны ли двигатели */
	private boolean active;
	/** принудительная ли синхронизация */
	private boolean force;

	public ShipFlyTask() {
		super(PacketTaskManager.EXECUTE_LIMIT);
		this.location = new Vector3f();
	}

	@Override
	protected boolean executeImpl(LocalObjects local, long currentTime) {

		final SpaceShip ship = SPACE_LOCATION.getObject(objectId);

		if(ship != null) {
			ship.fly(location, accel, speed, maxSpeed, maxCurrentSpeed, active, force);
			return true;
		}

		return false;
	}

	@Override
	public void finalyze() {
	}

	@Override
	public void fold() {
		POOL.put(this);
	}

	@Override
	public boolean isMatches(Object object) {
		return objectId == ((SpaceShip) object).getObjectId();
	}

	@Override
	public void reinit() {
	}
}
