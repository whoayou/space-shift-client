package com.ss.client.network.packet.server;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.ss.client.Game;
import com.ss.client.model.FriendStatus;
import com.ss.client.model.module.Module;
import com.ss.client.model.module.system.ModuleSystem;
import com.ss.client.model.ship.nps.Nps;
import com.ss.client.model.state.GameState;
import com.ss.client.model.state.StateId;
import com.ss.client.network.Network;
import com.ss.client.network.ServerPacket;
import com.ss.client.network.packet.client.RequestForceSyncObject;
import com.ss.client.network.packet.client.RequestObjectFriendStatus;
import com.ss.client.table.ModuleTable;
import com.ss.client.table.ShipTable;
import com.ss.client.template.ModuleTemplate;
import com.ss.client.template.ship.NpsTemplate;
import com.ss.client.util.GameUtil;

/**
 * Пакет с информацией о новом Nps в космосе.
 * 
 * @author Ronn
 */
public class NpsInfo extends ServerPacket {

	private static final StateId GAME_STATE = StateId.GAME_STATE;

	private static final ShipTable SHIP_TABLE = ShipTable.getInstance();
	private static final ModuleTable MODULE_TABLE = ModuleTable.getInstance();
	private static final Game GAME = Game.getInstance();

	/** позиция, на которой остался корабль */
	private final Vector3f location;
	/** разворот корабля */
	private final Quaternion rotation;

	/** буффер с сохраненными данными */
	private final ByteBuffer saveBuffer;

	public NpsInfo() {
		this.location = new Vector3f();
		this.rotation = new Quaternion();
		this.saveBuffer = ByteBuffer.allocate(4096).order(ByteOrder.LITTLE_ENDIAN);
	}

	private final ByteBuffer getSaveBuffer() {
		return saveBuffer;
	}

	@Override
	public boolean isSynchronized() {
		return true;
	}

	@Override
	protected void readImpl() {

		final ByteBuffer saveBuffer = getSaveBuffer();
		final ByteBuffer buffer = getBuffer();

		saveBuffer.clear();
		saveBuffer.put(buffer.array(), buffer.position(), Math.min(buffer.remaining(), saveBuffer.capacity()));
		saveBuffer.flip();

		LOGGER.info(this, "read nps info");
	}

	@Override
	protected void runImpl() {

		final GameState state = GAME_STATE.getState();

		GameUtil.waitInitialize(state);

		setBuffer(getSaveBuffer());

		final long objectId = readLong();

		int templateId = readInt();

		final NpsTemplate template = SHIP_TABLE.getTemplate(templateId);

		if(template == null) {
			LOGGER.warning(this, new Exception("not found template for id " + templateId));
			return;
		}

		final Nps nps = template.takeInstance(objectId);
		final ModuleSystem moduleSystem = nps.getModuleSystem();

		if(moduleSystem == null) {
			LOGGER.warning(this, new Exception("not found module system"));
			return;
		}

		nps.setFriendStatus(FriendStatus.valueOf(readByte()));

		for(int i = 0, length = readByte(); i < length; i++) {

			final long moduleId = readLong();

			templateId = readInt();

			final ModuleTemplate moduleTemplate = MODULE_TABLE.getTemplate(templateId);

			final int index = readByte();

			if(moduleTemplate == null) {
				LOGGER.warning(this, new Exception("not found module for id " + templateId));
				continue;
			}

			moduleSystem.setModule(index, (Module) moduleTemplate.takeInstance(moduleId));
		}

		location.setX(readFloat());
		location.setY(readFloat());
		location.setZ(readFloat());

		rotation.set(readFloat(), readFloat(), readFloat(), readFloat());

		GAME.syncLock();
		try {

			nps.setParentNode(state.getObjectState());
			nps.setLocation(location, Vector3f.ZERO);
			nps.setRotation(rotation);
			nps.spawnMe();

		} finally {
			GAME.syncUnlock();
		}

		LOGGER.info(this, "spawn " + nps.getName());

		Network network = Network.getInstance();
		network.sendPacketToGameServer(RequestForceSyncObject.getInstance(nps));
		network.sendPacketToGameServer(RequestObjectFriendStatus.getInstance(nps));
	}
}
