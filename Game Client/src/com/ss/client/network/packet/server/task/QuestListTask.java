package com.ss.client.network.packet.server.task;

import rlib.util.array.Array;
import rlib.util.array.Arrays;
import rlib.util.pools.FoldablePool;
import rlib.util.pools.Pools;

import com.ss.client.gui.controller.game.GameUIController;
import com.ss.client.gui.controller.game.event.impl.UpdateQuestListEvent;
import com.ss.client.gui.model.quest.QuestState;
import com.ss.client.manager.PacketTaskManager;
import com.ss.client.util.FoldablePacketTask;
import com.ss.client.util.LimitedPacketTask;
import com.ss.client.util.LocalObjects;

/**
 * Пакетная задача для внесение списка заданий в журнал.
 * 
 * @author Ronn
 */
public class QuestListTask extends LimitedPacketTask implements FoldablePacketTask {

	private static final FoldablePool<QuestListTask> POOL = Pools.newConcurrentFoldablePool(QuestListTask.class);

	public static final QuestListTask getInstance(Array<QuestState> container) {

		QuestListTask task = POOL.take();

		if(task == null) {
			task = new QuestListTask();
		}

		task.container.addAll(container);

		return task;
	}

	/** контейнер заданий */
	private final Array<QuestState> container;

	public QuestListTask() {
		super(PacketTaskManager.EXECUTE_LIMIT);
		this.container = Arrays.toArray(QuestState.class);
	}

	@Override
	protected boolean executeImpl(LocalObjects local, long currentTime) {
		GameUIController controller = GameUIController.getInstance();
		controller.notifyEvent(UpdateQuestListEvent.get(container));
		return true;
	}

	@Override
	public void finalyze() {
		container.clear();
	}

	@Override
	public void fold() {
		POOL.put(this);
	}

	@Override
	public void reinit() {
	}
}
