package com.ss.client.network.packet.server;

import rlib.concurrent.ConcurrentUtils;
import rlib.util.VarTable;

import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.ss.client.model.location.BackgroundInfo;
import com.ss.client.model.location.BackgroundInfo.BackgroundSide;
import com.ss.client.model.location.LightInfo;
import com.ss.client.model.location.LightInfo.LightType;
import com.ss.client.model.location.LocationInfo;
import com.ss.client.network.ServerPacket;
import com.ss.client.table.LocationTable;

/**
 * Серверный пакет с информацией о локации.
 * 
 * @author Ronn
 */
public class ResponseLocationInfo extends ServerPacket {

	private final VarTable vars;

	public ResponseLocationInfo() {
		vars = VarTable.newInstance();
	}

	public VarTable getVars() {
		return vars;
	}

	private BackgroundInfo readBackgroundInfo() {

		final VarTable vars = VarTable.newInstance();

		final int count = readByte();

		for(int i = 0; i < count; i++) {
			final BackgroundSide type = BackgroundSide.valueOf(readByte());
			vars.set(type.name(), readString(readInt()));
		}

		return new BackgroundInfo(vars);
	}

	private ColorRGBA readColor() {
		return new ColorRGBA(readFloat(), readFloat(), readFloat(), 1);
	}

	@Override
	protected void readImpl() {

		final VarTable vars = getVars();

		vars.set(LocationInfo.ID, readInt());
		vars.set(LocationInfo.BACKGROUND, readBackgroundInfo());
		vars.set(LocationInfo.VECTOR_STAR, new Vector3f(readFloat(), readFloat(), readFloat()));
		vars.set(LocationInfo.STAR_LIGHT_COLOR, readColor());
		vars.set(LocationInfo.SHADOW_LIGHTCOLOR, readColor());
		vars.set(LocationInfo.AMBIENT_COLOR, readColor());
		vars.set(LocationInfo.LIGHTS, readLightInfos());
	}

	private LightInfo[] readLightInfos() {

		final VarTable vars = VarTable.newInstance();

		final int count = readByte();

		final LightInfo[] infos = new LightInfo[count];

		for(int i = 0; i < count; i++) {

			vars.clear();
			vars.set(LightInfo.TYPE, LightType.VALUES[readByte()]);
			vars.set(LightInfo.RADIUS, readInt());
			vars.set(LightInfo.COLOR, readColor());
			vars.set(LightInfo.POSITION, new Vector3f(readFloat(), readFloat(), readFloat()));
			vars.set(LightInfo.MAIN, readByte() == 1);

			infos[i] = new LightInfo(vars);
		}

		return infos;
	}

	@Override
	public void reinit() {
		vars.clear();
	}

	@Override
	protected void runImpl() {

		LocationTable locationTable = LocationTable.getInstance();
		locationTable.addLocation(new LocationInfo(getVars()));

		ConcurrentUtils.notifyAll(locationTable);
	}
}
