package com.ss.client.network.packet.server;

import com.jme3.math.Vector3f;
import com.ss.client.model.ship.SpaceShip;
import com.ss.client.model.space.SpaceLocation;
import com.ss.client.network.ServerPacket;

/**
 * Пакет с уведомлением о выключении двигателя у корабля.
 * 
 * @author Ronn
 */
public class ShipFly extends ServerPacket {

	private static final SpaceLocation SPACE_LOCATION = SpaceLocation.getInstance();

	/** позиция корабля */
	private final Vector3f location;

	/** ид корабля */
	private long objectId;

	/** ускорение */
	private float accel;
	/** текущая скорость */
	private float speed;
	/** максимальная скорость */
	private float maxSpeed;
	/** максимальная текущая скорость */
	private float maxCurrentSpeed;

	/** активны ли двигатели */
	private boolean active;
	/** принудительная ли синхронизация */
	private boolean force;

	public ShipFly() {
		this.location = new Vector3f();
	}

	/**
	 * @return ускорение.
	 */
	public float getAccel() {
		return accel;
	}

	/**
	 * @return позиция корабля.
	 */
	public Vector3f getLocation() {
		return location;
	}

	/**
	 * @return максимальная текущая скорость.
	 */
	public float getMaxCurrentSpeed() {
		return maxCurrentSpeed;
	}

	/**
	 * @return максимальная скорость.
	 */
	public float getMaxSpeed() {
		return maxSpeed;
	}

	/**
	 * @return ид корабля.
	 */
	public long getObjectId() {
		return objectId;
	}

	/**
	 * @return текущая скорость.
	 */
	public float getSpeed() {
		return speed;
	}

	/**
	 * @return активны ли двигатели.
	 */
	public boolean isActive() {
		return active;
	}

	/**
	 * @return принудительная ли синхронизация.
	 */
	public boolean isForce() {
		return force;
	}

	@Override
	protected void readImpl() {

		objectId = readLong();

		accel = readFloat();
		speed = readFloat();
		maxSpeed = readFloat();
		maxCurrentSpeed = readFloat();

		location.set(readFloat(), readFloat(), readFloat());

		active = readByte() == 1;
		force = readByte() == 1;
	}

	@Override
	protected void runImpl() {

		final SpaceShip ship = SPACE_LOCATION.getObject(objectId);

		if(ship == null) {
			return;
		}

		ship.fly(location, accel, speed, maxSpeed, maxCurrentSpeed, active, force);
	}
}
