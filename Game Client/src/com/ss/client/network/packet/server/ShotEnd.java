package com.ss.client.network.packet.server;

import com.ss.client.manager.PacketTaskManager;
import com.ss.client.network.ServerPacket;
import com.ss.client.network.packet.server.task.ShotEndTask;

/**
 * Пакет с уведомление о завершении полета выстрела.
 * 
 * @author Ronn
 */
public class ShotEnd extends ServerPacket {

	private static final PacketTaskManager TASK_MANAGER = PacketTaskManager.getInstance();

	/** уникальный ид выстрела */
	private int objectId;

	/** уничтожение выстрела */
	private boolean destruct;

	@Override
	protected void readImpl() {
		objectId = readInt();
		destruct = readByte() == 1;
	}

	@Override
	protected void runImpl() {
		TASK_MANAGER.addSyncTask(ShotEndTask.getInstance(objectId, destruct));
	}
}
