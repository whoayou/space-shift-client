package com.ss.client.network.packet.server.task;

import rlib.util.pools.FoldablePool;
import rlib.util.pools.Pools;

import com.ss.client.manager.PacketTaskManager;
import com.ss.client.model.module.Module;
import com.ss.client.model.module.system.ModuleSystem;
import com.ss.client.model.ship.SpaceShip;
import com.ss.client.model.skills.Skill;
import com.ss.client.model.space.SpaceLocation;
import com.ss.client.table.SkillTable;
import com.ss.client.util.FoldablePacketTask;
import com.ss.client.util.LimitedPacketTask;
import com.ss.client.util.LocalObjects;

/**
 * Пакетная задача для отображение использование скила корабля.
 * 
 * @author Ronn
 */
public class ModuleActivateTask extends LimitedPacketTask implements FoldablePacketTask {

	private static final SpaceLocation SPACE_LOCATION = SpaceLocation.getInstance();
	private static final SkillTable SKILL_TABLE = SkillTable.getInstance();

	private static final FoldablePool<ModuleActivateTask> POOL = Pools.newConcurrentFoldablePool(ModuleActivateTask.class);

	public static final ModuleActivateTask getInstance(long shipId, long moduleId, int skillId, int active) {

		ModuleActivateTask task = POOL.take();

		if(task == null) {
			task = new ModuleActivateTask();
		}

		task.shipId = shipId;
		task.moduleId = moduleId;
		task.skillId = skillId;
		task.active = active;

		return task;
	}

	/** ид корабля */
	private long shipId;
	/** ид модуля */
	private long moduleId;

	/** ид скила */
	private int skillId;
	/** активность модуля */
	private int active;

	public ModuleActivateTask() {
		super(PacketTaskManager.EXECUTE_LIMIT);
	}

	@Override
	protected boolean executeImpl(LocalObjects local, long currentTime) {

		final Skill skill = SKILL_TABLE.getInstance(skillId);

		if(skill == null) {
			return true;
		}

		SpaceShip ship = SPACE_LOCATION.getObject(shipId);

		if(ship == null) {
			return false;
		}

		final ModuleSystem system = ship.getModuleSystem();
		final Module module = system.getModule(moduleId);

		if(module == null) {
			return false;
		}

		skill.useSkill(module, active);
		return true;
	}

	@Override
	public void finalyze() {
	}

	@Override
	public void fold() {
		POOL.put(this);
	}

	@Override
	public void reinit() {
	}
}
