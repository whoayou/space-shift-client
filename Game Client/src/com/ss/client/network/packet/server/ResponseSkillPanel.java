package com.ss.client.network.packet.server;

import rlib.util.array.Array;
import rlib.util.array.Arrays;

import com.ss.client.gui.model.SkillPanelInfo;
import com.ss.client.manager.PacketTaskManager;
import com.ss.client.network.ServerPacket;
import com.ss.client.network.packet.server.task.ResponseSkillPanelTask;

/**
 * Пакет с настройками расположения скилов на панели.
 * 
 * @author Ronn
 */
public class ResponseSkillPanel extends ServerPacket {

	private static final PacketTaskManager TASK_MANAGER = PacketTaskManager.getInstance();

	private final Array<SkillPanelInfo> skillPanelInfo;

	public ResponseSkillPanel() {
		this.skillPanelInfo = Arrays.toArray(SkillPanelInfo.class);
	}

	@Override
	public void finalyze() {
		skillPanelInfo.clear();
	}

	@Override
	protected void readImpl() {

		final int size = readByte();

		for(int i = 0; i < size; i++) {
			skillPanelInfo.add(SkillPanelInfo.newInstance(readInt(), readInt(), readByte()));
		}
	}

	@Override
	protected void runImpl() {
		if(!skillPanelInfo.isEmpty()) {
			TASK_MANAGER.addUITask(ResponseSkillPanelTask.getInstance(skillPanelInfo));
		}
	}
}
