package com.ss.client.network.packet.server.task;

import rlib.util.pools.FoldablePool;
import rlib.util.pools.Pools;

import com.ss.client.manager.PacketTaskManager;
import com.ss.client.model.SpaceObject;
import com.ss.client.model.module.Module;
import com.ss.client.model.module.ShotModule;
import com.ss.client.model.shots.Shot;
import com.ss.client.model.space.SpaceLocation;
import com.ss.client.util.FoldablePacketTask;
import com.ss.client.util.LimitedPacketTask;
import com.ss.client.util.LocalObjects;

/**
 * Пакетная задача для отображения попадания бластера.
 * 
 * @author Ronn
 */
public class ShotHitInfoTask extends LimitedPacketTask implements FoldablePacketTask {

	private static final SpaceLocation SPACE_LOCATION = SpaceLocation.getInstance();

	private static final FoldablePool<ShotHitInfoTask> POOL = Pools.newConcurrentFoldablePool(ShotHitInfoTask.class);

	public static final ShotHitInfoTask getInstance(long targetObjectId, int objectId, boolean onShield) {

		ShotHitInfoTask task = POOL.take();

		if(task == null) {
			task = new ShotHitInfoTask();
		}

		task.targetObjectId = targetObjectId;
		task.objectId = objectId;
		task.onShield = onShield;

		return task;
	}

	/** ид попавшей цели */
	private long targetObjectId;

	/** ид выстрела */
	private int objectId;

	/** было ли попадание в щит */
	private boolean onShield;

	public ShotHitInfoTask() {
		super(PacketTaskManager.EXECUTE_LIMIT);
	}

	@Override
	protected boolean executeImpl(LocalObjects local, long currentTime) {

		Shot shot = SPACE_LOCATION.getShot(objectId);

		if(shot == null) {
			return true;
		}

		Module module = shot.getModule();

		if(!(module instanceof ShotModule)) {
			return true;
		}

		SpaceObject target = SPACE_LOCATION.getObject(targetObjectId);

		if(target == null) {
			return true;
		}

		ShotModule shotModule = (ShotModule) module;
		shotModule.onHit(shot, target, onShield, local);
		return true;
	}

	@Override
	public void finalyze() {
	}

	@Override
	public void fold() {
		POOL.put(this);
	}

	@Override
	public void reinit() {
	}
}
