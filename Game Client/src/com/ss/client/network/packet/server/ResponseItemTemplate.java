package com.ss.client.network.packet.server;

import rlib.util.VarTable;

import com.ss.client.model.effect.GraficEffectType;
import com.ss.client.template.item.ItemTemplate;

/**
 * Серверный пакет с информацией о шаблоне предмета.
 * 
 * @author Ronn
 */
public abstract class ResponseItemTemplate extends ResponseObjectTemplate {

	@Override
	protected void readImpl() {
		super.readImpl();

		final VarTable vars = getVars();
		vars.set(ItemTemplate.NAME, readString(readByte()));
		vars.set(ItemTemplate.ICON, readString(readByte()));
		vars.set(ItemTemplate.DESCRIPTION, readString(readInt()));
		vars.set(ItemTemplate.STACKABLE, readByte() == 1);

		if(readByte() == 1) {
			vars.set(ItemTemplate.START_TELEPORT_EFFECT, GraficEffectType.newTemplate(this));
		}

		if(readByte() == 1) {
			vars.set(ItemTemplate.FINISH_TELEPORT_EFFECT, GraficEffectType.newTemplate(this));
		}
	}
}
