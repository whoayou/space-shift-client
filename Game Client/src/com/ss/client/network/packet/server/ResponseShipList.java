package com.ss.client.network.packet.server;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import rlib.util.array.Array;
import rlib.util.array.Arrays;

import com.ss.client.gui.controller.HangarController;
import com.ss.client.model.module.Module;
import com.ss.client.model.module.system.ModuleSystem;
import com.ss.client.model.ship.player.PlayerShip;
import com.ss.client.model.state.HangarState;
import com.ss.client.model.state.StateId;
import com.ss.client.network.ServerPacket;
import com.ss.client.table.ModuleTable;
import com.ss.client.table.ShipTable;
import com.ss.client.template.ModuleTemplate;
import com.ss.client.template.ship.PlayerShipTemplate;

/**
 * Серверный пакет с инфой о корабля на аккаунте игрока.
 * 
 * @author Ronn
 */
public class ResponseShipList extends ServerPacket {

	private static final ShipTable SHIP_TABLE = ShipTable.getInstance();
	private static final ModuleTable MODULE_TABLE = ModuleTable.getInstance();

	private static final StateId HANGAR_STATE = StateId.HANGAR_STATE;

	/** список кораблей игрока */
	private final Array<PlayerShip> shipList;

	/** буффер с сохраненными данными */
	private final ByteBuffer saveBuffer;

	public ResponseShipList() {
		this.shipList = Arrays.toArray(PlayerShip.class);
		this.saveBuffer = ByteBuffer.allocate(1024).order(ByteOrder.LITTLE_ENDIAN);
	}

	@Override
	public void finalyze() {
		shipList.clear();
		saveBuffer.clear();
	}

	/**
	 * @return сохраненный буффер данных.
	 */
	public ByteBuffer getSaveBuffer() {
		return saveBuffer;
	}

	/**
	 * @return список кораблей на аккаунте.
	 */
	public Array<PlayerShip> getShipList() {
		return shipList;
	}

	@Override
	public boolean isSynchronized() {
		return true;
	}

	@Override
	protected void readImpl() {

		final ByteBuffer saveBuffer = getSaveBuffer();
		final ByteBuffer buffer = getBuffer();

		saveBuffer.clear();
		saveBuffer.put(buffer.array(), buffer.position(), Math.min(buffer.remaining(), saveBuffer.remaining()));
		saveBuffer.flip();

		LOGGER.info(this, "read ship list");
	}

	@Override
	protected void runImpl() {

		LOGGER.info(this, "check hangar state");

		final HangarState state = (HangarState) HANGAR_STATE.getState();

		if(state == null || !state.isInitialized()) {
			// return;
		}

		final HangarController controller = HangarController.getInstance();

		// GameUtil.waitInitialize(controller);

		setBuffer(getSaveBuffer());

		final Array<PlayerShip> shipList = getShipList();

		for(int i = 0, count = readByte(); i < count; i++) {

			final long objectId = readLong();

			LOGGER.info(this, "read ship for objectId " + objectId);

			final PlayerShipTemplate template = SHIP_TABLE.getTemplate(readInt());
			final PlayerShip playerShip = template.takeInstance(objectId);

			playerShip.setName(readString(readByte()));

			LOGGER.info(this, "readed " + playerShip.getName());

			final ModuleSystem moduleSystem = playerShip.getModuleSystem();

			for(int g = 0, length = readByte(); g < length; g++) {

				final long moduleId = readLong();
				final int templateId = readInt();

				LOGGER.info(this, "read module for template id " + templateId);

				final ModuleTemplate moduleTemplate = MODULE_TABLE.getTemplate(templateId);

				final int index = readByte();

				if(moduleTemplate == null) {
					LOGGER.warning(this, "not found module for id " + templateId);
					continue;
				}

				moduleSystem.setModule(index, (Module) moduleTemplate.takeInstance(moduleId));
			}

			LOGGER.info(this, "finish reading ship" + playerShip.getName());

			shipList.add(playerShip);
		}

		state.playerShipClear();

		for(final PlayerShip playerShip : shipList.array()) {

			if(playerShip == null) {
				break;
			}

			LOGGER.info(this, "add ship" + playerShip.getName() + " to hangar");

			state.addPlayerShip(playerShip);
		}

		shipList.clear();
	}
}
