package com.ss.client.network.packet.server.task;

import rlib.util.pools.FoldablePool;
import rlib.util.pools.Pools;

import com.ss.client.gui.controller.game.GameUIController;
import com.ss.client.gui.controller.game.event.impl.ObjectReuseEvent;
import com.ss.client.gui.controller.game.panel.fast.FastPanelItem;
import com.ss.client.manager.PacketTaskManager;
import com.ss.client.util.FoldablePacketTask;
import com.ss.client.util.LimitedPacketTask;
import com.ss.client.util.LocalObjects;

/**
 * Пакетная задача для обновления отката умения.
 * 
 * @author Ronn
 */
public class SkillReuseInfoTask extends LimitedPacketTask implements FoldablePacketTask {

	private static final FoldablePool<SkillReuseInfoTask> POOL = Pools.newConcurrentFoldablePool(SkillReuseInfoTask.class);

	public static final SkillReuseInfoTask getInstance(long startReuseTime, int objectId, int templateId, int reuseTime) {

		SkillReuseInfoTask task = POOL.take();

		if(task == null) {
			task = new SkillReuseInfoTask();
		}

		task.startReuseTime = startReuseTime;
		task.objectId = objectId;
		task.templateId = templateId;
		task.reuseTime = reuseTime;

		return task;
	}

	/** дата старта отката */
	private long startReuseTime;

	/** уникальный ид скила */
	private int objectId;
	/** ид шаблона скила */
	private int templateId;
	/** время отката скила */
	private int reuseTime;

	public SkillReuseInfoTask() {
		super(PacketTaskManager.EXECUTE_LIMIT);
	}

	@Override
	protected boolean executeImpl(LocalObjects local, long currentTime) {

		GameUIController controller = GameUIController.getInstance();
		controller.notifyEvent(ObjectReuseEvent.get(objectId, templateId, reuseTime, FastPanelItem.OBJECT_TYPE_SKILL, startReuseTime));

		return true;
	}

	@Override
	public void finalyze() {
	}

	@Override
	public void fold() {
		POOL.put(this);
	}

	@Override
	public void reinit() {
	}
}
