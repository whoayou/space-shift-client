package com.ss.client.network.packet.server;

import com.ss.client.manager.PacketTaskManager;
import com.ss.client.network.ServerPacket;
import com.ss.client.network.packet.server.task.ShotHitInfoTask;

/**
 * Пакет с информацией о попадании выстрела.
 * 
 * @author Ronn
 */
public class ShotHitInfo extends ServerPacket {

	private static final PacketTaskManager TASK_MANAGER = PacketTaskManager.getInstance();

	/** ид попавшей цели */
	private long targetObjectId;

	/** ид выстрела */
	private int objectId;

	/** было ли попадание в щит */
	private boolean onShield;

	@Override
	protected void readImpl() {
		objectId = readInt();
		targetObjectId = readLong();
		onShield = readByte() == 1;
	}

	@Override
	protected void runImpl() {
		TASK_MANAGER.addSyncTask(ShotHitInfoTask.getInstance(targetObjectId, objectId, onShield));
	}
}
