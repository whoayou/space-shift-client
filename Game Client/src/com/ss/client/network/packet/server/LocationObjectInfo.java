package com.ss.client.network.packet.server;

import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.ss.client.Game;
import com.ss.client.model.location.object.LocationObject;
import com.ss.client.model.state.GameState;
import com.ss.client.model.state.StateId;
import com.ss.client.network.ServerPacket;
import com.ss.client.table.LocationObjectTable;
import com.ss.client.template.LocationObjectTemplate;
import com.ss.client.util.GameUtil;

/**
 * Пакет с информацией о новом локационном объекте в космосе.
 * 
 * @author Ronn
 */
public class LocationObjectInfo extends ServerPacket {

	private static final StateId GAME_STATE = StateId.GAME_STATE;

	private static final LocationObjectTable OBJECT_TABLE = LocationObjectTable.getInstance();
	private static final Game GAME = Game.getInstance();

	/** позиция локационного объекта */
	private final Vector3f location;
	/** разворот корабля */
	private final Quaternion rotation;

	/** уникальный ид объекта */
	private long objectId;

	/** ид шаблона объекта */
	private int templateId;

	public LocationObjectInfo() {
		this.location = new Vector3f();
		this.rotation = new Quaternion();
	}

	@Override
	public boolean isSynchronized() {
		return true;
	}

	@Override
	protected void readImpl() {
		objectId = readLong();
		templateId = readInt();
		location.set(readFloat(), readFloat(), readFloat());
		rotation.set(readFloat(), readFloat(), readFloat(), readFloat());
	}

	@Override
	protected void runImpl() {

		final GameState state = GAME_STATE.getState();
		GameUtil.waitInitialize(state);

		final LocationObjectTemplate template = OBJECT_TABLE.getTemplate(templateId);

		if(template == null) {
			LOGGER.warning(this, new Exception("not found template for id " + templateId));
			return;
		}

		final LocationObject item = template.takeInstance(objectId);

		GAME.syncLock();
		try {
			item.setParentNode(state.getObjectState());
			item.setLocation(location, Vector3f.ZERO);
			item.setRotation(rotation);
			item.spawnMe();
		} finally {
			GAME.syncUnlock();
		}
	}
}
