package com.ss.client.network.packet.server;

import com.jme3.math.Quaternion;
import com.ss.client.manager.PacketTaskManager;
import com.ss.client.network.ServerPacket;
import com.ss.client.network.packet.server.task.DebugObjectRotationTask;

/**
 * Пакет с разворотом дебага объекта.
 * 
 * @author Ronn
 */
public class DebugObjectRotation extends ServerPacket {

	private static final PacketTaskManager TASK_MANAGER = PacketTaskManager.getInstance();

	/** разворот дебага */
	private final Quaternion rotation;

	/** уникальный ид объекта */
	private long objectId;

	public DebugObjectRotation() {
		this.rotation = new Quaternion();
	}

	@Override
	protected void readImpl() {
		objectId = readLong();
		rotation.set(readFloat(), readFloat(), readFloat(), readFloat());
	}

	@Override
	protected void runImpl() {
		TASK_MANAGER.addGeomTask(DebugObjectRotationTask.getInstance(rotation, objectId));
	}
}
