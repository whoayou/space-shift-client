package com.ss.client.network.packet.server;

import com.ss.client.model.SpaceObject;
import com.ss.client.model.ship.player.PlayerShip;
import com.ss.client.model.space.SpaceLocation;
import com.ss.client.network.ServerPacket;

/**
 * Пакет с обновлением уровня объекта.
 * 
 * @author Ronn
 */
public class ChangeLevel extends ServerPacket {

	private static final SpaceLocation SPACE_LOCATION = SpaceLocation.getInstance();

	/** уникальный ид объекта */
	private long objectId;

	/** новый уровень объекта */
	private int level;

	@Override
	protected void readImpl() {
		this.objectId = readLong();
		this.level = readInt();
	}

	@Override
	protected void runImpl() {

		SpaceObject object = SPACE_LOCATION.getObject(objectId);

		if(object != null && object.isPlayerShip()) {
			PlayerShip ship = object.getPlayerShip();
			ship.setLevel(level);
		}
	}
}
