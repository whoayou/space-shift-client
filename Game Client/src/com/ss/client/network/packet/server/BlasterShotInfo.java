package com.ss.client.network.packet.server;

import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.ss.client.Game;
import com.ss.client.model.module.Module;
import com.ss.client.model.module.ShotModule;
import com.ss.client.model.module.system.ModuleSystem;
import com.ss.client.model.ship.SpaceShip;
import com.ss.client.model.shots.BlasterShot;
import com.ss.client.model.space.SpaceLocation;
import com.ss.client.network.ServerPacket;

/**
 * Серверный пакет с информацией о выпущеном бластерном выстреле.
 * 
 * @author Ronn
 */
public class BlasterShotInfo extends ServerPacket {

	private static final SpaceLocation SPACE_LOCATION = SpaceLocation.getInstance();
	private static final Game GAME = Game.getInstance();

	/** координаты старта и конца */
	private final Vector3f startLoc;
	private final Vector3f targetLoc;

	/** разворот выстрела */
	private final Quaternion rotation;

	/** текущая скорость выстрела */
	private float speed;

	/** ид стрелявшего корабля */
	private long shipObjectId;
	/** ид стрелявшего модуля */
	private long moduleObjectId;

	/** уникальный ид выстрела */
	private int objectId;
	/** максимальная дистанция выстрела */
	private int maxDistance;

	/** индекс ствола модуля */
	private int index;

	public BlasterShotInfo() {
		this.startLoc = new Vector3f();
		this.targetLoc = new Vector3f();
		this.rotation = new Quaternion();
	}

	@Override
	protected void readImpl() {

		shipObjectId = readLong();
		moduleObjectId = readLong();
		objectId = readInt();
		maxDistance = readInt();
		index = readInt();

		speed = readFloat();

		startLoc.set(readFloat(), readFloat(), readFloat());
		targetLoc.set(readFloat(), readFloat(), readFloat());
		rotation.set(readFloat(), readFloat(), readFloat(), readFloat());
	}

	@Override
	protected void runImpl() {

		SpaceShip ship = SPACE_LOCATION.getObject(shipObjectId);

		if(ship == null) {
			return;
		}

		ModuleSystem moduleSystem = ship.getModuleSystem();
		Module module = moduleSystem.getModule(moduleObjectId);

		if(module == null || !(module instanceof ShotModule)) {
			return;
		}

		ShotModule shotModule = (ShotModule) module;

		BlasterShot shot = (BlasterShot) shotModule.getNextShot(index);
		shot.setNode(ship.getParentNode());
		shot.setMaxDistance(maxDistance);
		shot.setObjectId(objectId);
		shot.setShooter(ship);
		shot.setSpeed(speed);
		shot.setStartLoc(startLoc);
		shot.setTargetLoc(targetLoc);
		shot.setRotation(rotation);
		shot.setIndex(index);
		shot.setModule((ShotModule) module);

		GAME.syncLock();
		try {
			shot.start();
		} finally {
			GAME.syncUnlock();
		}
	}
}
