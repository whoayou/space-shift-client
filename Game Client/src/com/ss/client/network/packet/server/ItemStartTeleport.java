package com.ss.client.network.packet.server;

import com.ss.client.model.item.SpaceItem;
import com.ss.client.model.item.SpaceItemView;
import com.ss.client.model.space.SpaceLocation;
import com.ss.client.model.state.GameState;
import com.ss.client.model.state.StateId;
import com.ss.client.network.ServerPacket;
import com.ss.client.util.GameUtil;

/**
 * Пакет с уведомлением о старте телепорта предмета.
 * 
 * @author Ronn
 */
public class ItemStartTeleport extends ServerPacket {

	private static final SpaceLocation SPACE_LOCATION = SpaceLocation.getInstance();
	private static final StateId GAME_STATE = StateId.GAME_STATE;

	/** уникальный ид предмета */
	private long objectId;

	@Override
	protected void readImpl() {
		objectId = readLong();
	}

	@Override
	protected void runImpl() {

		final GameState state = GAME_STATE.getState();

		GameUtil.waitInitialize(state);

		SpaceItem item = SPACE_LOCATION.getObject(objectId);

		if(item == null) {
			LOGGER.warning(getClass(), "not found space item for objectId " + objectId);
			return;
		}

		SpaceItemView view = item.getView();
		view.startTeleportEffect();
	}
}
