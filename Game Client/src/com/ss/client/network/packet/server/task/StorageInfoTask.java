package com.ss.client.network.packet.server.task;

import rlib.util.array.Array;
import rlib.util.array.Arrays;
import rlib.util.array.FuncElement;
import rlib.util.pools.FoldablePool;
import rlib.util.pools.Pools;

import com.ss.client.gui.controller.game.GameUIController;
import com.ss.client.gui.controller.game.event.impl.UpdateStorageEvent;
import com.ss.client.gui.controller.game.window.storage.CellInfo;
import com.ss.client.manager.PacketTaskManager;
import com.ss.client.util.FoldablePacketTask;
import com.ss.client.util.LimitedPacketTask;
import com.ss.client.util.LocalObjects;

/**
 * Пакетная задача для обновления ячеек хранилища корабля.
 * 
 * @author Ronn
 */
public class StorageInfoTask extends LimitedPacketTask implements FoldablePacketTask {

	private static final FoldablePool<StorageInfoTask> POOL = Pools.newConcurrentFoldablePool(StorageInfoTask.class);

	private static final FuncElement<CellInfo> FUNC_ELEMENT = new FuncElement<CellInfo>() {

		@Override
		public void apply(CellInfo info) {
			info.fold();
		}
	};

	public static final StorageInfoTask getInstance(Array<CellInfo> container) {

		StorageInfoTask task = POOL.take();

		if(task == null) {
			task = new StorageInfoTask();
		}

		task.container.addAll(container);

		return task;
	}

	/** контейнер информаций о ячейках */
	private final Array<CellInfo> container;

	public StorageInfoTask() {
		super(PacketTaskManager.EXECUTE_LIMIT);
		this.container = Arrays.toArray(CellInfo.class);
	}

	@Override
	protected boolean executeImpl(LocalObjects local, long currentTime) {

		GameUIController controller = GameUIController.getInstance();
		controller.notifyEvent(UpdateStorageEvent.get(container));

		return true;
	}

	@Override
	public void finalyze() {
		container.apply(FUNC_ELEMENT);
		container.clear();
	}

	@Override
	public void fold() {
		POOL.put(this);
	}

	@Override
	public void reinit() {
	}
}
