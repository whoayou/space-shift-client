package com.ss.client.network.packet.server;

import com.ss.client.network.ServerPacket;

/**
 * Запрос на открытие панели скилов.
 * 
 * @author Ronn
 */
public class OpenSkillPanel extends ServerPacket {

	@Override
	protected void readImpl() {
	}

	@Override
	protected void runImpl() {

		// FIXME
		// final MainPanelController controler =
		// MainPanelController.getInstance();
		//
		// GameUtil.waitInitialize(controler);
		//
		// controler.showWindow(ElementId.GAME_WINDOW_SKILL_LIST);
	}
}
