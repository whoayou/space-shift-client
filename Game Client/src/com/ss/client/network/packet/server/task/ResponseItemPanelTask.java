package com.ss.client.network.packet.server.task;

import rlib.util.array.Array;
import rlib.util.array.Arrays;
import rlib.util.array.FuncElement;
import rlib.util.pools.FoldablePool;
import rlib.util.pools.Pools;

import com.ss.client.gui.controller.game.GameUIController;
import com.ss.client.gui.controller.game.event.impl.UpdateItemPanelEvent;
import com.ss.client.gui.model.ItemPanelInfo;
import com.ss.client.manager.PacketTaskManager;
import com.ss.client.util.FoldablePacketTask;
import com.ss.client.util.LimitedPacketTask;
import com.ss.client.util.LocalObjects;

/**
 * Пакетная задача для обновления размещения предметов на панели.
 * 
 * @author Ronn
 */
public class ResponseItemPanelTask extends LimitedPacketTask implements FoldablePacketTask {

	private static final FoldablePool<ResponseItemPanelTask> POOL = Pools.newConcurrentFoldablePool(ResponseItemPanelTask.class);

	private static final FuncElement<ItemPanelInfo> INFO_FOLD = new FuncElement<ItemPanelInfo>() {

		@Override
		public void apply(final ItemPanelInfo info) {
			info.fold();
		}
	};

	public static final ResponseItemPanelTask getInstance(Array<ItemPanelInfo> itemPanelInfo) {

		ResponseItemPanelTask task = POOL.take();

		if(task == null) {
			task = new ResponseItemPanelTask();
		}

		task.itemPanelInfo.addAll(itemPanelInfo);

		return task;
	}

	/** список размещений предметов на панели */
	private final Array<ItemPanelInfo> itemPanelInfo;

	public ResponseItemPanelTask() {
		super(PacketTaskManager.EXECUTE_LIMIT);
		this.itemPanelInfo = Arrays.toArray(ItemPanelInfo.class);
	}

	@Override
	protected boolean executeImpl(LocalObjects local, long currentTime) {

		final GameUIController controler = GameUIController.getInstance();
		controler.notifyEvent(UpdateItemPanelEvent.get(itemPanelInfo));

		return true;
	}

	@Override
	public void finalyze() {
		itemPanelInfo.apply(INFO_FOLD);
		itemPanelInfo.clear();
	}

	@Override
	public void fold() {
		POOL.put(this);
	}

	@Override
	public void reinit() {
	}
}
