package com.ss.client.network.packet.server;

import com.ss.client.model.gravity.GravityObject;
import com.ss.client.model.space.SpaceLocation;
import com.ss.client.network.ServerPacket;

/**
 * Серверный пакет с инфой для синхронизации грави объекта.
 * 
 * @author Ronn
 */
public class UpdateGravityObject extends ServerPacket {

	private static final SpaceLocation SPACE_LOCATION = SpaceLocation.getInstance();

	/** процент выполненности оборота */
	private double done;

	/** уникальный ид объекта */
	private long objectId;

	@Override
	protected void readImpl() {
		objectId = readLong();
		done = readDouble();
	}

	@Override
	protected void runImpl() {

		final GravityObject object = SPACE_LOCATION.getObject(objectId);

		if(object == null) {
			LOGGER.warning(getClass(), "not found object for objectId " + objectId);
			return;
		}

		object.updatePosition(done);
	}
}
