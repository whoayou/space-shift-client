package com.ss.client.network.packet.server;

import com.ss.client.gui.controller.game.GameUIController;
import com.ss.client.network.ServerPacket;

/**
 * Запрос сервера на обновление состояние индикатора ангара
 * 
 * @author Ronn
 */
public class UpdateHangarIndicator extends ServerPacket {

	/** состояние индикатора */
	private int state;

	@Override
	protected void readImpl() {
		state = readByte();
	}

	@Override
	protected void runImpl() {

		final GameUIController control = GameUIController.getInstance();

		if(state == 1) {
			control.showHangarIndicator();
		} else {
			control.hideHangarIndicator();
		}
	}
}
