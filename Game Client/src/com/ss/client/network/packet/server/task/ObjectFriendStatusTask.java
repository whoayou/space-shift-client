package com.ss.client.network.packet.server.task;

import rlib.util.pools.FoldablePool;
import rlib.util.pools.Pools;

import com.ss.client.gui.controller.game.GameUIController;
import com.ss.client.gui.controller.game.event.impl.ObjectChangeFriendStatusEvent;
import com.ss.client.manager.PacketTaskManager;
import com.ss.client.model.SpaceObject;
import com.ss.client.util.FoldablePacketTask;
import com.ss.client.util.LimitedPacketTask;
import com.ss.client.util.LocalObjects;

/**
 * Пакетная задача для обновления статуса дружелюбности объекта.
 * 
 * @author Ronn
 */
public class ObjectFriendStatusTask extends LimitedPacketTask implements FoldablePacketTask {

	private static final FoldablePool<ObjectFriendStatusTask> POOL = Pools.newConcurrentFoldablePool(ObjectFriendStatusTask.class);

	public static final ObjectFriendStatusTask getInstance(SpaceObject object) {

		ObjectFriendStatusTask task = POOL.take();

		if(task == null) {
			task = new ObjectFriendStatusTask();
		}

		task.object = object;

		return task;
	}

	/** объект изменивший статус */
	private SpaceObject object;

	public ObjectFriendStatusTask() {
		super(PacketTaskManager.EXECUTE_LIMIT);
	}

	@Override
	protected boolean executeImpl(LocalObjects local, long currentTime) {

		GameUIController controller = GameUIController.getInstance();
		controller.notifyEvent(ObjectChangeFriendStatusEvent.get(getObject()));

		return true;
	}

	@Override
	public void finalyze() {
	}

	@Override
	public void fold() {
		POOL.put(this);
	}

	/**
	 * @return объект изменивший статус.
	 */
	public SpaceObject getObject() {
		return object;
	}

	@Override
	public void reinit() {
	}
}
