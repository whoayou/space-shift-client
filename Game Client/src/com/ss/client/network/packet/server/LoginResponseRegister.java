package com.ss.client.network.packet.server;

import com.ss.client.gui.controller.LoginController;
import com.ss.client.gui.controller.LoginController.LoginState;
import com.ss.client.network.ServerPacket;
import com.ss.client.table.LangTable;

/**
 * Ответный пакет на попытку зарегистрироваться.
 * 
 * @author Ronn
 */
public class LoginResponseRegister extends ServerPacket {

	private static enum ResultType {
		SUCCESSFUL,
		INCORRECT_LOGIN,
		INCORRECT_EMAIL,
	}

	private static final LangTable LANG_TABLE = LangTable.getInstance();

	/** сообщение об успешной регистрации */
	private static final String REGISTER_SUCCESSFUL = LANG_TABLE.getText("@interface:loginLabeRegisterSuccessful@");
	/** сообщение о неккоректности почты */
	private static final String INCORRECT_EMAIL = LANG_TABLE.getText("@interface:loginLabeIncorrectEmail@");
	/** сообщение о неккорктном логине */
	private static final String INCORRECT_LOGIN = LANG_TABLE.getText("@interface:loginLabeIncorrectLogin@");

	/** результат */
	private ResultType type;

	@Override
	protected void readImpl() {
		type = ResultType.values()[readByte()];
	}

	@Override
	protected void runImpl() {
		final LoginController controller = LoginController.getInstance();

		if(controller == null || type == null)
			return;

		switch(type) {
			case SUCCESSFUL: {
				controller.unblocking();
				controller.warningShow(REGISTER_SUCCESSFUL, LoginState.WAIT_AUTH);

				break;
			}
			case INCORRECT_EMAIL: {
				controller.unblocking();
				controller.warningShow(INCORRECT_EMAIL, null);

				break;
			}
			case INCORRECT_LOGIN: {
				controller.unblocking();
				controller.warningShow(INCORRECT_LOGIN, null);
			}
		}

		controller.registerClear();
	}
}
