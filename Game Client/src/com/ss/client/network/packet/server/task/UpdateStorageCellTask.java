package com.ss.client.network.packet.server.task;

import rlib.util.pools.FoldablePool;
import rlib.util.pools.Pools;

import com.ss.client.gui.controller.game.GameUIController;
import com.ss.client.gui.controller.game.event.impl.UpdateStorageCellEvent;
import com.ss.client.gui.controller.game.window.storage.CellInfo;
import com.ss.client.manager.PacketTaskManager;
import com.ss.client.util.FoldablePacketTask;
import com.ss.client.util.LimitedPacketTask;
import com.ss.client.util.LocalObjects;

/**
 * Пакетная задача для обновления ячейки хранилища корабля.
 * 
 * @author Ronn
 */
public class UpdateStorageCellTask extends LimitedPacketTask implements FoldablePacketTask {

	private static final FoldablePool<UpdateStorageCellTask> POOL = Pools.newConcurrentFoldablePool(UpdateStorageCellTask.class);

	public static final UpdateStorageCellTask getInstance(CellInfo info) {

		UpdateStorageCellTask task = POOL.take();

		if(task == null) {
			task = new UpdateStorageCellTask();
		}

		task.info.set(info);

		return task;
	}

	/** информация о ячейке */
	private final CellInfo info;

	public UpdateStorageCellTask() {
		super(PacketTaskManager.EXECUTE_LIMIT);
		this.info = new CellInfo();
	}

	@Override
	protected boolean executeImpl(LocalObjects local, long currentTime) {

		GameUIController controller = GameUIController.getInstance();
		controller.notifyEvent(UpdateStorageCellEvent.get(info));

		return true;
	}

	@Override
	public void finalyze() {
	}

	@Override
	public void fold() {
		POOL.put(this);
	}

	@Override
	public void reinit() {
	}
}
