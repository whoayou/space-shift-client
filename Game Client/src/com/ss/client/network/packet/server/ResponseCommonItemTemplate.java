package com.ss.client.network.packet.server;

import rlib.concurrent.ConcurrentUtils;

import com.ss.client.table.ItemTable;
import com.ss.client.template.item.CommonItemTemplate;

/**
 * Пакет с информацией о шаблоне предмета.
 * 
 * @author Ronn
 */
public class ResponseCommonItemTemplate extends ResponseItemTemplate {

	@Override
	protected void runImpl() {

		ItemTable itemTable = ItemTable.getInstance();
		itemTable.addTemplate(new CommonItemTemplate(getVars()));

		ConcurrentUtils.notifyAll(itemTable);
	}
}
