package com.ss.client.network.packet.server;

import rlib.util.array.Array;
import rlib.util.array.Arrays;

import com.ss.client.gui.controller.game.GameUIController;
import com.ss.client.gui.controller.position.ElementType;
import com.ss.client.gui.model.InterfaceInfo;
import com.ss.client.manager.PacketTaskManager;
import com.ss.client.network.ServerPacket;
import com.ss.client.network.packet.server.task.ResponseIterfaceTask;
import com.ss.client.util.GameUtil;

/**
 * Пакет с уведомлением о выключении двигателя у корабля.
 * 
 * @author Ronn
 */
public class ResponseInterface extends ServerPacket {

	private static final PacketTaskManager TASK_MANAGER = PacketTaskManager.getInstance();

	/** тип сообщения */
	private final Array<InterfaceInfo> interfaceInfo;

	public ResponseInterface() {
		this.interfaceInfo = Arrays.toArray(InterfaceInfo.class);
	}

	@Override
	public void finalyze() {
		interfaceInfo.clear();
	}

	@Override
	protected void readImpl() {

		final int size = readByte();

		for(int i = 0; i < size; i++) {
			interfaceInfo.add(InterfaceInfo.newInstance(ElementType.valueOf(readByte()), readInt(), readInt(), readByte() != 0));
		}
	}

	@Override
	protected void runImpl() {

		final GameUIController gameController = GameUIController.getInstance();

		GameUtil.waitInitialize(gameController);

		TASK_MANAGER.addUITask(ResponseIterfaceTask.getInstance(interfaceInfo));
	}
}
