package com.ss.client.network.packet.server.task;

import rlib.util.pools.FoldablePool;
import rlib.util.pools.Pools;

import com.jme3.math.Quaternion;
import com.ss.client.manager.PacketTaskManager;
import com.ss.client.model.ship.SpaceShip;
import com.ss.client.model.space.SpaceLocation;
import com.ss.client.network.packet.server.ShipRotation;
import com.ss.client.util.FoldablePacketTask;
import com.ss.client.util.LimitedPacketTask;
import com.ss.client.util.LocalObjects;

/**
 * Пакетная задача для обновления разворота корабля.
 * 
 * @author Ronn
 */
public class ShipRotateTask extends LimitedPacketTask implements FoldablePacketTask {

	private static final SpaceLocation SPACE_LOCATION = SpaceLocation.getInstance();

	private static final FoldablePool<ShipRotateTask> POOL = Pools.newConcurrentFoldablePool(ShipRotateTask.class);

	public static final ShipRotateTask getInstance(ShipRotation packet) {

		ShipRotateTask task = POOL.take();

		if(task == null) {
			task = new ShipRotateTask();
		}

		task.currentRotation.set(packet.getCurrentRotation());
		task.targetRotation.set(packet.getTargetRotation());

		task.objectId = packet.getObjectId();
		task.step = packet.getSpeed();
		task.done = packet.getDone();
		task.infinity = packet.isInfinity();
		task.force = packet.isForce();

		return task;
	}

	/** текущий разворот корабля */
	private final Quaternion currentRotation;
	/** целевой разворот корабля */
	private final Quaternion targetRotation;

	/** ид корабля */
	private long objectId;

	/** шаг разворота */
	private float step;
	/** выполненность разворота */
	private float done;

	/** бесконечное ли вращение */
	private boolean infinity;
	/** принудительное ли обновление */
	private boolean force;

	public ShipRotateTask() {
		super(PacketTaskManager.EXECUTE_LIMIT);
		this.targetRotation = new Quaternion();
		this.currentRotation = new Quaternion();
	}

	@Override
	protected boolean executeImpl(LocalObjects local, long currentTime) {

		final SpaceShip ship = SPACE_LOCATION.getObject(objectId);

		if(ship != null) {
			ship.rotate(LocalObjects.get(), currentRotation, targetRotation, step, done, infinity, force);
			return true;
		}

		return true;
	}

	@Override
	public void finalyze() {
	}

	@Override
	public void fold() {
		POOL.put(this);
	}

	@Override
	public boolean isMatches(Object object) {
		return objectId == ((SpaceShip) object).getObjectId();
	}

	@Override
	public void reinit() {
	}
}
