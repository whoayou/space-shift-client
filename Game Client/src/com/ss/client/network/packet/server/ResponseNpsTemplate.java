package com.ss.client.network.packet.server;

import rlib.concurrent.ConcurrentUtils;
import rlib.util.VarTable;

import com.ss.client.table.ShipTable;
import com.ss.client.template.ship.NpsTemplate;

/**
 * Серверный пакет с данными о шаблоне Nps.
 * 
 * @author Ronn
 */
public class ResponseNpsTemplate extends ResponseShipTemplate {

	@Override
	protected void readImpl() {
		super.readImpl();

		final VarTable vars = getVars();
		vars.set(NpsTemplate.LEVEL, readByte());
		vars.set(NpsTemplate.NAME, readString(readByte()));
	}

	@Override
	protected void runImpl() {

		ShipTable shipTable = ShipTable.getInstance();
		shipTable.addTemplate(new NpsTemplate(getVars()));

		ConcurrentUtils.notifyAll(shipTable);
	}
}
