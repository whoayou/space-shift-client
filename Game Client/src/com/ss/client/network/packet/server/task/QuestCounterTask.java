package com.ss.client.network.packet.server.task;

import rlib.util.pools.FoldablePool;
import rlib.util.pools.Pools;

import com.ss.client.gui.controller.game.GameUIController;
import com.ss.client.gui.controller.game.event.impl.UpdateQuestCounterEvent;
import com.ss.client.manager.PacketTaskManager;
import com.ss.client.util.FoldablePacketTask;
import com.ss.client.util.LimitedPacketTask;
import com.ss.client.util.LocalObjects;

/**
 * Пакетная задача для внесение списка заданий в журнал.
 * 
 * @author Ronn
 */
public class QuestCounterTask extends LimitedPacketTask implements FoldablePacketTask {

	private static final FoldablePool<QuestCounterTask> POOL = Pools.newConcurrentFoldablePool(QuestCounterTask.class);

	public static final QuestCounterTask get(int questId, int objectId, int counterId, int value) {

		QuestCounterTask task = POOL.take();

		if(task == null) {
			task = new QuestCounterTask();
		}

		task.counterId = counterId;
		task.objectId = objectId;
		task.value = value;
		task.questId = questId;

		return task;
	}

	/** ид задания */
	private int questId;
	/** уникальный ид задания */
	private int objectId;
	/** ид счетчика */
	private int counterId;
	/** текущее значение */
	private int value;

	public QuestCounterTask() {
		super(PacketTaskManager.EXECUTE_LIMIT);
	}

	@Override
	protected boolean executeImpl(LocalObjects local, long currentTime) {
		GameUIController controller = GameUIController.getInstance();
		controller.notifyEvent(UpdateQuestCounterEvent.get(questId, objectId, counterId, value));
		return true;
	}

	@Override
	public void fold() {
		POOL.put(this);
	}

	@Override
	public void reinit() {
	}

	@Override
	public void finalyze() {
	}
}
