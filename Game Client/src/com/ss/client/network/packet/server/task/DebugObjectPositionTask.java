package com.ss.client.network.packet.server.task;

import rlib.util.pools.FoldablePool;
import rlib.util.pools.Pools;

import com.jme3.math.Vector3f;
import com.ss.client.manager.PacketTaskManager;
import com.ss.client.model.SpaceObject;
import com.ss.client.model.space.SpaceLocation;
import com.ss.client.util.FoldablePacketTask;
import com.ss.client.util.LimitedPacketTask;
import com.ss.client.util.LocalObjects;

/**
 * Пакетная задача для обновления дебаг-положения объекта.
 * 
 * @author Ronn
 */
public class DebugObjectPositionTask extends LimitedPacketTask implements FoldablePacketTask {

	private static final SpaceLocation SPACE_LOCATION = SpaceLocation.getInstance();

	private static final FoldablePool<DebugObjectPositionTask> POOL = Pools.newConcurrentFoldablePool(DebugObjectPositionTask.class);

	public static final DebugObjectPositionTask getInstance(Vector3f position, long objectId) {

		DebugObjectPositionTask task = POOL.take();

		if(task == null) {
			task = new DebugObjectPositionTask();
		}

		task.position.set(position);
		task.objectId = objectId;

		return task;
	}

	/** позиция дебага */
	private final Vector3f position;

	/** уникальный ид объекта */
	private long objectId;

	public DebugObjectPositionTask() {
		super(PacketTaskManager.EXECUTE_LIMIT);
		this.position = new Vector3f();
	}

	@Override
	protected boolean executeImpl(LocalObjects local, long currentTime) {

		SpaceObject object = SPACE_LOCATION.getObject(objectId);

		if(object == null) {
			return true;
		}

		object.getDebug().setLocalTranslation(position);

		return true;
	}

	@Override
	public void finalyze() {
	}

	@Override
	public void fold() {
		POOL.put(this);
	}

	@Override
	public void reinit() {
	}
}
