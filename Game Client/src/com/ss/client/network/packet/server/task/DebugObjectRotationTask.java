package com.ss.client.network.packet.server.task;

import rlib.util.pools.FoldablePool;
import rlib.util.pools.Pools;

import com.jme3.math.Quaternion;
import com.ss.client.manager.PacketTaskManager;
import com.ss.client.model.SpaceObject;
import com.ss.client.model.space.SpaceLocation;
import com.ss.client.util.FoldablePacketTask;
import com.ss.client.util.LimitedPacketTask;
import com.ss.client.util.LocalObjects;

/**
 * Пакетная задача для обновления дебаг-разворота объекта.
 * 
 * @author Ronn
 */
public class DebugObjectRotationTask extends LimitedPacketTask implements FoldablePacketTask {

	private static final SpaceLocation SPACE_LOCATION = SpaceLocation.getInstance();

	private static final FoldablePool<DebugObjectRotationTask> POOL = Pools.newConcurrentFoldablePool(DebugObjectRotationTask.class);

	public static final DebugObjectRotationTask getInstance(Quaternion rotation, long objectId) {

		DebugObjectRotationTask task = POOL.take();

		if(task == null) {
			task = new DebugObjectRotationTask();
		}

		task.rotation.set(rotation);
		task.objectId = objectId;

		return task;
	}

	/** разворот дебага */
	private final Quaternion rotation;

	/** уникальный ид объекта */
	private long objectId;

	public DebugObjectRotationTask() {
		super(PacketTaskManager.EXECUTE_LIMIT);
		this.rotation = new Quaternion();
	}

	@Override
	protected boolean executeImpl(LocalObjects local, long currentTime) {

		SpaceObject object = SPACE_LOCATION.getObject(objectId);

		if(object == null) {
			return true;
		}

		object.getDebug().setLocalRotation(rotation);

		return true;
	}

	@Override
	public void finalyze() {
	}

	@Override
	public void fold() {
		POOL.put(this);
	}

	@Override
	public void reinit() {
	}
}
