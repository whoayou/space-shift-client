package com.ss.client.network.packet.server;

import rlib.util.SafeTask;

import com.ss.client.Game;
import com.ss.client.manager.ExecutorManager;
import com.ss.client.model.ObjectContainer;
import com.ss.client.model.SpaceObject;
import com.ss.client.model.space.SpaceLocation;
import com.ss.client.model.state.GameState;
import com.ss.client.model.state.StateId;
import com.ss.client.network.ServerPacket;
import com.ss.client.util.GameUtil;

/**
 * Пакет запроса на удаление объека из контейнера.
 * 
 * @author Ronn
 */
public class ObjectExitContainer extends ServerPacket {

	private static final int WAIT_COUNTER = 100;
	private static final int WAIT_TIME = 100;

	private static final StateId GAME_STATE = StateId.GAME_STATE;

	private static final ExecutorManager EXECUTOR_MANAGER = ExecutorManager.getInstance();
	private static final SpaceLocation SPACE_LOCATION = SpaceLocation.getInstance();
	private static final Game GAME = Game.getInstance();

	/** ид вошедшего объекта объекта */
	private long objectId;
	/** ид целевого контейнера */
	private long targetId;

	/**
	 * @return уникальный ид вошедшего объекта.
	 */
	public long getObjectId() {
		return objectId;
	}

	/**
	 * @return уникальный ид целевого контейнера.
	 */
	public long getTargetId() {
		return targetId;
	}

	@Override
	protected void readImpl() {
		objectId = readLong();
		targetId = readLong();
	}

	@Override
	protected void runImpl() {

		final GameState state = GAME_STATE.getState();

		GameUtil.waitInitialize(state);

		final long objectId = getObjectId();
		final long targetId = getTargetId();

		GAME.syncLock();
		try {

			final SpaceObject object = SPACE_LOCATION.getObject(objectId);
			final ObjectContainer container = SPACE_LOCATION.getObject(targetId);

			if(object == null || container == null || !container.contains(object)) {

				final SafeTask task = new SafeTask() {

					private int counter;

					@Override
					protected void runImpl() {
						counter++;

						GAME.syncLock();
						try {
							final SpaceObject object = SPACE_LOCATION.getObject(objectId);
							final ObjectContainer container = SPACE_LOCATION.getObject(targetId);

							if(object != null && container != null && container.contains(object))
								container.remove(object);
							else if(counter < WAIT_COUNTER)
								EXECUTOR_MANAGER.scheduleGeneral(this, WAIT_TIME);
						} finally {
							GAME.syncUnlock();
						}
					}
				};

				EXECUTOR_MANAGER.scheduleGeneral(task, WAIT_TIME);
				return;
			}

			container.remove(object);
		} finally {
			GAME.syncUnlock();
		}
	}
}
