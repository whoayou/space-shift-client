package com.ss.client.network.packet.server;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.ss.client.Game;
import com.ss.client.gui.controller.game.GameUIController;
import com.ss.client.model.module.Module;
import com.ss.client.model.module.system.ModuleSystem;
import com.ss.client.model.ship.player.PlayerShip;
import com.ss.client.model.space.SpaceLocation;
import com.ss.client.model.state.GameState;
import com.ss.client.model.state.StateId;
import com.ss.client.network.Network;
import com.ss.client.network.ServerPacket;
import com.ss.client.network.model.NetServer;
import com.ss.client.network.packet.client.RequestSpawn;
import com.ss.client.table.ModuleTable;
import com.ss.client.table.ShipTable;
import com.ss.client.template.ModuleTemplate;
import com.ss.client.template.ship.PlayerShipTemplate;
import com.ss.client.util.GameUtil;

/**
 * Пакет с информацией, необходимой для входа в игру.
 * 
 * @author Ronn
 */
public class ResponseEnterInfo extends ServerPacket {

	private static final StateId GAME_STATE = StateId.GAME_STATE;

	private static final ShipTable SHIP_TABLE = ShipTable.getInstance();
	private static final ModuleTable MODULE_TABLE = ModuleTable.getInstance();
	private static final SpaceLocation SPACE_LOCATION = SpaceLocation.getInstance();
	private static final Game GAME = Game.getInstance();

	/** позиция, на которой остался корабль */
	private final Vector3f location;
	/** разворот в пространстве */
	private final Quaternion rotation;
	/** временный буффер данных */
	private final ByteBuffer saveBuffer;

	/** ид локации, в которой находится корабль */
	private int locationId;

	public ResponseEnterInfo() {
		this.location = new Vector3f();
		this.rotation = new Quaternion();
		this.saveBuffer = ByteBuffer.allocate(1024).order(ByteOrder.LITTLE_ENDIAN);
	}

	/**
	 * @return временный буфер.
	 */
	private final ByteBuffer getSaveBuffer() {
		return saveBuffer;
	}

	@Override
	protected void readImpl() {
		final ByteBuffer saveBuffer = getSaveBuffer();
		saveBuffer.clear();
		saveBuffer.put(getBuffer());
		saveBuffer.flip();
	}

	@Override
	protected void runImpl() {

		setBuffer(getSaveBuffer());

		final long objectId = readLong();

		int templateId = readInt();

		final PlayerShipTemplate template = SHIP_TABLE.getTemplate(templateId);

		if(template == null) {
			return;
		}

		final PlayerShip ship = template.takeInstance(objectId);
		ship.setName(readString(readByte()));

		final ModuleSystem moduleSystem = ship.getModuleSystem();

		for(int i = 0, length = readByte(); i < length; i++) {

			final long moduleId = readLong();

			templateId = readInt();

			final ModuleTemplate moduleTemplate = MODULE_TABLE.getTemplate(templateId);

			final int index = readByte();

			if(moduleTemplate == null) {
				LOGGER.warning(this, "not found module for id " + templateId);
				continue;
			}

			moduleSystem.setModule(index, (Module) moduleTemplate.takeInstance(moduleId));
		}

		locationId = readInt();

		location.set(readFloat(), readFloat(), readFloat());
		rotation.set(readFloat(), readFloat(), readFloat(), readFloat());

		SPACE_LOCATION.setPlayerShip(ship);

		GAME.gotoState(GAME_STATE);

		final GameState state = GAME_STATE.getState();
		final GameUIController controller = GameUIController.getInstance();

		GameUtil.waitInitialize(controller);
		GameUtil.waitInitialize(state);

		state.gotoLocation(locationId);
		ship.setRotation(rotation);
		state.setPlayerShip(ship);

		GAME.syncLock();
		try {

			ship.setLocation(location, Vector3f.ZERO);
			ship.spawnMe();

			final Network network = Network.getInstance();
			final NetServer server = network.getGameServer();

			if(server != null) {
				server.sendPacket(RequestSpawn.getInstance());
			}

		} finally {
			GAME.syncUnlock();
		}
	}
}
