package com.ss.client.network.packet.client;

import java.nio.ByteBuffer;

import com.ss.client.model.SpaceObject;
import com.ss.client.network.ClientPacket;
import com.ss.client.network.ClientPacketType;

/**
 * Запрос на выделение цели.
 * 
 * @author Ronn
 */
public class RequestSelectTarget extends ClientPacket {

	private static final RequestSelectTarget instance = new RequestSelectTarget();

	public static RequestSelectTarget getInstance(SpaceObject object) {

		RequestSelectTarget packet = instance.newInstance();
		packet.objectId = object == null ? 0 : object.getObjectId();

		return packet;
	}

	/** уникальный ид выделяемого объекта */
	private long objectId;

	@Override
	public ClientPacketType getPacketType() {
		return ClientPacketType.REQUEST_SELECT_TARGET;
	}

	@Override
	protected void writeImpl(ByteBuffer buffer) {
		writeOpcode(buffer);
		writeLong(buffer, objectId);
	}
}
