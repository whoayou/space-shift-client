package com.ss.client.network.packet.client;

import java.nio.ByteBuffer;

import com.ss.client.network.ClientPacket;
import com.ss.client.network.ClientPacketType;


/**
 * Клиентский пакет с запросом на получение шаблона модуля.
 * 
 * @author Ronn
 */
public class RequestModuleTemplate extends ClientPacket {

	private static final RequestModuleTemplate instance = new RequestModuleTemplate();

	public static final ClientPacket getInstance(final int templateId) {
		final RequestModuleTemplate packet = instance.newInstance();

		packet.templateId = templateId;

		return packet;
	}

	/** ид шаблона модуля */
	private int templateId;

	@Override
	public ClientPacketType getPacketType() {
		return ClientPacketType.REQUEST_MODULE_TEMPLATE;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
		writeInt(buffer, templateId);
	}
}
