package com.ss.client.network.packet.client;

import java.nio.ByteBuffer;

import com.ss.client.network.ClientPacket;
import com.ss.client.network.ClientPacketType;

/**
 * Запрос на получение настроек расположения предметов на панели.
 * 
 * @author Ronn
 */
public final class RequestItemPanel extends ClientPacket {

	private static final RequestItemPanel instance = new RequestItemPanel();

	public static ClientPacket getInstance() {
		return instance.newInstance();
	}

	@Override
	public ClientPacketType getPacketType() {
		return ClientPacketType.REQUEST_ITEM_PANEL;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
	}
}
