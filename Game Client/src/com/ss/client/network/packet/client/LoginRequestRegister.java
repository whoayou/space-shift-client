package com.ss.client.network.packet.client;

import java.nio.ByteBuffer;

import com.ss.client.network.ClientPacket;
import com.ss.client.network.ClientPacketType;


/**
 * Запрос на регистрацию аккаунта.
 * 
 * @author Ronn
 */
public class LoginRequestRegister extends ClientPacket {

	private static final LoginRequestRegister instance = new LoginRequestRegister();

	public static LoginRequestRegister getInstance(final String login, final String password, final String email) {
		final LoginRequestRegister packet = instance.newInstance();

		packet.login = login;
		packet.password = password;
		packet.email = email;

		return packet;
	}

	/** логин аккаунта */
	private String login;
	/** пароль аккаунта */
	private String password;
	/** имеил аккаунта */
	private String email;

	@Override
	public void finalyze() {
		login = null;
		email = null;
		password = null;
	}

	@Override
	public ClientPacketType getPacketType() {
		return ClientPacketType.REQUEST_REGISTER;
	}

	@Override
	public String toString() {
		return "RequestRegister [login=" + login + ", password=" + password + ", email=" + email + "]";
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
		writeByte(buffer, login.length());
		writeString(buffer, login);
		writeByte(buffer, password.length());
		writeString(buffer, password);
		writeByte(buffer, email.length());
		writeString(buffer, email);
	}
}
