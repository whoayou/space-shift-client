package com.ss.client.network.packet.client;

import java.nio.ByteBuffer;

import com.ss.client.network.ClientPacket;
import com.ss.client.network.ClientPacketType;


/**
 * Отсылув информации о аккаунте гейм серверу.
 * 
 * @author Ronn
 */
public class RequestDeleteShip extends ClientPacket {

	private static final RequestDeleteShip instance = new RequestDeleteShip();

	public static RequestDeleteShip getInstance(final String name) {
		final RequestDeleteShip packet = instance.newInstance();

		packet.name = name;

		return packet;
	}

	/** логин аккаунта */
	private String name;

	@Override
	public void finalyze() {
		name = null;
	}

	@Override
	public ClientPacketType getPacketType() {
		return ClientPacketType.REQUEST_DELETE_SHIP;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
		writeByte(buffer, name.length());
		writeString(buffer, name);
	}
}
