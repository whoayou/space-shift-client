package com.ss.client.network.packet.client;

import java.nio.ByteBuffer;

import com.ss.client.network.ClientPacket;
import com.ss.client.network.ClientPacketType;


/**
 * Клиентский пакет с запросом на получение шаблона предмета.
 * 
 * @author Ronn
 */
public class RequestItemTemplate extends ClientPacket {

	private static final RequestItemTemplate instance = new RequestItemTemplate();

	public static final ClientPacket getInstance(final int templateId) {
		final RequestItemTemplate packet = instance.newInstance();

		packet.templateId = templateId;

		return packet;
	}

	/** ид шаблона предмета */
	private int templateId;

	@Override
	public ClientPacketType getPacketType() {
		return ClientPacketType.REQUEST_ITEM_TEMPLATE;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
		writeInt(buffer, templateId);
	}
}
