package com.ss.client.network.packet.client;

import java.nio.ByteBuffer;

import com.ss.client.network.ClientPacket;
import com.ss.client.network.ClientPacketType;

/**
 * Запрос на обновление положения предмета на панели.
 * 
 * @author Ronn
 */
public final class RequestItemPanelUpdate extends ClientPacket {

	private static final RequestItemPanelUpdate instance = new RequestItemPanelUpdate();

	public static ClientPacket getInstance(final long objectId, final int oldOrder, final int newOrder) {

		final RequestItemPanelUpdate packet = instance.newInstance();

		packet.objectId = objectId;
		packet.oldOrder = oldOrder;
		packet.newOrder = newOrder;

		return packet;
	}

	/** уникальный ид предмета */
	private long objectId;

	/** старое положение на панели */
	private int oldOrder;
	/** новое положение на панели */
	private int newOrder;

	@Override
	public void finalyze() {
	}

	@Override
	public ClientPacketType getPacketType() {
		return ClientPacketType.REQUEST_ITEM_PANEL_UPDATE;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
		writeLong(buffer, objectId);
		writeByte(buffer, oldOrder);
		writeByte(buffer, newOrder);
	}
}
