package com.ss.client.network.packet.client;

import java.nio.ByteBuffer;

import com.ss.client.network.ClientPacket;
import com.ss.client.network.ClientPacketType;


/**
 * Отсылув информации о аккаунте гейм серверу.
 * 
 * @author Ronn
 */
public final class RequestShipList extends ClientPacket {

	private static final RequestShipList instance = new RequestShipList();

	public static ClientPacket getInstance() {
		return instance.newInstance();
	}

	@Override
	public ClientPacketType getPacketType() {
		return ClientPacketType.REQUEST_SHIP_LIST;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
	}
}
