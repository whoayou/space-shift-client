package com.ss.client.network.packet.client;

import java.nio.ByteBuffer;

import com.ss.client.model.SpaceObject;
import com.ss.client.network.ClientPacket;
import com.ss.client.network.ClientPacketType;


/**
 * Пакет с запросом о принудительной синхронизации объекта с сервером.
 * 
 * @author Ronn
 */
public class RequestForceSyncObject extends ClientPacket {

	private static final RequestForceSyncObject instance = new RequestForceSyncObject();

	public static RequestForceSyncObject getInstance(SpaceObject object) {

		RequestForceSyncObject packet = instance.newInstance();
		packet.objectId = object.getObjectId();

		return packet;
	}

	/** уникальный ид объекта */
	private long objectId;

	@Override
	public ClientPacketType getPacketType() {
		return ClientPacketType.REQUEST_FORCE_SYNC_OBJECT;
	}

	@Override
	protected void writeImpl(ByteBuffer buffer) {
		writeOpcode(buffer);
		writeLong(buffer, objectId);
	}
}
