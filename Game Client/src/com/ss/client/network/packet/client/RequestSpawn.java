package com.ss.client.network.packet.client;

import java.nio.ByteBuffer;

import com.ss.client.network.ClientPacket;
import com.ss.client.network.ClientPacketType;


/**
 * Запрос на отспавн выбранного корабля.
 * 
 * @author Ronn
 */
public final class RequestSpawn extends ClientPacket {

	private static final RequestSpawn instance = new RequestSpawn();

	public static ClientPacket getInstance() {
		return instance.newInstance();
	}

	@Override
	public ClientPacketType getPacketType() {
		return ClientPacketType.REQUEST_SPAWN;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
	}
}
