package com.ss.client.network.packet.client;

import java.nio.ByteBuffer;

import com.ss.client.network.ClientPacket;
import com.ss.client.network.ClientPacketType;


/**
 * Запрос на получение списка выполненых квестов.
 * 
 * @author Ronn
 */
public final class RequestQuestCompleteList extends ClientPacket {

	private static final RequestQuestCompleteList instance = new RequestQuestCompleteList();

	public static ClientPacket getInstance() {
		return instance.newInstance();
	}

	@Override
	public ClientPacketType getPacketType() {
		return ClientPacketType.REQUEST_QUEST_COMPLETE_LIST;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
	}
}
