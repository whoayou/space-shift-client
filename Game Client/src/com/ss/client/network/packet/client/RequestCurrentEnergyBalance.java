package com.ss.client.network.packet.client;

import java.nio.ByteBuffer;

import com.ss.client.network.ClientPacket;
import com.ss.client.network.ClientPacketType;


/**
 * Запрос текущего баланса распределения энергии.
 * 
 * @author Ronn
 */
public class RequestCurrentEnergyBalance extends ClientPacket {

	private static final RequestCurrentEnergyBalance instance = new RequestCurrentEnergyBalance();

	public static RequestCurrentEnergyBalance getInstance() {
		return instance.newInstance();
	}

	@Override
	public ClientPacketType getPacketType() {
		return ClientPacketType.REQUEST_CURRENT_ENERGY_BALANCE;
	}

	@Override
	protected void writeImpl(ByteBuffer buffer) {
		writeOpcode(buffer);
	}
}
