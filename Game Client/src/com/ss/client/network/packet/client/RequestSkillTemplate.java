package com.ss.client.network.packet.client;

import java.nio.ByteBuffer;

import com.ss.client.network.ClientPacket;
import com.ss.client.network.ClientPacketType;


/**
 * Клиентский пакет с запрсом инфы о шаблоне скила.
 * 
 * @author Ronn
 */
public class RequestSkillTemplate extends ClientPacket {

	private static final RequestSkillTemplate instance = new RequestSkillTemplate();

	public static final ClientPacket getInstance(final int templateId) {
		final RequestSkillTemplate packet = instance.newInstance();

		packet.templateId = templateId;

		return packet;
	}

	/** ид шаблона скила */
	private int templateId;

	@Override
	public ClientPacketType getPacketType() {
		return ClientPacketType.REQUEST_SKILL_TEMPLATE;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
		writeInt(buffer, templateId);
	}
}
