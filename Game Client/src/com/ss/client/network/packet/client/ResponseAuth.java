package com.ss.client.network.packet.client;

import java.nio.ByteBuffer;

import com.ss.client.network.ClientPacket;
import com.ss.client.network.ClientPacketType;


/**
 * Отсылув информации о аккаунте гейм серверу.
 * 
 * @author Ronn
 */
public class ResponseAuth extends ClientPacket {

	private static final ResponseAuth instance = new ResponseAuth();

	public static ResponseAuth getInstance(final String name, final String password) {
		final ResponseAuth packet = instance.newInstance();

		packet.name = name;
		packet.password = password;

		return packet;
	}

	/** логин аккаунта */
	private String name;
	/** пароль аккаунта */
	private String password;

	@Override
	public void finalyze() {
		name = null;
		password = null;
	}

	@Override
	public ClientPacketType getPacketType() {
		return ClientPacketType.RESPONSE_AUTH;
	}

	@Override
	public String toString() {
		return "ResponseAuth [name=" + name + ", password=" + password + "]";
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
		writeByte(buffer, name.length());
		writeString(buffer, name);
		writeByte(buffer, password.length());
		writeString(buffer, password);
	}
}
