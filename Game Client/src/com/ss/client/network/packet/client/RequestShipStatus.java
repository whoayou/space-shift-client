package com.ss.client.network.packet.client;

import java.nio.ByteBuffer;

import com.ss.client.model.ship.SpaceShip;
import com.ss.client.network.ClientPacket;
import com.ss.client.network.ClientPacketType;


/**
 * Запрос статуса корабля.
 * 
 * @author Ronn
 */
public class RequestShipStatus extends ClientPacket {

	private static final RequestShipStatus instance = new RequestShipStatus();

	public static RequestShipStatus getInstance(SpaceShip ship) {
		RequestShipStatus packet = instance.newInstance();
		packet.objectId = ship.getObjectId();
		return packet;
	}

	/** уникальный ид объекта */
	private long objectId;

	@Override
	public ClientPacketType getPacketType() {
		return ClientPacketType.REQUEST_SHIP_STATUS;
	}

	@Override
	protected void writeImpl(ByteBuffer buffer) {
		writeOpcode(buffer);
		writeLong(buffer, objectId);
	}
}
