package com.ss.client.network.packet.client;

import java.nio.ByteBuffer;

import com.ss.client.network.ClientPacket;
import com.ss.client.network.ClientPacketType;


/**
 * Запрос на перемещение предмета в другую ячейку в хранилище.
 * 
 * @author Ronn
 */
public class RequestMoveStorageItem extends ClientPacket {

	private static final RequestMoveStorageItem instance = new RequestMoveStorageItem();

	public static RequestMoveStorageItem getInstance(int startIndex, int endIndex) {

		RequestMoveStorageItem packet = instance.newInstance();

		packet.startIndex = startIndex;
		packet.endIndex = endIndex;

		return packet;
	}

	/** стартовый индекс ячейки */
	private int startIndex;
	/** конечный индекс ячейки */
	private int endIndex;

	@Override
	public ClientPacketType getPacketType() {
		return ClientPacketType.REQUEST_MOVE_STORAGE_ITEM;
	}

	@Override
	protected void writeImpl(ByteBuffer buffer) {
		writeOpcode(buffer);
		writeInt(buffer, startIndex);
		writeInt(buffer, endIndex);
	}
}
