package com.ss.client.network.packet.client;

import java.nio.ByteBuffer;

import com.ss.client.network.ClientPacket;
import com.ss.client.network.ClientPacketType;


/**
 * Запрос на получение содержимого хранилища.
 * 
 * @author Ronn
 */
public class RequestStorageInfo extends ClientPacket {

	private static final RequestStorageInfo instance = new RequestStorageInfo();

	public static RequestStorageInfo getInstance() {
		return instance.newInstance();
	}

	@Override
	public ClientPacketType getPacketType() {
		return ClientPacketType.REQUEST_STORAGE_INFO;
	}

	@Override
	protected void writeImpl(ByteBuffer buffer) {
		writeOpcode(buffer);
	}
}
