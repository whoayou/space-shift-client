package com.ss.client.network.model;

import java.net.ConnectException;
import java.nio.channels.AsynchronousSocketChannel;

import com.ss.client.gui.controller.LoginController;
import com.ss.client.network.ClientPacket;
import com.ss.client.network.Network;

import rlib.logging.Loggers;
import rlib.network.client.ConnectHandler;

/**
 * Обработчик подключения к гейм серверу.
 * 
 * @author Ronn
 */
public class GameConnectHandler extends ConnectHandler {

	private static GameConnectHandler instance;

	public static GameConnectHandler getInstance() {
		if(instance == null)
			instance = new GameConnectHandler();

		return instance;
	}

	private GameConnectHandler() {
		super();
	}

	@Override
	protected void onConnect(final AsynchronousSocketChannel channel) {
		final Network network = Network.getInstance();
		final NetConnection connection = new NetConnection(network.getGameNetwork(), channel, ClientPacket.class);
		final NetServer server = new NetServer(connection, ServerType.GAME_SERVER, null);

		network.setGameServer(server);

		connection.setServer(server);

		final LoginController controller = LoginController.getInstance();

		server.setInfo(controller.getSelectionServer());

		connection.startRead();
	}

	@Override
	protected void onFailed(final Throwable exc) {
		if(exc instanceof ConnectException)
			Loggers.info(this, "невозможно подключиться.");
		else
			Loggers.warning(this, new Exception(exc));
	}
}
