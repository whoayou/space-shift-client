package com.ss.client.network.model;

/**
 * Перечисление типов серверов.
 * 
 * @author Ronn
 */
public enum ServerType {
	LOGIN_SERVER,
	GAME_SERVER;
}
