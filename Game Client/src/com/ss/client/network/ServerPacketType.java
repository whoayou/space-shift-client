package com.ss.client.network;

import java.util.HashSet;
import java.util.Set;

import rlib.logging.Logger;
import rlib.logging.Loggers;
import rlib.util.pools.FoldablePool;
import rlib.util.pools.Pools;

import com.ss.client.network.packet.server.AuthSuccessful;
import com.ss.client.network.packet.server.BlasterShotInfo;
import com.ss.client.network.packet.server.ChangeExp;
import com.ss.client.network.packet.server.ChangeLevel;
import com.ss.client.network.packet.server.DebugObjectPosition;
import com.ss.client.network.packet.server.DebugObjectRotation;
import com.ss.client.network.packet.server.GravityObjectInfo;
import com.ss.client.network.packet.server.GravityObjectPos;
import com.ss.client.network.packet.server.ItemFinishTeleport;
import com.ss.client.network.packet.server.ItemInfo;
import com.ss.client.network.packet.server.ItemStartTeleport;
import com.ss.client.network.packet.server.LocationObjectInfo;
import com.ss.client.network.packet.server.LoginResponseAuth;
import com.ss.client.network.packet.server.LoginResponseRegister;
import com.ss.client.network.packet.server.LoginResponseServerList;
import com.ss.client.network.packet.server.LoginServerConnected;
import com.ss.client.network.packet.server.ModuleActive;
import com.ss.client.network.packet.server.NpsInfo;
import com.ss.client.network.packet.server.ObjectDelete;
import com.ss.client.network.packet.server.ObjectDestructed;
import com.ss.client.network.packet.server.ObjectEnterContainer;
import com.ss.client.network.packet.server.ObjectExitContainer;
import com.ss.client.network.packet.server.ObjectFriendStatus;
import com.ss.client.network.packet.server.ObjectHide;
import com.ss.client.network.packet.server.ObjectShow;
import com.ss.client.network.packet.server.OpenSkillPanel;
import com.ss.client.network.packet.server.PlayerShipEnergyBalance;
import com.ss.client.network.packet.server.PlayerShipInfo;
import com.ss.client.network.packet.server.PlayerShipStatus;
import com.ss.client.network.packet.server.PlayerShipTeleport;
import com.ss.client.network.packet.server.QuestCounterInfo;
import com.ss.client.network.packet.server.QuestDialogClose;
import com.ss.client.network.packet.server.QuestDialogInfo;
import com.ss.client.network.packet.server.QuestListInfo;
import com.ss.client.network.packet.server.QuestStateInfo;
import com.ss.client.network.packet.server.RequestAuth;
import com.ss.client.network.packet.server.ResponseCommonItemTemplate;
import com.ss.client.network.packet.server.ResponseCreateShip;
import com.ss.client.network.packet.server.ResponseEnterInfo;
import com.ss.client.network.packet.server.ResponseGravityObjectTemplate;
import com.ss.client.network.packet.server.ResponseInterface;
import com.ss.client.network.packet.server.ResponseItemPanel;
import com.ss.client.network.packet.server.ResponseLocationInfo;
import com.ss.client.network.packet.server.ResponseLocationObjectTemplate;
import com.ss.client.network.packet.server.ResponseModuleTemplate;
import com.ss.client.network.packet.server.ResponseNpsTemplate;
import com.ss.client.network.packet.server.ResponsePlayerShipTemplate;
import com.ss.client.network.packet.server.ResponseShipList;
import com.ss.client.network.packet.server.ResponseSkillList;
import com.ss.client.network.packet.server.ResponseSkillPanel;
import com.ss.client.network.packet.server.ResponseSkillTemplate;
import com.ss.client.network.packet.server.ResponseStationTemplate;
import com.ss.client.network.packet.server.RocketShotInfo;
import com.ss.client.network.packet.server.SayMessage;
import com.ss.client.network.packet.server.ServerTime;
import com.ss.client.network.packet.server.ShipDelete;
import com.ss.client.network.packet.server.ShipFly;
import com.ss.client.network.packet.server.ShipRotation;
import com.ss.client.network.packet.server.ShipStatus;
import com.ss.client.network.packet.server.ShotEnd;
import com.ss.client.network.packet.server.ShotHitInfo;
import com.ss.client.network.packet.server.SkillReuseInfo;
import com.ss.client.network.packet.server.SpaceStationInfo;
import com.ss.client.network.packet.server.StorageInfo;
import com.ss.client.network.packet.server.UpdateGravityObject;
import com.ss.client.network.packet.server.UpdateHangarIndicator;
import com.ss.client.network.packet.server.UpdateStorageCell;
import com.ss.client.network.packet.server.UserInfo;

/**
 * Перечисление типов серверных пакетов.
 * 
 * @author Ronn
 */
public enum ServerPacketType {
	LOGIN_SERVER_CONNECTED(0x1000, new LoginServerConnected()),
	RESPONSE_REGISTER(0x1001, new LoginResponseRegister()),
	RESPONSE_AUTH(0x1002, new LoginResponseAuth()),
	RESPONSE_SERVER_LIST(0x1003, new LoginResponseServerList()),

	/** запрос на данные для авторизации на сервере */
	REQUEST_AUTH(0x3000, new RequestAuth()),
	/** результат авторизации на гейм сервере */
	SERVER_AUTH_SUCCESSFUL(0x3001, new AuthSuccessful()),
	/** список кораблей на аккаунте */
	RESPONSE_SHIP_LIST(0x3002, new ResponseShipList()),
	/** результат создания корабля */
	RESPONSE_CREATE_SHIP(0x3003, new ResponseCreateShip()),
	/** информация о корабле игрока, необходимая для входа в игру */
	RESPONSE_ENTER_INFO(0x3004, new ResponseEnterInfo()),
	/** настройки расположения скилов на панели */
	RESPONSE_SKILL_PANEL(0x3005, new ResponseSkillPanel()),
	/** обновление активности модуля */
	MODULE_ACTIVE(0x3006, new ModuleActive()),
	/** обновление данных о полете корабля */
	SHIP_FLY(0x3007, new ShipFly()),
	/** обновление данных о развороте корабял */
	SHIP_ROTATE(0x3008, new ShipRotation()),
	/** информация о корабле игрока */
	PLAYER_SHIP_INFO(0x3009, new PlayerShipInfo()),
	/** сообщение для чата */
	SAY_MESSAGE(0x3010, new SayMessage()),
	/** удаление корабля из видимости */
	DELETE_SHIP(0x3011, new ShipDelete()),
	/** информация о станции */
	STATION_INFO(0x3012, new SpaceStationInfo()),
	/** удаление объекта из видимости */
	OBJECT_DELETE(0x3013, new ObjectDelete()),
	/** обновление настроек интерфейса */
	RESPONSE_INTERFACE(0x3014, new ResponseInterface()),
	/** обновление списка скилов */
	RESPONSE_SKILL_LIST(0x3015, new ResponseSkillList()),
	/** запрос сервера на обновление состояние индикатора ангара */
	UPDATE_HANGAR_INDICATOR(0x3016, new UpdateHangarIndicator()),
	/** информация квестового диалога */
	QUEST_DIALOG_INFO(0x3017, new QuestDialogInfo()),
	/** информация о гравитационном объекте */
	GRAVITY_OBJECT_INFO(0x3018, new GravityObjectInfo()),
	/** пакет с позицией гравитационного объекта */
	GRAVITY_OBJECT_POS(0x3019, new GravityObjectPos()),
	/** запрос на скрытие объекта */
	OBJECT_HIDE(0x3020, new ObjectHide()),
	/** запрос на отображение объекта */
	OBJECT_SHOW(0x3021, new ObjectShow()),
	/** запрос на добавление объекта в контейнер */
	OBJECT_ENTER_CONTAINER(0x3022, new ObjectEnterContainer()),
	/** запрос на удаление объекта из контейнера */
	OBJECT_EXIT_CONTAINER(0x3023, new ObjectExitContainer()),
	/** пакет с данными о шаблоне корабля игрока */
	RESPONSE_PLAYER_SHIP_TEMPLATE(0x3024, new ResponsePlayerShipTemplate()),
	/** пакет с данными о шаблоне модуля */
	RESPONSE_MODULE_TEMPLATE(0x3025, new ResponseModuleTemplate()),
	/** пакет с данными о шаблоне гравитационном объекте */
	RESPONSE_GRAVITY_OBJECT_TEMPLATE(0x3026, new ResponseGravityObjectTemplate()),
	/** пакет с данными о локации */
	RESPONSE_LOCATION_INFO(0x3027, new ResponseLocationInfo()),
	/** пакет с данными о шаблоне космической станции */
	RESPONSE_STATION_TEMPLATE(0x3028, new ResponseStationTemplate()),
	/** пакет с данными о шаблоне скила */
	RESPONSE_SKILL_TEMPLAE(0x3029, new ResponseSkillTemplate()),
	/** пакет с данными о положение на орбите объеекта */
	UPDATE_GRAVITY_OBJECT(0x3030, new UpdateGravityObject()),
	/** запрос о закрытии окна квестового диалога */
	QUEST_DIALOG_CLOSE(0x3031, new QuestDialogClose()),
	/** пакет с инфой о квестовой книге */
	QUEST_LIST_INFO(0x3033, new QuestListInfo()),
	/** пакет с обновлением инфы о состоянии квеста */
	QUEST_STATE_INFO(0x3034, new QuestStateInfo()),
	/** запрос на открытие панели скилов */
	OPEN_SKILL_PANEL(0x3035, new OpenSkillPanel()),
	/** пакет с информацией о шаблоне предмета */
	RESPONSE_COMMON_ITEM_TEMPLATE(0x3036, new ResponseCommonItemTemplate()),
	/** информация о предмете */
	ITEM_INFO(0x3037, new ItemInfo()),
	/** обновление ячейки хранилища корабля */
	UPDATE_STORAGE_CELL(0x3038, new UpdateStorageCell()),
	/** информация о модержании хранилища */
	STORAGE_INFO(0x3039, new StorageInfo()),
	/** информация о новом бластерном выстреле */
	BLASTER_SHOT_INFO(0x3040, new BlasterShotInfo()),
	/** уведомление о завершении полета выстрела */
	SHOT_END(0x3041, new ShotEnd()),
	/** информацияоб откате скила */
	SKILL_REUSE_INFO(0x3042, new SkillReuseInfo()),
	/** информация о попадании выстрела в объект */
	SHOT_HIT_INFO(0x3043, new ShotHitInfo()),
	/** пакет с серверным временм */
	SERVER_TIME(0x3044, new ServerTime()),
	/** пакет с информацией о новом Nps в космосе */
	NPS_INFO(0x3046, new NpsInfo()),
	/** пакет с инфой о шаблоне NPS */
	RESPONSE_NPS_TEMPLATE(0x3047, new ResponseNpsTemplate()),
	/** пакет с разворотом дебага объекта */
	DEBUG_OBJECT_ROTATION(0x3048, new DebugObjectRotation()),
	/** пакет с положением дебага объекта */
	DEBUF_OBJECT_POSITION(0x3049, new DebugObjectPosition()),
	/** информация о корабле игрока */
	USER_INFO(0x3050, new UserInfo()),
	/** уведомление о разрушении объекта */
	OBJECT_DESTRUCTED(0x3051, new ObjectDestructed()),
	/** пакет с уведомлением о старте телепорта предмета */
	ITEM_START_TELEPOR(0x3052, new ItemStartTeleport()),
	/** пакет с уведомлением о завершении телепорта предмета */
	ITEM_FINISH_TELEPORT(0x3053, new ItemFinishTeleport()),
	/** пакет с уведомлением о телепорте корабял игрока */
	PLAYER_SHIP_TELEPORT(0x3054, new PlayerShipTeleport()),
	/** пакет со статусом корабля */
	SHIP_STATUS(0x3055, new ShipStatus()),
	/** пакет со статусом корабля игрока */
	PLAYER_SHIP_STATUS(0x3056, new PlayerShipStatus()),
	/** пакет с текущим распределением энргии корабля */
	PLAYER_SHIP_ENERGY_BALANCE(0x3057, new PlayerShipEnergyBalance()),
	/** информация о выпущенной ракете */
	ROCKET_SHOT_INFO(0x3058, new RocketShotInfo()),
	/** пакет со статусом дружелюбности объекта */
	OBJECT_FRIEND_STATUS(0x3059, new ObjectFriendStatus()),
	/** пакет с информацией о шаблоне локационных объектах */
	RESPONSE_LOCATION_OBJECT_TEMPLATE(0x3060, new ResponseLocationObjectTemplate()),
	/** пакет с информацией о локационном объекте */
	LOCATION_OBJECT_INFO(0x3061, new LocationObjectInfo()),
	/** информация по размещению предметов на панели */
	RESPONSE_ITEM_PANEL(0x3062, new ResponseItemPanel()),
	/** пакет с обновлением опыта игрока */
	CHANGE_EXP(0x3063, new ChangeExp()),
	/** пакет с изменением уровня объекта */
	CHANGE_LEVEL(0x3064, new ChangeLevel()),
	/** пакет с актуальной информацией о счетчике задания */
	QUEST_COUNTER_INFO(0x3065, new QuestCounterInfo()), ;

	private static final Logger LOGGER = Loggers.getLogger(ServerPacketType.class);

	/** массив всех типов */
	public static final ServerPacketType[] VALUES = values();

	/** кол-во всех типов */
	public static final int SIZE = VALUES.length;

	/** массив пакетов */
	private static ServerPacket[] packets;

	/**
	 * Возвращает новый экземпляр пакета в соответствии с опкодом
	 * 
	 * @param opcode опкод пакета.
	 * @return экземпляр нужного пакета.
	 */
	public static ServerPacket getPacketForOpcode(final int opcode) {
		final ServerPacket packet = packets[opcode];
		return packet == null ? null : packet.newInstance();
	}

	/**
	 * Инициализация клиентских пакетов.
	 */
	public static void init() {

		final Set<Integer> set = new HashSet<Integer>();

		for(final ServerPacketType packet : values()) {

			final int index = packet.getOpcode();

			if(set.contains(index)) {
				LOGGER.warning("found duplicate opcode " + index + " or " + Integer.toHexString(packet.getOpcode()) + "!");
			}

			set.add(index);
		}

		set.clear();

		packets = new ServerPacket[Short.MAX_VALUE * 2];

		for(final ServerPacketType packet : values()) {
			packets[packet.getOpcode()] = packet.getPacket();
		}

		LOGGER.info("server packets prepared.");
	}

	/** пул пакетов */
	private final FoldablePool<ServerPacket> pool;

	/** экземпляр пакета */
	private final ServerPacket packet;

	/** опкод пакета */
	private final int opcode;

	/**
	 * @param opcode опкод пакета.
	 * @param packet экземпляр пакета.
	 */
	private ServerPacketType(final int opcode, final ServerPacket packet) {
		this.opcode = opcode;
		this.packet = packet;
		this.packet.setPacketType(this);
		this.pool = Pools.newConcurrentFoldablePool(ServerPacket.class);
	}

	/**
	 * @return опкод пакета.
	 */
	public int getOpcode() {
		return opcode;
	}

	/**
	 * @return экземпляр пакета.
	 */
	public ServerPacket getPacket() {
		return packet;
	}

	/**
	 * @return пул пакетов.
	 */
	public final FoldablePool<ServerPacket> getPool() {
		return pool;
	}
}