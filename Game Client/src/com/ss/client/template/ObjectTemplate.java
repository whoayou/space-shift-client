package com.ss.client.template;

import java.lang.reflect.Constructor;

import rlib.logging.Logger;
import rlib.logging.Loggers;
import rlib.util.ClassUtil;
import rlib.util.Objects;
import rlib.util.Reloadable;
import rlib.util.VarTable;
import rlib.util.pools.FoldablePool;
import rlib.util.pools.Pools;

import com.jme3.asset.AssetManager;
import com.jme3.asset.ModelKey;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.ss.client.Game;
import com.ss.client.model.SpaceObject;
import com.ss.client.model.SpaceObjectType;
import com.ss.client.model.SpaceObjectView;
import com.ss.client.template.grafic.effect.GraficEffectTemplate;

/**
 * Шаблон любого объекта в игре.
 * 
 * @author Ronn
 */
public abstract class ObjectTemplate implements Reloadable<ObjectTemplate> {

	public static final String EMPTY_MODEL = "empty";

	/** логгер темплейтов */
	protected static final Logger LOGGER = Loggers.getLogger(ObjectTemplate.class);

	public static final String DESTRUCT_EFFECT = "destructEffect";
	public static final String KEY = "key";
	public static final String VISIBLE_RANGE = "visibleRange";
	public static final String SIZE_Y = "sizeY";
	public static final String SIZE_Z = "sizeZ";
	public static final String SIZE_X = "sizeX";
	public static final String ID = "id";
	public static final String TYPE = "type";

	/** менеджер для загрузки всего */
	protected static final AssetManager assetManager = Game.getInstance().getAssetManager();

	/** пул инстансов темплейта */
	protected final FoldablePool<SpaceObject> instancePool;
	/** пул внешностей темплейта */
	protected final FoldablePool<SpaceObjectView> viewPool;

	/** тип объекта */
	protected SpaceObjectType type;

	/** эффект разрушения */
	protected GraficEffectTemplate destructEffect;

	/** ид модели */
	protected final int id;
	/** размер объекта по X */
	protected final int sizeX;
	/** размер объекта по Y */
	protected final int sizeY;
	/** размер объекта по Z */
	protected final int sizeZ;
	/** дистанция отображения объекта */
	protected final int visibleRange;

	/** ключ для загрузки модели */
	protected ModelKey modelKey;

	public ObjectTemplate(final VarTable vars) {
		this.id = vars.getInteger(ID);
		this.sizeX = vars.getInteger(SIZE_X, 1);
		this.sizeY = vars.getInteger(SIZE_Y, 1);
		this.sizeZ = vars.getInteger(SIZE_Z, 1);
		this.visibleRange = vars.getInteger(VISIBLE_RANGE, 0);

		this.modelKey = new ModelKey(vars.getString(KEY));

		this.destructEffect = vars.get(DESTRUCT_EFFECT, GraficEffectTemplate.class, null);

		this.type = getType(vars);

		this.instancePool = Pools.newConcurrentFoldablePool(SpaceObject.class);
		this.viewPool = Pools.newConcurrentFoldablePool(SpaceObjectView.class);
	}

	/**
	 * @return эффект разрушения.
	 */
	public GraficEffectTemplate getDestructEffect() {
		return destructEffect;
	}

	/**
	 * @return ид модели.
	 */
	public final int getId() {
		return id;
	}

	/**
	 * @return класс объекта.
	 */
	public Class<? extends SpaceObject> getInstanceClass() {
		return type.getInstanceClass();
	}

	/**
	 * @return ключ для загрузки модели.
	 */
	public final ModelKey getModelKey() {
		return modelKey;
	}

	/**
	 * @return ширина модели.
	 */
	public final int getSizeX() {
		return sizeX;
	}

	/**
	 * @return длинна модели.
	 */
	public final int getSizeY() {
		return sizeY;
	}

	/**
	 * @return высота модели.
	 */
	public final int getSizeZ() {
		return sizeZ;
	}

	/**
	 * @return тип объекта этого шаблона.
	 */
	public SpaceObjectType getType() {
		return type;
	}

	/**
	 * Получпение типа объекта.
	 * 
	 * @param vars набор параметров.
	 * @return тип объекта.
	 */
	protected abstract SpaceObjectType getType(VarTable vars);

	/**
	 * @return класс внешности объектов.
	 */
	public Class<? extends SpaceObjectView> getViewClass() {
		return type.getViewClass();
	}

	/**
	 * @return получаем дистанцию отображения.
	 */
	public int getVisibleRange() {
		return visibleRange;
	}

	/**
	 * @return создание новой 3д модели.
	 */
	public Spatial loadModel() {

		if(EMPTY_MODEL.equals(modelKey.getName())) {
			return new Node(getClass().getSimpleName());
		}

		return assetManager.loadModel(modelKey);
	}

	/**
	 * Складировать внешность в пул.
	 */
	public void putAppearance(final SpaceObjectView appearance) {
		viewPool.put(appearance);
	}

	/**
	 * Складировать объект в пул.
	 */
	public void putInstance(final SpaceObject object) {
		instancePool.put(object);
	}

	@Override
	public void reload(final ObjectTemplate update) {
		Objects.reload(this, update);
	}

	/**
	 * Удаление из пула объекта.
	 * 
	 * @param object удаляемый объект.
	 */
	public void removeInstance(SpaceObject object) {
		instancePool.remove(object);
	}

	/**
	 * @param objectId уникальный ид объекта.
	 * @return экземпля объекта.
	 */
	@SuppressWarnings("unchecked")
	public <T extends SpaceObject> T takeInstance(final long objectId) {

		SpaceObject instance = instancePool.take();

		if(instance == null) {
			Constructor<?> constructor = ClassUtil.getConstructor(getInstanceClass(), long.class, getClass());
			instance = ClassUtil.newInstance(constructor, objectId, this);
		}

		instance.setObjectId(objectId);

		return (T) instance;
	}

	/**
	 * @return соответствующая внешность для объекта.
	 */
	public SpaceObjectView takeView() {

		SpaceObjectView appearance = viewPool.take();

		if(appearance == null) {
			final Constructor<? extends SpaceObjectView> constructor = ClassUtil.getConstructor(getViewClass(), Spatial.class, getClass());
			appearance = ClassUtil.newInstance(constructor, loadModel(), this);
		}

		return appearance;
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + " type = " + type + ",  id = " + id + ",  sizeX = " + sizeX + ",  sizeY = " + sizeY + ",  sizeZ = " + sizeZ + ",  visibleRange = " + visibleRange
				+ ", modelKey = " + modelKey;
	}
}
