package com.ss.client.template;

import rlib.util.VarTable;

import com.jme3.scene.Spatial;
import com.ss.client.model.SpaceObjectType;
import com.ss.client.model.location.object.LocationObjectType;

/**
 * Шаблон локационного объекта.
 * 
 * @author Ronn
 */
public class LocationObjectTemplate extends ObjectTemplate {

	public static final String SCALE = "scale";

	/** маштаб модели */
	private final float scale;

	public LocationObjectTemplate(VarTable vars) {
		super(vars);

		this.scale = vars.getFloat(SCALE);
	}

	/**
	 * @return маштаб модели.
	 */
	public float getScale() {
		return scale;
	}

	@Override
	protected SpaceObjectType getType(VarTable vars) {
		return LocationObjectType.valueOf(vars.getInteger(TYPE));
	}

	@Override
	public Spatial loadModel() {

		Spatial model = super.loadModel();
		model.scale(getScale());

		return model;
	}
}
