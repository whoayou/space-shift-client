package com.ss.client.template;

import rlib.util.VarTable;

import com.jme3.scene.Spatial;
import com.ss.client.model.SpaceObjectType;
import com.ss.client.model.gravity.GravityObjectType;

/**
 * Модель шаблона гравитационных объектов.
 * 
 * @author Ronn
 */
public class GravityObjectTemplate extends ObjectTemplate {

	public static final String RADIUS = "radius";
	public static final String BRIGHTNESS = "brightness";

	/** яркость объекта */
	private final int brightness;
	/** радиус объекта */
	private final int radius;

	public GravityObjectTemplate(final VarTable vars) {
		super(vars);

		this.brightness = vars.getInteger(BRIGHTNESS, 0);
		this.radius = vars.getInteger(RADIUS, 1);
	}

	/**
	 * @return яркость объекта.
	 */
	public int getBrightness() {
		return brightness;
	}

	/**
	 * @return радиус объекта.
	 */
	public int getRadius() {
		return radius;
	}

	@Override
	protected SpaceObjectType getType(final VarTable vars) {
		return GravityObjectType.valueOf(vars.getInteger(TYPE));
	}

	@Override
	public Spatial loadModel() {
		final Spatial spatial = super.loadModel();
		spatial.scale(20);
		return spatial;
	}

	@Override
	public String toString() {
		return super.toString() + " brightness = " + brightness;
	}
}
