package com.ss.client.template;

import com.ss.client.model.skills.Skill;
import com.ss.client.model.skills.SkillType;
import com.ss.client.table.LangTable;

import rlib.util.Strings;
import rlib.util.VarTable;
import rlib.util.pools.FoldablePool;
import rlib.util.pools.Pools;

/**
 * Темплейт скила.
 * 
 * @author Ronn
 */
public class SkillTemplate {

	public static final String STAT_OFFSET = "___";
	public static final String DESCRIPTION_START = "%description_start%";
	public static final String STAT_START = "%stat_start%";
	public static final String NAME = "name";
	public static final String SKILL_TYPE = "skillType";
	public static final String ICON = "icon";
	public static final String DESCRIPTION = "description";
	public static final String ID = "id";

	private static final LangTable LANG_TABLE = LangTable.getInstance();

	private final FoldablePool<Skill> pool = Pools.newConcurrentFoldablePool(Skill.class);

	/** тип скила */
	private final SkillType skillType;

	/** адресс иконки скила */
	private final String icon;
	/** название скила */
	private final String name;
	/** описание скила */
	private final String description;

	/** ид темплейта скила */
	private final int id;

	public SkillTemplate(final VarTable vars) {
		this.id = vars.getInteger(ID);
		this.name = LANG_TABLE.getText(vars.getString(NAME));
		this.description = updateDescription(vars.getString(DESCRIPTION));
		this.icon = vars.getString(ICON);
		this.skillType = SkillType.valueOf(vars.getInteger(SKILL_TYPE));
	}

	/**
	 * @return описание скила.
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Получение первой заменяемой строки на локализацию.
	 * 
	 * @param source исходный текст.
	 * @return заменяемая строка.
	 */
	private String getFirstMatch(final String source) {

		int index = source.indexOf('@');

		if(index < 0) {
			return null;
		}

		for(int i = ++index, length = source.length(); i < length; i++) {
			if(source.charAt(i) == '@') {
				return source.substring(index - 1, i + 1);
			}
		}

		return null;
	}

	/**
	 * @return адресс иконки скила.
	 */
	public String getIcon() {
		return icon;
	}

	/**
	 * @return ид иконки скила.
	 */
	public int getId() {
		return id;
	}

	/**
	 * @return название
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return тип скила.
	 */
	public SkillType getSkillType() {
		return skillType;
	}

	/**
	 * 
	 * @return новый экземпляр скила.
	 */
	public Skill newInstance(final int objectId) {

		Skill skill = pool.take();

		if(skill == null) {
			skill = skillType.newInstance(this);
		}

		skill.setObjectId(objectId);

		return skill;
	}

	/**
	 * Складирование скила в пул.
	 * 
	 * @param skill складируемый скил.
	 */
	public void put(final Skill skill) {
		pool.put(skill);
	}

	@Override
	public String toString() {
		return "SkillTemplate skillType = " + skillType + ",  icon = " + icon + ",  id = " + id + ",  description = " + description;
	}

	/**
	 * Приминение локализацмм на описание.
	 * 
	 * @param source исходное описание.
	 * @return итоговое описание.
	 */
	private String updateDescription(final String source) {

		String result = source;

		for(String match = getFirstMatch(result); match != null; match = getFirstMatch(result)) {
			result = result.replace(match, LANG_TABLE.getText(match));
		}

		String name = Strings.EMPTY;

		// получение названия скила
		{
			int end = result.indexOf(STAT_START) - 1;

			if(end < 0) {
				end = result.indexOf(DESCRIPTION_START) - 1;
			}

			if(end < 0) {
				end = result.length();
			}

			name = result.substring(0, end);
		}

		int maxLength = name.length();

		String statDescription = null;

		// получение отформатированных статов
		if(result.contains(STAT_START)) {

			final int statStart = result.indexOf(STAT_START) + STAT_START.length() + 1;

			statDescription = result.substring(statStart, result.length());

			int statEnd = statDescription.indexOf('%') - 1;

			if(statEnd < 0) {
				statEnd = statDescription.length();
			}

			statDescription = statDescription.substring(0, statEnd);

			final String[] fields = statDescription.split("\n");

			int offsetEnd = -1;

			for(final String field : fields) {
				offsetEnd = Math.max(offsetEnd, field.lastIndexOf('_'));
				maxLength = Math.max(maxLength, field.length());
			}

			for(int i = 0, length = fields.length; i < length; i++) {

				final String field = fields[i];

				final int offsetStart = field.indexOf('_');
				final int offsetLength = offsetEnd - offsetStart;

				final char[] offset = new char[offsetLength];

				for(int g = 0; g < offsetLength; g++) {
					offset[g] = ' ';
				}

				fields[i] = field.replace(STAT_OFFSET, String.valueOf(offset));
			}

			statDescription = Strings.EMPTY;

			for(final String field : fields)
				statDescription += field + "\n";

			statDescription = statDescription.substring(0, statDescription.length() - 1);
		}

		String skillDescription = null;

		// получение форматированного описания
		if(result.contains(DESCRIPTION_START)) {

			final int statStart = result.indexOf(DESCRIPTION_START) + DESCRIPTION_START.length() + 1;

			skillDescription = result.substring(statStart, result.length());

			int count = skillDescription.length() * 100 / maxLength;

			if(count % 10 != 0 || count % 100 != 0) {
				count = count / 100 + 1;
			} else {
				count /= 100;
			}

			int start = 0;
			int end = 0;
			int offset = 0;

			char ch = ' ';

			final StringBuilder descBuilder = new StringBuilder();

			for(int i = 0, limit = count - 1; i < count; i++) {

				start = i * maxLength + offset;
				end = Math.min((i + 1) * maxLength + offset, skillDescription.length());

				if(start >= skillDescription.length()) {
					break;
				}

				while(true) {

					ch = skillDescription.charAt(start);

					if(ch == ' ' || ch == '	') {
						offset++;
						start = i * maxLength + offset;
						end = Math.min((i + 1) * maxLength + offset, skillDescription.length());
						continue;
					}

					break;
				}

				while(end < skillDescription.length() && skillDescription.charAt(end) != ' ') {
					offset++;
					end = Math.min((i + 1) * maxLength + offset, skillDescription.length());
				}

				descBuilder.append(skillDescription, start, end);

				if(i != limit) {
					descBuilder.append("\n");
				}
			}

			skillDescription = descBuilder.toString();
		}

		// создаем конктруктор описания
		final StringBuilder builder = new StringBuilder(name);

		if(!Strings.isEmpty(statDescription)) {
			builder.append("\n\n").append(statDescription);
		}
		if(!Strings.isEmpty(skillDescription)) {
			builder.append("\n\n").append(skillDescription);
		}

		return builder.toString();
	}
}
