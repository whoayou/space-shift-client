package com.ss.client.template.grafic.effect;


import com.jme3.math.ColorRGBA;
import com.ss.client.model.effect.GraficEffectType;
import com.ss.client.network.ServerPacket;

/**
 * Модель шаблона эффекта телепорта предмета.
 * 
 * @author Ronn
 */
public class ItemTeleportEffectTemplate extends GraficEffectTemplate {

	public static final String HAS_ROUND_SPARK = "hasRoundSpark";
	public static final String HAS_FLASH = "hasFlash";

	/** текстура вспышки */
	protected String flashTexture;
	/** текстура остаточных искр */
	protected String roundSparkTexture;

	/** цвет старта вспышки */
	protected ColorRGBA flashStarColor;
	/** цвет финиша вспышки */
	protected ColorRGBA flashEndColor;
	/** цвет старта окружающих частиц */
	protected ColorRGBA roundSparkStarColor;
	/** цвет финиша окружающих частиц */
	protected ColorRGBA roundSparkEndColor;

	/** размеры вспышки */
	protected float flashStartSize;
	protected float flashEndSize;
	/** вразмеры остаточных частиц */
	protected float roundSparkStartSize;
	protected float roundSparkEndSize;

	/** время жизни вспышки */
	protected float flashMinLife;
	protected float flashMaxLife;
	/** время жизни остаточных частиц */
	protected float roundSparkMinLife;
	protected float roundSparkMaxLife;

	/** время эффекта */
	protected int timeEffect;

	/** есть ли вспышка */
	protected boolean hasFlash;
	/** есть ли остаточные искры */
	protected boolean hasRoundSpark;

	public ItemTeleportEffectTemplate(ServerPacket packet) {
		super(packet);

		this.timeEffect = packet.readInt();
		this.hasFlash = packet.readByte() == 1;

		if(isHasFlash()) {
			this.flashTexture = packet.readString(packet.readByte());
			this.flashStarColor = new ColorRGBA(packet.readInt() / 255F, packet.readInt() / 255F, packet.readInt() / 255F, 1);
			this.flashEndColor = new ColorRGBA(packet.readInt() / 255F, packet.readInt() / 255F, packet.readInt() / 255F, 1);
			this.flashMinLife = packet.readFloat();
			this.flashMaxLife = packet.readFloat();
			this.flashStartSize = packet.readFloat();
			this.flashEndSize = packet.readFloat();
		}

		this.hasRoundSpark = packet.readByte() == 1;

		if(isHasRoundSpark()) {
			this.roundSparkTexture = packet.readString(packet.readByte());
			this.roundSparkStarColor = new ColorRGBA(packet.readInt() / 255F, packet.readInt() / 255F, packet.readInt() / 255F, 1);
			this.roundSparkEndColor = new ColorRGBA(packet.readInt() / 255F, packet.readInt() / 255F, packet.readInt() / 255F, 1);
			this.roundSparkStartSize = packet.readFloat();
			this.roundSparkEndSize = packet.readFloat();
			this.roundSparkMinLife = packet.readFloat();
			this.roundSparkMaxLife = packet.readFloat();
		}
	}

	@Override
	public GraficEffectType getEffectType() {
		return GraficEffectType.ITEM_TELEPORT;
	}

	/**
	 * @return цвет финиша вспышки.
	 */
	public ColorRGBA getFlashEndColor() {
		return flashEndColor;
	}

	/**
	 * @return размер вспышки.
	 */
	public float getFlashEndSize() {
		return flashEndSize;
	}

	/**
	 * @return время жизни вспышки.
	 */
	public float getFlashMaxLife() {
		return flashMaxLife;
	}

	/**
	 * @return время жизни вспышки.
	 */
	public float getFlashMinLife() {
		return flashMinLife;
	}

	/**
	 * @return цвет старта вспышки.
	 */
	public ColorRGBA getFlashStarColor() {
		return flashStarColor;
	}

	/**
	 * @return размеры вспышки.
	 */
	public float getFlashStartSize() {
		return flashStartSize;
	}

	/**
	 * @return текстура вспышки.
	 */
	public String getFlashTexture() {
		return flashTexture;
	}

	/**
	 * @return цвет финиша окружающих частиц.
	 */
	public ColorRGBA getRoundSparkEndColor() {
		return roundSparkEndColor;
	}

	/**
	 * @return вразмеры остаточных частиц.
	 */
	public float getRoundSparkEndSize() {
		return roundSparkEndSize;
	}

	/**
	 * @return время жизни остаточных частиц.
	 */
	public float getRoundSparkMaxLife() {
		return roundSparkMaxLife;
	}

	/**
	 * @return время жизни остаточных частиц.
	 */
	public float getRoundSparkMinLife() {
		return roundSparkMinLife;
	}

	/**
	 * @return цвет старта окружающих частиц.
	 */
	public ColorRGBA getRoundSparkStarColor() {
		return roundSparkStarColor;
	}

	/**
	 * @return вразмеры остаточных частиц.
	 */
	public float getRoundSparkStartSize() {
		return roundSparkStartSize;
	}

	/**
	 * @return текстура остаточных искр.
	 */
	public String getRoundSparkTexture() {
		return roundSparkTexture;
	}

	/**
	 * @return время эффекта.
	 */
	public int getTimeEffect() {
		return timeEffect;
	}

	/**
	 * @return есть ли вспышка.
	 */
	public boolean isHasFlash() {
		return hasFlash;
	}

	/**
	 * @return есть ли остаточные искры.
	 */
	public boolean isHasRoundSpark() {
		return hasRoundSpark;
	}
}
