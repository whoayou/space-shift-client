package com.ss.client.template.grafic.effect;


import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.ss.client.model.effect.GraficEffectType;
import com.ss.client.network.ServerPacket;

/**
 * Модель шаблона эффекта взрыва.
 * 
 * @author Ronn
 */
public class ExplosionEffectTemplate extends GraficEffectTemplate {

	public static final String HAS_SHOCK_WAVE = "hasShockWave";
	public static final String HAS_DERBIS = "hasDerbis";
	public static final String HAS_SMOKE_TRAIL = "hasSmokeTrail";
	public static final String HAS_ROUND_SPARK = "hasRoundSpark";
	public static final String HAS_SPARK = "hasSpark";
	public static final String HAS_FLASH = "hasFlash";
	public static final String HAS_FLAME = "hasFlame";

	/** текстура пламени */
	protected String flameTexture;
	/** текстура вспышки */
	protected String flashTexture;
	/** текстура остаточных искр */
	protected String roundSparkTexture;
	/** текстура разлетающихся искр */
	protected String sparkTexture;
	/** текстура дымовых частиц */
	protected String smokeTrailTexture;
	/** текстура осколков */
	protected String debrisTexture;
	/** текстура взрывной волны */
	protected String shockWaveTexture;

	/** цвет частиц */
	protected ColorRGBA sparkColor;
	/** цвет старта пламени */
	protected ColorRGBA flameStarColor;
	/** цвет финиша пламени */
	protected ColorRGBA flameEndColor;
	/** цвет старта вспышки */
	protected ColorRGBA flashStarColor;
	/** цвет финиша вспышки */
	protected ColorRGBA flashEndColor;
	/** цвет старта волны */
	protected ColorRGBA shockWaveStarColor;
	/** цвет финиша волны */
	protected ColorRGBA shockWaveEndColor;

	/** направление частиц */
	protected Vector3f sparkGravity;

	/** размеры пламени */
	protected float flameStartSize;
	protected float flameEndSize;
	/** размеры частиц */
	protected float sparkStartSize;
	protected float sparkEndSize;
	/** размеры вспышки */
	protected float flashStartSize;
	protected float flashEndSize;
	/** размеры пламени */
	protected float shockWaveStartSize;
	protected float shockWaveEndSize;
	/** вразмеры остаточных частиц */
	protected float roundSparkStartSize;
	protected float roundSparkEndSize;

	/** время жизни пламени */
	protected float flameMinLife;
	protected float flameMaxLife;
	/** время жизни частиц */
	protected float sparkMinLife;
	protected float sparkMaxLife;
	/** время жизни вспышки */
	protected float flashMinLife;
	protected float flashMaxLife;
	/** время жизни волны */
	protected float shockWaveMinLife;
	protected float shockWaveMaxLife;
	/** время жизни остаточных частиц */
	protected float roundSparkMinLife;
	protected float roundSparkMaxLife;

	/** есть ли огонь */
	protected boolean hasFlame;
	/** есть ли вспышка */
	protected boolean hasFlash;
	/** есть ли разлетающиеся искры */
	protected boolean hasSpark;
	/** есть ли остаточные искры */
	protected boolean hasRoundSpark;
	/** есть ли разлетающийся дым */
	protected boolean hasSmokeTrail;
	/** есть ли разлетающиеся куски */
	protected boolean hasDebris;
	/** есть ли взрывная волна */
	protected boolean hasShockWave;

	public ExplosionEffectTemplate(ServerPacket packet) {
		super(packet);

		this.hasFlame = packet.readByte() == 1;

		if(isHasFlame()) {
			this.flameTexture = packet.readString(packet.readByte());
			this.flameStartSize = packet.readFloat();
			this.flameEndSize = packet.readFloat();
			this.flameMinLife = packet.readFloat();
			this.flameMaxLife = packet.readFloat();
			this.flameStarColor = new ColorRGBA(packet.readInt() / 255F, packet.readInt() / 255F, packet.readInt() / 255F, 1);
			this.flameEndColor = new ColorRGBA(packet.readInt() / 255F, packet.readInt() / 255F, packet.readInt() / 255F, 1);
		}

		this.hasFlash = packet.readByte() == 1;

		if(isHasFlash()) {
			this.flashTexture = packet.readString(packet.readByte());
			this.flashStarColor = new ColorRGBA(packet.readInt() / 255F, packet.readInt() / 255F, packet.readInt() / 255F, 1);
			this.flashEndColor = new ColorRGBA(packet.readInt() / 255F, packet.readInt() / 255F, packet.readInt() / 255F, 1);
			this.flashMinLife = packet.readFloat();
			this.flashMaxLife = packet.readFloat();
			this.flashStartSize = packet.readFloat();
			this.flashEndSize = packet.readFloat();
		}

		this.hasRoundSpark = packet.readByte() == 1;

		if(isHasRoundSpark()) {
			this.roundSparkTexture = packet.readString(packet.readByte());
			this.roundSparkStartSize = packet.readFloat();
			this.roundSparkEndSize = packet.readFloat();
			this.roundSparkMinLife = packet.readFloat();
			this.roundSparkMaxLife = packet.readFloat();
		}

		this.hasDebris = packet.readByte() == 1;

		if(isHasDebris()) {
			this.debrisTexture = packet.readString(packet.readByte());
		}

		this.hasShockWave = packet.readByte() == 1;

		if(isHasShockWave()) {
			this.shockWaveTexture = packet.readString(packet.readByte());
			this.shockWaveStartSize = packet.readFloat();
			this.shockWaveEndSize = packet.readFloat();
			this.shockWaveMinLife = packet.readFloat();
			this.shockWaveMaxLife = packet.readFloat();
			this.shockWaveStarColor = new ColorRGBA(packet.readInt() / 255F, packet.readInt() / 255F, packet.readInt() / 255F, 1);
			this.shockWaveEndColor = new ColorRGBA(packet.readInt() / 255F, packet.readInt() / 255F, packet.readInt() / 255F, 1);
		}

		this.hasSmokeTrail = packet.readByte() == 1;

		if(isHasSmokeTrail()) {
			this.smokeTrailTexture = packet.readString(packet.readByte());
		}

		this.hasSpark = packet.readByte() == 1;

		if(isHasSpark()) {
			this.sparkTexture = packet.readString(packet.readByte());
			this.sparkColor = new ColorRGBA(packet.readInt() / 255F, packet.readInt() / 255F, packet.readInt() / 255F, 1);
			this.sparkStartSize = packet.readFloat();
			this.sparkEndSize = packet.readFloat();
			this.sparkGravity = new Vector3f(packet.readFloat(), packet.readFloat(), packet.readFloat());
			this.sparkMinLife = packet.readFloat();
			this.sparkMaxLife = packet.readFloat();
		}
	}

	/**
	 * @return текстура осколков.
	 */
	public String getDebrisTexture() {
		return debrisTexture;
	}

	@Override
	public GraficEffectType getEffectType() {
		return GraficEffectType.EXPLOSION;
	}

	/**
	 * @return цвет финиша пламени.
	 */
	public ColorRGBA getFlameEndColor() {
		return flameEndColor;
	}

	/**
	 * @return размеры пламени.
	 */
	public float getFlameEndSize() {
		return flameEndSize;
	}

	/**
	 * @return время жизни пламени.
	 */
	public float getFlameMaxLife() {
		return flameMaxLife;
	}

	/**
	 * @return время жизни пламени.
	 */
	public float getFlameMinLife() {
		return flameMinLife;
	}

	/**
	 * @return цвет старта пламени.
	 */
	public ColorRGBA getFlameStarColor() {
		return flameStarColor;
	}

	/**
	 * @return размеры пламени.
	 */
	public float getFlameStartSize() {
		return flameStartSize;
	}

	/**
	 * @return текстура пламени взрыва.
	 */
	public String getFlameTexture() {
		return flameTexture;
	}

	/**
	 * @return цвет финиша вспышки.
	 */
	public ColorRGBA getFlashEndColor() {
		return flashEndColor;
	}

	/**
	 * @return размер вспышки.
	 */
	public float getFlashEndSize() {
		return flashEndSize;
	}

	/**
	 * @return время жизни вспышки.
	 */
	public float getFlashMaxLife() {
		return flashMaxLife;
	}

	/**
	 * @return время жизни вспышки.
	 */
	public float getFlashMinLife() {
		return flashMinLife;
	}

	/**
	 * @return цвет старта вспышки.
	 */
	public ColorRGBA getFlashStarColor() {
		return flashStarColor;
	}

	/**
	 * @return размеры вспышки.
	 */
	public float getFlashStartSize() {
		return flashStartSize;
	}

	/**
	 * @return текстура вспышки.
	 */
	public String getFlashTexture() {
		return flashTexture;
	}

	/**
	 * @return вразмеры остаточных частиц.
	 */
	public float getRoundSparkEndSize() {
		return roundSparkEndSize;
	}

	/**
	 * @return время жизни остаточных частиц.
	 */
	public float getRoundSparkMaxLife() {
		return roundSparkMaxLife;
	}

	/**
	 * @return время жизни остаточных частиц.
	 */
	public float getRoundSparkMinLife() {
		return roundSparkMinLife;
	}

	/**
	 * @return вразмеры остаточных частиц.
	 */
	public float getRoundSparkStartSize() {
		return roundSparkStartSize;
	}

	/**
	 * @return текстура остаточных искр.
	 */
	public String getRoundSparkTexture() {
		return roundSparkTexture;
	}

	/**
	 * @return цвет финиша волны.
	 */
	public ColorRGBA getShockWaveEndColor() {
		return shockWaveEndColor;
	}

	/**
	 * @return размеры пламени.
	 */
	public float getShockWaveEndSize() {
		return shockWaveEndSize;
	}

	/**
	 * @return время жизни волны.
	 */
	public float getShockWaveMaxLife() {
		return shockWaveMaxLife;
	}

	/**
	 * @return время жизни волны.
	 */
	public float getShockWaveMinLife() {
		return shockWaveMinLife;
	}

	/**
	 * @return цвет старта волны.
	 */
	public ColorRGBA getShockWaveStarColor() {
		return shockWaveStarColor;
	}

	/**
	 * @return размеры пламени.
	 */
	public float getShockWaveStartSize() {
		return shockWaveStartSize;
	}

	/**
	 * @return текстура взрывной волны.
	 */
	public String getShockWaveTexture() {
		return shockWaveTexture;
	}

	/**
	 * @return текстура дымовых частиц.
	 */
	public String getSmokeTrailTexture() {
		return smokeTrailTexture;
	}

	/**
	 * @return цвет частиц.
	 */
	public ColorRGBA getSparkColor() {
		return sparkColor;
	}

	/**
	 * @return конечный размер частиц.
	 */
	public float getSparkEndSize() {
		return sparkEndSize;
	}

	/**
	 * @return направление частиц.
	 */
	public Vector3f getSparkGravity() {
		return sparkGravity;
	}

	/**
	 * @return максимальное время жизни частиц.
	 */
	public float getSparkMaxLife() {
		return sparkMaxLife;
	}

	/**
	 * @return минимальное время жизни частиц.
	 */
	public float getSparkMinLife() {
		return sparkMinLife;
	}

	/**
	 * @return начальный размер частиц.
	 */
	public float getSparkStartSize() {
		return sparkStartSize;
	}

	/**
	 * @return текстура разлетающихся искр.
	 */
	public String getSparkTexture() {
		return sparkTexture;
	}

	/**
	 * @return есть ли разлетающиеся куски.
	 */
	public boolean isHasDebris() {
		return hasDebris;
	}

	/**
	 * @return есть ли огонь.
	 */
	public boolean isHasFlame() {
		return hasFlame;
	}

	/**
	 * @return есть ли вспышка.
	 */
	public boolean isHasFlash() {
		return hasFlash;
	}

	/**
	 * @return есть ли остаточные искры.
	 */
	public boolean isHasRoundSpark() {
		return hasRoundSpark;
	}

	/**
	 * @return есть ли взрывная волна.
	 */
	public boolean isHasShockWave() {
		return hasShockWave;
	}

	/**
	 * @return есть ли разлетающийся дым.
	 */
	public boolean isHasSmokeTrail() {
		return hasSmokeTrail;
	}

	/**
	 * @return есть ли разлетающиеся искры.
	 */
	public boolean isHasSpark() {
		return hasSpark;
	}
}
