package com.ss.client.template.item;

import rlib.util.Strings;
import rlib.util.VarTable;

import com.ss.client.table.LangTable;
import com.ss.client.template.ObjectTemplate;
import com.ss.client.template.grafic.effect.GraficEffectTemplate;

/**
 * Шаблон комичческого предмета.
 * 
 * @author Ronn
 */
public abstract class ItemTemplate extends ObjectTemplate {

	public static final String STAT_OFFSET = "___";
	public static final String DESCRIPTION_START = "%description_start%";
	public static final String STAT_START = "%stat_start%";
	public static final String ITEN_NAME = "%item_name%";

	public static final String ICON = "icon";
	public static final String NAME = "name";
	public static final String STACKABLE = "stackable";
	public static final String DESCRIPTION = "description";
	public static final String FINISH_TELEPORT_EFFECT = "finishTeleportEffect";
	public static final String START_TELEPORT_EFFECT = "startTeleportEffect";

	private static final LangTable LANG_TABLE = LangTable.getInstance();

	/** эффект старта телепортации предмета */
	private final GraficEffectTemplate startTeleportEffect;
	/** эффект завершения телепортации предмета */
	private final GraficEffectTemplate finishTeleportEffect;

	/** название предмета */
	private final String name;
	/** иконка предмета */
	private final String icon;
	/** описание предмета */
	private final String description;

	/** стакуемый ли предмет */
	private final boolean stackable;

	public ItemTemplate(final VarTable vars) {
		super(vars);

		this.icon = vars.getString(ICON);
		this.name = LANG_TABLE.getText(vars.getString(NAME));
		this.description = updateDescription(vars.getString(DESCRIPTION));
		this.startTeleportEffect = vars.get(START_TELEPORT_EFFECT, GraficEffectTemplate.class, null);
		this.finishTeleportEffect = vars.get(FINISH_TELEPORT_EFFECT, GraficEffectTemplate.class, null);
		this.stackable = vars.getBoolean(STACKABLE, false);
	}

	/**
	 * @return описание предмета.
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @return эффект завершения телепортации предмета.
	 */
	public GraficEffectTemplate getFinishTeleportEffect() {
		return finishTeleportEffect;
	}

	/**
	 * Получение первой заменяемой строки на локализацию.
	 * 
	 * @param source исходный текст.
	 * @return заменяемая строка.
	 */
	private String getFirstMatch(final String source) {

		int index = source.indexOf('@');

		if(index < 0) {
			return null;
		}

		for(int i = ++index, length = source.length(); i < length; i++) {
			if(source.charAt(i) == '@') {
				return source.substring(index - 1, i + 1);
			}
		}

		return null;
	}

	/**
	 * @return путь к иконке предмета.
	 */
	public String getIcon() {
		return icon;
	}

	/**
	 * @return название предмета.
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return эффект старта телепортации предмета
	 */
	public GraficEffectTemplate getStartTeleportEffect() {
		return startTeleportEffect;
	}

	/**
	 * @return стакуемый ли предмет.
	 */
	public boolean isStackable() {
		return stackable;
	}

	/**
	 * Приминение локализацмм на описание.
	 * 
	 * @param source исходное описание.
	 * @return итоговое описание.
	 */
	private String updateDescription(final String source) {

		String result = source;

		for(String match = getFirstMatch(result); match != null; match = getFirstMatch(result)) {
			result = result.replace(match, LANG_TABLE.getText(match));
		}

		String name = Strings.EMPTY;

		// получение названия предмета
		{
			int end = result.indexOf(STAT_START) - 1;

			if(end < 0) {
				end = result.indexOf(DESCRIPTION_START) - 1;
			}

			if(end < 0) {
				end = result.length();
			}

			name = result.substring(0, end);
		}

		if(name.contains(ITEN_NAME)) {
			name = name.replace(ITEN_NAME, getName());
		}

		int maxLength = name.length();

		String statDescription = null;

		// получение отформатированных статов
		if(result.contains(STAT_START)) {

			final int statStart = result.indexOf(STAT_START) + STAT_START.length() + 1;

			statDescription = result.substring(statStart, result.length());

			int statEnd = statDescription.indexOf('%') - 1;

			if(statEnd < 0) {
				statEnd = statDescription.length();
			}

			statDescription = statDescription.substring(0, statEnd);

			final String[] fields = statDescription.split("\n");

			int offsetEnd = -1;

			for(final String field : fields) {
				offsetEnd = Math.max(offsetEnd, field.lastIndexOf('_'));
				maxLength = Math.max(maxLength, field.length());
			}

			for(int i = 0, length = fields.length; i < length; i++) {

				final String field = fields[i];

				final int offsetStart = field.indexOf('_');
				final int offsetLength = offsetEnd - offsetStart;

				final char[] offset = new char[offsetLength];

				for(int g = 0; g < offsetLength; g++) {
					offset[g] = ' ';
				}

				fields[i] = field.replace(STAT_OFFSET, String.valueOf(offset));
			}

			statDescription = Strings.EMPTY;

			for(final String field : fields) {
				statDescription += field + "\n";
			}

			statDescription = statDescription.substring(0, statDescription.length() - 1);
		}

		String description = null;

		// получение форматированного описания
		if(result.contains(DESCRIPTION_START)) {
			final int statStart = result.indexOf(DESCRIPTION_START) + DESCRIPTION_START.length() + 1;
			description = result.substring(statStart, result.length());
			// skillDescription = wrapDescription(maxLength, skillDescription);
		}

		// создаем конктруктор описания
		final StringBuilder builder = new StringBuilder(/* name */);

		if(builder.length() > 0) {
			builder.append("\n\n");
		}

		if(!Strings.isEmpty(statDescription)) {
			builder.append(statDescription);
		}

		if(builder.length() > 0) {
			builder.append("\n\n");
		}

		if(!Strings.isEmpty(description)) {
			builder.append(description);
		}

		return builder.toString();
	}

	@SuppressWarnings("unused")
	private String wrapDescription(int maxLength, String description) {

		int count = description.length() * 100 / maxLength;

		if(count % 10 != 0 || count % 100 != 0) {
			count = count / 100 + 1;
		} else {
			count /= 100;
		}

		int start = 0;
		int end = 0;
		int offset = 0;

		char ch = ' ';

		final StringBuilder result = new StringBuilder();

		for(int i = 0, limit = count - 1; i < count; i++) {

			start = i * maxLength + offset;
			end = Math.min((i + 1) * maxLength + offset, description.length());

			if(start >= description.length()) {
				break;
			}

			while(true) {

				ch = description.charAt(start);

				if(ch == ' ' || ch == '	') {
					offset++;
					start = i * maxLength + offset;
					end = Math.min((i + 1) * maxLength + offset, description.length());
					continue;
				}

				break;
			}

			while(end < description.length() && description.charAt(end) != ' ') {
				offset++;
				end = Math.min((i + 1) * maxLength + offset, description.length());
			}

			result.append(description, start, end);

			if(i != limit) {
				result.append("\n");
			}
		}

		description = result.toString();
		return description;
	}
}
