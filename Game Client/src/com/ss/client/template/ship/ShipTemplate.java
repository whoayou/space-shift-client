package com.ss.client.template.ship;

import com.ss.client.model.SpaceObjectType;
import com.ss.client.model.module.system.ModuleSystem;
import com.ss.client.model.module.system.SlotInfo;
import com.ss.client.model.ship.SpaceShipType;
import com.ss.client.template.ObjectTemplate;

import rlib.util.VarTable;
import rlib.util.pools.FoldablePool;
import rlib.util.pools.Pools;

/**
 * Темплейт корабля.
 * 
 * @author Ronn
 */
public abstract class ShipTemplate extends ObjectTemplate {

	public static final SlotInfo[] EMPTY_SLOTS = new SlotInfo[0];

	public static final String SLOTS = "slots";

	/** пул инстансов систем модулей */
	protected final FoldablePool<ModuleSystem> moduleSystemPool;

	/** конфигурация системы модулей */
	protected final SlotInfo[] slots;

	public ShipTemplate(final VarTable vars) {
		super(vars);

		this.slots = vars.get(SLOTS, SlotInfo[].class, EMPTY_SLOTS);
		this.moduleSystemPool = Pools.newConcurrentFoldablePool(ModuleSystem.class);
	}

	/**
	 * @return класс системы модулей.
	 */
	public abstract Class<? extends ModuleSystem> getModuleSystemClass();

	/**
	 * @return набор слотов.
	 */
	public final SlotInfo[] getSlots() {
		return slots;
	}

	@Override
	protected SpaceObjectType getType(final VarTable vars) {
		return SpaceShipType.valueOf(vars.getInteger(TYPE));
	}

	/**
	 * @param moduleSystem использованная система модулей.
	 */
	public final void putModuleSystem(final ModuleSystem moduleSystem) {
		moduleSystemPool.put(moduleSystem);
	}

	/**
	 * @return новая система модулей.
	 */
	public final ModuleSystem takeModuleSystem() {

		ModuleSystem system = moduleSystemPool.take();

		if(system == null) {
			try {
				system = getModuleSystemClass().newInstance();
				system.init(slots);
			} catch(InstantiationException | IllegalAccessException e) {
				LOGGER.warning(this, e);
			}
		}

		return system;
	}
}
